#ifndef SIM_SPIN_CONTROLLER
#define SIM_SPIN_CONTROLLER

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <matec_msgs/FullJointStates.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>

#include <laser_geometry/laser_geometry.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/ManageJointInterpolator.h"

namespace matec_sim
{
  class MultisenseSim
  {
  public:
    MultisenseSim(const ros::NodeHandle& nh);
    ~MultisenseSim();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    double m_min_thresh;
    double m_max_thresh;
    double m_noise_buffer;
    std::string m_behavior_name;

    std::vector<std::string> m_joint_names;
    int m_hokuyo_joint_idx;

    ros::ServiceServer m_manage_service_server;

    laser_geometry::LaserProjection m_projector;

    double m_last_carrot;

    ros::Subscriber m_scan_sub;
    ros::Subscriber m_left_rect_sub;
    ros::Subscriber m_right_rect_sub;
    ros::Subscriber m_spin_sub;
    ros::Publisher m_cloud_pub;
    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_spin_pub;

    double m_spin_rate;

    bool managementCallback(matec_msgs::ManageJointInterpolator::Request& req, matec_msgs::ManageJointInterpolator::Response& res);
    
    void sendSpindleCarrot();

    void controlSpindleCallback(const std_msgs::Float64ConstPtr& msg);
    void scanCallback(const sensor_msgs::LaserScanConstPtr& msg);
  };
}

#endif //SIM_SPIN_CONTROLLER
