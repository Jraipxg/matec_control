#include "matec_sim/multisense_sim.h"

namespace matec_sim
{
  MultisenseSim::MultisenseSim(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 250.0);
    m_nh.param("initial_spin_rate", m_spin_rate, 3.0);
    m_nh.param("min_thresh", m_min_thresh, 0.0);
    m_nh.param("max_thresh", m_max_thresh, std::numeric_limits<double>::max());
    m_nh.param("noise_buffer", m_noise_buffer, 0.1);
    m_nh.param("behavior", m_behavior_name, std::string("hokuyo_control"));

    matec_utils::blockOnSMJointNames(m_joint_names);

    std::string hokuyo_joint_name;
    m_nh.param("hokuyo_joint_name", hokuyo_joint_name, std::string("motor_joint"));
    m_hokuyo_joint_idx = std::find(m_joint_names.begin(), m_joint_names.end(), hokuyo_joint_name) - m_joint_names.begin();

    if(m_hokuyo_joint_idx == (int) m_joint_names.size())
    {
      ROS_ERROR("%s: Couldn't find motor_joint for the hokuyo!", m_nh.getNamespace().c_str());
      ros::shutdown();
    }

    m_spin_sub = m_nh.subscribe<std_msgs::Float64>("/multisense_sl/set_spindle_speed", 5, boost::bind(&MultisenseSim::controlSpindleCallback, this, _1));
    m_spin_pub.advertise("carrot");

    m_scan_sub = m_nh.subscribe<sensor_msgs::LaserScan>("/multisense/laser_scan", 5, boost::bind(&MultisenseSim::scanCallback, this, _1));
    m_cloud_pub = m_nh.advertise<sensor_msgs::PointCloud2>("/multisense/lidar_points2", 1);

    m_manage_service_server = m_nh.advertiseService("manage", &MultisenseSim::managementCallback, this);

    m_last_carrot = 0;
  }

  MultisenseSim::~MultisenseSim()
  {
  }

  bool MultisenseSim::managementCallback(matec_msgs::ManageJointInterpolator::Request& req, matec_msgs::ManageJointInterpolator::Response& res)
  {
    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    m_last_carrot = req.new_setpoint.position.at(0);
    return true;
  }

  void MultisenseSim::scanCallback(const sensor_msgs::LaserScanConstPtr& msg)
  {
    sensor_msgs::LaserScan laser = *msg;
    laser.range_min = m_min_thresh;

    double max_range = std::min(m_max_thresh, laser.range_max - m_noise_buffer);

    sensor_msgs::PointCloud2 cloud;
    m_projector.projectLaser(laser, cloud, max_range, laser_geometry::channel_option::Default);

    m_cloud_pub.publish(cloud);
  }

  void MultisenseSim::controlSpindleCallback(const std_msgs::Float64ConstPtr& msg)
  {
    m_spin_rate = msg->data;
  }

  void MultisenseSim::sendSpindleCarrot()
  {
    m_last_carrot += m_spin_rate / m_loop_rate;

    matec_msgs::FullJointStates carrot;

    carrot.joint_indices.push_back(m_hokuyo_joint_idx);
    carrot.position.push_back(m_last_carrot);
    carrot.velocity.push_back(m_spin_rate);
    carrot.acceleration.push_back(0.0);
    carrot.torque.push_back(0.0);
    carrot.header.stamp = ros::Time::now();

    m_spin_pub.publish(carrot);
  }

  void MultisenseSim::spin()
  {
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      sendSpindleCarrot();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "multisense_sim");
  ros::NodeHandle nh("~");

  matec_sim::MultisenseSim node(nh);
  node.spin();

  return 0;
}
