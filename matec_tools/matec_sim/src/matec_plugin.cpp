#include <ros/ros.h>
#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/physics/Contact.hh>
#include <gazebo/common/common.hh>
#include <sdf/parser.hh>
#include <stdio.h>
#include <unistd.h>
#include <sensor_msgs/JointState.h>
#include <matec_msgs/Odometry.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/FullLinkState.h>
#include <matec_msgs/StringArray.h>
#include <matec_utils/biquad.h>
#include <matec_msgs/Tether.h>
#include <matec_msgs/WrenchArray.h>
#include <geometry_msgs/WrenchStamped.h>

#include <urdf/model.h>
#include <urdf_parser/urdf_parser.h>

#include <matec_dynamics_tree/dynamics_simulator.h>
#define WHITE_TEXT  "\033[0m"
#define RED_TEXT "\033[31m"

namespace gazebo
{
  enum ControlMode
  {
    POSITION_MODE,
    VELOCITY_MODE,
    TORQUE_MODE
  };

  enum PinState
  {
    PINNED,
    UNPINNED
  };

  class MATECPlugin: public ModelPlugin
  {
  public:
    MATECPlugin()
    {
      int argc = 0;
      ros::init(argc, NULL, "matec_plugin");
      m_num_times_activated_collisions = 0;
    }

    ~MATECPlugin()
    {
    }

    void getParams()
    {
      ROS_INFO("%s: Getting params", m_nh->getNamespace().c_str());
      while(!m_nh->hasParam("/control_mode"))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Waiting for /control_mode parameter...", m_nh->getNamespace().c_str());
        ros::spinOnce();
      }
      std::string mode_string, pinned_string;
      m_nh->param("/control_mode", mode_string, std::string("torque"));
      if(mode_string == "position")
      {
        m_control_mode = POSITION_MODE;
        ROS_INFO("%s: Running in position control mode.", m_nh->getNamespace().c_str());
      }
      else if(mode_string == "velocity")
      {
        m_control_mode = VELOCITY_MODE;
        ROS_INFO("%s: Running in velocity control mode.", m_nh->getNamespace().c_str());
      }
      else if(mode_string == "torque")
      {
        m_control_mode = TORQUE_MODE;
        ROS_INFO("%s: Running in torque control mode.", m_nh->getNamespace().c_str());
      }
      else
      {
        ROS_ERROR_STREAM("INVALID CONTROL MODE PARAMETER \"" << mode_string << "\" DETECTED! Using torque mode");
        m_control_mode = TORQUE_MODE;
      }

      while(!m_nh->hasParam("/pinned"))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Waiting for /pinned parameter...", m_nh->getNamespace().c_str());
        ros::spinOnce();
      }
      m_nh->param("/pinned", pinned_string, std::string("true"));
      if(pinned_string == "true")
      {
        if(m_nh->hasParam("/pinned_link")) //split because m_nh->param requires a const
        {
          m_nh->getParam("/pinned_link", m_pinned_link);
        }
        else
        {
          m_pinned_link = m_links[0]->GetName();
        }
        ROS_INFO_STREAM(m_nh->getNamespace()<<": Pinning the robot at " << m_pinned_link);
        pinRobot(m_pinned_link);
      }
      else if(pinned_string == "false")
      {
        ROS_INFO("%s: Not pinning the robot.", m_nh->getNamespace().c_str());
      }
      else
      {
        ROS_ERROR_STREAM(m_nh->getNamespace().c_str() << ": INVALID PINNED MODE PARAMETER \"" << pinned_string << "\" DETECTED! Not pinning the robot!");
      }
    }

    void Load(physics::ModelPtr _parent, sdf::ElementPtr sdfElement)
    {
      std::cerr << "Loading MATEC plugin" << std::endl;
      m_nh = new ros::NodeHandle("~");
      m_model = _parent;
      m_joints = m_model->GetJoints();
      m_links = m_model->GetLinks();
      updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&MATECPlugin::OnUpdate, this, _1));

      m_contact_manager = this->m_model->GetWorld()->GetPhysicsEngine()->GetContactManager();
      m_transport_node = transport::NodePtr(new transport::Node());
      m_transport_node->Init();
      transport::requestNoReply(m_transport_node->GetTopicNamespace(), "show_contact", "all");

      m_total_mass = 0.0;
      for(unsigned int i = 0; i < m_links.size(); i++)
      {
        m_total_mass += m_links[i]->GetInertial()->GetMass();
      }

      m_joint_states.position.resize(m_joints.size(), 0.0);
      m_joint_states.velocity.resize(m_joints.size(), 0.0);
      m_joint_states.acceleration.resize(m_joints.size(), 0.0);
      m_joint_states.torque.resize(m_joints.size(), 0.0);
      m_joint_states.compliance.resize(m_joints.size(), 0.5);

      for(unsigned int i = 0; i < m_joints.size(); i++)
      {
        m_joint_states.joint_indices.push_back(i);
        m_joint_names.push_back(m_joints[i]->GetName());
        m_original_low_stops.push_back(m_joints[i]->GetLowStop(0).Radian());
        m_original_high_stops.push_back(m_joints[i]->GetHighStop(0).Radian());
        std::cerr << "Original stops for " << m_joints[i]->GetName() << " were " << m_joints[i]->GetLowStop(0).Radian() << " and " << m_joints[i]->GetHighStop(0).Radian() << std::endl;
      }
      m_joint_commands = m_joint_states;
      m_next_joint_states = m_joint_states;
      m_noisy_joint_states = m_joint_states;
      joint_states.name = m_joint_names;
      matec_msgs::StringArray joint_names;
      joint_names.data = m_joint_names;

      getParams();

      //todo: handle not pinned
      math::Pose initial_root_link_pose = this->m_model->GetLink(m_pinned_link)->GetWorldPose();
      m_root_link_odometry.header.stamp = m_joint_states.header.stamp;
      m_root_link_odometry.header.frame_id = "raw_odometry";
      m_root_link_odometry.pose.position.x = initial_root_link_pose.pos.x;
      m_root_link_odometry.pose.position.y = initial_root_link_pose.pos.y;
      m_root_link_odometry.pose.position.z = initial_root_link_pose.pos.z;
      m_root_link_odometry.pose.orientation.w = initial_root_link_pose.rot.w;
      m_root_link_odometry.pose.orientation.x = initial_root_link_pose.rot.x;
      m_root_link_odometry.pose.orientation.y = initial_root_link_pose.rot.y;
      m_root_link_odometry.pose.orientation.z = initial_root_link_pose.rot.z;
      m_next_root_link_odometry = m_root_link_odometry;

      m_dynamics_simulator.loadFromParameterServer(joint_names.data, m_pinned_link, false);
      m_dynamics_simulator.initialize(m_joint_states, m_root_link_odometry);

      std::cerr << "Initial root link odometry is:\n" << m_root_link_odometry << std::endl;

      m_root_link_odom_pub.advertise("/raw_odometry");
      m_ankle_wrench_pub.advertise("/ankle_wrenches");
      m_joint_states_pub.advertise("/joint_states");
      m_true_joint_states_pub.advertise("/true_joint_states");
      m_joint_names_pub.advertise("/joint_names");
      m_joint_names_pub.publish(joint_names);

      m_joint_commands_sub.subscribe("/joint_commands");

      m_js_pub = m_nh->advertise<sensor_msgs::JointState>("/joint_states", 1, true);
      m_tether_srv = m_nh->advertiseService("/manage_tether", &MATECPlugin::tetherCallback, this);

      std::cerr << "Done loading MATEC plugin!" << std::endl;
    }

    bool tetherCallback(matec_msgs::Tether::Request& req, matec_msgs::Tether::Response& res)
    {
      gazebo::physics::JointPtr x_joint = this->m_model->GetJoints()[0];
      gazebo::physics::JointPtr height_joint = this->m_model->GetJoints()[1];
      gazebo::physics::JointPtr rot_joint = this->m_model->GetJoints()[2];
      if(req.tethered)
      {
        height_joint->SetHighStop(0, req.height);
        height_joint->SetLowStop(0, req.height);
        rot_joint->SetDamping(0, 5.0);
        x_joint->SetDamping(0, 5.0);
      }
      else
      {
        height_joint->SetHighStop(0, -100000);
        height_joint->SetLowStop(0, 100000);
        rot_joint->SetDamping(0, 0.0);
        x_joint->SetDamping(0, 0.0);
      }
      return true;
    }

    //type is prismatic or revolute
    physics::JointPtr createJoint(std::string name, std::string type, gazebo::physics::LinkPtr parent_link, gazebo::physics::LinkPtr child_link, math::Vector3 axis, double low_stop, double high_stop)
    {
      physics::JointPtr joint = this->m_model->GetWorld()->GetPhysicsEngine()->CreateJoint(type, this->m_model);
      joint->Attach(parent_link, child_link);
      joint->Load(parent_link, child_link, math::Pose(parent_link->GetWorldPose().pos, math::Quaternion()).GetInverse()); // load adds the joint to a vector of shared pointers kept in parent and child links, preventing joint from being destroyed.
      joint->SetAxis(0, axis);
      joint->SetHighStop(0, gazebo::math::Angle(high_stop));
      joint->SetLowStop(0, gazebo::math::Angle(low_stop));
      joint->SetName(name);
      joint->Init();

      return joint;
    }

    void pinRobot(std::string pin_link_name)
    {
      physics::LinkPtr world_link = physics::LinkPtr();
      gazebo::physics::LinkPtr pin_link = this->m_model->GetLink(pin_link_name);
      if(!pin_link)
      {
        ROS_FATAL("Requested pin link %s does not exist!", pin_link_name.c_str());
        assert(0);
      }

      createJoint("pin", "prismatic", pin_link, world_link, math::Vector3(0, 0, 1), 0, 0);
    }

    // Called by the world update start event
    void OnUpdate(const common::UpdateInfo & /*_info*/)
    {
      double time = m_model->GetWorld()->GetSimTime().Double();
      double dt = 1.0 / 1000.0; //TODO: read actual step dt

      if(m_num_times_activated_collisions < 1)
      {
        std::cerr << "MATEC Plugin: Activating collision detection! If this awful simulator decides to freeze or crash because of it, just try restarting!" << std::endl;
        transport::requestNoReply(m_transport_node->GetTopicNamespace(), "show_contact", "all");
        std::cerr << "MATEC Plugin: Collisions activated?" << std::endl;
        m_num_times_activated_collisions++;
      }

      std::vector<gazebo::physics::Contact*> contacts = m_contact_manager->GetContacts();
      std::vector<matec_utils::Vector6> external_wrenches(m_joint_names.size(), matec_utils::Vector6::Zero());
      if(contacts.size() != 0)
      {
//        std::cerr << "CONTACT GET?!?" << std::endl;
        for(unsigned int i = 0; i < contacts.size(); i++)
        {
          if(!contacts[i]->collision1 || !contacts[i]->collision1->GetLink() || (contacts[i]->collision1->GetLink()->GetParentJoints().size() == 0))
          {
            continue;
          }
          std::string link_name = contacts[i]->collision1->GetLink()->GetName();
          std::string joint_name = contacts[i]->collision1->GetLink()->GetParentJoints()[0]->GetName();
          unsigned int joint_idx = std::find(m_joint_names.begin(), m_joint_names.end(), joint_name) - m_joint_names.begin();
//          std::cerr << "Contact #" << i << " on link " << link_name << " attached to joint " << joint_name << " had " << contacts[i]->count << " contact points!";
          for(int j = 0; j < contacts[i]->count; j++)
          {
//            std::cerr << "\tContact " << i << "-" << j << ":";
//            std::cerr << "\t\tPosition:" << contacts[i]->positions[j] << std::endl;
//            std::cerr << "\t\tNormal:" << contacts[i]->normals[j] << std::endl;
//            std::cerr << "\t\tWrench body 1 force:" << contacts[i]->wrench[j].body1Force << std::endl;
//            std::cerr << "\t\tWrench body 1 torque:" << contacts[i]->wrench[j].body1Torque << std::endl;
//            std::cerr << "\t\tWrench body 2 force:" << contacts[i]->wrench[j].body2Force << std::endl;
//            std::cerr << "\t\tWrench body 2 torque:" << contacts[i]->wrench[j].body2Torque << std::endl;

            double stupid_gazebo_factor = 0.0001; //assume that Gazebo is being stupid and specifying contact forces that are way larger than is physically possible
            external_wrenches[joint_idx](0) = external_wrenches[joint_idx](0) + stupid_gazebo_factor * contacts[i]->wrench[j].body1Torque.x;
            external_wrenches[joint_idx](1) = external_wrenches[joint_idx](1) + stupid_gazebo_factor * contacts[i]->wrench[j].body1Torque.y;
            external_wrenches[joint_idx](2) = external_wrenches[joint_idx](2) + stupid_gazebo_factor * contacts[i]->wrench[j].body1Torque.z;
            external_wrenches[joint_idx](3) = external_wrenches[joint_idx](3) + stupid_gazebo_factor * contacts[i]->wrench[j].body1Force.x;
            external_wrenches[joint_idx](4) = external_wrenches[joint_idx](4) + stupid_gazebo_factor * contacts[i]->wrench[j].body1Force.y;
            external_wrenches[joint_idx](5) = external_wrenches[joint_idx](5) + stupid_gazebo_factor * contacts[i]->wrench[j].body1Force.z;
          }

          double stupidity_scale = 1.0;
          double absolute_max_stupidity_force = 0.5 * m_total_mass * 9.81;
          double absolute_max_stupidity_torque = 0.1 * m_total_mass * 9.81; //TODO: use the max radius of the robot * mass of the robot * gravity?
          for(unsigned int j = 0; j < 3; j++)
          {
            if(external_wrenches[joint_idx](j) != 0)
            {
              stupidity_scale = std::min(stupidity_scale, fabs(matec_utils::clamp((double) external_wrenches[joint_idx](j), -absolute_max_stupidity_force, absolute_max_stupidity_force) / (double) external_wrenches[joint_idx](j)));
            }
          }
          for(unsigned int j = 3; j < 6; j++)
          {
            if(external_wrenches[joint_idx](j) != 0)
            {
              stupidity_scale = std::min(stupidity_scale, fabs(matec_utils::clamp((double) external_wrenches[joint_idx](j), -absolute_max_stupidity_torque, absolute_max_stupidity_torque) / (double) external_wrenches[joint_idx](j)));
            }
          }
          external_wrenches[joint_idx] *= stupidity_scale;
        }
      }

      //get command
      if(!m_joint_commands_sub.getCurrentMessage(m_joint_commands))
      {
        m_joint_commands.torque.clear();
        m_joint_commands.torque.resize(m_joints.size(), 0.0);
      }

      switch(m_control_mode)
      {
      case POSITION_MODE:
      {
        for(unsigned int i = 0; i < m_joint_commands.position.size(); i++)
        {
          if(isnan(m_joint_commands.position[i]) || isinf(m_joint_commands.position[i]))
          {
            std::cerr << "RECEIVED BAD JOINT POSITION " << m_joint_commands.position[i] << " FOR JOINT " << i << "!" << std::endl;
            continue;
          }

          double new_position = matec_utils::clamp(m_joint_commands.position[i], m_original_low_stops[i], m_original_high_stops[i]);
          double new_velocity = (new_position - m_joint_states.position[i]) / dt;
          double new_acceleration = (new_velocity - m_joint_states.velocity[i]) / dt;
          m_joint_states.position[i] = new_position;
          m_joint_states.velocity[i] = new_velocity;
          m_joint_states.acceleration[i] = new_acceleration;
        }
        break;
      }
      case VELOCITY_MODE:
      {
        for(unsigned int i = 0; i < m_joint_commands.velocity.size(); i++)
        {
          if(isnan(m_joint_commands.velocity[i]) || isinf(m_joint_commands.velocity[i]))
          {
            std::cerr << "RECEIVED BAD JOINT VELOCITY " << m_joint_commands.velocity[i] << " FOR JOINT " << i << "!" << std::endl;
            m_joint_states.velocity[i] = 0;
          }

          double new_velocity = m_joint_commands.velocity[i]; //TODO: clamp?
          double new_position = matec_utils::clamp(m_joint_commands.position[i] + new_velocity * dt, m_original_low_stops[i], m_original_high_stops[i]);
          double new_acceleration = (new_velocity - m_joint_states.velocity[i]) / dt;
          m_joint_states.position[i] = new_position;
          m_joint_states.velocity[i] = new_velocity;
          m_joint_states.acceleration[i] = new_acceleration;
        }
        break;
      }
      case TORQUE_MODE:
      {
        //make sure torques are valid
        for(unsigned int i = 0; i < m_joint_commands.torque.size(); i++) //TODO: move torque limits into dynamics tree
        {
//          if(isnan(m_joint_commands.torque[i]) || isinf(m_joint_commands.torque[i]))
//          {
//            std::cerr << "RECEIVED BAD JOINT TORQUE " << m_joint_commands.torque[i] << " FOR JOINT " << i << "!" << std::endl;
//            m_joint_commands.torque[i] = 0.0;
//          }
//          m_joint_commands.torque[i] = matec_utils::clamp(m_joint_commands.torque[i], -m_joints[i]->GetEffortLimit(0), m_joints[i]->GetEffortLimit(0));
          m_joint_commands.torque[i] = 0; //matec_utils::clamp(m_joint_commands.torque[i], -m_joints[i]->GetEffortLimit(0), m_joints[i]->GetEffortLimit(0));
        }

        //calculate dynamics
        m_joint_states = m_next_joint_states;
        m_root_link_odometry = m_next_root_link_odometry;
        m_dynamics_simulator.simulationStep(m_joint_commands.torque, external_wrenches, m_next_joint_states, m_next_root_link_odometry);
        m_joint_states.torque = m_joint_commands.torque;
        m_joint_states.acceleration = m_next_joint_states.acceleration;
        break;
      }
      default:
        ROS_ERROR("BAD CONTROL MODE");
      }

      //set gazebo joint positions
      for(unsigned int i = 0; i < m_joints.size(); i++)
      {
        double position_setpoint = m_joint_states.position[i];
        m_joints[i]->SetHighStop(0, position_setpoint);
        m_joints[i]->SetLowStop(0, position_setpoint);
      }

      //TODO: set root link position!

      //broadcast new states
      m_joint_states.header.stamp = ros::Time(time);
      m_true_joint_states_pub.publish(m_joint_states);
      double noise_variance = 0.005; //todo:param
      for(unsigned int i = 0; i < m_noisy_joint_states.joint_indices.size(); i++)
      {
        double position = m_joint_states.position[i] + matec_utils::normalRandom(0.0, noise_variance);
        double velocity = (position - m_noisy_joint_states.position[i]) / dt;
        double acceleration = (velocity - m_noisy_joint_states.velocity[i]) / dt;
        m_noisy_joint_states.position[i] = position;
        m_noisy_joint_states.velocity[i] = velocity;
        m_noisy_joint_states.acceleration[i] = acceleration;
      }
      m_noisy_joint_states.torque = m_joint_states.torque;
      m_noisy_joint_states.compliance = m_joint_states.compliance;
      m_noisy_joint_states.header.stamp = m_joint_states.header.stamp;
//      m_joint_states_pub.publish(m_noisy_joint_states); //todo: param
      m_joint_states_pub.publish(m_joint_states);

      m_root_link_odometry.header.stamp = m_joint_states.header.stamp;
      m_root_link_odom_pub.publish(m_root_link_odometry);

      joint_states.position = m_joint_states.position;
      joint_states.velocity = m_joint_states.velocity;
      joint_states.effort = m_joint_states.torque;
      joint_states.header.stamp = m_joint_states.header.stamp;
      m_js_pub.publish(joint_states);
    }

  private:
    physics::ModelPtr m_model;
    physics::ContactManager* m_contact_manager;
    transport::NodePtr m_transport_node;
    event::ConnectionPtr updateConnection;

    int m_num_times_activated_collisions;
    double m_total_mass;

    ros::NodeHandle* m_nh;
    ros::Publisher m_js_pub;

    gazebo::physics::Joint_V m_joints;
    gazebo::physics::Link_V m_links;
    std::vector<std::string> m_joint_names;
    std::string m_pinned_link;

    ControlMode m_control_mode;

    ros::ServiceServer m_tether_srv;

    dynamics_tree::DynamicsSimulator m_dynamics_simulator;

    shared_memory_interface::Publisher<matec_msgs::Odometry> m_root_link_odom_pub;
    shared_memory_interface::Publisher<matec_msgs::WrenchArray> m_ankle_wrench_pub;
    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_states_pub;
    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_true_joint_states_pub;
    shared_memory_interface::Publisher<matec_msgs::StringArray> m_joint_names_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_commands_sub;

    physics::JointPtr pin_joint_handle;

    matec_msgs::FullJointStates m_noisy_joint_states;
    matec_msgs::FullJointStates m_joint_states;
    matec_msgs::FullJointStates m_next_joint_states;
    matec_msgs::Odometry m_root_link_odometry;
    matec_msgs::Odometry m_next_root_link_odometry;

    std::vector<double> m_original_low_stops;
    std::vector<double> m_original_high_stops;

    sensor_msgs::JointState joint_states;

    matec_msgs::FullJointStates m_joint_commands;
  };

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(MATECPlugin)

}
