#include "system_id/coefficient_optimizer.h"
#include <cmath>
#include "system_id/common_math.h"

#include "system_id/training_data.h"

using namespace system_id;

int main()
{
  std::string expression_string = "a*x + b";

  std::vector<std::string> coefficient_symbols;
  coefficient_symbols.push_back("a");
  coefficient_symbols.push_back("b");

  std::vector<std::string> input_symbols;
  input_symbols.push_back("x");

  system_id::TrainingData data;
  for(double x = -5.0; x <= 5.0; x += 0.1)
  {
    data.addSingleInput(x);
    data.addSingleOutput(9.75 * x + 3.14);//) + system_id::uniformRandomCentered());
  }

  system_id::ArrayGenome::GeneType initial_guess;
  initial_guess.first.push_back(0);
  initial_guess.second.push_back(9.75);
  initial_guess.second.push_back(3.14);

  GAParams params(coefficient_symbols.size(), 3, 0.001, 100000, 10);

  system_id::CoefficientOptimizer opt(&data);
  system_id::ArrayGenome best = opt.optimize(expression_string, coefficient_symbols, input_symbols, params);
  best = opt.optimize(expression_string, coefficient_symbols, input_symbols, params, &best.gene);
  std::cerr << best << std::endl;

}
