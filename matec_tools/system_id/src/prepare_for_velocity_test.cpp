#include <cmath>
#include <set>

#include "system_id/training_data.h"

int main(int argc, char* argv[])
{
  if(argc != 3)
  {
    std::cerr << "Args: [filename in], [filename out]" << std::endl;
    return 0;
  }

  std::string filename_in(argv[1]);
  std::string filename_out(argv[2]);
  system_id::TrainingData data(filename_in);

  std::vector<std::string> input_labels = data.getInputLabels();
  int position_idx = std::find(input_labels.begin(), input_labels.end(), "p") - input_labels.begin();
  int velocity_idx = std::find(input_labels.begin(), input_labels.end(), "v") - input_labels.begin();
  int transmission_idx = std::find(input_labels.begin(), input_labels.end(), "tr") - input_labels.begin();
  int grav_idx = std::find(input_labels.begin(), input_labels.end(), "g") - input_labels.begin();

  if(position_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'p'!";
    return 0;
  }

  long double max_position = -std::numeric_limits<long double>::max();
  long double min_position = std::numeric_limits<long double>::max();
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    if(input_set[position_idx] > max_position)
    {
      max_position = input_set[position_idx];
    }
    if(input_set[position_idx] < min_position)
    {
      min_position = input_set[position_idx];
    }
  }

  long double threshold = (max_position - min_position) * 0.01; //remove points within 1% of a joint limit
  std::cerr << "Found joint limits at " << min_position << " and " << max_position << std::endl;

  std::vector<std::vector<long double> > input_sets, output_sets;
  for(unsigned int i = 0; i < data.size(); i++)
  {
    long double gravity = data.getInputSet(i)[grav_idx];

    std::vector<long double> input_set;
    input_set.push_back(data.getInputSet(i)[velocity_idx]);
    input_set.push_back(data.getInputSet(i)[transmission_idx]);
    std::vector<long double> output_set;
    output_set.push_back(data.getOutputSet(i)[0]);

    long double position = data.getInputSet(i)[position_idx];
    long double max_limit_distance = max_position - position;
    long double min_limit_distance = position - min_position;
    assert(max_limit_distance >= 0.0 && min_limit_distance >= 0.0);

    if((max_limit_distance > threshold) && (min_limit_distance > threshold)) //we're more than threshold away from either limit
    {
      input_sets.push_back(input_set);
      output_sets.push_back(output_set);
    }
  }

  std::vector<std::string> new_input_labels;
  new_input_labels.push_back("v");
  new_input_labels.push_back("tr");

  std::vector<std::string> new_output_labels;
  new_output_labels.push_back("cmd");

  system_id::TrainingData::writeDataFile(filename_out, input_sets, new_input_labels, output_sets, new_output_labels);
}
