#include "system_id/coefficient_optimizer.h"
#include "system_id/exprtk.hpp"

namespace system_id
{
  exprtk::symbol_table<long double> m_symbol_table;
  exprtk::expression<long double> m_expression;
  exprtk::parser<long double> m_parser;

  CoefficientOptimizer::CoefficientOptimizer(system_id::TrainingData* data) :
      m_plotter(data->getInputLabels().size() + 1)
  {
    while(!m_plotter.isReady())
    {

    }

    m_training_data = data;

    unsigned int num_inputs = m_training_data->getInputLabels().size();

    for(unsigned int j = 0; j < data->size(); j++)
    {
      m_t_data.push_back(j);
      m_target_output_data.push_back(data->getOutputSet(j)[0]);
    }

    for(unsigned int i = 0; i < num_inputs; i++)
    {
      std::vector<long double> x_data;
      for(unsigned int j = 0; j < data->size(); j++)
      {
        x_data.push_back(data->getInputSet(j)[i]);
      }
      m_plotter.setData(i, "1target", x_data, m_target_output_data, "r");
    }
    m_plotter.setData(num_inputs, "1target_output", m_t_data, m_target_output_data, "r");
    m_next_plot_time = boost::get_system_time();
  }

  ArrayGenome CoefficientOptimizer::optimize(std::string expression_string, std::vector<std::string> coefficient_symbols, std::vector<std::string> input_symbols, GAParams ga_params, ArrayGenome::GeneType* min_bound, ArrayGenome::GeneType* max_bound, ArrayGenome::GeneType* initial_guess)
  {
    m_best_fitness = std::numeric_limits<long double>::max();
    //expression parser setup
    m_symbol_table = exprtk::symbol_table<long double>();
    m_expression = exprtk::expression<long double>();
//    m_parser = exprtk::parser<long double>();
    m_inputs.clear();
    m_coefficients.clear();

    m_coefficients.resize(coefficient_symbols.size());
    for(unsigned int i = 0; i < coefficient_symbols.size(); i++)
    {
      m_symbol_table.add_variable(coefficient_symbols[i], m_coefficients[i]);
    }

    m_inputs.resize(input_symbols.size());
    for(unsigned int i = 0; i < input_symbols.size(); i++)
    {
      m_symbol_table.add_variable(input_symbols[i], m_inputs[i]);
    }

    m_symbol_table.add_constants();
    m_expression.register_symbol_table(m_symbol_table);

    m_parser.compile(expression_string, m_expression);
    m_expression_string = expression_string;

    m_output_min = std::numeric_limits<long double>::max();
    m_output_max = -std::numeric_limits<long double>::max();
    for(unsigned int i = 0; i < m_training_data->size(); i++)
    {
      long double output = m_training_data->getOutputSet(i)[0]; //assuming only one output for now

      if(output < m_output_min)
      {
        m_output_min = output;
      }
      if(output > m_output_max)
      {
        m_output_max = output;
      }
    }

    ArrayGenome::GeneType min, max;
    for(unsigned int i = 0; i < m_inputs.size(); i++)
    {
      //don't shift more than 5% of the dataset
      long max_shift = m_training_data->size() * 0.05; //TODO: make param
      min.first.push_back(-max_shift);
      max.first.push_back(max_shift);
    }
    for(unsigned int i = 0; i < m_coefficients.size(); i++)
    {
      min.second.push_back(2.0 * m_output_min);
      max.second.push_back(2.0 * m_output_max);
    }

    if(min_bound)
    {
      min = *min_bound;
    }

    if(max_bound)
    {
      max = *max_bound;
    }

    //GA setup
    GeneticAlgorithmNumeric ga(boost::bind(&CoefficientOptimizer::fitness_function, this, _1), min, max);
    return ga.optimize(ga_params, initial_guess);
  }

  void CoefficientOptimizer::fitness_function(ArrayGenome& genome)
  {
    if(genome.gene.first.size() == 0 || genome.gene.second.size() == 0)
    {
      std::cerr << genome << std::endl;
      assert(genome.gene.first.size() != 0);
      assert(genome.gene.second.size() != 0);
    }
    assert(m_inputs.size() == genome.gene.first.size());
    assert(m_coefficients.size() == genome.gene.second.size());
    m_coefficients = genome.gene.second;

    for(unsigned int i = 0; i < m_coefficients.size(); i++)
    {
      if(isnan(m_coefficients[i]) || isinf(m_coefficients[i]))
      {
        PRINTLN_ERROR_THROTTLE(1.0, "Coefficient was nan! Throwing the genome away!");
        genome.fitness = std::numeric_limits<long double>::max();
        return;
      }
    }

    std::vector<long> shifts = genome.gene.first;
    long min_shift = *std::min_element(shifts.begin(), shifts.end());
    long max_shift = *std::max_element(shifts.begin(), shifts.end());

    min_shift = std::min(min_shift, (long) 0);
    max_shift = std::max(max_shift, (long) 0);

    if(-min_shift >= (m_training_data->size() - max_shift)) // way too much shifting happening!
    {
      PRINTLN_ERROR("BAD SHIFT IS HAPPENING!! %d, %d, %d", (int)min_shift, (int)max_shift, (int)(m_training_data->size() - max_shift));
      genome.fitness = std::numeric_limits<long double>::max();
      return;
    }

    long double sse = 0;
    std::vector<long double> t_data;
    std::vector<std::vector<long double> > x_data(m_training_data->getInputLabels().size());
    std::vector<long double> y_data;
    for(unsigned int i = -min_shift; i < m_training_data->size() - max_shift; i++)
    {
      m_inputs = m_training_data->getInputSet(i, shifts);
      assert(!isnan(m_inputs[0]));
      assert(!isinf(m_inputs[0]));
      long double output = m_expression.value();

      for(unsigned int j = 0; j < m_inputs.size(); j++)
      {
        x_data[j].push_back(m_inputs[j]);
      }
      y_data.push_back(output);
      t_data.push_back(i);

      if(std::isnan(output))
      {
//        std::cerr << "Expression " << m_expression_string << " had nanout for coeffs [";
//        for(unsigned int j = 0; j < m_coefficients.size(); j++)
//        {
//          if(j != 0)
//          {
//            std::cerr << ", ";
//          }
//          std::cerr << m_coefficients[j];
//        }
//        std::cerr << "] at x=" << m_inputs[0] << std::endl;

        genome.fitness = std::numeric_limits<long double>::max();
        return;
      }
//      else
//      {
//        std::cerr << "Expression " << m_expression_string << " was ok for coeffs [";
//        for(unsigned int i = 0; i < m_coefficients.size(); i++)
//        {
//          if(i != 0)
//          {
//            std::cerr << ", ";
//          }
//          std::cerr << m_coefficients[i];
//        }
//        std::cerr << "] at x=" << m_inputs[0] << std::endl;
//
//        genome.fitness = std::numeric_limits<long double>::max();
//      }

      double training_output = m_training_data->getOutputSet(i)[0]; //TODO: allow multiple outputs!
      long double err = training_output - output;
      sse += err * err;
    }
    genome.fitness = sqrt(sse) / t_data.size();
    PRINTLN_INFO_STREAM_THROTTLE(0.1, "==>Currently evaluating expression " << m_expression_string << ".");

    if(genome.fitness < m_best_fitness)
    {
      for(unsigned int i = 0; i < x_data.size(); i++)
      {
        std::string name = m_training_data->getInputLabels()[i];
        m_plotter.setData(i, "2 current (" + name + ")", x_data[i], y_data, "b", false);
      }
      m_plotter.setData(x_data.size(), "2current_output (time)", t_data, y_data, "b", false);
      m_next_plot_time = boost::get_system_time() + boost::posix_time::milliseconds(1000);
      if(genome.fitness < m_best_fitness)
      {
        m_best_fitness = genome.fitness;
      }
    }
    else if(m_next_plot_time < boost::get_system_time())
    {
      m_plotter.refreshPlot();
      m_next_plot_time = boost::get_system_time() + boost::posix_time::milliseconds(1000);
    }
  }
}
