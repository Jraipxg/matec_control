#include <cmath>
#include <set>

#include "system_id/training_data.h"

int main(int argc, char* argv[])
{
  if(argc != 4)
  {
    std::cerr << "Args: [filename in], [filename out], [remove data near joint limits (0) or remove everything else (1)]" << std::endl;
    return 0;
  }

  std::string filename_in(argv[1]);
  std::string filename_out(argv[2]);
  bool remove_joint_limit_data = !atoi(argv[3]);
  system_id::TrainingData data(filename_in);

  std::vector<std::string> input_labels = data.getInputLabels();
  int position_idx = std::find(input_labels.begin(), input_labels.end(), "p") - input_labels.begin();

  if(position_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'p'!";
    return 0;
  }

  long double max_position = -std::numeric_limits<long double>::max();
  long double min_position = std::numeric_limits<long double>::max();
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    if(input_set[position_idx] > max_position)
    {
      max_position = input_set[position_idx];
    }
    if(input_set[position_idx] < min_position)
    {
      min_position = input_set[position_idx];
    }
  }

  long double threshold = (max_position - min_position) * 0.01; //remove points within 1% of a joint limit
  std::cerr << "Found joint limits at " << min_position << " and " << max_position << std::endl;

  if(remove_joint_limit_data)
  {
    std::cerr << "Removing points within 1% (" << threshold << " rad) of the limits" << std::endl;
  }
  else
  {
    std::cerr << "Keeping only points within 1% (" << threshold << " rad) of the limits" << std::endl;
  }

  std::vector<std::vector<long double> > input_sets, output_sets;
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    std::vector<long double> output_set = data.getOutputSet(i);

    long double position = input_set[position_idx];
    long double max_limit_distance = max_position - position;
    long double min_limit_distance = position - min_position;
    assert(max_limit_distance >= 0.0 && min_limit_distance >= 0.0);

    if(remove_joint_limit_data)
    {
      if((max_limit_distance > threshold) && (min_limit_distance > threshold)) //we're more than threshold away from either limit
      {
        input_sets.push_back(input_set);
        output_sets.push_back(output_set);
      }
    }
    else
    {
      if(!((max_limit_distance > threshold) && (min_limit_distance > threshold)))
      {
        input_sets.push_back(input_set);
        output_sets.push_back(output_set);
      }
    }
  }

  system_id::TrainingData::writeDataFile(filename_out, input_sets, data.getInputLabels(), output_sets, data.getOutputLabels());
}
