#include "system_id/plotter.h"
#include "system_id/debug_levels.h"
#include "system_id/common_math.h"
#include "system_id/training_data.h"

#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

std::string colors[] = {"b", "g", "r", "y", "c", "m", "k", "w"};

int main(int argc, char* argv[])
{
  /*if(argc != 3)
   {
   std::cerr << "Args: filename, all_together(0/1)" << std::endl;
   return 0;
   }*/

  std::string filename(argv[1]);
  system_id::TrainingData data(filename);
  std::map<std::string, std::vector<long double> > data_map;
  std::map<std::string, std::pair<long double, long double> > data_limits; //min, max
  std::vector<std::string> input_labels = data.getInputLabels();
  std::vector<std::string> output_labels = data.getOutputLabels();

  data_map["_idx_"] = std::vector<long double>();
  for(unsigned int i = 0; i < input_labels.size(); i++)
  {
    data_map[input_labels[i]] = std::vector<long double>();
    data_limits[input_labels[i]] = std::pair<long double, long double>(std::numeric_limits<long double>::max(), -std::numeric_limits<long double>::max());
  }
  for(unsigned int i = 0; i < output_labels.size(); i++)
  {
    data_map[output_labels[i]] = std::vector<long double>();
    data_limits[output_labels[i]] = std::pair<long double, long double>(std::numeric_limits<long double>::max(), -std::numeric_limits<long double>::max());
  }
  for(unsigned int i = 0; i < data.size(); i++)
  {
    data_map["_idx_"].push_back(i);

    std::vector<long double> inputs = data.getInputSet(i);
    for(unsigned int j = 0; j < inputs.size(); j++)
    {
      data_map[input_labels[j]].push_back(inputs[j]);

      if(inputs[j] < data_limits[input_labels[j]].first)
      {
        data_limits[input_labels[j]].first = inputs[j];
      }
      if(inputs[j] > data_limits[input_labels[j]].second)
      {
        data_limits[input_labels[j]].second = inputs[j];
      }
    }

    std::vector<long double> outputs = data.getOutputSet(i);
    for(unsigned int j = 0; j < outputs.size(); j++)
    {
      data_map[output_labels[j]].push_back(outputs[j]);

      if(outputs[j] < data_limits[output_labels[j]].first)
      {
        data_limits[output_labels[j]].first = outputs[j];
      }
      if(outputs[j] > data_limits[output_labels[j]].second)
      {
        data_limits[output_labels[j]].second = outputs[j];
      }
    }
  }

  std::vector<std::vector<std::string> > plots;
  boost::char_separator<char> sep(",");
  for(int i = 2; i < argc; i++)
  {
    std::string line(argv[i]);
    boost::tokenizer<boost::char_separator<char> > tokens(line, sep);
    std::vector<std::string> labels = std::vector<std::string>(tokens.begin(), tokens.end());
    plots.push_back(labels);
  }

  system_id::Plotter plotter(plots.size());
  while(!plotter.isReady())
  {
  }

  while(1)
  {
    for(unsigned int plot = 0; plot < plots.size(); plot++)
    {
      if(plots[plot].size() == 1)
      {
        plotter.setData(plot, plots[plot][0], data_map["_idx_"], data_map[plots[plot][0]], colors[0]);
      }
      else if(plots[plot][0] == "_norm_") //implies that all following fields should be normalized and plotted together against index
      {
        std::vector<std::vector<long double> > normalized_data(plots[plot].size());
        for(unsigned int i = 1; i < plots[plot].size(); i++)
        {
          long double scale = 2.0 / (data_limits[plots[plot][i]].second - data_limits[plots[plot][i]].first);
          long double shift = -(data_limits[plots[plot][i]].second + data_limits[plots[plot][i]].first) / 2;
          for(unsigned int j = 0; j < data.size(); j++)
          {
            normalized_data[i].push_back((data_map[plots[plot][i]][j] + shift) * scale);
          }
        }
        for(unsigned int i = 1; i < plots[plot].size(); i++)
        {
          plotter.setData(plot, plots[plot][i], data_map["_idx_"], normalized_data[i], colors[i]);
        }
      }
      else //first argument is the x axis, other args are on the y
      {
        for(unsigned int i = 1; i < plots[plot].size(); i++)
        {
          std::string plot_label = plots[plot][0] + " vs " + plots[plot][i];
          plotter.setData(plot, plot_label, data_map[plots[plot][0]], data_map[plots[plot][i]], colors[i]);
        }
      }
    }
  }

  //bool all_together(atoi(argv[2]));

  /*if(all_together)
   {
   system_id::Plotter plotter(1);
   while(!plotter.isReady())
   {
   }

   std::vector<double> input_mins(data.getInputLabels().size(), std::numeric_limits<double>::max());
   std::vector<double> output_mins(data.getOutputLabels().size(), std::numeric_limits<double>::max());
   std::vector<double> input_maxes(data.getInputLabels().size(), -std::numeric_limits<double>::max());
   std::vector<double> output_maxes(data.getOutputLabels().size(), -std::numeric_limits<double>::max());
   for(unsigned int i = 0; i < data.size(); i++)
   {
   std::vector<long double> inputs = data.getInputSet(i);
   for(unsigned int j = 0; j < inputs.size(); j++)
   {
   if(inputs[j] < input_mins[j])
   {
   input_mins[j] = inputs[j];
   }
   if(inputs[j] > input_maxes[j])
   {
   input_maxes[j] = inputs[j];
   }
   }
   std::vector<long double> outputs = data.getOutputSet(i);
   for(unsigned int j = 0; j < outputs.size(); j++)
   {
   if(outputs[j] < output_mins[j])
   {
   output_mins[j] = outputs[j];
   }
   if(outputs[j] > output_maxes[j])
   {
   output_maxes[j] = outputs[j];
   }
   }
   }

   std::vector<std::vector<double> > normalized_inputs(data.getInputLabels().size());
   std::vector<std::vector<double> > normalized_outputs(data.getOutputLabels().size());
   for(unsigned int i = 0; i < data.size(); i++)
   {
   std::vector<long double> inputs = data.getInputSet(i);
   for(unsigned int j = 0; j < inputs.size(); j++)
   {
   double scale = 2.0 / (input_maxes[j] - input_mins[j]);
   double shift = -(input_maxes[j] + input_mins[j]) / 2;
   normalized_inputs[j].push_back((inputs[j] + shift) * scale);
   }
   std::vector<long double> outputs = data.getOutputSet(i);
   for(unsigned int j = 0; j < outputs.size(); j++)
   {
   double scale = 2.0 / (output_maxes[j] - output_mins[j]);
   double shift = -(output_maxes[j] + output_mins[j]) / 2;
   normalized_outputs[j].push_back((outputs[j] + shift) * scale);
   }
   }

   while(1)
   {
   for(unsigned int i = 0; i < data.getInputLabels().size(); i++)
   {
   std::vector<long double> x_data, y_data;
   for(unsigned int j = 0; j < data.size(); j++)
   {
   x_data.push_back(j);
   y_data.push_back(normalized_inputs[i][j]);
   }
   plotter.setData(0, data.getInputLabels()[i], x_data, y_data, colors[i]);
   }
   for(unsigned int i = 0; i < data.getOutputLabels().size(); i++)
   {
   std::vector<long double> x_data, y_data;
   for(unsigned int j = 0; j < data.size(); j++)
   {
   x_data.push_back(j);
   y_data.push_back(normalized_outputs[i][j]);
   }
   plotter.setData(0, data.getOutputLabels()[i], x_data, y_data, colors[i + data.getInputLabels().size()]);
   }
   }
   }
   else
   {
   system_id::Plotter plotter(data.getInputLabels().size() + data.getOutputLabels().size());
   while(!plotter.isReady())
   {
   }

   while(1)
   {
   for(unsigned int i = 0; i < data.getInputLabels().size(); i++)
   {
   std::vector<long double> x_data, y_data;
   for(unsigned int j = 0; j < data.size(); j++)
   {
   x_data.push_back(j);
   y_data.push_back(data.getInputSet(j)[i]);
   }
   plotter.setData(i, data.getInputLabels()[i], x_data, y_data, colors[i]);
   }
   for(unsigned int i = 0; i < data.getOutputLabels().size(); i++)
   {
   std::vector<long double> x_data, y_data;
   for(unsigned int j = 0; j < data.size(); j++)
   {
   x_data.push_back(j);
   y_data.push_back(data.getOutputSet(j)[i]);
   }
   plotter.setData(i + data.getInputLabels().size(), data.getOutputLabels()[i], x_data, y_data, colors[i + data.getInputLabels().size()]);
   }
   }
   }*/
}
