#include <cmath>
#include <set>

#include "system_id/training_data.h"

int main(int argc, char* argv[])
{
  if(argc != 4)
  {
    std::cerr << "Args: [filename in], [filename out], [remove data near joint limits (0) or remove everything else (1)]" << std::endl;
    return 0;
  }

  std::string filename_in(argv[1]);
  std::string filename_out(argv[2]);
  bool remove_joint_limit_data = !atoi(argv[3]);
  system_id::TrainingData data(filename_in);

  std::vector<std::string> input_labels = data.getInputLabels();
  int position_x_idx = std::find(input_labels.begin(), input_labels.end(), "px") - input_labels.begin();
  int position_y_idx = std::find(input_labels.begin(), input_labels.end(), "py") - input_labels.begin();

  if(position_x_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'px'!";
    return 0;
  }
  if(position_y_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'py'!";
    return 0;
  }

  long double max_x_position = -std::numeric_limits<long double>::max();
  long double min_x_position = std::numeric_limits<long double>::max();
  long double max_y_position = -std::numeric_limits<long double>::max();
  long double min_y_position = std::numeric_limits<long double>::max();
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    if(input_set[position_x_idx] > max_x_position)
    {
      max_x_position = input_set[position_x_idx];
    }
    if(input_set[position_x_idx] < min_x_position)
    {
      min_x_position = input_set[position_x_idx];
    }
    if(input_set[position_y_idx] > max_y_position)
    {
      max_y_position = input_set[position_y_idx];
    }
    if(input_set[position_y_idx] < min_y_position)
    {
      min_y_position = input_set[position_y_idx];
    }
  }

  long double x_threshold = (max_x_position - min_x_position) * 0.01; //remove points within 1% of a joint limit
  long double y_threshold = (max_y_position - min_y_position) * 0.01; //remove points within 1% of a joint limit

  if(remove_joint_limit_data)
  {
    std::cerr << "Removing points within 1% (" << x_threshold << " rad) of the limits" << std::endl;
  }
  else
  {
    std::cerr << "Keeping only points within 1% (" << x_threshold << " rad) of the limits" << std::endl;
  }

  std::vector<std::vector<long double> > input_sets, output_sets;
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    std::vector<long double> output_set = data.getOutputSet(i);

    long double x_position = input_set[position_x_idx];
    long double y_position = input_set[position_y_idx];
    long double max_x_limit_distance = max_x_position - x_position;
    long double min_x_limit_distance = x_position - min_x_position;
    long double max_y_limit_distance = max_y_position - y_position;
    long double min_y_limit_distance = y_position - min_y_position;

    if(remove_joint_limit_data)
    {
      if((max_x_limit_distance > x_threshold) && (min_x_limit_distance > x_threshold) && (max_y_limit_distance > y_threshold) && (min_y_limit_distance > y_threshold)) //we're more than threshold away from either limit
      {
        input_sets.push_back(input_set);
        output_sets.push_back(output_set);
      }
    }
    else
    {
      if(!((max_x_limit_distance > x_threshold) && (min_x_limit_distance > x_threshold) && (max_y_limit_distance > y_threshold) && (min_y_limit_distance > y_threshold)))
      {
        input_sets.push_back(input_set);
        output_sets.push_back(output_set);
      }
    }
  }

  system_id::TrainingData::writeDataFile(filename_out, input_sets, data.getInputLabels(), output_sets, data.getOutputLabels());
}
