#include "system_id/training_data.h"
#include "system_id/common_math.h"
#include <cmath>
#include <sys/stat.h>

void generateLine()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = 0.0; i <= 1.0; i += 0.001)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(10.0 * i + 3.14);

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/line", input_sets, input_symbols, output_sets, output_symbols);
}

void generateSquare()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = 0.0; i <= 2.0; i += 0.002)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(12.3 * (i + 1.23) * (i + 1.23) + 0.117 * i + 3.45);

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/square", input_sets, input_symbols, output_sets, output_symbols);
}

void generateCube()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = 0.0; i <= 3.0; i += 0.003)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(-4.2 * (i - 0.149) * (i - 0.149) * (i - 0.149) + 12.3 * (i + 1.23) * (i + 1.23) + 7.9);

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/cube", input_sets, input_symbols, output_sets, output_symbols);
}

void generateSin1()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = 0.0; i <= 6.0; i += 0.006)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(sin(i));

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/sin_1", input_sets, input_symbols, output_sets, output_symbols);
}

void generateSin2()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = 0.0; i <= 6.0; i += 0.006)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(sin(10.0 * i));

    input_sets.push_back(in);
    output_sets.push_back(out);
  }
  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");
  system_id::TrainingData::writeDataFile("test_data/sin_2", input_sets, input_symbols, output_sets, output_symbols);
}

void generateSin3()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = 0.0; i <= 6.0; i += 0.006)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(30 * sin(1.5 * (i + 3.14)) + 10);

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/sin_3", input_sets, input_symbols, output_sets, output_symbols);
}

void generateTanh1()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = -2.5; i <= 2.5; i += 0.005)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(tanh(i));

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/tanh1", input_sets, input_symbols, output_sets, output_symbols);
}

void generateTanh2()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = -1.0; i <= 1.0; i += 0.002)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(tanh(3.14 * i));

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/tanh2", input_sets, input_symbols, output_sets, output_symbols);
}

void generateTanh3()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = -1.5; i <= 1.5; i += 0.003)
  {
    std::vector<long double> in, out;

    in.push_back(i);
    out.push_back(30 * tanh(-1.5 * (i + 0.234)) + 10);

    input_sets.push_back(in);
    output_sets.push_back(out);
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  output_symbols.push_back("y");

  system_id::TrainingData::writeDataFile("test_data/tanh3", input_sets, input_symbols, output_sets, output_symbols);
}

void generateMulti1()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = -1.0; i <= 1.0; i += 0.04)
  {
    for(long double j = -1.0; j <= 1.0; j += 0.04)
    {
      std::vector<long double> in, out;
      long double x = i + 0.02 * system_id::uniformRandomCentered();
      long double y = j + 0.02 * system_id::uniformRandomCentered();

      in.push_back(x);
      in.push_back(y);
      out.push_back(10.57 * x * x + 1.9 * y * y);

      input_sets.push_back(in);
      output_sets.push_back(out);
    }
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  input_symbols.push_back("y");
  output_symbols.push_back("z");

  system_id::TrainingData::writeDataFile("test_data/multi1", input_sets, input_symbols, output_sets, output_symbols);
}

void generateMulti2()
{
  std::vector<std::vector<long double> > input_sets, output_sets;
  for(long double i = -1.0; i <= 1.0; i += 0.04)
  {
    for(long double j = -1.0; j <= 1.0; j += 0.04)
    {
      std::vector<long double> in, out;
      long double x = i + 0.02 * system_id::uniformRandomCentered();
      long double y = j + 0.02 * system_id::uniformRandomCentered();

      in.push_back(x);
      in.push_back(y);
      out.push_back(10.57 * x * x + 6.6 * x * y + 1.9 * y * y + 10.0 * tanh(10.0 * y + 3.0 * x + 7.0));

      input_sets.push_back(in);
      output_sets.push_back(out);
    }
  }

  std::vector<std::string> input_symbols, output_symbols;
  input_symbols.push_back("x");
  input_symbols.push_back("y");
  output_symbols.push_back("z");

  system_id::TrainingData::writeDataFile("test_data/multi2", input_sets, input_symbols, output_sets, output_symbols);
}

int main()
{
  mkdir("test_data", 0777);
  generateLine();
  generateSquare();
  generateCube();

  generateSin1();
  generateSin2();
  generateSin3();

  generateTanh1();
  generateTanh2();
  generateTanh3();

  generateMulti1();
  generateMulti2();
}
