#include "system_id/genetic.h"

using namespace system_id;

ArrayGenome* setpoint;

void fitness_function(ArrayGenome& genome)
{
  long double sse = 0;
  for(int i = 0; i < genome.gene.first.size(); i++)
  {
    long double err = (long double) (setpoint->gene.first[i] - genome.gene.first[i]);
    sse += err * err;
  }
  for(int i = 0; i < genome.gene.second.size(); i++)
  {
    long double err = setpoint->gene.second[i] - genome.gene.second[i];
    sse += err * err;
  }
  genome.fitness = sqrt(sse);
//  std::cerr << genome << std::endl;
}

int main()
{
  ArrayGenome::GeneType max, min;
  for(unsigned int i = 0; i < 5; i++)
  {
    min.first.push_back(-20);
    max.first.push_back(20);
  }
  for(unsigned int i = 0; i < 7; i++)
  {
    min.second.push_back(-10.0);
    max.second.push_back(10.0);
  }
  setpoint = new ArrayGenome(min, max);
  setpoint->hypermutate();

  GeneticAlgorithmNumeric ga(boost::bind(&fitness_function, _1), min, max);

  //solve for it
  system_id::ArrayGenome best = ga.optimize(GAParams(20, 3, 0.001, 100000, 10));
  best = ga.optimize(GAParams(20, 3, 0.001, 100000, 10), &best.gene);

  std::cerr << "Wanted:         " << *setpoint << std::endl;
  std::cerr << "Final solution: " << best << std::endl;
}
