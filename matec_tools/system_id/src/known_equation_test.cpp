#include "system_id/coefficient_optimizer.h"
#include <cmath>
#include <set>
#include "system_id/common_math.h"

#include "system_id/training_data.h"
#include "system_id/expression_genome.h"

using namespace system_id;

int main(int argc, char* argv[])
{
  if(argc != 2)
  {
    std::cerr << "Args: filename" << std::endl;
    return 0;
  }

  std::string filename(argv[1]);
  system_id::TrainingData data(filename);

  std::vector<std::string> input_symbols = data.getInputLabels();
  EGParams eg_params(input_symbols, 0, 500);

  std::string expression_string = "if(enc>C16) {(C0*(C1*(enc - C16))^(C12) + C2) + (C3*(C4*(enc - C16))^(C13) + C5)} else {(C6*(C7*(enc - C16))^(C14) + C8) + (C9*(C10*(enc - C16))^(C15) + C11)}";
  // std::string expression_string = "if(enc>=0.02) {C0*log(C1*(enc-0.02 + C6)) + C2} else {C3*log(C4*(enc-0.02 + C7)) + C5}";

  system_id::CoefficientOptimizer opt(&data);
  std::vector<std::string> coefficient_symbols;
  coefficient_symbols.push_back("C0");
  coefficient_symbols.push_back("C1");
  coefficient_symbols.push_back("C2");
  coefficient_symbols.push_back("C3");
  coefficient_symbols.push_back("C4");
  coefficient_symbols.push_back("C5");
  coefficient_symbols.push_back("C6");
  coefficient_symbols.push_back("C7");
  coefficient_symbols.push_back("C8");
  coefficient_symbols.push_back("C9");
  coefficient_symbols.push_back("C10");
  coefficient_symbols.push_back("C11");
  coefficient_symbols.push_back("C12");
  coefficient_symbols.push_back("C13");
  coefficient_symbols.push_back("C14");
  coefficient_symbols.push_back("C15");
  coefficient_symbols.push_back("C16");

  ArrayGenome::GeneType min, max, initial_guess;
  for(unsigned int i = 0; i < input_symbols.size(); i++)
  {
    initial_guess.first.push_back(0);
    min.first.push_back(0);
    max.first.push_back(0);
  }

  double nominal_values[] = {0.0624109, 5.80979, -0.225107, 0.362989, 2.56002, 0.253403, 1.55352, -0.335657, 1.06715, -0.585293, -4.51419, -1.01568, 0.911178, 0.53955, 0.743592, 0.668608, 0.0200619};
  for(unsigned int i = 0; i < coefficient_symbols.size(); i++)
  {
    initial_guess.second.push_back(nominal_values[i]);
    min.second.push_back(nominal_values[i] * 0.5);
    max.second.push_back(nominal_values[i] * 1.5);
  }

  // min.second.push_back(-5); //C0
  // max.second.push_back(5);

  // min.second.push_back(-5); //C1
  // max.second.push_back(5);

  // min.second.push_back(-5); //C2
  // max.second.push_back(5);

  // min.second.push_back(-5); //C3
  // max.second.push_back(5);

  // min.second.push_back(-5); //C4
  // max.second.push_back(5);

  // min.second.push_back(-5); //C5
  // max.second.push_back(5);

  // min.second.push_back(-5); //C6
  // max.second.push_back(5);

  // min.second.push_back(-5); //C7
  // max.second.push_back(5);

  // min.second.push_back(-5); //C8
  // max.second.push_back(5);

  // min.second.push_back(-5); //C9
  // max.second.push_back(5);

  // min.second.push_back(-5); //C10
  // max.second.push_back(5);

  // min.second.push_back(-5); //C11
  // max.second.push_back(5);

  // min.second.push_back(0.01); //C12
  // max.second.push_back(1.0);

  // min.second.push_back(0.01); //C13
  // max.second.push_back(1.0);

  // min.second.push_back(0.01); //C14
  // max.second.push_back(1.0);

  // min.second.push_back(0.01); //C15
  // max.second.push_back(1.0);

  // min.second.push_back(-0.1); //C16
  // max.second.push_back(0.1);

  GAParams params(50, 3, 0.000001, 100000, 200, 10 * data.size());
  system_id::ArrayGenome best = opt.optimize(expression_string, coefficient_symbols, input_symbols, params, &min, &max, &initial_guess);
  std::cerr << best << std::endl;

  std::cerr << "\n\n Final equation: " << expression_string << ", " << best << std::endl;

  while(1)
  {

  }
}
