#ifndef GENOMES_H
#define GENOMES_H

#include <ostream>
#include <deque>
#include <boost/function.hpp>
#include <math.h>
#include <map>
#include <string>
#include <sstream>

#include <numeric>
#include <vector>
#include <deque>
#include <cmath>

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "system_id/common_math.h"

namespace system_id
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //genome contains a 1-D array of longs and a 1-D array of long doubles
  class ArrayGenome
  {
  public:
    bool operator<(const ArrayGenome &rhs) const
    {
      return fitness < rhs.fitness;
    }
    long double fitness;

    typedef std::pair<std::vector<long>, std::vector<long double> > GeneType;

    static GeneType defaultMin(unsigned long int_size, unsigned long double_size, long int_oom = std::numeric_limits<long>::max(), long double double_oom = std::numeric_limits<long double>::max())
    {
      GeneType min;

      min.first.resize(int_size, -int_oom);
      min.second.resize(double_size, -double_oom);

      return min;
    }

    static GeneType defaultMax(unsigned long int_size, unsigned long double_size, long int_oom = std::numeric_limits<long>::max(), long double double_oom = std::numeric_limits<long double>::max())
    {
      GeneType max;

      max.first.resize(int_size, int_oom);
      max.second.resize(double_size, double_oom);

      return max;
    }

    ArrayGenome(GeneType min, GeneType max, GeneType* guess = NULL)
    {
      m_absolute_min_values = min;
      m_absolute_max_values = max;

      if(guess)
      {
        gene = *guess;
      }
      else
      {
        gene.first.resize(m_absolute_max_values.first.size());
        gene.second.resize(m_absolute_max_values.second.size());
        hypermutate();
      }

      fitness = std::numeric_limits<long double>::max();
    }

    double clamp(double a, double b, double c)
    {
      return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
    }

    void hypermutate()
    {
      //mutate integer part
      for(unsigned int i = 0; i < gene.first.size(); i++)
      {
        gene.first[i] = m_absolute_min_values.first[i] + (long) ((m_absolute_max_values.first[i] - m_absolute_min_values.first[i]) * system_id::uniformRandom());
      }
      //mutate double part
      for(unsigned int i = 0; i < gene.second.size(); i++)
      {
        gene.second[i] = m_absolute_min_values.second[i] + (m_absolute_max_values.second[i] - m_absolute_min_values.second[i]) * system_id::uniformRandom();
      }
    }

    void mutate(long double mutation_magnitude, long avg_num_int_mutations, long avg_num_double_mutations)
    {
      //mutate integer part
      for(unsigned int i = 0; i < gene.first.size(); i++)
      {
        long double mutation_chance = ((long double) avg_num_int_mutations) / ((long double) gene.first.size());
        if(system_id::uniformRandom() <= mutation_chance)
        {
          double mutation = mutation_magnitude * 0.5 * (m_absolute_max_values.first[i] - m_absolute_min_values.first[i]) * system_id::uniformRandomCentered();
          long int_mutation = std::ceil(fabs(mutation)) * (mutation < 0? -1 : 1);
          gene.first[i] = clamp(gene.first[i] + mutation, m_absolute_min_values.first[i], m_absolute_max_values.first[i]);
        }
      }
      //mutate double part
      for(unsigned int i = 0; i < gene.second.size(); i++)
      {
        long double mutation_chance = ((long double) avg_num_double_mutations) / ((long double) gene.second.size());
        if(system_id::uniformRandom() <= mutation_chance)
        {
          long double mutation = mutation_magnitude * 0.5 * (m_absolute_max_values.second[i] - m_absolute_min_values.second[i]) * system_id::uniformRandomCentered();
          gene.second[i] = clamp(gene.second[i] + mutation, m_absolute_min_values.second[i], m_absolute_max_values.second[i]);
        }
      }
    }

    ArrayGenome cross(ArrayGenome lesser_parent)
    {
      ArrayGenome child = *this;
      child.fitness = std::numeric_limits<long double>::max();

      for(unsigned int i = 0; i < gene.first.size(); i++)
      {
        if(system_id::uniformRandom() < 0.5)
        {
          child.gene.first[i] = lesser_parent.gene.first[i];
        }
      }

      for(unsigned int i = 0; i < gene.second.size(); i++)
      {
        if(system_id::uniformRandom() < 0.5)
        {
          child.gene.second[i] = lesser_parent.gene.second[i];
        }
      }

      return child;
    }

    void forceEvolution(ArrayGenome::GeneType evolution_force, long double region_scale, long double forced_evolution_probability = 0.5)
    {
      long double magnitude = region_scale * (2.0 * system_id::uniformRandom() - 1.0); // -1 to 1
      for(unsigned int i = 0; i < gene.first.size(); i++)
      {
//        gene.first[i] = clamp(gene.first[i] + evolution_force.first[i], m_absolute_min_values.first[i], m_absolute_max_values.first[i]);
      }
      for(unsigned int i = 0; i < gene.second.size(); i++)
      {
        if(forced_evolution_probability < system_id::uniformRandom())
        {
          gene.second[i] = clamp(gene.second[i] + magnitude * evolution_force.second[i], m_absolute_min_values.second[i], m_absolute_max_values.second[i]);
        }
      }
    }

    void print()
    {
      std::cerr << *this << std::endl;
    }

    friend std::ostream &operator<<(std::ostream &cout, ArrayGenome &obj)
    {
      std::cout << "(";
      for(unsigned int i = 0; i < obj.gene.first.size(); i++)
      {
        if(i != 0)
        {
          std::cout << ", ";
        }
        std::cout << (obj.gene.first[i]);
      }
      std::cout << " | ";
      for(unsigned int i = 0; i < obj.gene.second.size(); i++)
      {
        if(i != 0)
        {
          std::cout << ", ";
        }
        std::cout << (obj.gene.second[i]);
      }
      std::cout << ")";

//      if(obj.fitness < std::numeric_limits<long double>::max())
//      {
      std::cout << (" => ") << (obj.fitness);
//      }

      return cout;
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & gene;
      ar & fitness;
      ar & m_absolute_min_values;
      ar & m_absolute_max_values;
    }

    GeneType gene;
    GeneType m_absolute_min_values;
    GeneType m_absolute_max_values;
  };
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  class ExpressionNode
//  {
//  public:
//    enum NodeSlot
//    {
//      ADD_SLOT,
//      MUL_SLOT,
//      ARG_SLOT,
//      POW_SLOT
//    };
//
//    ExpressionNode()
//    {
//      m_valid = false;
//    }
//
//    ExpressionNode(std::string function_string, ExpressionNode add = ExpressionNode(), ExpressionNode mult = ExpressionNode(), ExpressionNode arg = ExpressionNode(), ExpressionNode pow = ExpressionNode())
//    {
//      if(function_string.length() == 0)
//      {
//        m_valid = false;
//        return;
//      }
//
//      m_nodes.resize(4);
//      m_nodes[ADD_SLOT] = add;
//      m_nodes[MUL_SLOT] = mult;
//      m_nodes[ARG_SLOT] = arg;
//      m_nodes[POW_SLOT] = pow;
//      m_function_string = function_string;
//
//      m_valid = true;
//    }
//
//    std::string getExpressionString()
//    {
//      assert(m_valid);
//
//      if(!m_nodes[ADD_SLOT].isValid() && !m_nodes[MUL_SLOT].isValid() && !m_nodes[ARG_SLOT].isValid() && !m_nodes[POW_SLOT].isValid())
//      {
//        return m_function_string;
//      }
//
//      std::string expression = "("; //  (add+mult*f(arg))^(pow)
//
//      if(m_nodes[ADD_SLOT].isValid())
//      {
//        expression += m_nodes[ADD_SLOT].getExpressionString() + "+";
//      }
//
//      if(m_nodes[MUL_SLOT].isValid())
//      {
//        expression += m_nodes[MUL_SLOT].getExpressionString() + "*";
//      }
//
//      if(m_nodes[ARG_SLOT].isValid())
//      {
//        expression += m_function_string + "(" + m_nodes[ARG_SLOT].getExpressionString() + ")";
//      }
//      else
//      {
//        expression += m_function_string;
//      }
//
//      expression += ")";
//
//      if(m_nodes[POW_SLOT].isValid())
//      {
//        expression += "^(" + m_nodes[POW_SLOT].getExpressionString() + ")";
//      }
//
//      return expression;
//    }
//
//    void getSymbols(std::vector<std::string>& symbols)
//    {
//      if(isValid())
//      {
//        if(isSymbol())
//        {
//          symbols.push_back(m_function_string);
//        }
//        if(m_nodes[ADD_SLOT].isValid())
//        {
//          m_nodes[ADD_SLOT].getSymbols(symbols);
//        }
//        if(m_nodes[MUL_SLOT].isValid())
//        {
//          m_nodes[MUL_SLOT].getSymbols(symbols);
//        }
//        if(m_nodes[ARG_SLOT].isValid())
//        {
//          m_nodes[ARG_SLOT].getSymbols(symbols);
//        }
//        if(m_nodes[POW_SLOT].isValid())
//        {
//          m_nodes[POW_SLOT].getSymbols(symbols);
//        }
//      }
//    }
//
//    unsigned long nodeCount()
//    {
//      if(!isValid())
//      {
//        return 0;
//      }
//
//      if(isSymbol())
//      {
//        return 1;
//      }
//
//      unsigned long sum = 0;
//      for(unsigned int i = 0; i < m_nodes.size(); i++)
//      {
//        sum += m_nodes[i].nodeCount();
//      }
//
//      return sum;
//    }
//
//    void setRandomNode(double stop_probability, std::vector<double> node_probabilities, ExpressionNode new_node, bool ignore_root_node = false)
//    {
//      ExpressionNode* random_node = getRandomNode(stop_probability, node_probabilities, ignore_root_node);
//      *random_node = new_node;
//    }
//
//    ExpressionNode* getRandomNode(double stop_probability, std::vector<double> node_probabilities, bool ignore_root_node = false) //node probabilities is always size 4
//    {
//      assert(node_probabilities.size() == 4);
//      double stop_roll = system_id::uniformRandom();
//      if((stop_roll < stop_probability) && !ignore_root_node)
//      {
//        return this;
//      }
//      double node_roll = system_id::uniformRandom();
//
//      double sum = 0.0;
//      std::vector<double> my_probabilities;
//      for(unsigned int i = 0; i < m_nodes.size(); i++)
//      {
//        if(m_nodes[i].isValid())
//        {
//          my_probabilities.push_back(node_probabilities[i]);
//          sum += node_probabilities[i];
//        }
//        else
//        {
//          my_probabilities.push_back(0.0);
//        }
//      }
//
//      if(sum == 0.0)
//      {
//        return this;
//      }
//
//      double accumulated = 0.0;
//      for(unsigned int i = 0; i < m_nodes.size(); i++)
//      {
//        accumulated += (my_probabilities[i] / sum);
//        if(node_roll < accumulated && m_nodes[i].isValid()) //todo make replacement of symbols a param?
//        {
//          return m_nodes[i].getRandomNode(stop_probability, node_probabilities);
//        }
//      }
//
//      std::cerr << "WARNING: random node selection is broken!";
//      return this;
//    }
//
//    //(last_allocated + 1) is num_coefficients
//    void makeCoefficientsUnique(std::vector<std::string> input_symbols, unsigned long& last_allocated)
//    {
//      for(unsigned int i = 0; i < m_nodes.size(); i++)
//      {
//        if(m_nodes[i].isValid())
//        {
//          m_nodes[i].makeCoefficientsUnique(input_symbols, last_allocated);
//        }
//      }
//
//      if(isSymbol())
//      {
//        bool is_a_coefficient = (std::find(input_symbols.begin(), input_symbols.end(), m_function_string) == input_symbols.end());
//        if(is_a_coefficient) //the function is a symbol that is not an input, so it must be a coefficient
//        {
//          std::stringstream ss;
//          ss << "c" << last_allocated;
//          m_function_string = ss.str();
//          last_allocated++;
//        }
//      }
//    }
//
//    bool isSymbol() //no arg ==> is symbol
//    {
//      assert(m_valid);
//      return !m_nodes[ARG_SLOT].isValid();
//    }
//
//    bool isValid()
//    {
//      return m_valid;
//    }
//
//    friend class boost::serialization::access;
//    template<class Archive>
//    void serialize(Archive & ar, const unsigned int version)
//    {
//      ar & m_function_string;
//      ar & m_nodes;
//      ar & m_valid;
//    }
//
//  private:
//    std::string m_function_string;
//    std::vector<ExpressionNode> m_nodes;
//
//    bool m_valid;
//  };
//  typedef ExpressionNode EN;
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  class ExpressionGenome
//  {
//  public:
//    typedef std::pair<ExpressionNode, ArrayGenomeMixed> GeneType;
//
//    ExpressionGenome()
//    {
//      valid = true;
//      m_params = params;
//      m_max_symbols = m_params->getParam<unsigned long>("max_symbols", (unsigned long) 20);
//
//      assert(m_params->hasParam("input_symbols"));
//      m_input_symbols = m_params->getParam < std::vector<std::string> > ("input_symbols", std::vector<std::string>());
//      assert(m_input_symbols.size() != 0);
//
//      fitness = std::numeric_limits<long double>::max();
//
//      m_node_probabilities.resize(4);
//      m_node_probabilities[EN::ADD_SLOT] = 1.0;
//      m_node_probabilities[EN::MUL_SLOT] = 0.5;
//      m_node_probabilities[EN::ARG_SLOT] = 0.25;
//      m_node_probabilities[EN::POW_SLOT] = 0.05;
//
//      assert(!gene.second.valid);
//      hypermutate();
//      assert(gene.second.valid);
//      gene.second.hypermutate();
//      std::cerr << gene.second;
//    }
//
//    ~ExpressionGenome()
//    {
//    }
//
//    bool getRandomFunction(std::string& function_string, double function_selection_probability = 0.25)
//    {
//      static unsigned int num_functions = 4;
//      static std::string function_library[] = {"tanh", "sin", "log", "exp"};
//
//      double type_roll = system_id::uniformRandom();
//      if(type_roll < function_selection_probability) //make a function
//      {
//        double function_roll = system_id::uniformRandom();
//        unsigned int function_idx = function_roll * (num_functions - 0.0001); //0.0001 to prevent bad indices
//        function_string = function_library[function_idx];
//
//        return true;
//      }
//      else //make an input
//      {
//        double input_roll = system_id::uniformRandom();
//        unsigned int input_idx = input_roll * (m_input_symbols.size() - 0.0001); //0.0001 to prevent bad indices
//
//        assert(m_input_symbols.size() != 0);
//        assert(m_input_symbols.size() > input_idx);
//        function_string = m_input_symbols[input_idx];
//
//        return false;
//      }
//    }
//
//    EN makeRandomNode(double exponent_probability = 0.1, double prune_probability = 0.0)
//    {
//      if(prune_probability > 0)
//      {
//        double prune_roll = system_id::uniformRandom();
//        if(prune_roll < prune_probability)
//        {
//          return EN();
//        }
//      }
//
//      std::string function_name;
//      bool add_arg = getRandomFunction(function_name);
//
//      double exponent_roll = system_id::uniformRandom();
//      bool add_exp = exponent_roll < exponent_probability;
//
//      EN arg, exp;
//      if(add_arg)
//      {
//        std::string var_name;
//        assert(!getRandomFunction(var_name, 0.0));
//        arg = EN(var_name, EN("_c_"), EN("_c_"));
//      }
//      if(add_exp)
//      {
//        exp = EN("_c_");
//      }
//
//      return EN(function_name, EN("_c_"), EN("_c_"), arg, exp);
//    }
//
//    void forceNonZeroNumCoefficients()
//    {
//      unsigned long num_coeffs = 0;
//      gene.first.makeCoefficientsUnique(m_input_symbols, num_coeffs);
//      while(num_coeffs == 0)
//      {
//        hypermutate();
//        gene.first.makeCoefficientsUnique(m_input_symbols, num_coeffs);
//      }
//    }
//
//    void handleEquationChanges()
//    {
//      unsigned long last_coeff = 0;
//      gene.first.makeCoefficientsUnique(m_input_symbols, last_coeff);
//
//      gene.second.gene.first.resize(m_input_symbols.size()); //make the inner genome match the modified number of inputs
//      gene.second.gene.second.resize(last_coeff); //make the inner genome match the modified number of coefficients
//      if(!gene.second.valid)
//      {
//        std::cerr << "wat";
//        system_id::ParameterMap inner_params;
//        inner_params.setParam<ArrayGenomeMixed::GeneType>("initial_guess", gene.second.gene);
//        //TODO: make min/max?
//        gene.second = ArrayGenomeMixed(&inner_params);
//      }
//      assert(gene.second.gene.first.size() != 0);
//      assert(gene.second.gene.second.size() != 0);
//    }
//
//    void hypermutate()
//    {
//      //choose random primary function
//      gene.first = makeRandomNode();
//
//      double stop_probability = 0.5;
//      double complicate_probability = 0.75;
//      double complicate_roll = system_id::uniformRandom();
//      //TODO: check for not exceeding max symbols
//      while(complicate_roll < complicate_probability)
//      {
////        std::cerr << "complicating!" << std::endl;
//        gene.first.setRandomNode(stop_probability, m_node_probabilities, makeRandomNode(), true);
//        complicate_roll = system_id::uniformRandom();
//      }
//
//      if(!gene.first.isValid())
//      {
//        PRINTLN_ERROR("Hypermutated function was not valid!");
//        gene.first = makeRandomNode();
//      }
//
//      handleEquationChanges();
//    }
//
//    void mutate(long double mutation_magnitude)
//    {
//      gene.first.setRandomNode(0.5, m_node_probabilities, makeRandomNode(0.1, 0.25), true);
//
//      if(!gene.first.isValid())
//      {
//        PRINTLN_ERROR("Mutated function was not valid!");
//        gene.first = makeRandomNode();
//      }
//
//      forceNonZeroNumCoefficients();
//      handleEquationChanges();
//    }
//
//    Genome* cross(Genome* lesser_parent_ptr)
//    {
//      ExpressionGenome* lesser_parent = static_cast<ExpressionGenome*>(lesser_parent_ptr);
//      ExpressionGenome* child = new ExpressionGenome(*this);
//      child->fitness = std::numeric_limits<long double>::max();
//
//      unsigned int num_swaps = 2;
//      for(unsigned int i = 0; i < num_swaps; i++)
//      {
//        ExpressionNode* lesser_parent_node = lesser_parent->gene.first.getRandomNode(0.5, m_node_probabilities);
//        child->gene.first.setRandomNode(0.5, m_node_probabilities, *lesser_parent_node, true);
//      }
//
//      if(!child->gene.first.isValid())
//      {
//        PRINTLN_ERROR("child was not valid!");
//        child->gene.first = makeRandomNode();
//      }
//
//      child->forceNonZeroNumCoefficients();
//      child->handleEquationChanges();
//
//      return static_cast<Genome*>(child);
//    }
//
//    void print()
//    {
//      std::cerr << *this << std::endl;
//    }
//
//    friend std::ostream &operator<<(std::ostream &cout, ExpressionGenome &obj)
//    {
//      std::cout << "f(";
//      for(unsigned int i = 0; i < obj.m_input_symbols.size(); i++)
//      {
//        if(i != 0)
//        {
//          std::cout << ",";
//        }
//        std::cout << obj.m_input_symbols[i];
//      }
//      std::cout << ") = " << obj.gene.first.getExpressionString() << ", " << obj.gene.second;
//
////      if(obj.fitness < std::numeric_limits<long double>::max())
////      {
////        std::cout << (" => ") << (obj.fitness);
////      }
//
//      return cout;
//    }
//
//    friend class boost::serialization::access;
//    template<class Archive>
//    void serialize(Archive & ar, const unsigned int version)
//    {
//      ar & gene;
//      ar & m_max_symbols;
//      ar & m_input_symbols;
//      ar & m_node_probabilities;
//      ar & fitness;
//    }
//
//    GeneType gene;
//
//  protected:
//    unsigned long m_max_symbols;
//    std::vector<std::string> m_input_symbols;
//    std::vector<double> m_node_probabilities;
//  };
}

#endif //GENOMES_H
