#ifndef EXPRESSION_GENOME_H
#define EXPRESSION_GENOME_H

#include <ostream>
#include <deque>
#include <boost/function.hpp>
#include <math.h>
#include <map>
#include <string>
#include <sstream>

#include <numeric>
#include <vector>
#include <deque>
#include <cmath>

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "system_id/common_math.h"

#include "system_id/genomes.h"
#include "system_id/debug_levels.h"

namespace system_id
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class ExpressionNode
  {
  public:
    enum NodeSlot
    {
      ADD_SLOT,
      MUL_SLOT,
      ARG_SLOT,
      POW_SLOT
    };

    ExpressionNode()
    {
      m_valid = false;
    }

    ExpressionNode(std::string function_string, ExpressionNode add = ExpressionNode(), ExpressionNode mult = ExpressionNode(), ExpressionNode arg = ExpressionNode(), ExpressionNode pow = ExpressionNode())
    {
      if(function_string.length() == 0)
      {
        m_valid = false;
        return;
      }

      m_nodes.resize(4);
      m_nodes[ADD_SLOT] = add;
      m_nodes[MUL_SLOT] = mult;
      m_nodes[ARG_SLOT] = arg;
      m_nodes[POW_SLOT] = pow;
      m_function_string = function_string;

      m_valid = true;
    }

    std::string getExpressionString()
    {
      assert(m_valid);

      if(!m_nodes[ADD_SLOT].isValid() && !m_nodes[MUL_SLOT].isValid() && !m_nodes[ARG_SLOT].isValid() && !m_nodes[POW_SLOT].isValid())
      {
        return m_function_string;
      }

      std::string expression = "("; //  (add+mult*f(arg))^(pow)

      if(m_nodes[ADD_SLOT].isValid())
      {
        expression += m_nodes[ADD_SLOT].getExpressionString() + "+";
      }

      if(m_nodes[MUL_SLOT].isValid())
      {
        expression += m_nodes[MUL_SLOT].getExpressionString() + "*";
      }

      if(m_nodes[ARG_SLOT].isValid())
      {
        expression += m_function_string + "(" + m_nodes[ARG_SLOT].getExpressionString() + ")";
      }
      else
      {
        expression += m_function_string;
      }

      expression += ")";

      if(m_nodes[POW_SLOT].isValid())
      {
        expression += "^(" + m_nodes[POW_SLOT].getExpressionString() + ")";
      }

      return expression;
    }

    void getSymbols(std::vector<std::string>& symbols)
    {
      if(isValid())
      {
        if(isSymbol())
        {
          symbols.push_back(m_function_string);
        }
        if(m_nodes[ADD_SLOT].isValid())
        {
          m_nodes[ADD_SLOT].getSymbols(symbols);
        }
        if(m_nodes[MUL_SLOT].isValid())
        {
          m_nodes[MUL_SLOT].getSymbols(symbols);
        }
        if(m_nodes[ARG_SLOT].isValid())
        {
          m_nodes[ARG_SLOT].getSymbols(symbols);
        }
        if(m_nodes[POW_SLOT].isValid())
        {
          m_nodes[POW_SLOT].getSymbols(symbols);
        }
      }
    }

    unsigned long nodeCount()
    {
      if(!isValid())
      {
        return 0;
      }

      if(isSymbol())
      {
        return 1;
      }

      unsigned long sum = 0;
      for(unsigned int i = 0; i < m_nodes.size(); i++)
      {
        sum += m_nodes[i].nodeCount();
      }

      return sum;
    }

    void setRandomNode(double stop_probability, std::vector<double> node_probabilities, ExpressionNode new_node, bool ignore_root_node = false)
    {
      ExpressionNode* random_node = getRandomNode(stop_probability, node_probabilities, ignore_root_node);
      *random_node = new_node;
    }

    ExpressionNode* getRandomNode(double stop_probability, std::vector<double> node_probabilities, bool ignore_root_node = false) //node probabilities is always size 4
    {
      assert(node_probabilities.size() == 4);
      double stop_roll = system_id::uniformRandom();
      if((stop_roll < stop_probability) && !ignore_root_node)
      {
        return this;
      }
      double node_roll = system_id::uniformRandom();

      double sum = 0.0;
      std::vector<double> my_probabilities;
      for(unsigned int i = 0; i < m_nodes.size(); i++)
      {
        if(m_nodes[i].isValid())
        {
          my_probabilities.push_back(node_probabilities[i]);
          sum += node_probabilities[i];
        }
        else
        {
          my_probabilities.push_back(0.0);
        }
      }

      if(sum == 0.0)
      {
        return this;
      }

      double accumulated = 0.0;
      for(unsigned int i = 0; i < m_nodes.size(); i++)
      {
        accumulated += (my_probabilities[i] / sum);
        if(node_roll < accumulated && m_nodes[i].isValid()) //todo make replacement of symbols a param?
        {
          return m_nodes[i].getRandomNode(stop_probability, node_probabilities);
        }
      }

      std::cerr << "WARNING: random node selection is broken!";
      return this;
    }

    unsigned long makeCoefficientsUnique(std::vector<std::string> input_symbols, std::vector<std::string>& coefficient_symbols, bool first = false)
    {
      if(first)
      {
        coefficient_symbols.clear();
      }
      for(unsigned int i = 0; i < m_nodes.size(); i++)
      {
        if(m_nodes[i].isValid())
        {
          m_nodes[i].makeCoefficientsUnique(input_symbols, coefficient_symbols);
        }
      }

      if(isSymbol())
      {
        bool is_a_coefficient = (std::find(input_symbols.begin(), input_symbols.end(), m_function_string) == input_symbols.end());
        if(is_a_coefficient) //the function is a symbol that is not an input, so it must be a coefficient
        {
          std::stringstream ss;
          ss << "c" << coefficient_symbols.size();
          m_function_string = ss.str();
          coefficient_symbols.push_back(m_function_string);
        }
      }

      return input_symbols.size();
    }

    bool isSymbol() //no arg ==> is symbol
    {
      assert(m_valid);
      return !m_nodes[ARG_SLOT].isValid();
    }

    bool isValid()
    {
      return m_valid;
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & m_function_string;
      ar & m_nodes;
      ar & m_valid;
    }

  private:
    std::string m_function_string;
    std::vector<ExpressionNode> m_nodes;

    bool m_valid;
  };
  typedef ExpressionNode EN;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class EGParams
  {
  public:
    EGParams(std::vector<std::string> input_symbols_, long max_shift_, long double max_coeff_value_, std::vector<double> node_probabilities_ = std::vector<double>(), unsigned long max_symbols_ = 100, double stop_probability_ = 0.5, double function_selection_probability_ = 0.25, double complicate_probability_ = 0.5, long num_swaps_ = 2)
    {
      input_symbols = input_symbols_;
      max_shift = max_shift_;
      max_coeff_value = max_coeff_value_;
      max_symbols = max_symbols_;

      if(node_probabilities_.size() == 4)
      {
        node_probabilities = node_probabilities_;
      }
      else
      {
        node_probabilities.resize(4);
        node_probabilities[EN::ADD_SLOT] = 1.0;
        node_probabilities[EN::MUL_SLOT] = 0.5;
        node_probabilities[EN::ARG_SLOT] = 0.25;
        node_probabilities[EN::POW_SLOT] = 0.0; //0.05;
      }

      stop_probability = stop_probability_;
      function_selection_probability = function_selection_probability_;
      complicate_probability = complicate_probability_;
      num_swaps = num_swaps_;

//      std::cerr << "SANITY?" << input_symbols.size() << std::endl;
//      for(unsigned int i = 0; i < input_symbols.size(); i++)
//      {
//        std::cerr << input_symbols[i] << ",";
//      }
//      std::cerr << std::endl;
    }

    std::vector<std::string> input_symbols;
    std::vector<double> node_probabilities;
    long max_shift;
    long double max_coeff_value;
    unsigned long max_symbols;

    double stop_probability;
    double function_selection_probability;
    double complicate_probability;
    long num_swaps;
  };

  class ExpressionGenome
  {
  public:
    typedef std::pair<ExpressionNode, ArrayGenome> GeneType;
    bool operator<(const ExpressionGenome &rhs) const
    {
      return fitness < rhs.fitness;
    }
    long double fitness;

    static bool getRandomFunctionName(EGParams& params, std::string& function_string, double function_selection_probability = 0.25)
    {
//      std::cerr << "getRandomFunctionName: "<< params.input_symbols.size() << std::endl;
      static unsigned int num_functions = 1;
      static std::string function_library[] = {"tanh"};//, "sin", "exp"};

      double type_roll = system_id::uniformRandom();
      if(type_roll < function_selection_probability) //make a function
      {
        double function_roll = system_id::uniformRandom();
        unsigned int function_idx = function_roll * num_functions;
        function_idx = (function_idx == num_functions)? num_functions - 1 : function_idx;
        function_string = function_library[function_idx];

        return true;
      }
      else //make an input
      {
        double input_roll = system_id::uniformRandom();
        unsigned int input_idx = input_roll * params.input_symbols.size();
        input_idx = (input_idx == params.input_symbols.size())? params.input_symbols.size() - 1 : input_idx;

        assert(input_idx < params.input_symbols.size());
        assert(input_idx >= 0);
//        std::cerr << input_roll << "," << input_idx << "," << params.input_symbols.size() << std::endl;
        function_string = params.input_symbols.at(input_idx);

        return false;
      }
    }

    static EN makeRandomNode(EGParams& params, double exponent_probability = 0.0, double prune_probability = 0.0) //exponent used to be 0.1
    {
//      std::cerr << "makeRandomNode: "<< params.input_symbols.size() << std::endl;
      if(prune_probability > 0)
      {
        double prune_roll = system_id::uniformRandom();
        if(prune_roll < prune_probability)
        {
          return EN();
        }
      }

      std::string function_name;
      bool add_arg = getRandomFunctionName(params, function_name);

      double exponent_roll = system_id::uniformRandom();
      bool add_exp = exponent_roll < exponent_probability;

      EN arg, exp;
      if(add_arg)
      {
        std::string var_name;
        getRandomFunctionName(params, var_name, 0.0);
        arg = EN(var_name, EN("_c_"), EN("_c_"));
      }
      if(add_exp)
      {
        exp = EN("_c_");
      }

      return EN(function_name, EN("_c_"), EN("_c_"), arg, exp);
    }

    static void generateRandomExpression(EGParams& params, EN& expression, std::vector<std::string>& coefficient_symbols)
    {
      assert(coefficient_symbols.size() == 0);
//      std::cerr << "generateRandomExpression: "<< params.input_symbols.size() << std::endl;
      expression = makeRandomNode(params);

      double complicate_roll = system_id::uniformRandom();
      while((expression.makeCoefficientsUnique(params.input_symbols, coefficient_symbols, true) + 4) <= params.max_symbols && complicate_roll < params.complicate_probability) //we'll add at most 4 new coefficients each time
      {
        expression.setRandomNode(params.stop_probability, params.node_probabilities, makeRandomNode(params), true);
        complicate_roll = system_id::uniformRandom();
      }
      expression.makeCoefficientsUnique(params.input_symbols, coefficient_symbols, true);
//      PRINTLN_INFO("used up %d/%d coeffs", coefficient_symbols.size(), params.max_symbols);
    }

    static GeneType makeGene(EGParams& params) //, ArrayGenome::GeneType* coeff_guess = NULL, ExpressionNode* expression_guess = NULL)
    {
//      std::cerr << "makeGene: "<< params.input_symbols.size() << std::endl;

      ExpressionNode expression;
//          if(expression_guess)
//          {
//            expression = *expression_guess;
//            expression.makeCoefficientsUnique(m_params.input_symbols, coefficient_symbols);
//          }
//          else
//          {
      std::vector<std::string> coefficient_symbols;
      generateRandomExpression(params, expression, coefficient_symbols);
//          }

      ArrayGenome::GeneType min, max;
//          if(coeff_guess && coeff_guess->first.size() == params.input_symbols.size() && coeff_guess->second.size() == num_coeffs)
//          {
//            long max_long = 0;
//            for(unsigned int i = 0; i < coeff_guess->first.size(); i++)
//            {
//              if(coeff_guess->first[i] > max_long)
//              {
//                max_long = coeff_guess->first[i];
//              }
//            }
//            min.first.resize(max_long * 2);
//            long double max_double = 0.0;
//            for(unsigned int i = 0; i < coeff_guess->second.size(); i++)
//            {
//              if(coeff_guess->second[i] > max_double)
//              {
//                max_double = coeff_guess->second[i];
//              }
//            }
//          }
//          else
//          {
      min = ArrayGenome::defaultMin(params.input_symbols.size(), coefficient_symbols.size(), params.max_shift, params.max_coeff_value);
      max = ArrayGenome::defaultMax(params.input_symbols.size(), coefficient_symbols.size(), params.max_shift, params.max_coeff_value);
      return GeneType(expression, ArrayGenome(min, max));
//          }
    }

    void whyDoIHaveToWriteAFunctionToPrintAVectorOfStrings(std::vector<std::string> strings)
    {
      for(unsigned int i = 0; i < strings.size(); i++)
        std::cerr << strings[i] << ",";
      std::cerr << std::endl;
    }

    void updateCoefficients()
    {
      gene.first.makeCoefficientsUnique(m_params.input_symbols, coefficient_symbols, true);
      whyDoIHaveToWriteAFunctionToPrintAVectorOfStrings(coefficient_symbols);
    }

    ExpressionGenome(EGParams params) :
        m_params(params), gene(makeGene(params))
    {
      PRINTLN_INFO("const");
      updateCoefficients();
      input_symbols = m_params.input_symbols;
      fitness = std::numeric_limits<long double>::max();

      //TODO: if guess provided, don't hypermutate
      hypermutate();
    }

    void forceNonZeroNumCoefficients()
    {
      PRINTLN_INFO("force");
      while(gene.first.makeCoefficientsUnique(m_params.input_symbols, coefficient_symbols, true) == 0)
      {
        hypermutate();
      }
    }

    void hypermutate()
    {
      PRINTLN_INFO("hyper");
      gene = makeGene(m_params);
      updateCoefficients();
    }

    void mutate(long double mutation_magnitude)
    {
      PRINTLN_INFO("mut");
      gene.first.setRandomNode(m_params.stop_probability, m_params.node_probabilities, makeRandomNode(m_params, 0.0, 0.25), true); //exponent used to be 0.1
      updateCoefficients();

      forceNonZeroNumCoefficients();
    }

    ExpressionGenome cross(ExpressionGenome lesser_parent)
    {
      PRINTLN_INFO("cross");
      ExpressionGenome child = *this;
      child.fitness = std::numeric_limits<long double>::max();

      for(unsigned int i = 0; i < m_params.num_swaps; i++)
      {
        ExpressionNode* lesser_parent_node = lesser_parent.gene.first.getRandomNode(m_params.stop_probability, m_params.node_probabilities);
        child.gene.first.setRandomNode(m_params.stop_probability, m_params.node_probabilities, *lesser_parent_node, true);
      }

      gene.first.makeCoefficientsUnique(m_params.input_symbols, coefficient_symbols, true);
      updateCoefficients();

      return child;
    }

    void print()
    {
      std::cerr << *this << std::endl;
    }

    friend std::ostream &operator<<(std::ostream &cout, ExpressionGenome &obj)
    {
      std::cout << "f(";
      for(unsigned int i = 0; i < obj.m_params.input_symbols.size(); i++)
      {
        if(i != 0)
        {
          std::cout << ",";
        }
        std::cout << obj.m_params.input_symbols[i];
      }
      std::cout << ") = " << obj.gene.first.getExpressionString() << ", " << obj.gene.second;

//      if(obj.fitness < std::numeric_limits<long double>::max())
//      {
//        std::cout << (" => ") << (obj.fitness);
//      }

      return cout;
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & gene;
      ar & fitness;
      ar & m_params;
    }

    GeneType gene;
    std::vector<std::string> input_symbols;
    std::vector<std::string> coefficient_symbols;

  protected:
    EGParams m_params;
  };
}

#endif //EXPRESSION_GENOME_H
