#ifndef EXPRESSION_GA_H
#define EXPRESSION_GA_H

#include <stdio.h>
#include "system_id/expression_genome.h"
#include <deque>
#include <map>
#include <boost/bind.hpp>
#include <vector>

#include "system_id/debug_levels.h"
#include "system_id/common_math.h"

namespace system_id
{
  class EGAParams
  {
  public:
    EGAParams(unsigned long regional_population_size_ = 5, unsigned long num_regions_ = 1, long double fit_enough_ = 0.0, unsigned long max_iterations_ = 100, unsigned long stagnation_threshold_ = 10, bool print_ = true)
    {
      regional_population_size = regional_population_size_;
      num_regions = num_regions_;
      fit_enough = fit_enough_;
      max_iterations = max_iterations_;
      stagnation_threshold = stagnation_threshold_;
      print = print_;
    }

    unsigned long regional_population_size;
    unsigned long num_regions;
    long double fit_enough;
    unsigned long max_iterations;
    unsigned long stagnation_threshold;
    bool print;
  };

  class ExpressionGeneticAlgorithm
  {
    typedef boost::function<void(ExpressionGenome&)> FitnessFunction;

  public:
    ExpressionGeneticAlgorithm(FitnessFunction fitness, EGParams eg_params, bool seed_random = true) :
        m_eg_params(eg_params), m_best(eg_params)
    {
      if(seed_random)
      {
        srand(time(NULL));
      }
      m_fitness = fitness;
    }

    ExpressionGenome optimize(EGAParams params) //, ExpressionGenome::GeneType* initial_guess = NULL)
    {
//      if(initial_guess)
//      {
//        m_best = ExpressionGenome(m_eg_params, initial_guess);
//        m_fitness(m_best);
//
//        if(params.print)
//        {
//          PRINTLN_INFO_STREAM("Using initial guess " << m_best);
//        }
//      }

      populate(params.num_regions, params.regional_population_size);

      evaluate(); //make sure we're not done already

      unsigned long iterations = 0;
      unsigned long iterations_since_last_improvement = 0;
      long double old_best = std::numeric_limits<long double>::max();
      while(iterations < params.max_iterations && m_best.fitness > params.fit_enough)
      {
        procreate();
        mutate(false, ((double) iterations) / ((double) params.max_iterations));
        evaluate();

        if(m_best.fitness < old_best)
        {
          old_best = m_best.fitness;
          iterations_since_last_improvement = 0;
          if(params.print)
          {
            PRINTLN_INFO_STREAM("Performed iteration " << iterations << ". Found new best genome: " << m_best);
          }
        }
        else
        {
          if(params.print)
          {
            PRINTLN_INFO_STREAM_THROTTLE(1.0, "Performed iteration " << iterations << ". Current best genome: " << m_best);
          }
          iterations_since_last_improvement++;
          if(iterations_since_last_improvement > params.stagnation_threshold)
          {
            mutate(true, ((double) iterations) / ((double) params.max_iterations));
            evaluate();
            iterations_since_last_improvement = 0;
          }
        }

        iterations++;
      }

      if(params.print)
      {
        PRINTLN_INFO_STREAM("Finished with " << iterations << ". The final genome was:" << m_best);
      }

      return m_best;
    }

  private:
    void populate(int num_regions, int regional_population_size)
    {
      PRINTLN_INFO("pop");
      m_parent_population.clear();
      for(int i = 0; i < num_regions; i++)
      {
        std::deque<ExpressionGenome> region;
        for(int j = 0; j < regional_population_size; j++)
        {
          region.push_back(ExpressionGenome(m_eg_params));
        }
        m_parent_population.push_back(region);
      }
    }

    void evaluate()
    {
      PRINTLN_INFO("eval");
      for(unsigned int i = 0; i < m_parent_population.size(); i++)
      {
        for(unsigned int j = 0; j < m_parent_population[i].size(); j++)
        {
          m_fitness(m_parent_population[i][j]);

          if(m_parent_population[i][j] < m_best)
          {
            m_best = m_parent_population[i][j];
          }
        }
        std::sort(m_parent_population[i].begin(), m_parent_population[i].end());
      }
    }

    void mutate(bool hypermutate, long double iterations_completed)
    {
      PRINTLN_INFO("mutga");
      long double magnitude_base = 1.0 - iterations_completed * iterations_completed;
//      std::cerr << magnitude_base * (m_max.second[0] - m_min.second[0]) << std::endl;
      for(unsigned int i = 0; i < m_parent_population.size(); i++)
      {
        long double mutation_magnitude = magnitude_base * pow(2.0, -1 * (double) i); //higher regions mutate slower than lower regions
        for(unsigned int j = 0; j < m_parent_population[i].size(); j++)
        {
          if(hypermutate)
          {
            m_parent_population[i][j].hypermutate();
          }
          else
          {
            m_parent_population[i][j].mutate(mutation_magnitude);
          }
        }
      }
    }

    int randomBucket(std::vector<long double> thresholds)
    {
      PRINTLN_INFO("rb");
      long double roll = system_id::uniformRandom();
      for(unsigned int i = 0; i < thresholds.size(); i++)
      {
        if(roll < thresholds[i])
        {
          return i;
        }
      }
      assert(0);
      return 0; //THIS SHOULD NEVER HAPPEN!
    }

    std::vector<long double> makeRankThresholds(int num_ranks)
    {
      PRINTLN_INFO("mrt");
      std::vector<long double> rank_thresholds;

      int normalization = num_ranks * (num_ranks + 1) / 2;

      for(int i = 0; i < num_ranks; i++)
      {
        rank_thresholds.push_back(((long double) (num_ranks - i)) / ((long double) normalization));
        if(i > 0)
        {
          rank_thresholds[i] += rank_thresholds[i - 1];
        }
      }
      //todo: check that last rank threshold == 1

      return rank_thresholds;
    }

    void procreate()
    {
      PRINTLN_INFO("pro");
      unsigned int population_size = m_parent_population[0].size();
      std::vector<long double> thresholds = makeRankThresholds(population_size);

      std::deque<std::deque<ExpressionGenome> > child_population; //[region][individual]
      for(unsigned int region = 0; region < m_parent_population.size(); region++)
      {
        std::deque<ExpressionGenome> child_region;
        if(m_parent_population[region][0].fitness > m_best.fitness) //keep the best individual in every breeding session
        {
          m_parent_population[region].push_front(m_best);
          m_parent_population[region].pop_back();
        }
        for(unsigned int j = 0; j < population_size; j++)
        {
          int parent_idx_1 = randomBucket(thresholds);
          int parent_idx_2 = randomBucket(thresholds);
          if(m_parent_population[region][parent_idx_1] < m_parent_population[region][parent_idx_2])
          {
            child_region.push_back(m_parent_population[region][parent_idx_1].cross(m_parent_population[region][parent_idx_2]));
          }
          else
          {
            child_region.push_back(m_parent_population[region][parent_idx_2].cross(m_parent_population[region][parent_idx_1]));
          }
        }
        child_population.push_back(child_region);
      }

      assert(child_population.size() == m_parent_population.size());
      assert(child_population.size() > 0);
      assert(child_population[0].size() == m_parent_population[0].size());
      m_parent_population = child_population;
    }

  private:
    std::deque<std::deque<ExpressionGenome> > m_parent_population; //[region][individual]
    FitnessFunction m_fitness;
    EGParams m_eg_params;
    ExpressionGenome m_best;
  };
}

#endif //EXPRESSION_GA_H
