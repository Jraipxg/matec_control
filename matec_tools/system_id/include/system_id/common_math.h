#ifndef COMMON_MATH_H
#define COMMON_MATH_H

#include <stdlib.h>
#include <math.h>
#include <utility>

namespace system_id
{
  //returns a real value between 0 and 1
  inline long double uniformRandom()
  {
    return ((long double) rand()) / ((long double) RAND_MAX);
  }

  //returns a real value between -1 and 1
  inline long double uniformRandomCentered()
  {
    return 2 * ((long double) (rand() - (((long double) RAND_MAX) / 2.0))) / ((long double) RAND_MAX);
  }

  inline std::pair<long double, long double> uniformRandomCircle()
  {
    std::pair<long double, long double> coordinates;
    long double t = 2 * M_PI * uniformRandom();
    long double u = uniformRandom() + uniformRandom();
    long double r = u > 1? 2 - u : u;
    coordinates.first = r * cos(t);
    coordinates.second = r * sin(t);
    return coordinates;
  }

  //box-muller transform method
#define USE_BASIC_FORM
  inline long double normalRandom(long double mean, long double stddev)
  {
    long double z0;
    static long double z1 = 0.0;
    static bool z1_cached = 0;
    if(!z1_cached)
    {
#ifdef USE_BASIC_FORM
      //normal form
      long double r = sqrt(-2.0 * log(uniformRandom()));
      long double t = 2 * M_PI * uniformRandom();
      z0 = r * cos(t);
      z1 = r * sin(t);

#else
      //polar form
      long double x = uniformRandomCentered();
      long double y = uniformRandomCentered();
      long double r = x * x + y * y;
      while(r == 0.0 || r > 1.0)
      {
        x = uniformRandomCentered();
        y = uniformRandomCentered();
        r = x * x + y * y;
      }

      long double d = sqrt(-2.0 * log(r) / r);
      z0 = x * d;
      z1 = y * d;
#endif
//      double result = z0 * stddev + mean;
      z1_cached = true;
      return z0 * stddev + mean;
    }
    else
    {
      z1_cached = false;
      return z1 * stddev + mean;
    }
  }
}
#endif //COMMON_MATH_H
