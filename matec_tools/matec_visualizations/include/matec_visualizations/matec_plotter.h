#ifndef MATEC_PLOTTER_H
#define MATEC_PLOTTER_H

#include "matec_visualizations/qt_includes.h"
#include <boost/thread.hpp>
//#include "matec_plot/subplot.h"

namespace matec_visualizations
{
  class MatecPlotter
  {
  public:
    MatecPlotter()
    {
      m_plotting_thread = new boost::thread(boost::bind(&MatecPlotter::plottingThread, this));
    }

    void start();

  private:
    boost::thread* m_plotting_thread;
    QApplication* m_app;

    void plottingThread();
  };
}

#endif //MATEC_PLOTTER_H
