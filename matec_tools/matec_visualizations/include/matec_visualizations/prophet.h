#ifndef PROPHET_H
#define PROPHET_H

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/tfMessage.h>

#include <matec_utils/common_functions.h>
#include <matec_utils/common_initialization_components.h>
#include <matec_dynamics_tree/dynamics_graph.h>

#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/Odometry.h>
#include <matec_msgs/AddProphecies.h>
#include <std_srvs/Empty.h>

#include <matec_utils/linear_interpolator.h>

namespace matec_visualizations
{
  class Prophet
  {
  public:
    Prophet(const ros::NodeHandle& nh);
    ~Prophet();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_frequency;
    std::string m_tf_prefix;
    double m_prophecy_speed;

    ros::ServiceServer m_add_srv;
    ros::ServiceServer m_clear_srv;

    ros::Subscriber m_command_sub;
    ros::Subscriber m_odom_sub;
    matec_msgs::FullJointStates m_last_command;
    matec_msgs::Odometry m_odom;
    bool m_have_command;
    bool m_have_odom;

    boost::mutex m_mutex;

    std::vector<matec_msgs::Prophecy> m_prophecies;
    ros::Time m_prophecy_start;

    std::vector<std::string> m_joint_names;

    std::vector<geometry_msgs::TransformStamped> m_static_transforms;
    std::vector<ros::Publisher> m_static_broadcasters;

    tf::TransformBroadcaster m_broadcaster;

    urdf::Model m_urdf_model;
    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;

    void publish();
    void generateStaticTransforms(boost::shared_ptr<const urdf::Link> link, std::vector<geometry_msgs::TransformStamped>& static_transforms);

    void commandCallback(const matec_msgs::FullJointStates::ConstPtr& msg);
    void odomCallback(const matec_msgs::Odometry::ConstPtr& msg);

    bool addCallback(matec_msgs::AddProphecies::Request &req, matec_msgs::AddProphecies::Response &res);
    bool clearCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
  };
}

#endif //PROPHET_H
