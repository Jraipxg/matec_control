#ifndef MATEC_SUBPLOT_H
#define MATEC_SUBPLOT_H

#include "matec_visualizations/qt_includes.h"

namespace matec_visualizations
{
  class SubPlot: public QWidget
  {
  Q_OBJECT
  public:
    SubPlot(QWidget *parent = 0);

  protected:
    void paintEvent(QPaintEvent *e);
    void drawLines(QPainter *qp);
  };
}

#endif //MATEC_SUBPLOT_H
