#ifndef KINEMATIC_WORKSPACE_VISUALIZER_H
#define KINEMATIC_WORKSPACE_VISUALIZER_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/surface/convex_hull.h>
#include <urdf/model.h>
#include <matec_utils/common_functions.h>
#include <matec_dynamics_tree/dynamics_graph.h>
#include <visualization_msgs/MarkerArray.h>

namespace matec_visualizations
{
  class KinematicWorkspaceVisualizer
  {
  public:
    KinematicWorkspaceVisualizer(const ros::NodeHandle& nh);
    ~KinematicWorkspaceVisualizer();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    int m_steps_per_joint;
    int m_num_joints;
    std::string m_root_frame;
    std::string m_tool_frame;
    std::string m_model_path;

    urdf::Model m_urdf_model;
    std::vector<std::string> m_joint_names;
    std::vector<unsigned int> m_joint_indices;
    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;

    std::vector<double> m_min_bounds;
    std::vector<double> m_max_bounds;

    ros::Time m_start_time;
    int m_num_points_tested;
    int m_total_points_to_test;

    pcl::PointCloud<pcl::PointXYZRGB> m_pcl_cloud;
    sensor_msgs::PointCloud2 m_cloud;
    ros::Publisher m_cloud_pub;

    pcl::PointCloud<pcl::PointXYZRGB> m_pcl_cloud_hull;
    sensor_msgs::PointCloud2 m_cloud_hull;
    ros::Publisher m_cloud_hull_pub;

    visualization_msgs::Marker m_hull_marker;
    ros::Publisher m_hull_marker_pub;

    void initModel();
    void calculateKinematicsRecursive(unsigned int recursion_depth, std::vector<unsigned int> test_indices, std::vector<double>& positions);
    void calculateCloud();
  };
}
#endif //KINEMATIC_WORKSPACE_VISUALIZER_H
