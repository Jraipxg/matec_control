#ifndef SENSED_COP_VISUALIZER_H
#define SENSED_COP_VISUALIZER_H

#include <math.h>
#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <std_msgs/String.h>
#include <urdf/model.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud2.h>
#include <matec_msgs/Odometry.h>
#include <matec_msgs/WrenchArray.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/ContactSpecification.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/passthrough.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_utils/urdf_manipulations.h"
#include "matec_dynamics_tree/dynamics_graph.h"

namespace matec_visualizations
{
  class SensedCoPVisualizer
  {
  public:
    SensedCoPVisualizer(const ros::NodeHandle& nh);
    ~SensedCoPVisualizer();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_root_name;

    bool m_log;
    std::string m_log_filename;
    double m_joint_velocity_threshold;
    double m_cop_movement_threshold;
    double m_max_log_frequency;
    ros::Time m_last_log_write_time;

    bool m_have_odom;
    bool m_have_wrenches;

    std::vector<std::string> m_joint_names;

    tf::TransformListener m_tf_listener;

    ros::Subscriber m_joint_states_sub;
    ros::Subscriber m_root_link_odom_sub;
    ros::Subscriber m_wrench_array_sub;

    matec_msgs::Odometry m_odom;
    matec_msgs::WrenchArray m_wrenches;

    ros::Publisher m_cop_pub;
    sensor_msgs::PointCloud2 m_cops;

    ros::Publisher m_total_cop_pub;
    geometry_msgs::PointStamped m_total_cop;

    ros::Publisher m_total_com_pub;
    geometry_msgs::PointStamped m_total_com;

    ros::Publisher m_ground_com_pub;
    geometry_msgs::PointStamped m_ground_com;

    urdf::Model m_urdf_model;
    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;

    boost::mutex m_mutex;

    void initializeLog();
    void writeToLog(matec_msgs::FullJointStates joint_states, matec_msgs::Odometry odom, double cop_x, double cop_y);

    void sensorCallback(const matec_msgs::FullJointStatesConstPtr& msg);
    void odomCallback(const matec_msgs::OdometryConstPtr& msg);
    void wrenchesCallback(const matec_msgs::WrenchArrayConstPtr& msg);
  };
}
#endif //SENSED_COP_VISUALIZER_H
