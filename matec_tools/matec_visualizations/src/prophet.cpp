#include <matec_visualizations/prophet.h>

namespace matec_visualizations
{
  Prophet::Prophet(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("frequency", m_frequency, 50.0);
    m_nh.param("tf_prefix", m_tf_prefix, std::string("prophecy"));
    m_nh.param("prophecy_speed", m_prophecy_speed, 5.0);

    matec_utils::blockOnROSJointNames(m_joint_names);
    m_have_command = false;
    m_have_odom = false;
    m_command_sub = m_nh.subscribe("/smi/joint_commands", 1, &Prophet::commandCallback, this);
    m_odom_sub = m_nh.subscribe("/smi/root_link_odom", 1, &Prophet::odomCallback, this);

    while(!m_nh.hasParam("/robot_description"))
    {
      ROS_WARN_THROTTLE(1.0, "%s waiting for robot description", m_nh.getNamespace().c_str());
    }
    m_urdf_model.initParam("/robot_description");

    generateStaticTransforms(m_urdf_model.getRoot(), m_static_transforms);

    m_graph.loadFromURDF(m_joint_names, m_urdf_model);
    m_graph.spawnDynamicsTree(m_urdf_model.getRoot()->name, false, m_tree);

    m_prophecy_start = ros::Time(0);

    //todo: add streaming prophecy interface

    m_add_srv = m_nh.advertiseService("add_prophecies", &Prophet::addCallback, this);
    m_clear_srv = m_nh.advertiseService("erase_prophecies", &Prophet::clearCallback, this);
  }

  Prophet::~Prophet()
  {
  }

  void Prophet::commandCallback(const matec_msgs::FullJointStates::ConstPtr& msg)
  {
    m_last_command = *msg;
    m_have_command = true;
  }

  void Prophet::odomCallback(const matec_msgs::Odometry::ConstPtr& msg)
  {
    m_odom = *msg;
    m_have_odom = true;
  }

  bool Prophet::addCallback(matec_msgs::AddProphecies::Request &req, matec_msgs::AddProphecies::Response &res)
  {
    ROS_INFO("%s: Got new prophecy", m_nh.getNamespace().c_str());

    boost::mutex::scoped_lock lock(m_mutex);
    if(req.prophecies.size() == 0) //todo: add checks for sizes
    {
      return false;
    }

    if(m_prophecies.size() == 0)
    {
      m_prophecy_start = ros::Time::now();
    }

    double prophecy_time_shift = -req.prophecies.at(0).header.stamp.toSec() + ((m_prophecies.size() == 0)? 0.0 : m_prophecies.at(m_prophecies.size() - 1).header.stamp.toSec());
    for(unsigned int i = 0; i < req.prophecies.size(); i++)
    {
      req.prophecies.at(i).header.stamp = ros::Time(req.prophecies.at(i).header.stamp.toSec() + prophecy_time_shift);
      m_prophecies.push_back(req.prophecies.at(i));
    }

    ROS_INFO("%s: Now displaying %d prophecies", m_nh.getNamespace().c_str(), (int) m_prophecies.size());

    return true;
  }

  bool Prophet::clearCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_prophecies.clear();
    return true;
  }

  void Prophet::generateStaticTransforms(boost::shared_ptr<const urdf::Link> link, std::vector<geometry_msgs::TransformStamped>& static_transforms)
  {
    if(link->parent_joint && (link->parent_joint->type == urdf::Joint::FIXED || std::find(m_joint_names.begin(), m_joint_names.end(), link->parent_joint->name) == m_joint_names.end()))
    {
      geometry_msgs::TransformStamped fixed_transform;

      fixed_transform.transform.translation.x = link->parent_joint->parent_to_joint_origin_transform.position.x;
      fixed_transform.transform.translation.y = link->parent_joint->parent_to_joint_origin_transform.position.y;
      fixed_transform.transform.translation.z = link->parent_joint->parent_to_joint_origin_transform.position.z;

      fixed_transform.transform.rotation.w = link->parent_joint->parent_to_joint_origin_transform.rotation.w;
      fixed_transform.transform.rotation.x = link->parent_joint->parent_to_joint_origin_transform.rotation.x;
      fixed_transform.transform.rotation.y = link->parent_joint->parent_to_joint_origin_transform.rotation.y;
      fixed_transform.transform.rotation.z = link->parent_joint->parent_to_joint_origin_transform.rotation.z;

      fixed_transform.child_frame_id = m_tf_prefix + "/" + link->name;
      fixed_transform.header.frame_id = m_tf_prefix + "/" + link->parent_joint->parent_link_name;
      fixed_transform.header.stamp = ros::Time::now();

      static_transforms.push_back(fixed_transform);
    }

    for(unsigned int i = 0; i < link->child_links.size(); i++)
    {
      generateStaticTransforms(link->child_links[i], static_transforms);
    }
  }

  void Prophet::publish()
  {
    boost::mutex::scoped_lock lock(m_mutex);
    matec_msgs::Prophecy current_prophecy;
    if(m_prophecies.size() != 0)
    {
      //get current prophecy
      double time_from_start = m_prophecy_speed * ros::Duration(ros::Time::now() - m_prophecy_start).toSec();
      if(time_from_start >= m_prophecies.at(m_prophecies.size() - 1).header.stamp.toSec())
      {
        m_prophecy_start = ros::Time::now();
        return;
      }

      for(unsigned int i = 1; i < m_prophecies.size(); i++)
      {
        if(time_from_start < m_prophecies.at(i).header.stamp.toSec() && time_from_start >= m_prophecies.at(i - 1).header.stamp.toSec())
        {
          double time_fraction = (time_from_start - m_prophecies.at(i - 1).header.stamp.toSec()) / (m_prophecies.at(i).header.stamp.toSec() - m_prophecies.at(i - 1).header.stamp.toSec());
          current_prophecy = m_prophecies.at(i - 1);
          for(unsigned int j = 0; j < current_prophecy.joint_positions.size(); j++)
          {
            current_prophecy.joint_positions.at(j) += time_fraction * (m_prophecies.at(i).joint_positions.at(j) - m_prophecies.at(i - 1).joint_positions.at(j));
          }

          current_prophecy.root_link_pose.pose.position.x += time_fraction * (m_prophecies.at(i).root_link_pose.pose.position.x - m_prophecies.at(i - 1).root_link_pose.pose.position.x);
          current_prophecy.root_link_pose.pose.position.y += time_fraction * (m_prophecies.at(i).root_link_pose.pose.position.y - m_prophecies.at(i - 1).root_link_pose.pose.position.y);
          current_prophecy.root_link_pose.pose.position.z += time_fraction * (m_prophecies.at(i).root_link_pose.pose.position.z - m_prophecies.at(i - 1).root_link_pose.pose.position.z);

          tf::Quaternion start_quat, end_quat;
          tf::quaternionMsgToTF(m_prophecies.at(i - 1).root_link_pose.pose.orientation, start_quat);
          tf::quaternionMsgToTF(m_prophecies.at(i).root_link_pose.pose.orientation, end_quat);
          tf::quaternionTFToMsg(tf::slerp(start_quat, end_quat, time_fraction), current_prophecy.root_link_pose.pose.orientation);

          break;
        }
      }
    }
    else if(m_have_command && m_have_odom)
    {
      //no time like the present
      current_prophecy.joint_positions = m_last_command.position;
      current_prophecy.root_link_pose.pose = m_odom.pose;
      current_prophecy.root_link_pose.header = m_odom.header;
      current_prophecy.header = m_odom.header;
    }
    else
    {
      ROS_WARN_THROTTLE(1.0, "%s: No prophecies and no command/odom received!", m_nh.getNamespace().c_str());
      return;
    }

    //visualize the future
    matec_msgs::Odometry odom;
    odom.pose = current_prophecy.root_link_pose.pose;
    m_tree.kinematics(current_prophecy.joint_positions, odom);
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      matec_utils::Matrix4 matrix = m_tree.getNodeByIndex(i)->iTip.inverse();
      tf::StampedTransform trans = matec_utils::matrixToStampedTransform(matrix, m_tf_prefix + "/" + m_tree.getNodeByIndex(i)->parent->link_name, m_tf_prefix + "/" + m_tree.getNodeByIndex(i)->link_name, ros::Time::now());
      m_broadcaster.sendTransform(trans);
    }

    tf::StampedTransform root_trans = matec_utils::poseStampedToStampedTransform(current_prophecy.root_link_pose, m_tf_prefix + "/" + m_urdf_model.getRoot()->name);
    root_trans.stamp_ = ros::Time::now();
    m_broadcaster.sendTransform(root_trans);

    for(unsigned int i = 0; i < m_static_transforms.size(); i++)
    {
      m_static_transforms.at(i).header.stamp = ros::Time::now();
    }
    m_broadcaster.sendTransform(m_static_transforms);
  }

  void Prophet::spin()
  {
    ROS_INFO("%s: started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_frequency);
    while(ros::ok())
    {
      publish();
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "prophet");
  ros::NodeHandle nh("~");

  matec_visualizations::Prophet node(nh);
  node.spin();

  return 0;
}
