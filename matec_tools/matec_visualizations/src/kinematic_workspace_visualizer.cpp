#include "matec_visualizations/kinematic_workspace_visualizer.h"

namespace matec_visualizations
{
  KinematicWorkspaceVisualizer::KinematicWorkspaceVisualizer(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 50.0);
    m_nh.param("steps_per_joint", m_steps_per_joint, 4);
    m_nh.param("root_frame", m_root_frame, std::string(""));
    m_nh.param("tool_frame", m_tool_frame, std::string(""));
    m_nh.param("model_path", m_model_path, std::string(""));

    while(ros::Time::now().toSec() == 0.0)
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for time...", m_nh.getNamespace().c_str());
    }

    if(m_tool_frame.length() == 0)
    {
      ROS_ERROR("~tool_frame parameter must be specified");
      ros::shutdown();
      return;
    }

    m_cloud_pub = m_nh.advertise<sensor_msgs::PointCloud2>("/reachable_cloud", 1, true);
    m_cloud_hull_pub = m_nh.advertise<sensor_msgs::PointCloud2>("/reachable_cloud_hull", 1, true);
    m_hull_marker_pub = m_nh.advertise<visualization_msgs::Marker>("/reachable_cloud_hull_marker", 1, true);

    initModel();
    calculateCloud();
  }

  KinematicWorkspaceVisualizer::~KinematicWorkspaceVisualizer()
  {

  }

  void KinematicWorkspaceVisualizer::initModel()
  {
    if(m_model_path.length() != 0)
    {
      ROS_INFO("Using user-specified model path %s", m_model_path.c_str());
      WTFASSERT(m_urdf_model.initFile(m_model_path));
    }
    else
    {
      ROS_INFO("Using model stored in /robot_description");
      WTFASSERT(m_urdf_model.initParam("/robot_description"));
    }

    if(m_root_frame.length() == 0)
    {
      m_root_frame = m_urdf_model.getRoot()->name;
    }

    std::vector<boost::shared_ptr<urdf::Link> > links;
    m_urdf_model.getLinks(links);
    for(unsigned int i = 0; i < links.size(); i++)
    {
      if(links.at(i)->parent_joint && (links.at(i)->parent_joint->type == urdf::Joint::REVOLUTE || links.at(i)->parent_joint->type == urdf::Joint::PRISMATIC))
      {
        m_joint_names.push_back(links.at(i)->parent_joint->name);
        m_joint_indices.push_back(m_joint_indices.size());
      }
    }
    m_min_bounds.resize(m_joint_names.size(), 0.0);
    m_max_bounds.resize(m_joint_names.size(), 0.0);
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(m_joint_names.at(i));
      if(joint && joint->limits)
      {
        m_min_bounds.at(i) = joint->limits->lower;
        m_max_bounds.at(i) = joint->limits->upper;
      }
    }

    m_graph.loadFromURDF(m_joint_names, m_urdf_model, false);
    m_graph.spawnDynamicsTree(m_root_frame, false, m_tree);
  }

  double fixAngle(double angle)
  {
    while(angle >= (2.0 * M_PI))
    {
      angle -= 2.0 * M_PI;
    }

    while(angle < 0)
    {
      angle += 2.0 * M_PI;
    }

    return angle;
  }

  void KinematicWorkspaceVisualizer::calculateKinematicsRecursive(unsigned int recursion_depth, std::vector<unsigned int> test_indices, std::vector<double>& positions)
  {
    if(recursion_depth == test_indices.size())
    {
      matec_msgs::Odometry odom;
      odom.pose.orientation.w = 1.0;
      m_tree.kinematics(positions, odom);

      matec_utils::Matrix4 rootTtool;
      m_tree.lookupTransform(m_root_frame, m_tool_frame, rootTtool);
      double x = rootTtool(0, 3);
      double y = rootTtool(1, 3);
      double z = rootTtool(2, 3);
      double roll, pitch, yaw;
      matec_utils::extractRPY((matec_utils::Matrix3) rootTtool.topLeftCorner(3, 3), roll, pitch, yaw);

      pcl::PointXYZRGB point;
      point.x = x;
      point.y = y;
      point.z = z;
      point.r = 255 * fixAngle(roll) / (2.0 * M_PI);
      point.g = 255 * fixAngle(pitch) / (2.0 * M_PI);
      point.b = 255 * fixAngle(yaw) / (2.0 * M_PI);
      m_pcl_cloud.points.push_back(point);

      m_num_points_tested++;
      ros::Duration diff = ros::Time::now() - m_start_time;
      float elapsed_time = diff.toSec();
      float completion = ((float) m_num_points_tested) / ((float) m_total_points_to_test);
      float rate = completion / elapsed_time;
      if(rate != 0)
      {
        float remaining_time = (1 - completion) / rate;
        ROS_INFO_THROTTLE(1.0, "Elaspsed time is %gs. Testing is %g%% complete. ~%gs remaining.", elapsed_time, round(completion * 100.0), remaining_time);
      }

      return;
    }

    //generate joint positions
    for(unsigned int q = 0; ros::ok() && q < (unsigned int) m_steps_per_joint; q++)
    {
      unsigned int idx = test_indices.at(recursion_depth);
      positions.at(idx) = m_min_bounds[idx] + (m_max_bounds[idx] - m_min_bounds[idx]) * ((double) q / ((double) m_steps_per_joint - 1.0));
      calculateKinematicsRecursive(recursion_depth + 1, test_indices, positions);
    }
  }

  geometry_msgs::Point convertPoint(pcl::PointXYZRGB pcl_point)
  {
    geometry_msgs::Point point_msg;
    point_msg.x = pcl_point.x;
    point_msg.y = pcl_point.y;
    point_msg.z = pcl_point.z;
    return point_msg;
  }

  void KinematicWorkspaceVisualizer::calculateCloud()
  {
    matec_msgs::Odometry odom;
    odom.pose.orientation.w = 1.0;
    std::vector<double> positions(m_joint_names.size(), 0.0);
    m_tree.kinematics(positions, odom);

    //set up recursion
    std::vector<unsigned int> supporting_joints;
    if(!m_tree.getSupportingSubset(m_joint_indices, m_tool_frame, supporting_joints))
    {
      ROS_ERROR("%s: Kinematic workspace calculation failed!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }

    std::cerr << "Supporting joints are: ";
    matec_utils::printVector(supporting_joints);
    std::cerr << std::endl;

    m_start_time = ros::Time::now();
    m_num_points_tested = 0;
    m_total_points_to_test = std::pow(m_steps_per_joint, supporting_joints.size());
    ROS_INFO("%s: Testing %d joint configurations", m_nh.getNamespace().c_str(), m_total_points_to_test);

    //recurse
    calculateKinematicsRecursive(0, supporting_joints, positions);
    ROS_INFO("%s: Workspace generation complete. Building clouds...", m_nh.getNamespace().c_str());
    m_pcl_cloud.width = m_pcl_cloud.points.size();
    m_pcl_cloud.height = 1;
    m_pcl_cloud.header.frame_id = m_root_frame;
    pcl::toROSMsg(m_pcl_cloud, m_cloud);
    m_cloud.header.stamp = ros::Time(0);
    m_cloud_pub.publish(m_cloud);

    //generate convex hull
    pcl::ConvexHull<pcl::PointXYZRGB> convex_hull;
    convex_hull.setInputCloud(m_pcl_cloud.makeShared());
    std::vector<pcl::Vertices> polygons;
    convex_hull.reconstruct(m_pcl_cloud_hull, polygons);
    m_pcl_cloud_hull.header.frame_id = m_pcl_cloud.header.frame_id;
    pcl::toROSMsg(m_pcl_cloud_hull, m_cloud_hull);
    m_cloud_hull.header.stamp = ros::Time(0);
    m_cloud_hull_pub.publish(m_cloud_hull);

    //generate pretty marker
    ROS_INFO("%s: Generating convex hull marker...", m_nh.getNamespace().c_str());
    m_hull_marker.action = visualization_msgs::Marker::ADD;
    m_hull_marker.ns = "reachable_space";
    m_hull_marker.id = 0;
    m_hull_marker.header.frame_id = m_root_frame;
    m_hull_marker.header.stamp = ros::Time(0);
    m_hull_marker.frame_locked = true;
    m_hull_marker.scale.x = 1.0;
    m_hull_marker.scale.y = 1.0;
    m_hull_marker.scale.z = 1.0;
    m_hull_marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
    m_hull_marker.pose.orientation.w = 1.0;

    std_msgs::ColorRGBA color;
    color.r = 0.239;
    color.g = 0.600;
    color.b = 0.439;
    color.a = 0.300;
    m_hull_marker.color = color;

    for(unsigned int i = 0; i < polygons.size(); i++)
    {
      if(polygons.at(i).vertices.size() < 3)
      {
        continue;
      }

      for(unsigned int j = 0; j < polygons.at(i).vertices.size() - 2; j++)
      {
        pcl::PointXYZRGB p0 = m_pcl_cloud_hull.points.at(polygons.at(i).vertices.at(j));
        pcl::PointXYZRGB p1 = m_pcl_cloud_hull.points.at(polygons.at(i).vertices.at(j + 1));
        pcl::PointXYZRGB p2 = m_pcl_cloud_hull.points.at(polygons.at(i).vertices.at(j + 2));
        m_hull_marker.points.push_back(convertPoint(p0));
        m_hull_marker.points.push_back(convertPoint(p1));
        m_hull_marker.points.push_back(convertPoint(p2));
        m_hull_marker.colors.push_back(color);
        m_hull_marker.colors.push_back(color);
        m_hull_marker.colors.push_back(color);
      }
    }

//    for(unsigned int i = 0; i < m_pcl_cloud_hull.points.size(); i++)
//    {
//      for(unsigned int j = i; j < m_pcl_cloud_hull.points.size(); j++)
//      {
//        for(unsigned int k = j; k < m_pcl_cloud_hull.points.size(); k++)
//        {
//          pcl::PointXYZ p0 = m_pcl_cloud_hull.points.at(i);
//          pcl::PointXYZ p1 = m_pcl_cloud_hull.points.at(j);
//          pcl::PointXYZ p2 = m_pcl_cloud_hull.points.at(k);
//          matec_utils::Vector3 v0, v1, v2, vx;
//          v0 << p0.x, p0.y, p0.z;
//          v1 << p1.x, p1.y, p1.z;
//          v2 << p2.x, p2.y, p2.z;
//          bool triangle_ok = true;
//          int last_sign = 0;
//          for(unsigned int l = 0; l < m_pcl_cloud_hull.points.size(); l++)
//          {
//            pcl::PointXYZ px = m_pcl_cloud_hull.points.at(i);
//            vx << px.x, px.y, px.z;
//
//            matec_utils::Matrix3 mat;
//            mat.col(0) = v1 - v0;
//            mat.col(1) = v2 - v0;
//            mat.col(2) = vx - v0;
//
//            int det_sign = matec_utils::sign((double) mat.determinant());
//            if(last_sign != 0 && det_sign != last_sign)
//            {
//              triangle_ok = false;
//              break;
//            }
//          }
//          if(triangle_ok)
//          {
//            m_hull_marker.points.push_back(convertPoint(p0));
//            m_hull_marker.points.push_back(convertPoint(p1));
//            m_hull_marker.points.push_back(convertPoint(p2));
//            m_hull_marker.colors.push_back(color);
//          }
//        }
//      }
//    }

    m_hull_marker_pub.publish(m_hull_marker);
  }

  void KinematicWorkspaceVisualizer::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      m_cloud.header.stamp = ros::Time::now();
      m_cloud_pub.publish(m_cloud);
      m_cloud_hull.header.stamp = ros::Time::now();
      m_cloud_hull_pub.publish(m_cloud_hull);
      m_hull_marker.header.stamp = ros::Time::now();
      m_hull_marker_pub.publish(m_hull_marker);

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "kinematic_workspace_visualizer");
  ros::NodeHandle nh("~");

  matec_visualizations::KinematicWorkspaceVisualizer node(nh);
  node.spin();

  return 0;
}
