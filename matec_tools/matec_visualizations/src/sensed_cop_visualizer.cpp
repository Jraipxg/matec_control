#include <matec_visualizations/sensed_cop_visualizer.h>

namespace matec_visualizations
{
  SensedCoPVisualizer::SensedCoPVisualizer(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    //get robot model
    while(!m_nh.hasParam("/robot_description") && ros::ok())
    {
      ROS_WARN_THROTTLE(2.0, "sensed_cop_visualizer is waiting for robot description.");
    }
    m_urdf_model.initParam("/robot_description");

    m_nh.param("loop_rate", m_loop_rate, 100.0);
    m_nh.param("root_name", m_root_name, m_urdf_model.getRoot()->name);
    m_nh.param("log", m_log, false);
    m_nh.param("log_filename", m_log_filename, std::string("log_file"));
    m_nh.param("max_log_frequency", m_max_log_frequency, 1.0);
    m_nh.param("joint_velocity_threshold", m_joint_velocity_threshold, 0.1);
    m_nh.param("cop_movement_threshold", m_cop_movement_threshold, 0.01);

    m_total_cop_pub = m_nh.advertise<geometry_msgs::PointStamped>("/total_center_of_pressure", 1, true);
    m_total_com_pub = m_nh.advertise<geometry_msgs::PointStamped>("/total_center_of_mass", 1, true);
    m_ground_com_pub = m_nh.advertise<geometry_msgs::PointStamped>("/ground_center_of_mass", 1, true);
    m_cop_pub = m_nh.advertise<sensor_msgs::PointCloud2>("/centers_of_pressure", 1, true);

    matec_utils::blockOnROSJointNames(m_joint_names);
    if(m_log)
    {
      initializeLog();
    }

    m_have_odom = false;
    m_have_wrenches = false;

    m_cops.header.stamp = ros::Time(0);
    m_cops.header.frame_id = "global";
    m_total_cop.header = m_cops.header;
    m_total_com.header = m_cops.header;

    m_joint_states_sub = m_nh.subscribe<matec_msgs::FullJointStates>("/smi/estimated_joint_states", 1, boost::bind(&SensedCoPVisualizer::sensorCallback, this, _1));
    m_root_link_odom_sub = m_nh.subscribe<matec_msgs::Odometry>("/smi/root_link_odom", 1, boost::bind(&SensedCoPVisualizer::odomCallback, this, _1));
    m_wrench_array_sub = m_nh.subscribe<matec_msgs::WrenchArray>("/smi/wrenches", 1, boost::bind(&SensedCoPVisualizer::wrenchesCallback, this, _1));

    m_graph.loadFromURDF(m_joint_names, m_urdf_model);
    m_graph.spawnDynamicsTree(m_root_name, true, m_tree);
  }

  SensedCoPVisualizer::~SensedCoPVisualizer()
  {
  }

  void SensedCoPVisualizer::initializeLog()
  {
    m_last_log_write_time = ros::Time(0);
    if(std::ifstream(m_log_filename)) //file already initialized
    {
      return;
    }

    std::ofstream log(m_log_filename, std::ios_base::app | std::ios_base::out);

    //odometry
    log << "px, py, pz, ow, ox, oy, oz, ";

    //joint_positions
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      log << m_joint_names[i] << ", ";
    }

    //cop
    log << "cop_x, cop_y";
    log << std::endl;
  }

  void SensedCoPVisualizer::writeToLog(matec_msgs::FullJointStates joint_states, matec_msgs::Odometry odom, double cop_x, double cop_y)
  {
    ros::Time now_time = ros::Time::now();
    if((now_time - m_last_log_write_time).toSec() < (1.0 / m_max_log_frequency))
    {
      return;
    }
    else
    {
      m_last_log_write_time = now_time;
    }
    std::ofstream log(m_log_filename, std::ios_base::app | std::ios_base::out);
    std::cerr << "Logging cop!" << std::endl;

    //odometry
    log << odom.pose.position.x << ", ";
    log << odom.pose.position.y << ", ";
    log << odom.pose.position.z << ", ";
    log << odom.pose.orientation.w << ", ";
    log << odom.pose.orientation.x << ", ";
    log << odom.pose.orientation.y << ", ";
    log << odom.pose.orientation.z << ", ";

    //positions
    for(unsigned int i = 0; i < joint_states.joint_indices.size(); i++)
    {
      log << joint_states.position[i];
      log << ", ";
    }
    log << cop_x << ", " << cop_y;
    log << std::endl;
  }

  pcl::PointXYZ pointToPoint(geometry_msgs::Point point)
  {
    return pcl::PointXYZ(point.x, point.y, point.z);
  }

  bool calculateWrenchCoP(geometry_msgs::WrenchStamped wrench, geometry_msgs::PointStamped& point)
  {
    //assumes flat contact, with z coming out of the plane of contact
    if(fabs(wrench.wrench.force.z) < 1.0e-3 || wrench.wrench.force.z < 0) //no contact
    {
      return false;
    }

    point.point.x = -wrench.wrench.torque.y / wrench.wrench.force.z; //- m_s_p_y / f_r_z
    point.point.y = wrench.wrench.torque.x / wrench.wrench.force.z; //m_s_p_x / f_r_z
    point.point.z = 0;
    point.header.frame_id = wrench.header.frame_id;

    return true;
  }

  void SensedCoPVisualizer::odomCallback(const matec_msgs::OdometryConstPtr& msg)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_odom = *msg;
    m_have_odom = true;
  }

  void SensedCoPVisualizer::wrenchesCallback(const matec_msgs::WrenchArrayConstPtr& msg)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_wrenches = *msg;
    m_have_wrenches = true;
  }

  void SensedCoPVisualizer::sensorCallback(const matec_msgs::FullJointStatesConstPtr& msg)
  {
    if(!m_have_odom || !m_have_wrenches)
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for odometry and wrenches", m_nh.getNamespace().c_str());
      return;
    }
    boost::mutex::scoped_lock lock(m_mutex);
    matec_msgs::FullJointStates joint_states = *msg;

    m_tree.kinematics(joint_states.position, m_odom);

    m_tree.centerOfMass("pelvis", m_total_com);
    std::cerr << "pelvis com:\n" << m_total_com << std::endl << std::endl;
    m_tree.centerOfMass(m_odom.header.frame_id, m_total_com);
    m_total_com.header.frame_id = "global";
    std::cerr << "inertial com:\n" << m_total_com << std::endl << std::endl;

    pcl::PointCloud<pcl::PointXYZ> points;

    geometry_msgs::PointStamped old_cop = m_total_cop;
    m_total_cop.point.x = 0;
    m_total_cop.point.y = 0;
    m_total_cop.point.z = 0;
    for(unsigned int i = 0; i < m_wrenches.wrenches.size(); i++)
    {
      std::string contact_frame = "";
      if(m_wrenches.wrenches[i].header.frame_id == "l_foot_sensor") //TODO: don't hardcode contact frame
      {
        contact_frame = "l_sole";
      }
      else if(m_wrenches.wrenches[i].header.frame_id == "r_foot_sensor") //TODO: don't hardcode contact frame
      {
        contact_frame = "r_sole";
      }
      else
      {
        continue;
      }

      geometry_msgs::WrenchStamped contact_wrench;
      m_tree.transformWrench(contact_frame, m_wrenches.wrenches[i], contact_wrench);

      geometry_msgs::PointStamped cop, inertial_cop;
      if(!calculateWrenchCoP(contact_wrench, cop))
      {
        continue;
      }

      m_tree.transformPoint("INERTIAL", cop, inertial_cop);
      inertial_cop.header.frame_id = "global";

      m_total_cop.point.x += inertial_cop.point.x;
      m_total_cop.point.y += inertial_cop.point.y;
      m_total_cop.point.z += inertial_cop.point.z;

      points.points.push_back(pointToPoint(inertial_cop.point));
    }
    points.height = 1;
    points.width = points.points.size();

    if(points.points.size() != 0)
    {
      m_total_cop.point.x /= points.points.size();
      m_total_cop.point.y /= points.points.size();
      m_total_cop.point.z /= points.points.size();

      if(points.points.size() != 0)
      {
        pcl::toROSMsg(points, m_cops);
        m_cops.header.stamp = ros::Time(0);
        m_cops.header.frame_id = "global";
      }

      m_cop_pub.publish(m_cops);
      m_total_cop_pub.publish(m_total_cop);
      m_total_com_pub.publish(m_total_com);

      m_ground_com = m_total_com;
      m_ground_com.header.frame_id = "global";
      m_ground_com.point.z = 0;
      m_ground_com_pub.publish(m_ground_com);

      if(m_log) //see if it's a good time to log
      {
        //check joint velocities
        for(unsigned int i = 0; i < joint_states.joint_indices.size(); i++)
        {
          if(fabs(joint_states.velocity[i]) > m_joint_velocity_threshold)
          {
            std::cerr << "joint " << i << " is moving too fast!" << std::endl;
            return;
          }
        }

        //check cop velocities
        double cop_x_movement = m_total_cop.point.x - old_cop.point.x;
        double cop_y_movement = m_total_cop.point.y - old_cop.point.y;
        if(fabs(cop_x_movement) > m_cop_movement_threshold || fabs(cop_y_movement) > m_cop_movement_threshold)
        {
          return;
        }

        //write to log
        writeToLog(joint_states, m_odom, m_total_cop.point.x, m_total_cop.point.y);
      }
    }
    else
    {
      m_total_cop = old_cop;
    }
  }

  void SensedCoPVisualizer::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);

    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sensed_cop_visualizer");
  ros::NodeHandle nh("~");

  matec_visualizations::SensedCoPVisualizer node(nh);
  node.spin();

  return 0;
}
