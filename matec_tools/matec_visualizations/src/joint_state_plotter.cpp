#include <ros/ros.h>
#include <matec_utils/common_initialization_components.h>
#include <matec_utils/rolling_buffer.h>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>

enum FieldType
{
  POSITION,
  VELOCITY,
  ACCELERATION,
  TORQUE,
  NUM_FIELDS
};

class PlotSpec
{
public:
  PlotSpec()
  {
    topic = "";
    field_type = NUM_FIELDS;
    idx = -1;
  }

  PlotSpec(std::string topic_, FieldType field_type_, int idx_)
  {
    topic = topic_;
    field_type = field_type_;
    idx = idx_;
  }

  std::string topic;
  FieldType field_type;
  int idx;
  boost::shared_ptr<matec_msgs::FullJointStates> message_ptr;
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "joint_state_plotter");
  ros::NodeHandle nh("~");

  unsigned long buffer_size = 5000; //TODO: param

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  /////////////////////////////////////////////////
  //TODO: generate plotspecs from command line args
  std::vector<PlotSpec> specs;
  specs.push_back(PlotSpec("/joint_states", POSITION, 27));
  /////////////////////////////////////////////////

  std::vector<std::string> topics;
  std::vector<shared_memory_interface::Subscriber<matec_msgs::FullJointStates> > subscribers;
  std::vector<boost::shared_ptr<matec_msgs::FullJointStates> > message_ptrs;
  for(unsigned int i = 0; i < specs.size(); i++)
  {
    unsigned int topic_idx = std::find(topics.begin(), topics.end(), specs[i]);
    if(topic_idx == topics.size()) //new topic name
    {
      topics.push_back(specs[i].topic);
      message_ptrs.push_back(boost::shared_ptr<matec_msgs::FullJointStates>());
      message_ptrs[topic_idx].reset();
      subscribers.push_back(shared_memory_interface::Subscriber<matec_msgs::FullJointStates>());
      subscribers[topic_idx].subscribe(specs[i].topic);
    }
    specs[i].message_ptr = message_ptrs[topic_idx];
  }

  matec_msgs::FullJointStates states;
  double last_stamp = 0.0;
  std::vector<matec_utils::RollingBuffer<matec_utils::ScalarSample> > buffers(specs.size());



  while(ros::ok())
  {
    //get messages
    for(unsigned int i = 0; i < subscribers.size(); i++)
    {
      subscribers[i].getCurrentMessage(*message_ptrs[i]);
    }

    //TODO: multithread?

    //extract data
    for(unsigned int i = 0; i < specs.size(); i++)
    {
      double old_time = buffers[i](0).time;
      double message_time = specs[i].message_ptr->timestamp;
      if(old_time == message_time) //save some computation
      {
        continue;
      }

      double message_data = 0;
      int message_idx = specs[i].idx;//TODO: translate indices when message doesn't have the full set
      switch(specs[i].field_type)
      {
      case POSITION:
        message_data = specs[i].message_ptr->position[message_idx];
        break;
      case VELOCITY:
        message_data = specs[i].message_ptr->velocity[message_idx];
        break;
      case ACCELERATION:
        message_data = specs[i].message_ptr->acceleration[message_idx];
        break;
      case TORQUE:
        message_data = specs[i].message_ptr->torque[message_idx];
        break;
      default:
        //TODO: error message
        break;
      }

      buffers[i].push(matec_utils::ScalarSample(message_data, message_time));
    }

    //plot

  }

  return 0;
}
