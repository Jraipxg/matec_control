#ifndef DYNAMICS_TREE_FORWARD_DYNAMICS_H
#define DYNAMICS_TREE_FORWARD_DYNAMICS_H
#undef NDEBUG
#include <cassert>

namespace dynamics_tree
{
//  void DynamicsTree::forwardDynamics(matec_msgs::Odometry& root_link_odom, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, double gravity_magnitude) //current positions, velocities, accelerations => current torques
//  {
//    accelerations.resize(positions.size(), 0.0);
//
//    m_root_node->iTO = poseToMatrix(root_link_odom.pose);
//    Vector4 inertial_gravity, root_gravity;
//    inertial_gravity << 0.0, 0.0, -gravity_magnitude, 1.0;
//    root_gravity = m_root_node->iTO * inertial_gravity;
//
//    if(m_root_node->joint_type == DynamicsTreeNode::FIXED_BASE)
//    {
//      m_root_node->a.bottomRows(3) = -root_gravity.topRows(3);
//      m_root_node->v = Vector6::Zero();
//    }
//    else if(m_root_node->joint_type == DynamicsTreeNode::FLOATING_BASE)
//    {
//      m_root_node->a = Vector6::Zero();
//      m_root_node->v(0) = root_link_odom.velocity.angular.x;
//      m_root_node->v(1) = root_link_odom.velocity.angular.y;
//      m_root_node->v(2) = root_link_odom.velocity.linear.x;
//      m_root_node->v(4) = root_link_odom.velocity.linear.y;
//      m_root_node->v(5) = root_link_odom.velocity.linear.z;
//    }
//
//    std::vector<DynamicsTreeNode::RecursionOperation> forward, backward;
//    forward.push_back(boost::bind(&DynamicsTree::kinematicsForwardOperation, this, _1, _2, _3, _4, _5));
//    forward.push_back(boost::bind(&DynamicsTree::forwardDynamicsForwardOperation1, this, _1, _2, _3, _4, _5));
//    backward.push_back(boost::bind(&DynamicsTree::forwardDynamicsBackwardOperation2, this, _1, _2, _3, _4, _5));
//    m_root_node->recurse(forward, backward, positions, velocities, accelerations, torques);
//
//    forward.clear();
//    backward.clear();
//    forward.push_back(boost::bind(&DynamicsTree::forwardDynamicsForwardOperation3, this, _1, _2, _3, _4, _5));
//    m_root_node->recurse(forward, backward, positions, velocities, accelerations, torques);
//
//    if(m_root_node->joint_type == DynamicsTreeNode::FLOATING_BASE)
//    {
//      m_root_node->a.bottomRows(3) += root_gravity.topRows(3);
//      std::cerr << "FD base accel: " << m_root_node->a << std::endl;
//      std::cerr << "FD base force:\n" << m_root_node->pA << std::endl;
////      std::cerr << "FD base inertia:\n" << m_root_node->composite_inertia << std::endl;
//    }
//  }
//
//  void DynamicsTree::forwardDynamicsForwardOperation1(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques)
//  {
//    if(node->parent)
//    {
//      node->q_dot = (node->joint_type == DynamicsTreeNode::FIXED_JOINT)? 0.0 : velocities.at(node->joint_idx);
//      node->torque = (node->joint_type == DynamicsTreeNode::FIXED_JOINT)? 0.0 : torques.at(node->joint_idx);
//      Vector6 joint_spatial_velocity = (node->axis * node->q_dot);
//      node->v = (node->iXip * node->parent->v) + joint_spatial_velocity;
//      node->c = cross(node->v) * joint_spatial_velocity; //assuming cji = 0
//      node->composite_inertia = node->link_inertia;
//      node->pA = -cross(node->v).transpose() * node->link_inertia * node->v; // - transformed external forces
//    }
//    else //base
//    {
//      if(node->joint_type == DynamicsTreeNode::FLOATING_BASE)
//      {
//        node->composite_inertia = node->link_inertia;
//        node->pA = -cross(node->v).transpose() * node->link_inertia * node->v; // - transformed external forces
//      }
//    }
//  }
//
//  void DynamicsTree::forwardDynamicsBackwardOperation2(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques)
//  {
//    if(node->parent)
//    {
//      node->U = node->composite_inertia * node->axis;
//      node->D_inverse = 1.0 / ((double) (node->axis.transpose() * node->U));
//      assert(((double ) (node->axis.transpose() * node->U) != 0.0));
//      node->u = node->torque - node->axis.transpose() * node->pA;
//      Matrix6 Ia = node->composite_inertia - node->U * node->D_inverse * node->U.transpose();
//      Vector6 pa = node->pA + Ia * node->c + node->U * node->D_inverse * node->u;
//      Matrix6 iXip_tranpose = node->iXip.transpose();
//      node->parent->composite_inertia += iXip_tranpose * Ia * node->iXip;
//      node->parent->pA += iXip_tranpose * pa;
//    }
//    else //base
//    {
//    }
//  }
//
//  void DynamicsTree::forwardDynamicsForwardOperation3(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques)
//  {
//    if(node->parent)
//    {
////      std::cerr << "Joint " << node->joint_idx << " sees composite inertia " << std::endl << node->composite_inertia << std::endl;
//
//      Vector6 aprime = node->iXip * node->parent->a + node->c;
//      node->q_dot_dot = node->D_inverse * (node->u - node->U.transpose() * aprime);
//      node->a = aprime + node->axis * node->q_dot_dot;
//      if(node->joint_type != DynamicsTreeNode::FIXED_JOINT)
//      {
//        accelerations.at(node->joint_idx) = node->q_dot_dot;
//      }
//    }
//    else //base
//    {
//      if(node->joint_type == DynamicsTreeNode::FLOATING_BASE)
//      {
//        node->a = -node->composite_inertia.inverse() * node->pA;
//      }
//    }
//  }
}

#endif //DYNAMICS_TREE_FORWARD_DYNAMICS_H
