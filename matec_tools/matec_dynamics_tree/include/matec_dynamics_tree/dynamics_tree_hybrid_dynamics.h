#ifndef DYNAMICS_TREE_HYBRID_DYNAMICS_H
#define DYNAMICS_TREE_HYBRID_DYNAMICS_H

namespace dynamics_tree
{
  inline void DynamicsTree::hybridDynamics(matec_msgs::Odometry& root_link_odom, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches, double gravity_magnitude)
  {
    kinematicsInitialization(root_link_odom);

    accelerations.resize(positions.size(), 0.0);
    torques.resize(positions.size(), 0.0);

//    matec_utils::Vector6 inertial_gravity_accel;
//    inertial_gravity_accel << 0.0, 0.0, 0.0, 0.0, 0.0, -gravity_magnitude;

    matec_utils::Vector6 root_gravity = m_root_node->iXO.topRightCorner(6, 1) * gravity_magnitude;
    if(m_root_node->joint_type == FIXED_BASE)
    {
      m_root_node->a = root_gravity; //-m_root_node->iXO * inertial_gravity_accel;
      m_root_node->v = matec_utils::Vector6::Zero();
    }
    else if(m_root_node->joint_type == FLOATING_BASE)
    {
      m_root_node->a = matec_utils::Vector6::Zero();
      m_root_node->v = matec_utils::twistToVector(root_link_odom.velocity);
      m_root_node->articulated_body_inertia = m_root_node->link_inertia;
      m_root_node->pA = -matec_utils::cross(m_root_node->v).transpose() * m_root_node->link_inertia * m_root_node->v; // - transformed external forces
    }

    std::vector<DynamicsTreeNode::RecursionOperation> forward_pass_1, backward_pass_2, forward_pass_3, empty_backward_pass_4;
    forward_pass_1.push_back(boost::bind(&DynamicsTree::kinematicsPass, this, _1, _2, _3, _4, _5, _6));
    forward_pass_1.push_back(boost::bind(&DynamicsTree::hybridDynamicsPass1, this, _1, _2, _3, _4, _5, _6));
    backward_pass_2.push_back(boost::bind(&DynamicsTree::hybridDynamicsPass2, this, _1, _2, _3, _4, _5, _6));
    m_root_node->recurse(forward_pass_1, backward_pass_2, positions, velocities, accelerations, torques, external_wrenches);

    if(m_root_node->joint_type == FLOATING_BASE)
    {
      m_root_node->a = -m_root_node->articulated_body_inertia.inverse() * m_root_node->pA;
    }

    forward_pass_3.push_back(boost::bind(&DynamicsTree::hybridDynamicsPass3, this, _1, _2, _3, _4, _5, _6));
    m_root_node->recurse(forward_pass_3, empty_backward_pass_4, positions, velocities, accelerations, torques, external_wrenches);

    if(m_root_node->joint_type == FLOATING_BASE)
    {
      m_root_node->a -= root_gravity; //-m_root_node->iXO * inertial_gravity_accel;
      matec_utils::Vector6 inertial_root_acceleration = m_root_node->iXO.inverse() * m_root_node->a;
      root_link_odom.acceleration = matec_utils::vectorToTwist(inertial_root_acceleration);
    }
  }

  //forward
  inline void DynamicsTree::hybridDynamicsPass1(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches)
  {
    boost::this_thread::interruption_point();
    if(node->parent)
    {
      node->q_dot = (node->joint_type == FIXED_JOINT)? 0.0 : velocities.at(node->joint_idx);
      matec_utils::Vector6 joint_spatial_velocity = (node->axis * node->q_dot);
      node->v = (node->iXip * node->parent->v) + joint_spatial_velocity;

      if(node->dynamics_direction == DynamicsTreeNode::FORWARD_DYNAMICS)
      {
        node->torque = (node->joint_type == FIXED_JOINT)? 0.0 : (torques.at(node->joint_idx));
        node->c = matec_utils::cross(node->v) * joint_spatial_velocity; //assuming cji = 0
      }
      else
      {
        node->q_dot_dot = (node->joint_type == FIXED_JOINT)? 0.0 : accelerations.at(node->joint_idx);
        node->c = matec_utils::cross(node->v) * joint_spatial_velocity + node->axis * node->q_dot_dot; //assuming cji = 0
      }

      node->articulated_body_inertia = node->link_inertia;
      node->pA = -matec_utils::cross(node->v).transpose() * node->link_inertia * node->v;

      if(node->external_wrench_frame != NO_FRAME)
      {
        node->f_ext = (node->joint_type == FIXED_JOINT)? matec_utils::Vector6::Zero() : external_wrenches.at(node->joint_idx);
        switch(node->external_wrench_frame)
        {
        case INERTIAL_FRAME:
          node->f_ext = node->iXO.inverse().transpose() * node->f_ext; //TODO: only store inverse transpose if that's all we ever use
          break;
        case JOINT_FRAME:
          //no modification necessary
          break;
        case COM_FRAME:
          node->f_ext = node->iXicom.inverse().transpose() * node->f_ext; //TODO: iXicom is constant, buffer inverse transpose?
          break;
        default:
          std::cerr << "SPECIFIED EXTERNAL WRENCH FRAME WAS NOT AN ENUM VALUE!" << std::endl;
        }
        node->pA -= node->f_ext;
      }
    }
  }

  //backwards
  inline void DynamicsTree::hybridDynamicsPass2(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches)
  {
    boost::this_thread::interruption_point();
    if(node->parent)
    {
      matec_utils::Matrix6 iXip_tranpose = node->iXip.transpose();
      if(node->dynamics_direction == DynamicsTreeNode::FORWARD_DYNAMICS)
      {
        node->U = node->articulated_body_inertia * node->axis;
        node->D_inverse = 1.0 / ((double) (node->axis.transpose() * node->U));

        if(node->enforce_joint_limits)
        {
          double kp_restoration = 25000.0;
          double kv_restoration = sqrt(kp_restoration) * 2.0;
          double sanity_torque_max = 10.0 * node->max_torque;
          if(node->q < node->min_q)
          {
            long double p_err = node->min_q - node->q;
            long double v_err = 0.0 - node->q_dot;
            long double restoration_torque = kp_restoration * p_err + kv_restoration * v_err;
            node->torque = std::max(node->torque, matec_utils::clamp((double) restoration_torque / node->D_inverse, -sanity_torque_max, sanity_torque_max));
          }
          else if(node->q > node->max_q)
          {
            long double p_err = node->max_q - node->q;
            long double v_err = 0.0 - node->q_dot;
            long double restoration_torque = kp_restoration * p_err + kv_restoration * v_err;
            node->torque = std::min(node->torque, matec_utils::clamp((double) restoration_torque / node->D_inverse, -sanity_torque_max, sanity_torque_max));
          }
        }

        node->u = node->torque - node->axis.transpose() * node->pA - (node->viscous_friction * node->q_dot / node->D_inverse);
        matec_utils::Matrix6 Ia = Ia = node->articulated_body_inertia - node->U * node->D_inverse * node->U.transpose();
        node->pa = node->pA + Ia * node->c + node->U * node->D_inverse * node->u;
        node->parent->articulated_body_inertia += iXip_tranpose * Ia * node->iXip;
        node->parent->pA += iXip_tranpose * node->pa;
      }
      else
      {
        node->pa = node->pA + node->articulated_body_inertia * node->c;
        node->parent->articulated_body_inertia += iXip_tranpose * node->articulated_body_inertia * node->iXip;
        node->parent->pA += iXip_tranpose * node->pa;
      }
    }
  }

  //forward
  inline void DynamicsTree::hybridDynamicsPass3(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches)
  {
    boost::this_thread::interruption_point();
    if(node->parent)
    {
      if(node->dynamics_direction == DynamicsTreeNode::FORWARD_DYNAMICS)
      {
        matec_utils::Vector6 a_prime = node->iXip * node->parent->a + node->c;
        node->q_dot_dot = node->D_inverse * (node->u - node->U.transpose() * a_prime);
        node->a = a_prime + node->axis * node->q_dot_dot;
        if(node->joint_type != FIXED_JOINT)
        {
          accelerations.at(node->joint_idx) = node->q_dot_dot;
        }
      }
      else
      {
        node->a = node->iXip * node->parent->a + node->c;
        node->torque = node->axis.transpose() * (node->articulated_body_inertia * node->a + node->pA) + node->viscous_friction * node->q_dot;
        if(node->joint_type != FIXED_JOINT)
        {
          torques.at(node->joint_idx) = node->torque;
        }
      }
    }
  }
}

#endif //DYNAMICS_TREE_HYBRID_DYNAMICS_H
