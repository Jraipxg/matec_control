#ifndef DYNAMICS_TREE_INVERSE_DYNAMICS_H
#define DYNAMICS_TREE_INVERSE_DYNAMICS_H

namespace dynamics_tree
{
//  void DynamicsTree::inverseDynamics(matec_msgs::Odometry& root_link_odom, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, double gravity_magnitude) //current positions, velocities, accelerations => current torques
//  {
//    torques.resize(positions.size(), 0.0);
//
//    m_root_node->iTO = matec_utils::poseToMatrix(root_link_odom.pose);
//    matec_utils::Vector4 inertial_gravity, root_gravity;
//    inertial_gravity << 0.0, 0.0, -gravity_magnitude, 1.0;
//    root_gravity = m_root_node->iTO * inertial_gravity;
//    m_root_node->a.bottomRows(3) = -root_gravity.topRows(3);
//
//    if(m_root_node->joint_type == DynamicsTreeNode::FIXED_BASE)
//    {
//      m_root_node->v = matec_utils::Vector6::Zero();
//    }
//    else if(m_root_node->joint_type == DynamicsTreeNode::FLOATING_BASE)
//    {
//      m_root_node->v(0) = root_link_odom.velocity.angular.x;
//      m_root_node->v(1) = root_link_odom.velocity.angular.y;
//      m_root_node->v(2) = root_link_odom.velocity.angular.z;
//      m_root_node->v(3) = root_link_odom.velocity.linear.x;
//      m_root_node->v(4) = root_link_odom.velocity.linear.y;
//      m_root_node->v(5) = root_link_odom.velocity.linear.z;
//    }
//
//    std::vector<DynamicsTreeNode::RecursionOperation> forward, backward;
//    forward.push_back(boost::bind(&DynamicsTree::kinematicsForwardOperation, this, _1, _2, _3, _4, _5));
//    forward.push_back(boost::bind(&DynamicsTree::inverseDynamicsForwardOperation1, this, _1, _2, _3, _4, _5));
//    backward.push_back(boost::bind(&DynamicsTree::inverseDynamicsBackwardOperation2, this, _1, _2, _3, _4, _5));
//    m_root_node->recurse(forward, backward, positions, velocities, accelerations, torques);
//
//    forward.clear();
//    backward.clear();
//    forward.push_back(boost::bind(&DynamicsTree::inverseDynamicsForwardOperation3, this, _1, _2, _3, _4, _5));
//    m_root_node->recurse(forward, backward, positions, velocities, accelerations, torques);
//
//    if(m_root_node->joint_type == DynamicsTreeNode::FLOATING_BASE)
//    {
//      std::cerr << "ID base accel:\n" << m_root_node->a_base + m_root_node->a_base << std::endl;
//      std::cerr << "ID base force:\n" << (m_root_node->composite_inertia * m_root_node->a_base + m_root_node->pA) << std::endl;
////      std::cerr << "ID base inertia:\n" << m_root_node->composite_inertia << std::endl;
//    }
//  }
//
//  void DynamicsTree::inverseDynamicsForwardOperation1(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques)
//  {
//    if(node->parent)
//    {
//      node->q_dot = (node->joint_type == DynamicsTreeNode::FIXED_JOINT)? 0.0 : velocities.at(node->joint_idx);
//      node->q_dot_dot = (node->joint_type == DynamicsTreeNode::FIXED_JOINT)? 0.0 : accelerations.at(node->joint_idx);
//
//      matec_utils::Vector6 joint_spatial_velocity = (node->axis * node->q_dot);
//      node->v = (node->iXip * node->parent->v) + joint_spatial_velocity;
//      node->a = (node->iXip * node->parent->a) + (node->axis * node->q_dot_dot) + cross(node->v) * joint_spatial_velocity;
//      node->pA = node->link_inertia * node->a - cross(node->v).transpose() * node->link_inertia * node->v; // - transformed external forces
//      node->composite_inertia = node->link_inertia;
//    }
//    else
//    {
//      node->pA = Vector6::Zero(); //bad?
//      node->composite_inertia = node->link_inertia;
//    }
//  }
//
//  void DynamicsTree::inverseDynamicsBackwardOperation2(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques)
//  {
//    if(node->parent)
//    {
//      matec_utils::Matrix6 iXip_tranpose = node->iXip.transpose();
//      node->parent->pA += iXip_tranpose * node->pA;
//      node->parent->composite_inertia += iXip_tranpose * node->composite_inertia * node->iXip;
//    }
//  }
//
//  void DynamicsTree::inverseDynamicsForwardOperation3(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques)
//  {
//    if(node->parent)
//    {
////      std::cerr << "Joint " << node->joint_idx << " sees composite inertia " << std::endl << node->composite_inertia << std::endl;
//
//      node->a_base = node->iXip * node->parent->a_base;
//
//      if(node->joint_type != DynamicsTreeNode::FIXED_JOINT)
//      {
//        node->torque = node->axis.transpose() * (node->composite_inertia * node->a_base + node->pA);
//        torques[node->joint_idx] = node->torque;
//      }
//    }
//    else
//    {
//      if(node->joint_type == DynamicsTreeNode::FIXED_BASE)
//      {
//        node->a_base = Vector6::Zero(); //TODO: don't waste computation on a third recursion sweep when fixed base!
//      }
//      else
//      {
//        node->a_base = -node->composite_inertia.inverse() * node->pA;
//      }
//    }
//  }
}

#endif //DYNAMICS_TREE_INVERSE_DYNAMICS_H
