#ifndef STATIC_MASS_ADAPTER_H
#define STATIC_MASS_ADAPTER_H

#include "matec_dynamics_tree/dynamics_tree.h"

namespace dynamics_tree
{
  typedef std::map<std::string, std::string> SymmetryMap;

  class StaticMassAdapter
  {
    std::vector<std::vector<boost::shared_ptr<DynamicsTreeNode> > > m_evaluation_ordering; //list of symmetric sets

  public:
    StaticMassAdapter(DynamicsTree* tree, SymmetryMap map)
    {
      m_tree = tree;

      //generate symmetry
      std::vector<boost::shared_ptr<DynamicsTreeNode> > used_nodes;
      for(std::map<std::string, std::string>::iterator iter = map.begin(); iter != map.end(); iter++)
      { //TODO: don't assume that there are no duplicates in the symmetry map
        std::vector<boost::shared_ptr<DynamicsTreeNode> > symmetric_set; //TODO: handle more than two symmetric links
        boost::shared_ptr<DynamicsTreeNode> first_node = m_tree->getNodeByLink(iter->first);
        boost::shared_ptr<DynamicsTreeNode> second_node = m_tree->getNodeByLink(iter->second);
        if(!first_node || !second_node)
        {
          std::cerr << "BAD NODE LOOKUP FROM LINK!" << std::endl;
          continue;
        }
        symmetric_set.push_back(first_node);
        symmetric_set.push_back(second_node);
        used_nodes.push_back(first_node);
        used_nodes.push_back(second_node);
        m_evaluation_ordering.push_back(symmetric_set);
      }

      //gather the rest of the nodes
      std::map<std::string, boost::shared_ptr<DynamicsTreeNode> > nodes = m_tree->getNodes();
      for(std::map<std::string, boost::shared_ptr<DynamicsTreeNode> >::iterator iter = nodes.begin(); iter != nodes.end(); iter++)
      {
        bool already_used = false;
        for(unsigned int i = 0; i < used_nodes.size(); i++)
        {
          if(used_nodes[i] == iter->second)
          {
            already_used = true;
            break;
          }
        }
        if(already_used)
        {
          continue;
        }
        if(iter->second->mass == 1e-12) //don't count empty links //TODO: don't hardcode this horrible indicator
        {
          continue;
        }

        std::vector<boost::shared_ptr<DynamicsTreeNode> > symmetric_set;
        symmetric_set.push_back(iter->second);
        m_evaluation_ordering.push_back(symmetric_set);
      }
    }

    //returns error
    double process(matec_msgs::FullJointStates joint_states, matec_msgs::Odometry odom, matec_utils::Vector4 known_inertial_com_position, double known_total_mass, double error_scaling = 1.0)
    {
      //posi = 0Ti*[0;0;0;1]
      //com_posi = iTicom*[0;0;0;1]

      //when we're right:
      //(1) sum(mi): i in [1,n] = (const) known_total_mass
      //(2) sum(((const) 0Ti)*mi*com_posi): i in [1,n] = (const) known_inertial_com_position*known_total_mass

      //when we're not:
      //estimated_scaled_com = sum(m1*(0T1*1T1_com*[0;0;0;1])...mi*(0Ti*iTicom*[0;0;0;1]))
      //d(estimated_scaled_com)/d(mi*com_posi) = ((const) 0Ti)
      //d(estimated_scaled_com)/d(mi) = ((const) 0Ti)*com_posi
      //d(estimated_scaled_com)/d(com_posi) =  ((const) 0Ti)*mi

      //com_error = (known_inertial_com_position - estimated_scaled_com/known_total_mass)
      //delta(com_posi) = (((const) 0Ti)*mi).inverse()*com_error = mi*iT0*com_error
      //delta(mi) = ((const) 0Ti)*com_posi . com_error

      m_tree->kinematics(joint_states.position, odom);

      matec_utils::Vector4 estimated_com;
      m_tree->centerOfMass(odom.header.frame_id, estimated_com);
      matec_utils::Vector4 com_error = error_scaling * (known_inertial_com_position - estimated_com);
      com_error(3) = 0.0; //sometimes non-zero due to rounding

      //compute deltas
      double total_estimated_mass = 0.0;
      for(unsigned int i = 0; i < m_evaluation_ordering.size(); i++)
      {
        if(m_evaluation_ordering[i].size() == 0)
        {
          std::cerr << "EVALUATION ORDERING WAS NOT SET UP CORRECTLY!!!!" << std::endl;
          continue;
        }

        //average deltas from symmetric link sets
        matec_utils::Vector4 delta_com = matec_utils::Vector4::Zero();
        double delta_mass = 0.0;
        for(unsigned int j = 0; j < m_evaluation_ordering[i].size(); j++)
        {
          boost::shared_ptr<DynamicsTreeNode> node = m_evaluation_ordering[i][j];
          matec_utils::Vector4 OTicom = node->iTO.inverse() * node->iTicom.topRightCorner(4, 1);
          delta_com += node->mass * node->iTO * com_error;
          delta_mass += OTicom.dot(com_error);
        }

        delta_com /= (double) m_evaluation_ordering[i].size();
        delta_mass /= (double) m_evaluation_ordering[i].size();

        //apply the same deltas to each member of the symmetric set
        for(unsigned int j = 0; j < m_evaluation_ordering[i].size(); j++)
        {
          boost::shared_ptr<DynamicsTreeNode> node = m_evaluation_ordering[i][j];
          node->iTicom.topRightCorner(4, 1) += delta_com;
          node->mass += delta_mass;
          total_estimated_mass += node->mass;
        }
      }

      //normalize masses
      double mass_norm = known_total_mass / total_estimated_mass;
      for(unsigned int i = 0; i < m_evaluation_ordering.size(); i++)
      {
        for(unsigned int j = 0; j < m_evaluation_ordering[i].size(); j++)
        {
          boost::shared_ptr<DynamicsTreeNode> node = m_evaluation_ordering[i][j];
          node->mass = node->mass * mass_norm;
        }
      }

      return com_error(0) * com_error(0) + com_error(1) * com_error(1) + com_error(2) * com_error(2);
    }

  private:
    DynamicsTree* m_tree;
  };
}

#endif //STATIC_MASS_ADAPTER_H
