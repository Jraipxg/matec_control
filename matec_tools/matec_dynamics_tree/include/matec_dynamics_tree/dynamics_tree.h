#ifndef DYNAMICS_TREE_H
#define DYNAMICS_TREE_H

#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/DynamicsCommand.h>
#include <matec_msgs/StringArray.h>
#include <matec_msgs/Odometry.h>
#include "matec_utils/common_functions.h"
#include "matec_dynamics_tree/dynamics_tree_node.h"

namespace dynamics_tree
{
  class DynamicsTree
  {
  public:
    //initialization
    DynamicsTree();
    void load(std::vector<std::string>& joint_names, urdf::Model& model, bool floating_base = false);
    void configureDynamics(std::vector<DynamicsTreeNode::DynamicsDirection> dynamics_direction_config, std::vector<ExternalWrenchFrame> external_wrench_frame_config);

    //modification
    bool addFixedFrame(std::string parent, std::string child, matec_utils::Matrix4 cTp);
    bool updateFixedFrameOffset(std::string name, matec_utils::Matrix4 cTp);

    //dynamics
    void forwardDynamics(matec_msgs::Odometry& root_link_odom, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& torques, std::vector<double>& accelerations, double gravity_magnitude = 9.81); //current positions, velocities, torques => current accelerations
    void inverseDynamics(matec_msgs::Odometry& root_link_odom, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, double gravity_magnitude = 9.81); //current positions, velocities, accelerations => current torques
    void hybridDynamics(matec_msgs::Odometry& root_link_odom, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches, double gravity_magnitude = 9.81);

    //kinematics
    void kinematics(std::vector<double>& positions, matec_msgs::Odometry& root_link_odom); //propagates joint positions and computes transformation matrices to allow the use of transformation functions
    bool lookupTransform(std::string target_frame, std::string source_frame, matec_utils::Matrix4& transform);
    bool lookupMotionTransform(std::string target_frame, std::string source_frame, matec_utils::Matrix6& transform);
    bool transformVector(std::string target_frame, std::string source_frame, matec_utils::Vector3 input, matec_utils::Vector3& output);
    bool transformPoint(std::string target_frame, std::string source_frame, matec_utils::Vector4 input, matec_utils::Vector4& output);
    bool transformPoint(std::string target_frame, std::string source_frame, matec_utils::Vector3 input, matec_utils::Vector3& output);
    bool transformPoint(std::string target_frame, geometry_msgs::PointStamped point_in, geometry_msgs::PointStamped& point_out);
    bool transformPose(std::string target_frame, std::string source_frame, matec_utils::Matrix4 input, matec_utils::Matrix4& output);
    bool transformPose(std::string target_frame, geometry_msgs::PoseStamped pose_in, geometry_msgs::PoseStamped& pose_out);
    bool transformTwist(std::string target_frame, std::string source_frame, matec_utils::Vector6 input, matec_utils::Vector6& output);
    bool transformTwist(std::string target_frame, geometry_msgs::TwistStamped twist_in, geometry_msgs::TwistStamped& twist_out);
    bool transformWrench(std::string target_frame, std::string source_frame, matec_utils::Vector6 input, matec_utils::Vector6& output);
    bool transformWrench(std::string target_frame, geometry_msgs::WrenchStamped wrench_in, geometry_msgs::WrenchStamped& wrench_out);

    //meta properties of a tree (assumes that kinematics and / or dynamics routines have already been performed)
    void computeSupportedCentersOfMassRecursive(boost::shared_ptr<DynamicsTreeNode> node);
    void supportedCenterOfMass(boost::shared_ptr<DynamicsTreeNode> node, matec_utils::Vector4& com, double& supported_mass);
    void centerOfMass(std::string target_frame ,matec_utils::Vector4& com);
    void centerOfMass(std::string target_frame, geometry_msgs::PointStamped& com);
    void centerOfPressure();

    bool getSupportingSubset(std::vector<unsigned int> desired_joint_indices, std::string tool_frame, std::vector<unsigned int>& supporting_joint_indices);
    bool jacobian(std::vector<unsigned int> joint_indices, std::string goal_frame, std::string tool_frame, matec_utils::Matrix& jacobian);
    bool cartesianVelocityToJointVelocities(std::vector<unsigned int> joint_indices, matec_utils::Vector6 twist, std::string goal_frame, std::string tool_frame, std::vector<double>& joint_velocities);

    //node access
    std::map<std::string, boost::shared_ptr<DynamicsTreeNode> > getNodes()
    {
      return m_nodes;
    }
    boost::shared_ptr<DynamicsTreeNode> getNodeByLink(std::string link_name)
    {
      std::map<std::string, boost::shared_ptr<DynamicsTreeNode> >::iterator node_iter = m_nodes.find(link_name);
      if(node_iter == m_nodes.end())
      {
        return boost::shared_ptr<DynamicsTreeNode>();
      }
      return node_iter->second;
    }
    boost::shared_ptr<DynamicsTreeNode> getNodeByJoint(std::string joint_name)
    {
      if(m_joint_name_to_node.find(joint_name) != m_joint_name_to_node.end())
      {
        return m_joint_name_to_node[joint_name];
      }
      else
      {
        return boost::shared_ptr<DynamicsTreeNode>();
      }
    }
    boost::shared_ptr<DynamicsTreeNode> getNodeByIndex(unsigned int joint_idx)
    {
      if(m_joint_index_to_node.find(joint_idx) != m_joint_index_to_node.end())
      {
        return m_joint_index_to_node[joint_idx];
      }
      else
      {
        return boost::shared_ptr<DynamicsTreeNode>();
      }
    }

    void setNode(boost::shared_ptr<DynamicsTreeNode> node)
    {
      m_nodes[node->link_name] = node;
      if(node->joint_type != FIXED_JOINT)
      {
        m_joint_index_to_node[node->joint_idx] = node;
        m_joint_name_to_node[node->joint_name] = node;
      }
    }

    void setRootNode(boost::shared_ptr<DynamicsTreeNode> node)
    {
      m_root_node = node;
    }

    boost::shared_ptr<DynamicsTreeNode> getRootNode()
    {
      return m_root_node;
    }

    //getters and setters
    void setFloatingBase(bool floating_base)
    {
      m_floating_base = floating_base;
    }

    void setJointNames(std::vector<std::string> joint_names)
    {
      m_joint_names = joint_names;
    }

    void setTotalMass(double mass)
    {
      m_total_mass = mass;
    }

    double getTotalMass()
    {
      return m_total_mass;
    }

    std::string getInertialFrameName()
    {
      return m_inertial_frame_name;
    }

    void reset()
    {
      m_root_node.reset();
      m_nodes.clear();
      m_joint_index_to_node.clear();
      m_joint_name_to_node.clear();
      m_joint_names.clear();
      m_floating_base = false;
      m_total_mass = 0.0;
    }

  protected:
    boost::shared_ptr<DynamicsTreeNode> m_root_node;
    std::map<std::string, boost::shared_ptr<DynamicsTreeNode> > m_nodes;
    std::map<int, boost::shared_ptr<DynamicsTreeNode> > m_joint_index_to_node;
    std::map<std::string, boost::shared_ptr<DynamicsTreeNode> > m_joint_name_to_node;
    std::vector<std::string> m_joint_names;
    bool m_floating_base;
    double m_total_mass;

    bool m_inertial_frame_name_set;
    std::string m_inertial_frame_name;
    //todo: add num joints

    boost::shared_ptr<DynamicsTreeNode> loadRecursive(boost::shared_ptr<const urdf::Link> link, boost::shared_ptr<DynamicsTreeNode> parent);

    void kinematicsInitialization(matec_msgs::Odometry& root_link_odom);
    void kinematicsPass(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches);

    void hybridDynamicsPass1(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches);
    void hybridDynamicsPass2(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches);
    void hybridDynamicsPass3(boost::shared_ptr<DynamicsTreeNode> node, std::vector<double>& positions, std::vector<double>& velocities, std::vector<double>& accelerations, std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches);
  };
}

#include <matec_dynamics_tree/dynamics_tree_initialization.h>
#include <matec_dynamics_tree/dynamics_tree_kinematics.h>
//#include <matec_dynamics_tree/dynamics_tree_forward_dynamics.h>
#include <matec_dynamics_tree/dynamics_tree_hybrid_dynamics.h>
//#include <matec_dynamics_tree/dynamics_tree_inverse_dynamics.h>
#include <matec_dynamics_tree/dynamics_tree_meta_properties.h>

#endif //DYNAMICS_TREE_H
