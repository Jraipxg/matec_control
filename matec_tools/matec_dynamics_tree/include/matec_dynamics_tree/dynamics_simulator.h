#ifndef DYNAMICS_SIMULATOR_H
#define DYNAMICS_SIMULATOR_H

#include "matec_dynamics_tree/dynamics_graph.h"
#include "matec_dynamics_tree/dynamics_tree.h"
#include "matec_utils/runge_kutta.h"

namespace dynamics_tree
{
  class DynamicsSimulator
  {
  public:
    DynamicsSimulator()
    {
    }

    void loadFromURDF(std::vector<std::string> joint_names, urdf::Model urdf_model, std::string root_link, bool floating_base = false)
    {
      m_time = 0.0;
      m_num_joints = joint_names.size();
      m_num_states = m_num_joints * 2.0 + 12.0;
      m_current_state = matec_utils::Vector::Zero(m_num_states);
      m_num_substeps = 1;
      m_absolute_max_num_substeps = 15;
      m_error_min = 1e-2;
      m_error_max = 0.5;

      //higher fidelity:
//      m_absolute_max_num_substeps = 50;
//      m_error_min = 1e-9;
//      m_error_max = 1e-6;

      m_graph.loadFromURDF(joint_names, urdf_model);
      m_graph.print(root_link);
      m_graph.spawnDynamicsTree(root_link, floating_base, m_tree);

//      m_tree.load(joint_names, urdf_model, floating_base);

      std::vector<DynamicsTreeNode::DynamicsDirection> dynamics_direction_config(m_num_joints, DynamicsTreeNode::FORWARD_DYNAMICS);
      std::vector<ExternalWrenchFrame> external_wrench_frame_config(m_num_joints, COM_FRAME);
      m_tree.configureDynamics(dynamics_direction_config, external_wrench_frame_config);
    }

    void loadFromParameterServer(std::vector<std::string> joint_names, std::string root_link = "", bool floating_base = false, std::string parameter_name = "/robot_description")
    {
      ros::NodeHandle nh("~");
      while(!nh.hasParam(parameter_name) && ros::ok())
      {
        ROS_WARN_THROTTLE(0.5, "%s waiting for /robot_description parameter!", nh.getNamespace().c_str());
      }

      urdf::Model urdf_model;
      urdf_model.initParam(parameter_name);

      if(root_link.length() == 0)
      {
        root_link = urdf_model.getRoot()->name;
      }

      loadFromURDF(joint_names, urdf_model, root_link, floating_base);
    }

    void initialize(matec_msgs::FullJointStates& joint_states, matec_msgs::Odometry& root_link_odom)
    {
      packState(joint_states, root_link_odom, m_current_state);
    }

    void simulationStep(std::vector<double>& torques, std::vector<matec_utils::Vector6>& external_wrenches, matec_msgs::FullJointStates& next_joint_states, matec_msgs::Odometry& next_odom, double dt = 0.001)
    {
      m_current_torques = torques;
      m_external_wrenches = external_wrenches;

      //TODO: make flag for nan checking
//      for(unsigned int i = 0; i < m_external_wrenches.size(); i++)
//      {
//        for(unsigned int j = 0; j < 6; j++)
//        {
//          if(isnan((double) m_external_wrenches[i](j)) || isinf((double) m_external_wrenches[i](j)))
//          {
//            std::cerr << "BAD WRENCH COMPONENT (" << i << "," << j << ") DETECTED: " << m_external_wrenches[i](j) << std::endl;
//          }
//        }
//      }

//TODO: make flag to set integration method / sim fidelity level
//      matec_utils::Vector next_state = m_current_state + dt * calculateDerivative(m_time, m_current_state); //euler integration
      matec_utils::Vector next_state = matec_utils::integrateAdaptiveStep<matec_utils::Vector>(m_time, m_current_state, m_num_substeps, dt, boost::bind(&DynamicsSimulator::calculateDerivative, this, _1, _2), boost::bind(&DynamicsSimulator::calculateError, this, _1, _2), m_error_min, m_error_max, m_absolute_max_num_substeps);

      m_time += dt;
      unpackState(next_joint_states, next_odom, m_current_state, next_state, dt);
      m_current_state = next_state;
    }

    //version with no external torques
    void simulationStep(std::vector<double>& torques, matec_msgs::FullJointStates& next_joint_states, matec_msgs::Odometry& next_odom, double dt = 0.001)
    {
      std::vector<matec_utils::Vector6> external_wrenches(torques.size(), matec_utils::Vector6::Zero()); //todo: buffer and don't recalculate!
      simulationStep(torques, external_wrenches, next_joint_states, next_odom, dt);
    }

  protected:
    double m_time;
    std::vector<double> m_current_torques;
    std::vector<matec_utils::Vector6> m_external_wrenches;
    matec_utils::Vector m_current_state;
    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;
    unsigned int m_num_joints;
    unsigned int m_num_states;
    unsigned int m_num_substeps;
    double m_error_min;
    double m_error_max;
    unsigned int m_absolute_max_num_substeps;

    void unpackState(matec_msgs::FullJointStates& joint_states, matec_msgs::Odometry& odom, matec_utils::Vector& current_state, matec_utils::Vector& next_state, double dt)
    {
      joint_states.header.stamp = ros::Time(m_time);
      joint_states.position.resize(m_num_joints);
      joint_states.velocity.resize(m_num_joints);
      joint_states.acceleration.resize(m_num_joints);

      for(unsigned int i = 0; i < m_num_joints; i++)
      {
        joint_states.position.at(i) = next_state(i + 12);
        joint_states.velocity.at(i) = next_state(i + m_num_joints + 12);
        joint_states.acceleration.at(i) = (next_state(i + m_num_joints + 12) - current_state(i + m_num_joints + 12)) / dt;
      }
      joint_states.torque = m_current_torques;

      odom.pose.position.x = next_state(0);
      odom.pose.position.y = next_state(1);
      odom.pose.position.z = next_state(2);
      odom.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw((double) next_state(3), (double) next_state(4), (double) next_state(5));
      odom.velocity.linear.x = next_state(6);
      odom.velocity.linear.y = next_state(7);
      odom.velocity.linear.z = next_state(8);
      odom.velocity.angular.x = next_state(9);
      odom.velocity.angular.y = next_state(10);
      odom.velocity.angular.z = next_state(11);
      odom.acceleration.linear.x = (next_state(6) - current_state(6)) / dt;
      odom.acceleration.linear.y = (next_state(7) - current_state(7)) / dt;
      odom.acceleration.linear.z = (next_state(8) - current_state(8)) / dt;
      odom.acceleration.angular.x = (next_state(9) - current_state(9)) / dt;
      odom.acceleration.angular.y = (next_state(10) - current_state(10)) / dt;
      odom.acceleration.angular.z = (next_state(11) - current_state(11)) / dt;
    }

    void packState(matec_msgs::FullJointStates& joint_states, matec_msgs::Odometry& odom, matec_utils::Vector& state)
    {
      state.resize(m_num_states);
      double roll, pitch, yaw;
      matec_utils::quaternionToRPY(odom.pose.orientation, roll, pitch, yaw);
      state(0) = odom.pose.position.x;
      state(1) = odom.pose.position.y;
      state(2) = odom.pose.position.z;
      state(3) = roll;
      state(4) = pitch;
      state(5) = yaw;
      state(6) = odom.velocity.linear.x;
      state(7) = odom.velocity.linear.y;
      state(8) = odom.velocity.linear.z;
      state(9) = odom.velocity.angular.x;
      state(10) = odom.velocity.angular.y;
      state(11) = odom.velocity.angular.z;
      for(unsigned int i = 0; i < m_num_joints; i++)
      {
        state(i + 12) = joint_states.position.at(i);
        state(i + m_num_joints + 12) = joint_states.velocity.at(i);
      }
    }

    double calculateError(matec_utils::Vector s1, matec_utils::Vector s2)
    {
      return (s1 - s2).norm();
    }

    matec_utils::Vector calculateDerivative(double t, matec_utils::Vector state)
    {
      bool bad = false;
      matec_utils::Vector derivative = matec_utils::Vector::Zero(m_num_states);

      //unpack
      matec_msgs::Odometry odom;
      odom.pose.position.x = state(0);
      odom.pose.position.y = state(1);
      odom.pose.position.z = state(2);
      odom.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw((double) state(3), (double) state(4), (double) state(5));
      odom.velocity.linear.x = state(6);
      odom.velocity.linear.y = state(7);
      odom.velocity.linear.z = state(8);
      odom.velocity.angular.x = state(9);
      odom.velocity.angular.y = state(10);
      odom.velocity.angular.z = state(11);

      for(unsigned int i = 0; i < state.rows(); i++)
      {
        if(isnan((double) state(i)))
        {
          std::cerr << "NAN INPUT STATE AT POSITION " << i << "!!!" << std::endl;
          bad = true;
        }
      }

      std::vector<double> joint_positions(m_num_joints), joint_velocities(m_num_joints), joint_accelerations(m_num_joints);
      for(unsigned int i = 0; i < m_num_joints; i++)
      {
        joint_positions.at(i) = state(i + 12);
        joint_velocities.at(i) = state(i + m_num_joints + 12);
      }

      //solve dynamics
      m_tree.hybridDynamics(odom, joint_positions, joint_velocities, joint_accelerations, m_current_torques, m_external_wrenches); //Zero order hold: torques and wrenches (through wrenches shouldn't) stay constant across the time interval

      //position derivative is old velocity state
      for(int i = 0; i < 6; i++)
      {
        derivative(i) = state(i + 6);
      }
      for(unsigned int i = 0; i < m_num_joints; i++)
      {
        derivative(i + 12) = state(i + m_num_joints + 12);
      }

      //velocity derivative is new acceleration
      for(unsigned int i = 0; i < m_num_joints; i++)
      {
        derivative(i + m_num_joints + 12) = joint_accelerations.at(i);
      }
      derivative(6) = odom.acceleration.linear.x;
      derivative(7) = odom.acceleration.linear.y;
      derivative(8) = odom.acceleration.linear.z;
      derivative(9) = odom.acceleration.angular.x;
      derivative(10) = odom.acceleration.angular.y;
      derivative(11) = odom.acceleration.angular.z;

      for(unsigned int i = 0; i < derivative.rows(); i++)
      {
        if(isnan((double) derivative(i)))
        {
          std::cerr << "NAN OUTPUT STATE AT POSITION " << i << "!!!" << std::endl;
          bad = true;
        }
      }

      if(bad)
      {
        std::cerr << "(root vel: 0-5, root acc: 6-11, joint velocity: 12-" << (m_num_joints - 1) + 12 << ", joint accelerations: " << m_num_joints + 12 << "-" << (2 * m_num_joints - 1) + 12 << ", last idx: " << derivative.rows() - 1 << ")" << std::endl;
        std::cerr << "================================================================================" << std::endl;
      }

      return derivative;
    }
  };
}

#endif //DYNAMICS_SIMULATOR_H
