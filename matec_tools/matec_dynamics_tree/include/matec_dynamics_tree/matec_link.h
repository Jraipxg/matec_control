#ifndef MATEC_LINK_H
#define MATEC_LINK_H

namespace dynamics_tree
{
  class Joint;

  //frame in which external wrenches are specified
  enum ExternalWrenchFrame
  {
    NO_FRAME,
    INERTIAL_FRAME,
    JOINT_FRAME,
    COM_FRAME,
    NUM_EXTERNAL_WRENCH_FRAME_TYPES
  };
  static std::string external_wrench_frame_strings[NUM_EXTERNAL_WRENCH_FRAME_TYPES] = {"no frame", "inertial frame", "joint frame", "com frame"};

  class Link
  {
  public:
    std::string name; //name of the link
    double mass; //total mass of the link
    matec_utils::Vector3 com_position; //position of the CoM in link coordinates
    matec_utils::Matrix3 com_inertia; //inertia at the CoM
    matec_utils::Matrix6 spatial_inertia; //inertia of the current link
    matec_utils::Matrix4 linkToriginallink; //homogenous transform from the original frame location to the shifted dynamics tree version
    matec_utils::Matrix4 originallinkTlinkcom; //homogenous transform from center of mass to link frame
    ExternalWrenchFrame external_wrench_frame; //frame in which external wrenches should be expressed

    std::vector<boost::shared_ptr<Joint> > connected_joints;
    std::vector<matec_utils::Matrix4> jointToriginallink; //transform from link coordinates to joint coordinates

    Link()
    {
      name = "";
      mass = 1e-12; //small
      com_position = matec_utils::Vector3::Zero();
      com_inertia = matec_utils::Matrix3::Identity() * mass;
      spatial_inertia = matec_utils::Matrix6::Identity();
      originallinkTlinkcom = matec_utils::Matrix4::Identity();
      external_wrench_frame = NO_FRAME;
    }

    unsigned int findConnectedJointIndex(boost::shared_ptr<Joint> joint)
    {
      for(unsigned int i = 0; i < connected_joints.size(); i++)
      {
        if(joint == connected_joints[i])
        {
          return i;
        }
      }
      return connected_joints.size();
    }
  };
}

#endif //MATEC_LINK_H
