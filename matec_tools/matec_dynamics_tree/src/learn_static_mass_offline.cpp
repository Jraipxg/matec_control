#include <cmath>
#include <set>
#include <csignal>

#include "matec_dynamics_tree/dynamics_graph.h"
#include "matec_dynamics_tree/static_mass_adapter.h"
#include "matec_utils/common_initialization_components.h"

bool die;
void dead_handler(int x)
{
  std::cerr << "TRYING TO DIE" << std::endl;
  die = true;
}

struct DataType
{
  matec_msgs::FullJointStates joint_states;
  matec_msgs::Odometry odom;
  matec_utils::Vector4 known_com;
};

std::vector<DataType> parseDataFile(std::string filename)
{
  std::vector<DataType> dataset;

  std::ifstream file(filename.c_str());
  boost::char_separator<char> sep(" ,");
  unsigned long lines_read = 0;
  if(file.is_open())
  {
    ROS_INFO_STREAM("Parsing data file " << filename);
    std::string line;
    getline(file, line); //first line contains labels, ignore them for now
    while(getline(file, line))
    {
      std::vector<double> values = matec_utils::parameterStringToFPVector(line);

      DataType data;
      data.odom.pose.position.x = values[0];
      data.odom.pose.position.y = values[1];
      data.odom.pose.position.z = values[2];
      data.odom.pose.orientation.w = values[3];
      data.odom.pose.orientation.x = values[4];
      data.odom.pose.orientation.y = values[5];
      data.odom.pose.orientation.z = values[6];

      for(unsigned int i = 7; i < values.size() - 2; i++)
      {
        data.joint_states.joint_indices.push_back(i - 7);
        data.joint_states.position.push_back(values[i]);
        data.joint_states.velocity.push_back(0.0);
        data.joint_states.acceleration.push_back(0.0);
        data.joint_states.torque.push_back(0.0);
      }

      data.known_com(0) = values[values.size() - 2];
      data.known_com(1) = values[values.size() - 1];
      data.known_com(2) = 0.0;
      data.known_com(3) = 1.0;

      dataset.push_back(data);
      lines_read++;
    }
    file.close();
  }
  else
  {
    ROS_ERROR("Couldn't open file!");
  }
  ROS_INFO_STREAM("Parsing complete! Read " << lines_read << " lines!");

  return dataset;
}

int main(int argc, char* argv[])
{
  die = false;
  if(argc != 2)
  {
    std::cerr << "Args: [filename]" << std::endl;
    return 0;
  }
  std::vector<DataType> data = parseDataFile(argv[1]);

  ros::init(argc, argv, "learn_static_mass_offline");

  double total_mass = 177.05; //todo: stop hardcoding all the things

  //TODO: allow loading from urdf file directly

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  dynamics_tree::DynamicsGraph graph;
  graph.loadFromParameterServer(joint_names);
  dynamics_tree::DynamicsTree tree;
  graph.spawnDynamicsTree("pelvis", true, tree); //TODO: param

  dynamics_tree::SymmetryMap symmetry_map;
  symmetry_map["l_uglut"] = "r_uglut";
  symmetry_map["l_lglut"] = "r_lglut";
  symmetry_map["l_uleg"] = "r_uleg";
  symmetry_map["l_lleg"] = "r_lleg";
  symmetry_map["l_talus"] = "r_talus";
  symmetry_map["l_foot"] = "r_foot";

  symmetry_map["l_clav"] = "r_clav";
  symmetry_map["l_scap"] = "r_scap";
  symmetry_map["l_uarm"] = "r_uarm";
  symmetry_map["l_larm"] = "r_larm";
  symmetry_map["l_farm"] = "r_farm";
  symmetry_map["l_hand"] = "r_hand";

  dynamics_tree::StaticMassAdapter adapter(&tree, symmetry_map);

  signal(SIGINT, dead_handler);

  //learn
  unsigned int num_epochs = 100;
  double com_mse = 0.0;
  for(unsigned int e = 0; e < num_epochs; e++)
  {
    if(die)
    {
      break;
    }
    for(unsigned int i = 0; i < data.size(); i++)
    {
      if(die)
      {
        break;
      }
      com_mse += adapter.process(data[i].joint_states, data[i].odom, data[i].known_com, total_mass, 1e-6);
    }
    if(e % 10 == 0)
    {
      std::cerr << e << "/" << num_epochs << " epochs completed! Current error is " << com_mse << std::endl;
    }
  }

  //print
  std::map<std::string, boost::shared_ptr<dynamics_tree::DynamicsTreeNode> > nodes = tree.getNodes();
  for(std::map<std::string, boost::shared_ptr<dynamics_tree::DynamicsTreeNode> >::iterator iter = nodes.begin(); iter != nodes.end(); iter++)
  {
    boost::shared_ptr<dynamics_tree::DynamicsTreeNode> node = iter->second;
    std::cerr << node->link_name << ":" << std::endl;
    std::cerr << "  iTicom pos:" << node->iTicom.topRightCorner(4, 1).transpose() << std::endl;
    std::cerr << "  mass:" << node->mass << std::endl;
  }
}
