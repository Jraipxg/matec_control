#include "matec_dynamics_tree/dynamics_graph.h"
#include "ros/package.h"
#include "sensor_msgs/JointState.h"

int main(int argc, char **argv)
{
//  std::string urdf_path = ros::package::getPath("matec_utils") + "/garm_7_dof_test.urdf";
  std::string urdf_path = ros::package::getPath("matec_dynamics_tree") + "/atlas_test.urdf";
  urdf::Model urdf_model;
  urdf_model.initFile(urdf_path);

  std::vector<boost::shared_ptr<urdf::Link> > links;
  urdf_model.getLinks(links);

  std::vector<std::string> joint_names;
  for(unsigned int i = 0; i < links.size(); i++)
  {
    if(links.at(i)->parent_joint)
    {
      joint_names.push_back(links.at(i)->parent_joint->name);
    }
  }

  matec_msgs::Odometry odom;
  odom.pose.orientation.w = 1.0;

  std::vector<double> positions(joint_names.size(), 0.0);
  std::vector<double> velocities(joint_names.size(), 0.0);
  std::vector<double> accelerations(joint_names.size(), 0.0);
  std::vector<double> torques(joint_names.size(), 0.0);
  std::vector<matec_utils::Vector6> external_wrenches(joint_names.size(), matec_utils::Vector6::Zero());
  std::vector<dynamics_tree::DynamicsTreeNode::DynamicsDirection> dynamics_direction_config(joint_names.size(), dynamics_tree::DynamicsTreeNode::INVERSE_DYNAMICS);
  std::vector<dynamics_tree::ExternalWrenchFrame> external_wrench_frame_config(joint_names.size(), dynamics_tree::NO_FRAME);

  dynamics_tree::DynamicsGraph graph;
  dynamics_tree::DynamicsTree tree;
  graph.loadFromURDF(joint_names, urdf_model);

  for(unsigned int i = 0; i < links.size(); i++)
  {
    std::cerr << "USING LINK " << links[i]->name << " AS ROOT: " << std::endl;
    graph.print(links[i]->name);
    graph.spawnDynamicsTree(links[i]->name, false, tree);

    tree.configureDynamics(dynamics_direction_config, external_wrench_frame_config);
    tree.hybridDynamics(odom, positions, velocities, accelerations, torques, external_wrenches);

    for(unsigned int j = 0; j < torques.size(); j++)
    {
      std::cerr << joint_names[j] << ":\t" << torques[j] << std::endl;
    }
    std::cerr << "=====================================================" << std::endl;
  }

}
