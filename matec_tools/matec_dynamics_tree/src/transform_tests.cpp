#include "matec_dynamics_tree/dynamics_graph.h"
#include "matec_dynamics_tree/dynamics_tree.h"
#include "ros/package.h"
#include "sensor_msgs/JointState.h"
#include "kdl_parser/kdl_parser.hpp"
#undef NDEBUG
#include <cassert>

#define WHITE_TEXT  "\033[0m"
#define RED_TEXT "\033[31m"

struct TestSpec
{
  unsigned int num_joints;
  std::vector<double> min_pos;
  std::vector<double> max_pos;
  std::vector<unsigned int> pos_steps;
  double min_odom_pos;
  double max_odom_pos;
  unsigned int odom_pos_steps;
  double min_odom_rot;
  double max_odom_rot;
  unsigned int odom_rot_steps;
};

class SegmentPair
{
public:
  SegmentPair(const KDL::Segment& p_segment, const std::string& p_root, const std::string& p_tip) :
      segment(p_segment), root(p_root), tip(p_tip)
  {
  }

  KDL::Segment segment;
  std::string root, tip;
};

std::string m_root_name;
std::string m_inertial_name;
std::vector<std::string> m_link_names;
std::vector<std::string> m_joint_names;
std::vector<int> m_kdl_joint_map;
unsigned long m_total_num_tests;
std::map<std::string, SegmentPair> m_kdl_segments;
std::map<std::string, SegmentPair> m_kdl_fixed_segments;

void loadTransformer(std::vector<double>& positions, matec_msgs::Odometry& odom, tf::Transformer& transformer)
{
  //add odometry
  geometry_msgs::PoseStamped odom_pose;
  odom_pose.pose = odom.pose;
  odom_pose.header = odom.header;
  transformer.setTransform(matec_utils::poseStampedToStampedTransform(odom_pose, m_root_name));

  tf::StampedTransform trans;
  for(unsigned int i = 0; i < positions.size(); i++)
  {
    std::string joint_name = m_joint_names.at(i);
    double position = positions.at(i);
    std::map<std::string, SegmentPair>::const_iterator seg = m_kdl_segments.find(joint_name);
    if(seg != m_kdl_segments.end())
    {
      tf::transformKDLToTF(seg->second.segment.pose(position), trans);
      trans.frame_id_ = seg->second.root;
      trans.child_frame_id_ = seg->second.tip;
      transformer.setTransform(trans);
    }
  }

  for(std::map<std::string, SegmentPair>::const_iterator seg = m_kdl_fixed_segments.begin(); seg != m_kdl_fixed_segments.end(); seg++)
  {
    tf::transformKDLToTF(seg->second.segment.pose(0), trans);
    trans.frame_id_ = seg->second.root;
    trans.child_frame_id_ = seg->second.tip;
    transformer.setTransform(trans);
  }
}

void doCompare(unsigned long& num_tests_completed, unsigned long& num_tests_failed, dynamics_tree::DynamicsTree* dynamics_tree, matec_msgs::Odometry& odom, std::vector<double>& positions)
{
  tf::Transformer transformer;
  loadTransformer(positions, odom, transformer);
  dynamics_tree->kinematics(positions, odom);

  //compare
  bool fail = false;
  for(unsigned int i = 0; i < m_link_names.size(); i++)
  {
    matec_utils::Matrix4 rootTlink, inertialTlink, linkTinertial, linkTroot;
    dynamics_tree->lookupTransform(m_root_name, m_link_names.at(i), rootTlink);
    dynamics_tree->lookupTransform(m_inertial_name, m_link_names.at(i), inertialTlink);
    dynamics_tree->lookupTransform(m_link_names.at(i), m_inertial_name, linkTinertial);
    dynamics_tree->lookupTransform(m_link_names.at(i), m_root_name, linkTroot);

    tf::StampedTransform tf_rootTlink, tf_inertialTlink, tf_linkTinertial, tf_linkTroot;
    transformer.lookupTransform(m_root_name, m_link_names.at(i), ros::Time(0), tf_rootTlink);
    transformer.lookupTransform(m_inertial_name, m_link_names.at(i), ros::Time(0), tf_inertialTlink);
    transformer.lookupTransform(m_link_names.at(i), m_inertial_name, ros::Time(0), tf_linkTinertial);
    transformer.lookupTransform(m_link_names.at(i), m_root_name, ros::Time(0), tf_linkTroot);

    matec_utils::Matrix4 tf_matrix_rootTlink = matec_utils::stampedTransformToMatrix(tf_rootTlink);
    matec_utils::Matrix4 tf_matrix_inertialTlink = matec_utils::stampedTransformToMatrix(tf_inertialTlink);
    matec_utils::Matrix4 tf_matrix_linkTinertial = matec_utils::stampedTransformToMatrix(tf_linkTinertial);
    matec_utils::Matrix4 tf_matrix_linkTroot = matec_utils::stampedTransformToMatrix(tf_linkTroot);

    matec_utils::Matrix4 rootTlink_diff = rootTlink - tf_matrix_rootTlink;
    matec_utils::Matrix4 inertialTlink_diff = inertialTlink - tf_matrix_inertialTlink;
    matec_utils::Matrix4 linkTinertial_diff = linkTinertial - tf_matrix_linkTinertial;
    matec_utils::Matrix4 linkTroot_diff = linkTroot - tf_matrix_linkTroot;

    double rootTlink_err = rootTlink_diff.norm();
    double inertialTlink_err = inertialTlink_diff.norm();
    double linkTinertial_err = linkTinertial_diff.norm();
    double linkTroot_err = linkTroot_diff.norm();
    double total_err = rootTlink_err + inertialTlink_err + linkTinertial_err + linkTroot_err;

    if(fabs(total_err) > 1e-6 || isnan(total_err) || isinf(total_err))
    {
      fail = true;
      std::cerr << RED_TEXT;
      std::cerr << "FAIL: " << rootTlink_err << ", " << inertialTlink_err << ", " << linkTinertial_err << ", " << linkTroot_err << std::endl;
      std::cerr << "rootTlink_diff" << std::endl << rootTlink_diff << std::endl;
      std::cerr << "inertialTlink_diff" << std::endl << inertialTlink_diff << std::endl;
      std::cerr << "linkTinertial_diff" << std::endl << linkTinertial_diff << std::endl;
      std::cerr << "linkTroot_diff" << std::endl << linkTroot_diff << std::endl;
      std::cerr << WHITE_TEXT << std::endl;
    }
  }
  num_tests_completed++;
  if(num_tests_completed % 25000 == 0)
  {
    std::cerr << "Processed test #" << num_tests_completed << " / " << m_total_num_tests << ". " << 100.0 * (1.0 - (((double) num_tests_failed) / ((double) num_tests_completed))) << "% success so far!" << std::endl;
  }
  if(fail)
  {
    num_tests_failed++;
  }
}

void compare(unsigned int recursion_depth, TestSpec& spec, unsigned long& num_tests_completed, unsigned long& num_tests_failed, dynamics_tree::DynamicsTree* dynamics_tree, matec_msgs::Odometry& odom, std::vector<double>& positions)
{
  if(recursion_depth == spec.num_joints)
  {
    //generate odometry
    for(unsigned int x = 0; x < spec.odom_pos_steps; x++)
    {
      odom.pose.position.x = spec.min_odom_pos + (spec.max_odom_pos - spec.min_odom_pos) * ((double) x) / ((double) spec.odom_pos_steps - 1.0);
      for(unsigned int y = 0; y < spec.odom_pos_steps; y++)
      {
        odom.pose.position.y = spec.min_odom_pos + (spec.max_odom_pos - spec.min_odom_pos) * ((double) y) / ((double) spec.odom_pos_steps - 1.0);
        for(unsigned int z = 0; z < spec.odom_pos_steps; z++)
        {
          odom.pose.position.z = spec.min_odom_pos + (spec.max_odom_pos - spec.min_odom_pos) * ((double) z) / ((double) spec.odom_pos_steps - 1.0);
          for(unsigned int R = 0; R < spec.odom_rot_steps; R++)
          {
            for(unsigned int P = 0; P < spec.odom_rot_steps; P++)
            {
              for(unsigned int Y = 0; Y < spec.odom_rot_steps; Y++)
              {
                double roll = spec.min_odom_rot + (spec.max_odom_rot - spec.min_odom_rot) * ((double) R) / ((double) spec.odom_rot_steps - 1.0);
                double pitch = spec.min_odom_rot + (spec.max_odom_rot - spec.min_odom_rot) * ((double) P) / ((double) spec.odom_rot_steps - 1.0);
                double yaw = spec.min_odom_rot + (spec.max_odom_rot - spec.min_odom_rot) * ((double) Y) / ((double) spec.odom_rot_steps - 1.0);
                odom.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);

                //perform the comparisons
                doCompare(num_tests_completed, num_tests_failed, dynamics_tree, odom, positions);
              }
            }
          }
        }
      }
    }
    return;
  }

  //generate joint positions
  for(unsigned int q = 0; q < spec.pos_steps.at(recursion_depth); q++)
  {
    positions.at(recursion_depth) = spec.min_pos.at(recursion_depth) + (spec.max_pos.at(recursion_depth) - spec.min_pos.at(recursion_depth)) * ((double) q) / ((double) spec.pos_steps.at(recursion_depth) - 1.0);
    compare(recursion_depth + 1, spec, num_tests_completed, num_tests_failed, dynamics_tree, odom, positions);
  }
}

// add children to correct maps
void addChildren(const KDL::SegmentMap::const_iterator segment)
{
  const std::string& root = GetTreeElementSegment(segment->second).getName();

  const std::vector<KDL::SegmentMap::const_iterator>& children = GetTreeElementChildren(segment->second);
  for(unsigned int i = 0; i < children.size(); i++)
  {
    const KDL::Segment& child = GetTreeElementSegment(children[i]->second);
    SegmentPair s(GetTreeElementSegment(children[i]->second), root, child.getName());
    if(child.getJoint().getType() == KDL::Joint::None)
    {
      m_kdl_fixed_segments.insert(make_pair(child.getJoint().getName(), s));
      ROS_DEBUG("Adding fixed segment from %s to %s", root.c_str(), child.getName().c_str());
    }
    else
    {
      m_kdl_segments.insert(make_pair(child.getJoint().getName(), s));
      ROS_DEBUG("Adding moving segment from %s to %s", root.c_str(), child.getName().c_str());
    }
    addChildren(children[i]);
  }
}

int main(int argc, char **argv)
{
  unsigned long joint_position_tests_per_link = 3;
  std::string urdf_path = ros::package::getPath("matec_dynamics_tree") + "/atlas_hand.urdf";

  urdf::Model urdf_model;
  urdf_model.initFile(urdf_path);

  m_root_name = urdf_model.getRoot()->name;
  m_inertial_name = "global";

  std::vector<boost::shared_ptr<urdf::Link> > links;
  urdf_model.getLinks(links);

  TestSpec spec;
  std::string root = links.at(0)->name;
  std::string tip = "NONE FOUND";
  m_total_num_tests = 1;
  spec.num_joints = 0;
  for(unsigned int i = 0; i < links.size(); i++)
  {
    m_link_names.push_back(links.at(i)->name);

    int revolute_child_count = 0;
    for(unsigned int j = 0; j < links.at(i)->child_joints.size(); j++)
    {
      if(links.at(i)->child_joints.at(j)->type == urdf::Joint::REVOLUTE)
      {
        revolute_child_count++;
      }
    }
    if(revolute_child_count == 0)
    {
      tip = links.at(i)->name;
    }

    if(!links.at(i)->parent_joint)
    {
      std::cerr << "link " << links.at(i)->name << " had no parent joint! Skipping!" << std::endl;
      continue;
    }
    if(!links.at(i)->parent_joint->limits)
    {
      std::cerr << "link " << links.at(i)->name << "'s parent joint had no limits! Skipping!" << std::endl;
      continue;
    }

    m_joint_names.push_back(links.at(i)->parent_joint->name);

    spec.min_pos.push_back(links.at(i)->parent_joint->limits->lower);
    spec.max_pos.push_back(links.at(i)->parent_joint->limits->upper);
    spec.pos_steps.push_back(joint_position_tests_per_link);
    spec.num_joints++;

    m_total_num_tests *= joint_position_tests_per_link;
  }

  spec.min_odom_pos = -1.0;
  spec.max_odom_pos = 1.0;
  spec.odom_pos_steps = 3;
  m_total_num_tests *= spec.odom_pos_steps * spec.odom_pos_steps * spec.odom_pos_steps;

  spec.min_odom_rot = -2.0;
  spec.max_odom_rot = 2.0;
  spec.odom_rot_steps = 3;
  m_total_num_tests *= spec.odom_rot_steps * spec.odom_rot_steps * spec.odom_rot_steps;

  std::cerr << "CHAIN ROOT is " << root << std::endl;
  std::cerr << "CHAIN TIP is " << tip << std::endl;

  KDL::Tree kdl_tree;
  kdl_parser::treeFromUrdfModel(urdf_model, kdl_tree);
  addChildren(kdl_tree.getRootSegment());

  std::cerr << "============================ DYNAMICS TREE SETUP ===============================\n";
  dynamics_tree::DynamicsGraph graph;
  dynamics_tree::DynamicsTree tree;
  graph.loadFromURDF(m_joint_names, urdf_model);
  graph.print(m_root_name);
  graph.spawnDynamicsTree(m_root_name, true, tree);
  std::cerr << "===========================================================\n";

  std::vector<double> positions(spec.num_joints, 0.0);

  matec_msgs::Odometry odom;
  odom.header.frame_id = m_inertial_name;
  odom.pose.orientation.w = 1.0;

  unsigned long num_tests_completed = 0;
  unsigned long num_tests_failed = 0;

  compare(0, spec, num_tests_completed, num_tests_failed, &tree, odom, positions);
  std::cerr << "Completed " << num_tests_completed << " tests with " << num_tests_failed << " failures (" << 100.0 * (1.0 - (((double) num_tests_failed) / ((double) num_tests_completed))) << "% success)!" << std::endl;
}
