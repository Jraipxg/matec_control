#include "matec_dynamics_tree/dynamics_graph.h"
#include "matec_dynamics_tree/dynamics_tree.h"
#include "ros/package.h"
#include "sensor_msgs/JointState.h"
#include "kdl/chainidsolver_recursive_newton_euler.hpp"
#include "kdl_parser/kdl_parser.hpp"
#include "kdl_conversions/kdl_msg.h"
#include "kdl/chainjnttojacsolver.hpp"
#include "matec_utils/common_functions.h"
#undef NDEBUG
#include <cassert>

#define WHITE_TEXT  "\033[0m"
#define RED_TEXT "\033[31m"

struct TestSpec
{
  unsigned int num_joints;
  std::vector<double> min_pos;
  std::vector<double> max_pos;
  std::vector<unsigned int> pos_steps;
};

std::vector<std::string> m_joint_names;
std::vector<int> m_kdl_joint_map;
unsigned long m_total_num_tests;
std::string m_base_link, m_tip_link;

std::vector<dynamics_tree::DynamicsTreeNode::DynamicsDirection> inverse_dynamics_direction_config;
std::vector<dynamics_tree::ExternalWrenchFrame> external_wrench_frame_config;
std::vector<matec_utils::Vector6> external_wrenches;

int num_kdl_segments;
int num_kdl_joints;

inline KDL::JntArray mappedVectorToJntArray(std::vector<double> vec)
{
  KDL::JntArray jnt_array(vec.size());
  for(unsigned int i = 0; i < vec.size(); i++)
  {
    jnt_array(i) = vec[m_kdl_joint_map.at(i)];
  }
  return jnt_array;
}

void doCompare(unsigned long& num_tests_completed, unsigned long& num_tests_failed, matec_msgs::FullJointStates& test_states, dynamics_tree::DynamicsTree* dynamics_tree, KDL::ChainJntToJacSolver* kdl_solver, matec_msgs::Odometry& odom, matec_msgs::DynamicsCommand& dummy_command)
{
//  std::cerr << "Testing case: \n" << test_states << std::endl;

  std::vector<unsigned int> kdl_order_joint_indices;
  for(int i = test_states.joint_indices.size() - 1; i >= 0; --i)
  {
    kdl_order_joint_indices.push_back(i);
  }

  matec_utils::Matrix dynamics_tree_jacobian;
  dynamics_tree->kinematics(test_states.position, odom);
  dynamics_tree->jacobian(kdl_order_joint_indices, m_base_link, m_tip_link, dynamics_tree_jacobian);

  KDL::JntArray q = mappedVectorToJntArray(test_states.position);
  KDL::Jacobian kdl_jacobian(q.rows() * q.columns());
  if(kdl_solver->JntToJac(q, kdl_jacobian) != 0)
  {
    std::cerr << "KDL cart to jnt failed!" << std::endl;
    return;
  }

  //compare
  WTFASSERT(dynamics_tree_jacobian.rows() == kdl_jacobian.rows());
  WTFASSERT(dynamics_tree_jacobian.cols() == kdl_jacobian.columns());
//  std::cerr << "Test " << num_tests_completed << ":" << std::endl;
  bool fail = false;
  for(unsigned int col = 0; col < test_states.joint_indices.size(); col++)
  {
    for(unsigned int row = 0; row < 6; row++)
    {
      unsigned int dynamics_tree_row = row;
      unsigned int kdl_row = (row < 3)? row + 3 : row - 3;
      double err = dynamics_tree_jacobian(dynamics_tree_row, col) - kdl_jacobian(kdl_row, col);
      if(fabs(err) > 1e-6)
      {
        fail = true;
        std::cerr << RED_TEXT;
        std::cerr << "Joint " << m_joint_names.at(col) << "(" << col << "): Dynamics tree soln was " << dynamics_tree_jacobian(dynamics_tree_row, col) << ", KDL soln was " << kdl_jacobian(kdl_row, col) << ", error magnitude was " << fabs(err);
        std::cerr << WHITE_TEXT << std::endl;
      }
    }
  }

  if(fail)
  {
    std::cerr << RED_TEXT;
    std::cerr << "Test " << num_tests_completed << ": ";
    matec_utils::printVector(test_states.position);
    std::cerr << std::endl;

    std::cerr << "Joints: ";
    for(unsigned int i = 0; i < kdl_order_joint_indices.size(); i++)
    {
      if(i != 0)
      {
        std::cerr << ", " ;
      }
      std::cerr << m_joint_names.at(kdl_order_joint_indices.at(i));
    }
    std::cerr << std::endl;

    std::cerr << "kdl_jacobian:\n" << kdl_jacobian.data.bottomRows(3) << std::endl << kdl_jacobian.data.topRows(3) << std::endl;
    std::cerr << "dynamics_tree_jacobian:\n" << dynamics_tree_jacobian << std::endl;

    std::cerr << WHITE_TEXT << std::endl;
    WTFASSERT(false);
  }

//  std::cerr << std::endl;
  num_tests_completed++;
  if(num_tests_completed % 25000 == 0)
  {
    std::cerr << "Processed test #" << num_tests_completed << " / " << m_total_num_tests << ". " << 100.0 * (1.0 - (((double) num_tests_failed) / ((double) num_tests_completed))) << "% success so far!" << std::endl;
  }
  if(fail)
  {
    num_tests_failed++;
  }
}

void compare(unsigned int recursion_depth, TestSpec& spec, unsigned long& num_tests_completed, unsigned long& num_tests_failed, matec_msgs::FullJointStates& test_states, dynamics_tree::DynamicsTree* dynamics_tree, KDL::ChainJntToJacSolver* kdl_solver, matec_msgs::Odometry& odom, matec_msgs::DynamicsCommand& dummy_command)
{
  if(recursion_depth == spec.num_joints)
  {
    //perform the comparison
    doCompare(num_tests_completed, num_tests_failed, test_states, dynamics_tree, kdl_solver, odom, dummy_command);
    return;
  }

  //generate tests
  for(unsigned int i = 0; i < spec.pos_steps.at(recursion_depth); i++)
  {
    test_states.position.at(recursion_depth) = spec.min_pos.at(recursion_depth) + (spec.max_pos.at(recursion_depth) - spec.min_pos.at(recursion_depth)) * ((double) i) / ((double) spec.pos_steps.at(recursion_depth) - 1.0);
//    std::cerr << "Changed Joint " << recursion_depth << " position to " << test_states.position.at(recursion_depth) << std::endl;
    compare(recursion_depth + 1, spec, num_tests_completed, num_tests_failed, test_states, dynamics_tree, kdl_solver, odom, dummy_command);
  }
}

int main(int argc, char **argv)
{
  unsigned long position_tests_per_link = 8;
  std::string urdf_path = ros::package::getPath("matec_dynamics_tree") + "/garm_7_dof_test.urdf";

  urdf::Model urdf_model;
  urdf_model.initFile(urdf_path);

  std::vector<boost::shared_ptr<urdf::Link> > links;
  urdf_model.getLinks(links);

  matec_msgs::FullJointStates test_states;

  TestSpec spec;
  m_base_link = links.at(0)->name;
  m_tip_link = "NONE FOUND";
  m_total_num_tests = 1;
  for(unsigned int i = 0; i < links.size(); i++)
  {
    if(links.at(i)->child_joints.size() == 0)
    {
      m_tip_link = links.at(i)->name;
    }

    if(!links.at(i)->parent_joint)
    {
      std::cerr << "link " << links.at(i)->name << " had no parent joint! Skipping!" << std::endl;
      continue;
    }
    if(!links.at(i)->parent_joint->limits)
    {
      std::cerr << "link " << links.at(i)->name << "'s parent joint had no limits! Skipping!" << std::endl;
      continue;
    }

    test_states.joint_indices.push_back(m_joint_names.size());
    m_joint_names.push_back(links.at(i)->parent_joint->name);

    spec.min_pos.push_back(links.at(i)->parent_joint->limits->lower);
    spec.max_pos.push_back(links.at(i)->parent_joint->limits->upper);
    spec.pos_steps.push_back(position_tests_per_link);

    m_total_num_tests *= position_tests_per_link;
  }
  spec.num_joints = test_states.joint_indices.size();
  test_states.position.resize(test_states.joint_indices.size(), 0.0);
  test_states.velocity.resize(test_states.joint_indices.size(), 0.0);
  test_states.acceleration.resize(test_states.joint_indices.size(), 0.0);
  test_states.torque.resize(test_states.joint_indices.size(), 0.0);
  test_states.compliance.resize(test_states.joint_indices.size(), 0.0);

  std::cerr << "CHAIN ROOT is " << m_base_link << std::endl;
  std::cerr << "CHAIN TIP is " << m_tip_link << std::endl;

  std::cerr << "============================ DYNAMICS TREE SETUP ===============================\n";
  dynamics_tree::DynamicsGraph graph;
  dynamics_tree::DynamicsTree tree;
  graph.loadFromURDF(m_joint_names, urdf_model);
  graph.print("base");
  graph.spawnDynamicsTree("base", false, tree);

  inverse_dynamics_direction_config.resize(spec.num_joints, dynamics_tree::DynamicsTreeNode::INVERSE_DYNAMICS);
  external_wrench_frame_config.resize(spec.num_joints, dynamics_tree::NO_FRAME);
  external_wrenches.resize(spec.num_joints, matec_utils::Vector6::Zero());
  tree.configureDynamics(inverse_dynamics_direction_config, external_wrench_frame_config);
  std::cerr << "===========================================================\n";

  std::cerr << "============================ KDL SETUP ===============================\n";
  KDL::Tree kdl_tree;
  kdl_parser::treeFromUrdfModel((const urdf::Model) urdf_model, kdl_tree);
  KDL::Vector grav(0, 0, -9.81);
  KDL::Chain kdl_chain;
  kdl_tree.getChain(m_base_link, m_tip_link, kdl_chain);
  KDL::ChainJntToJacSolver kdl_solver(kdl_chain);
  std::cerr << "KDL chain has " << kdl_chain.getNrOfJoints() << " joints" << std::endl;
  num_kdl_joints = kdl_chain.getNrOfJoints();
  num_kdl_segments = kdl_chain.getNrOfSegments();

  m_kdl_joint_map.resize(kdl_chain.getNrOfJoints());
  for(unsigned int i = 0; i < kdl_chain.getNrOfJoints(); i++)
  {
    std::string kdl_joint_name = kdl_chain.getSegment(i).getJoint().getName();
    unsigned int idx = std::find(m_joint_names.begin(), m_joint_names.end(), kdl_joint_name) - m_joint_names.begin();

    std::cerr << "KDL joint #" << i << " maps to dynamics tree joint #" << idx << std::endl;
    m_kdl_joint_map[idx] = i;
  }
  std::cerr << "===========================================================\n";

  matec_msgs::DynamicsCommand dummy_command;
  dummy_command.feedforward = test_states;
  dummy_command.feedback = test_states;

  matec_msgs::Odometry odom;
  odom.pose.orientation.w = 1.0;

  unsigned long num_tests_completed = 0;
  unsigned long num_tests_failed = 0;
  compare(0, spec, num_tests_completed, num_tests_failed, test_states, &tree, &kdl_solver, odom, dummy_command);
  std::cerr << "Completed " << num_tests_completed << " tests with " << num_tests_failed << " failures (" << 100.0 * (1.0 - (((double) num_tests_failed) / ((double) num_tests_completed))) << "% success)!" << std::endl;
}
