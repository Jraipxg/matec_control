#include "matec_utils/joint_monitor.h"

namespace matec_utils
{
  JointMonitor::JointMonitor(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);

    m_initialized = false;
    m_min_pub = m_nh.advertise<matec_msgs::FullJointStates>("min_joint_states", 1, true);
    m_max_pub = m_nh.advertise<matec_msgs::FullJointStates>("max_joint_states", 1, true);
    m_joint_states_sub = m_nh.subscribe<matec_msgs::FullJointStates>("/smi/joint_states", 1, &JointMonitor::sensorCallback, this);
  }

  JointMonitor::~JointMonitor()
  {
  }

  void JointMonitor::sensorCallback(const matec_msgs::FullJointStatesConstPtr& msg)
  {
    if(!m_initialized)
    {
      m_min.joint_indices = msg->joint_indices;
      m_min.position.resize(msg->joint_indices.size(), std::numeric_limits<double>::max());
      m_min.velocity.resize(msg->joint_indices.size(), std::numeric_limits<double>::max());
      m_min.acceleration.resize(msg->joint_indices.size(), std::numeric_limits<double>::max());
      m_min.torque.resize(msg->joint_indices.size(), std::numeric_limits<double>::max());

      m_max.joint_indices = msg->joint_indices;
      m_max.position.resize(msg->joint_indices.size(), -std::numeric_limits<double>::max());
      m_max.velocity.resize(msg->joint_indices.size(), -std::numeric_limits<double>::max());
      m_max.acceleration.resize(msg->joint_indices.size(), -std::numeric_limits<double>::max());
      m_max.torque.resize(msg->joint_indices.size(), -std::numeric_limits<double>::max());

      m_initialized = true;
    }

    for(unsigned int i = 0; i < msg->joint_indices.size(); i++)
    {
      if(i < msg->position.size())
      {
        m_min.position.at(i) = std::min(m_min.position.at(i), msg->position.at(i));
        m_max.position.at(i) = std::max(m_max.position.at(i), msg->position.at(i));
      }
      else
      {
        m_min.position.at(i) = 0.0;
        m_max.position.at(i) = 0.0;
      }

      if(i < msg->velocity.size())
      {
        m_min.velocity.at(i) = std::min(m_min.velocity.at(i), msg->velocity.at(i));
        m_max.velocity.at(i) = std::max(m_max.velocity.at(i), msg->velocity.at(i));
      }
      else
      {
        m_min.velocity.at(i) = 0.0;
        m_max.velocity.at(i) = 0.0;
      }

      if(i < msg->acceleration.size())
      {
        m_min.acceleration.at(i) = std::min(m_min.acceleration.at(i), msg->acceleration.at(i));
        m_max.acceleration.at(i) = std::max(m_max.acceleration.at(i), msg->acceleration.at(i));
      }
      else
      {
        m_min.acceleration.at(i) = 0.0;
        m_max.acceleration.at(i) = 0.0;
      }

      if(i < msg->torque.size())
      {
        m_min.torque.at(i) = std::min(m_min.torque.at(i), msg->torque.at(i));
        m_max.torque.at(i) = std::max(m_max.torque.at(i), msg->torque.at(i));
      }
      else
      {
        m_min.torque.at(i) = 0.0;
        m_max.torque.at(i) = 0.0;
      }
    }

    m_min_pub.publish(m_min);
    m_max_pub.publish(m_max);
  }

  void JointMonitor::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "joint_monitor");
  ros::NodeHandle nh("~");

  matec_utils::JointMonitor node(nh);
  node.spin();

  return 0;
}
