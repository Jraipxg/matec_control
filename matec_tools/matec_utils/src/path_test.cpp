#include <math.h>
#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/tf.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseArray.h>
#include <matec_utils/ground_path_generators.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "spline_test");
  ros::NodeHandle nh;

  if(argc < 10)
  {
    ROS_ERROR("Need 9 arguments (coordinates in ground frame): start (x, y, theta), mid (x, y, theta), end (x, y, theta) ");
    return 0;
  }

  ros::Publisher tgt_path_pub = nh.advertise<geometry_msgs::PoseArray>("/tgt_path_vis", 1, true);
  ros::Publisher tgt_step_pub = nh.advertise<visualization_msgs::MarkerArray>("/tgt_step_vis", 1, true);

  ros::Publisher hermite_path_pub = nh.advertise<geometry_msgs::PoseArray>("/hermite_path_vis", 1, true);
  ros::Publisher hermite_step_pub = nh.advertise<visualization_msgs::MarkerArray>("/hermite_step_vis", 1, true);

  ros::Publisher sidestep_path_pub = nh.advertise<geometry_msgs::PoseArray>("/sidestep_path_vis", 1, true);
  ros::Publisher sidestep_step_pub = nh.advertise<visualization_msgs::MarkerArray>("/sidestep_step_vis", 1, true);

  geometry_msgs::Pose2D start, mid, end;

  start.x = atof(argv[1]);
  start.y = atof(argv[2]);
  start.theta = atof(argv[3]);
  mid.x = atof(argv[4]);
  mid.y = atof(argv[5]);
  mid.theta = atof(argv[6]);
  end.x = atof(argv[7]);
  end.y = atof(argv[8]);
  end.theta = atof(argv[9]);

  std::vector<geometry_msgs::Pose2D> waypoints = {start, mid, end};
  matec_utils::TGTGenerator tgt_footstep_generator;
  matec_utils::HermiteGenerator hermite_footstep_generator;
  matec_utils::SidestepGenerator sidestep_footstep_generator;
  tgt_footstep_generator.load(waypoints);
  hermite_footstep_generator.load(waypoints);
  sidestep_footstep_generator.load(waypoints);

  geometry_msgs::PoseArray tgt_path_vis, hermite_path_vis, sidestep_path_vis;
  matec_utils::visualizePath(tgt_footstep_generator.getPath(), tgt_path_vis);
  matec_utils::visualizePath(hermite_footstep_generator.getPath(), hermite_path_vis);
  matec_utils::visualizePath(sidestep_footstep_generator.getPath(), sidestep_path_vis);

  geometry_msgs::Pose fake_left_start, fake_right_start; //todo: populate these better;
  fake_left_start.orientation.w = 1.0;
  fake_left_start.position.y = 0.1;
  fake_right_start.orientation.w = 1.0;
  fake_right_start.position.y = -0.1;

  std::vector<geometry_msgs::Pose> tgt_steps, hermite_steps, sidestep_steps;
  if(tgt_footstep_generator.getSteps(fake_left_start, fake_right_start, true, 0.2, 0.05, tgt_steps))
  {
    std::cerr << "TGT Success (" << tgt_steps.size() << " steps)!" << std::endl;
  }
  if(hermite_footstep_generator.getSteps(fake_left_start, fake_right_start, true, 0.2, 0.05, hermite_steps))
  {
    std::cerr << "Hermite Success (" << hermite_steps.size() << " steps)!" << std::endl;
  }
  if(sidestep_footstep_generator.getSteps(fake_left_start, fake_right_start, true, 0.2, 0.05, sidestep_steps))
  {
    std::cerr << "Sidestep Success (" << sidestep_steps.size() << " steps)!" << std::endl;
  }

  visualization_msgs::MarkerArray tgt_step_vis, hermite_step_vis, sidestep_step_vis;
  matec_utils::visualizeSteps(tgt_steps, matec_utils::getAlternatingFeet(true, tgt_steps.size()), tgt_step_vis);
  matec_utils::visualizeSteps(hermite_steps, matec_utils::getAlternatingFeet(true, hermite_steps.size()), hermite_step_vis);
  matec_utils::visualizeSteps(sidestep_steps, matec_utils::getAlternatingFeet(true, sidestep_steps.size()), sidestep_step_vis);

  //publish
  while(ros::ok())
  {
    tgt_path_pub.publish(tgt_path_vis);
    tgt_step_pub.publish(tgt_step_vis);

    hermite_path_pub.publish(hermite_path_vis);
    hermite_step_pub.publish(hermite_step_vis);

    sidestep_path_pub.publish(sidestep_path_vis);
    sidestep_step_pub.publish(sidestep_step_vis);
  }

  return 0;
}

