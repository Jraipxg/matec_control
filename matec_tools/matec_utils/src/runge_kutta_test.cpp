#include "matec_utils/runge_kutta.h"
#include "matec_utils/plotter.h"

double r = 4;
double k = 1000;
double m = 1;

matec_utils::Vector2 function(double t, matec_utils::Vector2 x0)
{
  //wd = 5, 25 = k - r^2/4

  double delta = r / 2.0 / m;
  double w02 = k / m;
  double wd = sqrt(w02 - delta * delta);
  matec_utils::Vector2 ret;
  ret(0) = x0(0) * exp(-delta * t) * (cos(wd * t) + delta / wd * sin(wd * t));
  ret(1) = -delta * x0(0) * exp(-delta * t) * (cos(wd * t) + delta / wd * sin(wd * t)) + x0(0) * exp(-delta * t) * (-wd * sin(wd * t) + delta * cos(wd * t));
  return ret;
}

matec_utils::Vector2 derivative(double t, matec_utils::Vector2 x)
{
  matec_utils::Vector2 ret;
  ret(0) = x(1);
  ret(1) = -r * x(1) / m - k * x(0) / m;
  return ret;
}

double errorFunction(matec_utils::Vector2 a, matec_utils::Vector2 b)
{
  return (a - b).norm();
}

matec_utils::Vector2 euler(matec_utils::Vector2 current, matec_utils::Vector2 derivative, double h)
{
  return current + h * derivative;
}

int main(int argc, char **argv)
{
  matec_utils::Plotter plotter(1);
  while(plotter.isReady())
  {

  }

  double h = 0.001;
  matec_utils::Vector2 x0(1.0, 0.0);
  double t = 0.0;
  matec_utils::Vector2 x = function(t, x0);
  matec_utils::Vector2 rk_x = x;
  matec_utils::Vector2 adaptive_rk_x = x;
  matec_utils::Vector2 euler_x = x;
  double total_rk_error = 0.0;
  double total_adaptive_rk_error = 0.0;
  double total_euler_error = 0.0;
  std::vector<long double> time, real_series, rk_series, adaptive_rk_series, euler_series;
  unsigned int initial_num_substeps = 1;
  while(t < 1.0)
  {
    double error;
    x = function(t + h, x0);
    std::cerr << "runge-kutta" << std::endl;
    rk_x = matec_utils::integrateFixedStep<matec_utils::Vector2>(t, rk_x, h, derivative, errorFunction, error);
    std::cerr << "adaptive_runge-kutta" << std::endl;
    adaptive_rk_x = matec_utils::integrateAdaptiveStep<matec_utils::Vector2>(t, adaptive_rk_x, initial_num_substeps, h, derivative, errorFunction);
    std::cerr << "euler" << std::endl;
    euler_x = euler(euler_x, derivative(t, euler_x), h);
    double rk_error = errorFunction(rk_x, x);
    total_rk_error += rk_error * rk_error;
    double adaptive_rk_error = errorFunction(adaptive_rk_x, x);
    total_adaptive_rk_error += adaptive_rk_error * adaptive_rk_error;
    double euler_error = errorFunction(euler_x, x);
    total_euler_error += euler_error * euler_error;
    t += h;

    std::cerr << "t: " << t << std::endl;
    std::cerr << "\treal: " << x.transpose() << std::endl;
    std::cerr << "\trk: " << rk_x.transpose() << "\terr: " << rk_error << "\ttotal: " << total_rk_error << std::endl;
    std::cerr << "\tadaptive_rk: " << adaptive_rk_x.transpose() << "\terr: " << adaptive_rk_error << "\ttotal: " << total_adaptive_rk_error << std::endl;
    std::cerr << "\teuler: " << euler_x.transpose() << "\terr: " << euler_error << "\ttotal: " << total_euler_error << std::endl;
    std::cerr << std::endl << std::endl;

    //plot
    time.push_back(t);
    real_series.push_back((long double) x(0));
    rk_series.push_back((long double) rk_x(0));
    adaptive_rk_series.push_back((long double) adaptive_rk_x(0));
    euler_series.push_back((long double) euler_x(0));

    if(time.size() % 50 == 0)
    {
      plotter.setData(0, "real", time, real_series, "r");
//      plotter.setData(0, "rk", time, rk_series, "g");
//      plotter.setData(0, "euler", time, euler_series, "b");
    }
  }

  while(true)
  {
    plotter.setData(0, "real", time, real_series, "r");
    plotter.setData(0, "rk", time, rk_series, "g");
    plotter.setData(0, "euler", time, euler_series, "b");
  }
}
