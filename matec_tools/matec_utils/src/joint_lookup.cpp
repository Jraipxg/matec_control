#include <ros/ros.h>
#include <matec_utils/common_initialization_components.h>

inline bool isInteger(const std::string & s)
{
  if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+')))
    return false;

  char * p;
  strtol(s.c_str(), &p, 10);

  return (*p == 0);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "joint_lookup");
  ros::NodeHandle nh("~");

  if(argc > 2)
  {
    std::cerr << "Arg is nothing, joint name or idx" << std::endl;
    return 0;
  }

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  if(argc == 1)
  {
    for(unsigned int i = 0; i < joint_names.size(); i++)
    {
      std::cerr << "Joint " << i << " is " << joint_names[i] << std::endl;
    }
  }
  else if(argc == 2)
  {
    std::string arg = std::string(argv[1]);
    int idx = -1;
    std::string name = "";
    if(isInteger(arg))
    {
      idx = std::stoi(arg);
      if(idx < 0 || idx > (int) joint_names.size())
      {
        std::cerr << "Index " << idx << " is out of bounds!" << std::endl;
        return 0;
      }
      name = joint_names[idx];
    }
    else
    {
      name = arg;
      idx = std::find(joint_names.begin(), joint_names.end(), name) - joint_names.begin();
      if(idx == (int) joint_names.size())
      {
        std::cerr << "Joint name " << arg << " not found!" << std::endl;
        return 0;
      }
    }

    std::cerr << "Joint " << idx << " is " << name << std::endl;
  }

  return 0;
}
