#include "matec_utils/parameter_helper.h"

std::string string_param;

void callback(std::string& stringy)
{
  string_param = stringy;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "parameter_helper_test");
  ros::NodeHandle nh("~");

  boost::shared_ptr<matec_utils::ParameterManager> pm = matec_utils::ParameterManager::getInstance();

  matec_utils::Parameter<double> double_helper;
  matec_utils::Parameter<int> int_helper;
  matec_utils::Parameter<std::string> string_helper;

  pm->param("double_param", 0.0, double_helper);
  pm->param("int_param", 0, int_helper);
  pm->param<std::string>("string_param", "zero", string_helper);

  int int_param;

  string_helper.subscribe(&callback);
  int_helper.attach(&int_param);


  while(ros::ok())
  {
    std::cerr << "D: " << double_helper << ", I: " << int_param << ", S: " << string_param << std::endl;
  }
}
