#ifndef COMMON_INITIALIZATION_COMPONENTS_H
#define COMMON_INITIALIZATION_COMPONENTS_H

#include <ros/ros.h>
#include <ros/topic.h>
#include "matec_utils/string_parameter_parsers.h"
#include "shared_memory_interface/shared_memory_publisher.hpp"
#include "shared_memory_interface/shared_memory_subscriber.hpp"
#include "matec_msgs/StringArray.h"
#include "matec_msgs/RegisterBehavior.h"
#include "matec_msgs/AwakenBehavior.h"
#include "matec_msgs/RequestJointOwnershipChange.h"
#include "std_msgs/Int8.h"

namespace matec_utils
{
  inline bool expandWildcard(std::string wildcard_string, std::vector<std::string>& string_list, std::vector<std::string>& matching_strings)
  {
    std::string::size_type star_idx = wildcard_string.find('*');
    if(star_idx == wildcard_string.npos) //no star
    {
      //todo: double check that the string is in the string_list
      matching_strings.push_back(wildcard_string);
      return true;
    }

    std::string before_substr = wildcard_string.substr(0, star_idx);
    std::string after_substr = wildcard_string.substr(star_idx + 1);

    if((before_substr.find('*') != before_substr.npos) || (after_substr.find('*') != after_substr.npos)) //too many stars
    {
      ROS_ERROR("Multiple wildcards (*) detected in string %s (substrings %s and %s)! Only one wildcard is supported!", wildcard_string.c_str(), before_substr.c_str(), after_substr.c_str());
      return false;
    }

    for(unsigned int i = 0; i < string_list.size(); i++)
    {
      if((string_list[i].size() > (before_substr.size() + after_substr.size())) && (before_substr == string_list[i].substr(0, before_substr.size())) && (after_substr == string_list[i].substr(string_list[i].size() - after_substr.size())))
      {
        ROS_INFO("Selecting %s for wildcard string %s", string_list[i].c_str(), wildcard_string.c_str());
        matching_strings.push_back(string_list[i]);
      }
    }

    if(matching_strings.size() == 0)
    {
      ROS_ERROR("Wildcard string %s matched nothing in the string list!", wildcard_string.c_str());
      return false;
    }

    return true;
  }

  inline bool getControlledJoints(ros::NodeHandle nh, std::vector<std::string>& controlled_joints, std::vector<std::string>& all_joint_names, std::string ns = "")
  {
    std::string controlled_joints_string;
    if(ns.length() == 0)
    {
      nh.param("controlled_joints", controlled_joints_string, std::string(""));
    }
    else
    {
      nh.param("/" + ns + "/controlled_joints", controlled_joints_string, std::string(""));
    }
    if(controlled_joints_string.length() == 0)
    {
      return false;
    }
    std::vector<std::string> split_controlled_joints_string = matec_utils::parameterStringToStringVector(controlled_joints_string);

    for(unsigned int i = 0; i < split_controlled_joints_string.size(); i++)
    {
      if(!expandWildcard(split_controlled_joints_string[i], all_joint_names, controlled_joints))
      {
        ROS_ERROR("%s: Failed to parse controlled joints!", nh.getNamespace().c_str());
      }
    }

    return (controlled_joints.size() > 0);
  }

  inline bool getFPVector(ros::NodeHandle nh, std::string param_name, unsigned long expected_length, double default_value, std::vector<double>& values)
  {
    std::string string;
    nh.param(param_name, string, std::string(""));
    values = matec_utils::parameterStringToFPVector(string);

    if(values.size() != expected_length)
    {
      if(values.size() != 0)
      {
        ROS_WARN("Values provided in %s parameter were not the right length! Using default value %g instead!", param_name.c_str(), default_value);
      }
      values.clear();
      values.resize(expected_length, default_value);
      return false;
    }

    return true;
  }

  //wait for ros time to become valid
  inline void blockOnROSTime()
  {
    ros::NodeHandle nh("~");
    while(ros::ok() && (ros::Time::now().toSec() == 0.0))
    {
      ROS_WARN_THROTTLE(1.0, "%s waiting for ROS time to be populated...", nh.getNamespace().c_str());
    }
  }

  inline void blockOnSMJointNames(std::vector<std::string>& joint_names)
  {
    shared_memory_interface::Subscriber<matec_msgs::StringArray> names_sub;
    names_sub.subscribe("/joint_names");
    matec_msgs::StringArray names;
    ros::NodeHandle nh("~");
    while(ros::ok() && !names_sub.getCurrentMessage(names))
    {
      ROS_WARN_THROTTLE(1.0, "%s node is waiting for joint names...", nh.getNamespace().c_str());
    }
    joint_names = names.data;
  }

  inline void blockOnROSJointNames(std::vector<std::string>& joint_names)
  {
    ros::NodeHandle nh("~");
    boost::shared_ptr<const matec_msgs::StringArray> names_ptr = ros::topic::waitForMessage<matec_msgs::StringArray>("/smi/joint_names", nh, ros::Duration(0.1));
    while(ros::ok() && !names_ptr)
    {
      ROS_WARN_THROTTLE(1.0, "%s node is waiting for joint names...", nh.getNamespace().c_str());
      names_ptr = ros::topic::waitForMessage<matec_msgs::StringArray>("/smi/joint_names", nh, ros::Duration(3)); //3 is the magical constant of rostcp being slow (used in their code as well)
      ros::spinOnce();
    }
    joint_names = names_ptr->data;
  }

  inline bool namesToIndices(std::vector<std::string> all_names, std::vector<std::string> names, std::vector<unsigned char>& indices)
  {
    if(names.size() > all_names.size())
    {
      ROS_ERROR("The list of controlled joint names (%d) was longer than the list of available joints (%d)!", (int ) names.size(), (int ) all_names.size());
      return false;
    }

    indices.clear();
    for(unsigned int i = 0; i < names.size(); i++)
    {
      unsigned char joint_idx = std::find(all_names.begin(), all_names.end(), names[i]) - all_names.begin();
      if(joint_idx == all_names.size())
      {
        ROS_ERROR("Name %s was not found in the list of available names!", names[i].c_str());
        return false;
      }
      else
      {
        indices.push_back(joint_idx);
      }
    }
    return true;
  }

  inline bool indicesToNames(std::vector<std::string> all_names, std::vector<unsigned char> indices, std::vector<std::string>& names)
  {
    names.clear();
    for(unsigned int i = 0; i < indices.size(); i++)
    {
      if(indices[i] >= all_names.size())
      {
        ROS_ERROR("Requested index %d was out of bounds!", indices[i]);
        return false;
      }
      names.push_back(all_names[indices[i]]);
    }
    return true;
  }

  inline bool generateControlledJointMapping(std::vector<std::string> all_joint_names, std::vector<std::string> controlled_joint_names, std::vector<unsigned char>& controlled_joint_map)
  {
    if(controlled_joint_names.size() > all_joint_names.size())
    {
      ROS_ERROR("The list of controlled joint names (%d) was longer than the list of available joints (%d)!", (int ) controlled_joint_names.size(), (int ) all_joint_names.size());
      return false;
    }

    controlled_joint_map.clear();
    for(unsigned int i = 0; i < controlled_joint_names.size(); i++)
    {
      unsigned char joint_idx = std::find(all_joint_names.begin(), all_joint_names.end(), controlled_joint_names[i]) - all_joint_names.begin();
      if(joint_idx == all_joint_names.size())
      {
        ROS_ERROR("Specified controllable joint %s was not found in the list of available joints!", controlled_joint_names[i].c_str());
        return false;
      }
      else
      {
        controlled_joint_map.push_back(joint_idx);
      }
    }
    return true;
  }

  inline void behaviorHeartbeat(double heartbeat_period = 0.25)
  {
    static ros::Time last_beat_time = ros::Time(0);
    ros::Time now_time = ros::Time::now();
    if(ros::Duration(now_time - last_beat_time).toSec() < heartbeat_period) //not time to send
    {
      return;
    }

    static shared_memory_interface::Publisher<std_msgs::Int8> heartbeat_pub;
    static bool advertised = false;
    if(!advertised)
    {
      heartbeat_pub.advertise("heartbeat");
      advertised = true;
    }

    static std_msgs::Int8 last_beat;
    last_beat.data++;
    heartbeat_pub.publish(last_beat);
    last_beat_time = now_time;
  }

  inline void blockOnBehaviorRegistration(std::string behavior_name, bool base_behavior = false)
  {
    ROS_INFO("Registering behavior %s", behavior_name.c_str());
    while(ros::ok() && !ros::service::exists("/register_behavior", false))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for orchestrator to come up...", behavior_name.c_str());
    }

    matec_msgs::RegisterBehavior register_behavior;
    register_behavior.request.name = behavior_name;
    register_behavior.request.base_behavior = base_behavior;
    while(ros::ok() && !ros::service::call("/register_behavior", register_behavior))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for successful registration...", behavior_name.c_str());
    }

    ROS_INFO("Behavior %s registered", behavior_name.c_str());
  }

  inline bool requestOwnership(std::vector<unsigned char> joints, std::string behavior_name = "")
  {
    if(behavior_name.length() == 0)
    {
      ros::NodeHandle nh("~");
      behavior_name = nh.getNamespace();
    }

    matec_msgs::RequestJointOwnershipChange request_ownership;
    request_ownership.request.behavior_name = behavior_name;
    request_ownership.request.actuators = joints;

    ROS_INFO("Requesting ownership of joints for behavior %s", behavior_name.c_str());
    while(!ros::service::exists("/request_joint_ownership", false))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for orchestrator to come up...", behavior_name.c_str());
    }
    return ros::service::call("/request_joint_ownership", request_ownership);
  }

  inline bool releaseOwnership(std::vector<unsigned char> joints = std::vector<unsigned char>(), std::string behavior_name = "")
  {
    if(behavior_name.length() == 0)
    {
      ros::NodeHandle nh("~");
      behavior_name = nh.getNamespace();
    }

    matec_msgs::RequestJointOwnershipChange request_ownership;
    request_ownership.request.behavior_name = behavior_name;
    request_ownership.request.actuators = joints;

    ROS_INFO("Releasing ownership of joints for behavior %s", behavior_name.c_str());
    while(!ros::service::exists("/release_joint_ownership", false))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for orchestrator to come up...", behavior_name.c_str());
    }
    return ros::service::call("/release_joint_ownership", request_ownership);
  }

  inline bool awakenBehavior(std::string behavior_name, bool& awake, std::vector<unsigned char> joints = std::vector<unsigned char>())
  {
    ros::NodeHandle nh("~");

    std::string service_name = "/" + behavior_name + "/awaken";
    while(!ros::service::exists(service_name, false))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for orchestrator to come up...", behavior_name.c_str());
    }

    matec_msgs::AwakenBehavior awaken;
    awaken.request.actuators = joints;
    if(ros::service::call(service_name, awaken))
    {
      awake = awaken.response.awake;
      if(!awake)
      {
        ROS_INFO("%s: Waking up behavior %s", nh.getNamespace().c_str(), behavior_name.c_str());
      }
      return true;
    }
    awake = false;
    return false;
  }

  inline std::string stripLeadingSlash(std::string s)
  {
    if(s.at(0) == '/')
    {
      s.erase(s.begin());
    }
    return s;
  }

  template<typename T>
  inline void rosParamCached(std::string param_name, T& variable, T default_value, ros::NodeHandle nh = ros::NodeHandle("~"))
  {
    if(nh.hasParam(param_name))
    {
      nh.getParamCached(param_name, variable);
    }
    else
    {
      variable = default_value;
    }
  }

  template<typename T>
  inline T rosParamCached(std::string param_name, T default_value, ros::NodeHandle nh = ros::NodeHandle("~"))
  {
    T variable;
    rosParamCached(param_name, variable, default_value, nh);
  }
}

#endif //COMMON_INITIALIZATION_COMPONENTS_H
