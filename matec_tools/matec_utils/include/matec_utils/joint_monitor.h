#ifndef JOINT_MONITOR_H
#define JOINT_MONITOR_H
#include <ros/ros.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/DataVector.h>

namespace matec_utils
{
  class JointMonitor
  {
  public:
    JointMonitor(const ros::NodeHandle& nh);
    ~JointMonitor();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    ros::Subscriber m_joint_states_sub;
    ros::Publisher m_min_pub;
    ros::Publisher m_max_pub;

    bool m_initialized;
    matec_msgs::FullJointStates m_min;
    matec_msgs::FullJointStates m_max;

    void sensorCallback(const matec_msgs::FullJointStatesConstPtr& msg);
  };
}
#endif //JOINT_MONITOR_H
