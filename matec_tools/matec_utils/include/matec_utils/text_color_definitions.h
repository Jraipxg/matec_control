#ifndef TEXT_COLOR_DEFINITIONS_H
#define TEXT_COLOR_DEFINITIONS_H

namespace matec_utils
{
#define WHITE_TEXT  "\033[0m"
#define RED_TEXT "\033[31m"
#define YELLOW_TEXT  "\033[33m"
#define GREEN_TEXT  "\033[92m"
}

#endif //TEXT_COLOR_DEFINITIONS_H
