#ifndef GROUND_PATH_GENERATORS_H
#define GROUND_PATH_GENERATORS_H

#include "matec_utils/common_functions.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseArray.h>
#include <visualization_msgs/MarkerArray.h>

namespace matec_utils
{
  class HermiteSpline
  {
  public:
    HermiteSpline(geometry_msgs::Pose2D p0, geometry_msgs::Pose2D p1)
    {
      Vector2 x0, dx0, x1, dx1;
      x0(0) = p0.x;
      x0(1) = p0.y;
      dx0(0) = cos(p0.theta);
      dx0(1) = sin(p0.theta);
      x1(0) = p1.x;
      x1(1) = p1.y;
      dx1(0) = cos(p1.theta);
      dx1(1) = sin(p1.theta);

      a = 2.0 * x0 - 2.0 * x1 + dx0 + dx1;
      b = -3.0 * x0 + 3.0 * x1 - 2.0 * dx0 - dx1;
      c = dx0;
      d = x0;
    }

    geometry_msgs::Pose2D evaluate(double t)
    {
      double t2 = t * t;
      double t3 = t * t2;

      Vector2 x = a * t3 + b * t2 + c * t + d;
      Vector2 dx = 3.0 * a * t2 + 2.0 * b * t + c;

      geometry_msgs::Pose2D pose;
      pose.x = x(0);
      pose.y = x(1);
      pose.theta = atan2((double) dx(1), (double) dx(0));

      return pose;
    }

  private:
    Vector2 a;
    Vector2 b;
    Vector2 c;
    Vector2 d;
  };

  class LinearSpline
  {
  public:
    LinearSpline(geometry_msgs::Pose2D p0, geometry_msgs::Pose2D p1)
    {
      a(0) = p1.x - p0.x;
      a(1) = p1.y - p0.y;
      a(2) = matec_utils::angleSub(p1.theta, p0.theta);
      b(0) = p0.x;
      b(1) = p0.y;
      b(2) = p0.theta;
    }

    geometry_msgs::Pose2D evaluate(double t)
    {
      Vector3 x = a * t + b;
      x(2) = matec_utils::angleAdd((double) a(2) * t, (double) b(2)); //fix wrapping

      geometry_msgs::Pose2D pose;
      pose.x = x(0);
      pose.y = x(1);
      pose.theta = x(2);

      return pose;
    }

  private:
    Vector3 a;
    Vector3 b;
  };

  template<typename SplineType>
  class PathGenerator
  {
  public:
    PathGenerator()
    {
      m_loaded = false;
    }

    virtual void load(std::vector<geometry_msgs::Pose2D> ground_targets) = 0;

    geometry_msgs::Pose2D getPoseAtTime(double t) //t in [0,1] covering the entire path, not just one spline
    {
      if(!m_loaded || t > 1.0)
      {
        return geometry_msgs::Pose2D();
      }
      WTFASSERT(m_splines.size() > 0);
      WTFASSERT(t <= 1.0);
      double scaled_t = t * m_splines.size();
      unsigned int idx = matec_utils::clamp(std::floor(scaled_t), 0, m_splines.size() - 1);
      WTFASSERT(idx < m_splines.size());
      double spline_t = scaled_t - (double) idx;
      return m_splines.at(idx).evaluate(spline_t);
    }

    std::vector<geometry_msgs::Pose2D> getPathPoses(double num_poses = 100.0)
    {
      if(!m_loaded)
      {
        return std::vector<geometry_msgs::Pose2D>();
      }
      //evaluate all m_splines
      std::vector<geometry_msgs::Pose2D> spline_path;
      for(double t = 0.0; t <= 1.0; t += (1.0 / num_poses))
      {
        spline_path.push_back(getPoseAtTime(t));
      }
      return spline_path;
    }

  protected:
    std::vector<SplineType> m_splines;
    bool m_loaded;

    ~PathGenerator()
    {
    }
  };

  class HermitePath: public PathGenerator<HermiteSpline>
  {
  public:
    HermitePath() :
        PathGenerator()
    {
    }

    void load(std::vector<geometry_msgs::Pose2D> ground_targets) //assumes first ground_target is current location
    {
      m_splines.clear();
      //adjust orientations according to catmull-rom
      for(unsigned int i = 1; i < ground_targets.size() - 1; i++)
      {
        double dx = ground_targets.at(i + 1).x - ground_targets.at(i - 1).x;
        double dy = ground_targets.at(i + 1).y - ground_targets.at(i - 1).y;
        ground_targets.at(i).theta = atan2(dy, dx);
      }

      //calculate splines
      for(unsigned int i = 1; i < ground_targets.size(); i++)
      {
        m_splines.push_back(HermiteSpline(ground_targets.at(i - 1), ground_targets.at(i)));
      }

      m_loaded = true;
    }
  };

  class TurnGoTurnPath: public PathGenerator<LinearSpline>
  {
  public:
    TurnGoTurnPath() :
        PathGenerator()
    {
    }

    void load(std::vector<geometry_msgs::Pose2D> ground_targets) //assumes first ground_target is current location
    {
      m_splines.clear();
      //add pure turns between each movement
      std::vector<geometry_msgs::Pose2D> augmented_ground_targets;
      augmented_ground_targets.push_back(ground_targets.at(0));
      for(unsigned int i = 1; i < ground_targets.size(); i++)
      {
        double dx = ground_targets.at(i).x - ground_targets.at(i - 1).x;
        double dy = ground_targets.at(i).y - ground_targets.at(i - 1).y;
        double traversal_yaw = atan2(dy, dx);

        geometry_msgs::Pose2D turn = ground_targets.at(i - 1);
        turn.theta = traversal_yaw;
        augmented_ground_targets.push_back(turn);

        geometry_msgs::Pose2D go = ground_targets.at(i);
        go.theta = traversal_yaw;
        augmented_ground_targets.push_back(go);
      }
      augmented_ground_targets.push_back(ground_targets.at(ground_targets.size() - 1)); //final turn

      //calculate splines
      for(unsigned int i = 1; i < augmented_ground_targets.size(); i++)
      {
        m_splines.push_back(LinearSpline(augmented_ground_targets.at(i - 1), augmented_ground_targets.at(i)));
      }

      m_loaded = true;
    }
  };

  class SidestepPath: public PathGenerator<LinearSpline>
  {
  public:
    SidestepPath() :
        PathGenerator()
    {
    }

    void load(std::vector<geometry_msgs::Pose2D> ground_targets) //assumes first ground_target is current location
    {
      m_splines.clear();
      double start_yaw = ground_targets.at(0).theta;
      Vector2 side_vector;
      side_vector << sin(start_yaw), cos(start_yaw);

      //only translate until the very end
      std::vector<geometry_msgs::Pose2D> augmented_ground_targets;
      augmented_ground_targets.push_back(ground_targets.at(0));
      for(unsigned int i = 1; i < ground_targets.size(); i++)
      {
        Vector2 traversal_vector;
        traversal_vector(0) = ground_targets.at(i).x - ground_targets.at(i - 1).x;
        traversal_vector(1) = ground_targets.at(i).y - ground_targets.at(i - 1).y;

        Vector2 new_target;
        new_target = traversal_vector.dot(side_vector) / side_vector.norm() * side_vector;

        geometry_msgs::Pose2D sidestep = augmented_ground_targets.at(augmented_ground_targets.size() - 1);
        sidestep.x += new_target(0);
        sidestep.y += new_target(1);
        augmented_ground_targets.push_back(sidestep);

        geometry_msgs::Pose2D walk = ground_targets.at(i);
        walk.theta = start_yaw;
        augmented_ground_targets.push_back(walk);
      }
      augmented_ground_targets.push_back(ground_targets.at(ground_targets.size() - 1)); //final turn

      //calculate splines
      for(unsigned int i = 1; i < augmented_ground_targets.size(); i++)
      {
        m_splines.push_back(LinearSpline(augmented_ground_targets.at(i - 1), augmented_ground_targets.at(i)));
      }

      m_loaded = true;
    }
  };

  template<typename PathGeneratorType> //PathGeneratorType must be a subclass of PathGenerator
  class NaiveFootstepGenerator
  {
  public:
    //assumes rectangular bounding box for feet and square kinematic feasibility region
    //defaults are specific to Atlas
    NaiveFootstepGenerator(double foot_buffer = 0.015, double max_step_distance = 0.35, double min_turnout = -1e-2, double max_turnout = 1.0, double t_step = 1e-3, double foot_x_span = 0.265, double foot_y_span = 0.135, double foot_z_span = 0.056)
    {
      m_t_step = t_step;
      m_foot_buffer = foot_buffer;
      m_foot_x_span = foot_x_span;
      m_foot_y_span = foot_y_span;
      m_foot_z_span = foot_z_span;
      m_max_step_distance = max_step_distance;
      m_min_turnout = min_turnout;
      m_max_turnout = max_turnout;
    }

    void load(std::vector<geometry_msgs::PoseStamped> ground_targets)
    {
      std::vector<geometry_msgs::Pose2D> ground_poses;
      for(unsigned int i = 0; i < ground_targets.size(); i++)
      {
        geometry_msgs::Pose2D pose;
        pose.x = ground_targets.at(i).pose.position.x;
        pose.y = ground_targets.at(i).pose.position.y;
        pose.theta = tf::getYaw(ground_targets.at(i).pose.orientation);
      }
      m_generator.load(ground_poses);
    }

    void load(std::vector<geometry_msgs::Pose> ground_targets)
    {
      std::vector<geometry_msgs::Pose2D> ground_poses;
      for(unsigned int i = 0; i < ground_targets.size(); i++)
      {
        geometry_msgs::Pose2D pose;
        pose.x = ground_targets.at(i).position.x;
        pose.y = ground_targets.at(i).position.y;
        pose.theta = tf::getYaw(ground_targets.at(i).orientation);
      }
      m_generator.load(ground_poses);
    }

    void load(std::vector<geometry_msgs::Pose2D> ground_poses)
    {
      m_generator.load(ground_poses);
    }

    std::vector<geometry_msgs::Pose2D> getPath()
    {
      return m_generator.getPathPoses();
    }

    std::vector<geometry_msgs::Pose> getSimpleSteps(double max_linear_step, double max_angular_step, bool lead_with_left, double stride_width, double turn_out)
    {
      //todo: add loaded check
      std::vector<geometry_msgs::Pose> steps;
      geometry_msgs::Pose2D current_pose = m_generator.getPoseAtTime(0.0);
      double current_time = 0.0;
      while(ros::ok() && current_time < 1.0)
      {
        if(!findNextPathPose(current_pose, current_time, max_linear_step, max_angular_step, current_pose, current_time))
        {
          //add the last one
          current_time = 1.0;
          current_pose = m_generator.getPoseAtTime(current_time);
        }

        bool even_step = ((steps.size() % 2) == 0);
        bool left_foot = lead_with_left? even_step : !even_step;
        double y_offset = left_foot? stride_width / 2.0 : -stride_width / 2.0;
        double t_offset = left_foot? turn_out / 2.0 : -turn_out / 2.0;
        steps.push_back(getFootPose(current_pose, 0.0, y_offset, t_offset));

        if(current_time == 1.0) //add both feet for the last one
        {
          steps.push_back(getFootPose(current_pose, 0.0, -y_offset, -t_offset));
        }
      }

      return steps;
    }

    bool getSteps(geometry_msgs::Pose current_left_pose, geometry_msgs::Pose current_right_pose, bool lead_with_left, double stride_width, double turn_out, std::vector<geometry_msgs::Pose>& steps)
    {
      steps.clear();
      geometry_msgs::Pose last_finalized_step = lead_with_left? current_right_pose : current_left_pose;
      geometry_msgs::Pose last_good_step = lead_with_left? current_left_pose : current_right_pose;
      double last_good_time = 0.0;
      bool making_progress = false;
      for(double t = 0.0; t <= 1.0; t += m_t_step)
      {
        //todo: catch time loops
        bool even_step = ((steps.size() % 2) == 0);
        bool left_foot = lead_with_left? even_step : !even_step;
        double y_offset = left_foot? stride_width / 2.0 : -stride_width / 2.0;
        double t_offset = left_foot? turn_out / 2.0 : -turn_out / 2.0;

        if((t + m_t_step) > 1.0) //add both feet for the last one
        {
          //todo: check feasibility of adding the final steps
          geometry_msgs::Pose2D final_pose = m_generator.getPoseAtTime(1.0);
          steps.push_back(getFootPose(final_pose, 0.0, y_offset, t_offset));
          steps.push_back(getFootPose(final_pose, 0.0, -y_offset, -t_offset));
        }
        else
        {
          geometry_msgs::Pose2D test_pose = m_generator.getPoseAtTime(t);
          geometry_msgs::Pose test_step = getFootPose(test_pose, 0.0, y_offset, t_offset);
          if(stepValid(last_finalized_step, test_step, left_foot))
          {
            last_good_step = test_step;
            last_good_time = t;
            making_progress = true;
          }
          else
          {
            if(!making_progress)
            {
              return false; //complete failure
            }

            steps.push_back(last_good_step);
            last_finalized_step = last_good_step;
            t = last_good_time; // - m_t_step; //back up to the last good time for next iteration

            making_progress = false;
          }
        }
      }

      return true;
    }

  private:
    PathGeneratorType m_generator;
    double m_t_step;
    double m_foot_buffer;
    double m_foot_x_span;
    double m_foot_y_span;
    double m_foot_z_span;
    double m_max_step_distance;
    double m_min_turnout;
    double m_max_turnout;

    bool stepValid(geometry_msgs::Pose last_step, geometry_msgs::Pose test_step, bool testing_left_foot)
    {
      geometry_msgs::Pose left_foot = testing_left_foot? test_step : last_step;
      geometry_msgs::Pose right_foot = testing_left_foot? last_step : test_step;

      tf::Transformer transformer;
      transformer.setTransform(matec_utils::poseToStampedTransform(left_foot, "ground", "left"));
      transformer.setTransform(matec_utils::poseToStampedTransform(right_foot, "ground", "right"));

      tf::Stamped<tf::Vector3> left_tr_corner_in_right;
      tf::Stamped<tf::Vector3> left_br_corner_in_right;
      transformer.transformPoint("right", tf::Stamped<tf::Vector3>(tf::Vector3(m_foot_x_span / 2.0, -m_foot_y_span / 2.0, 0), ros::Time(0), "left"), left_tr_corner_in_right);
      transformer.transformPoint("right", tf::Stamped<tf::Vector3>(tf::Vector3(-m_foot_x_span / 2.0, -m_foot_y_span / 2.0, 0), ros::Time(0), "left"), left_br_corner_in_right);

      //don't step too far
      double dx = left_foot.position.x - right_foot.position.x;
      double dy = left_foot.position.y - right_foot.position.y;
      bool distance_ok = (dx * dx + dy * dy) < (m_max_step_distance * m_max_step_distance);

      //don't step on ourselves
      bool tr_y_ok = left_tr_corner_in_right.y() >= (m_foot_y_span / 2.0 + m_foot_buffer);
      bool br_y_ok = left_br_corner_in_right.y() >= (m_foot_y_span / 2.0 + m_foot_buffer);

      //don't allow turn-in
      double left_yaw = tf::getYaw(left_foot.orientation);
      double right_yaw = tf::getYaw(right_foot.orientation);
      double turnout = matec_utils::angleSub(left_yaw, right_yaw);
      bool turn_in_ok = matec_utils::clamp(turnout, m_min_turnout, m_max_turnout) == turnout; //todo: add flag to toggle this

      return distance_ok && turn_in_ok && tr_y_ok && br_y_ok;
    }

    bool findNextPathPose(geometry_msgs::Pose2D current_pose, double current_time, double max_linear_step, double max_angular_step, geometry_msgs::Pose2D& next_pose, double& next_time)
    {
      for(double t = current_time + m_t_step; t <= 1.0; t += m_t_step)
      {
        geometry_msgs::Pose2D new_pose = m_generator.getPoseAtTime(t);
        double dx = new_pose.x - current_pose.x;
        double dy = new_pose.y - current_pose.y;
        double linear_dist = sqrt(dx * dx + dy * dy);
        double angular_dist = fabs(matec_utils::angleSub(current_pose.theta, new_pose.theta));
        if(linear_dist < max_linear_step && angular_dist < max_angular_step)
        {
          next_pose = new_pose;
          next_time = t;
        }
        else if(t >= 1.0) //ran out of poses without finding a good one
        {
          return false;
        }
        else //went too far
        {
          return true;
        }
      }
      return false;
    }

    geometry_msgs::Pose getFootPose(geometry_msgs::Pose2D center_pose, double x_offset, double y_offset, double t_offset)
    {
      geometry_msgs::Pose pose;
      pose.position.x = center_pose.x + x_offset * cos(center_pose.theta) - y_offset * sin(center_pose.theta);
      pose.position.y = center_pose.y + x_offset * sin(center_pose.theta) + y_offset * cos(center_pose.theta);
      pose.position.z = 0.0;
      pose.orientation = tf::createQuaternionMsgFromYaw(center_pose.theta + t_offset);

      return pose;
    }
  };

  typedef matec_utils::NaiveFootstepGenerator<matec_utils::TurnGoTurnPath> TGTGenerator;
  typedef matec_utils::NaiveFootstepGenerator<matec_utils::HermitePath> HermiteGenerator;
  typedef matec_utils::NaiveFootstepGenerator<matec_utils::SidestepPath> SidestepGenerator;

  inline std::vector<unsigned char> getAlternatingFeet(bool lead_with_left, unsigned int length)
  {
    std::vector<unsigned char> feet(length, lead_with_left? 0 : 1); //todo: don't use magic numbers
    for(unsigned int i = 1; i < feet.size(); i++)
    {
      //alternate feet
      feet.at(i) = (feet.at(i - 1) == 0)? 1 : 0;
    }
    return feet;
  }

  inline void visualizePath(std::vector<geometry_msgs::Pose2D> path, geometry_msgs::PoseArray& path_visualization, std::string ground_frame = "ground")
  {
    path_visualization.poses.clear();
    path_visualization.header.frame_id = ground_frame;
    path_visualization.header.stamp = ros::Time(0);
    for(unsigned int i = 0; i < path.size(); i++)
    {
      geometry_msgs::Pose pose;
      pose.position.x = path.at(i).x;
      pose.position.y = path.at(i).y;
      pose.position.z = 0;
      pose.orientation = tf::createQuaternionMsgFromYaw(path.at(i).theta);
      path_visualization.poses.push_back(pose);
    }
  }

  inline void visualizeSteps(std::vector<geometry_msgs::Pose> steps, std::vector<unsigned char> feet, visualization_msgs::MarkerArray& step_visualization, std::string frame = "ground", double foot_x_span = 0.256, double foot_y_span = 0.135, double foot_z_span = 0.056)
  {
    visualization_msgs::Marker base_left_marker, base_right_marker;
    base_left_marker.ns = "steps";
    base_left_marker.type = visualization_msgs::Marker::CUBE;
    base_left_marker.action = visualization_msgs::Marker::ADD;
    base_left_marker.header.frame_id = frame;
    base_left_marker.header.stamp = ros::Time(0);
    base_left_marker.scale.x = foot_x_span;
    base_left_marker.scale.y = foot_y_span;
    base_left_marker.scale.z = foot_z_span;
    base_left_marker.color.r = 1.0;
    base_left_marker.color.g = 1.0;
    base_left_marker.color.b = 1.0;
    base_left_marker.color.a = 0.8;
    base_right_marker = base_left_marker;
    base_right_marker.color.r = 0.2;
    base_right_marker.color.g = 0.2;
    base_right_marker.color.b = 0.2;
    base_right_marker.color.a = 0.8;

    WTFASSERT(steps.size() == feet.size());
    step_visualization.markers.clear();
    for(unsigned int i = 0; i < steps.size(); i++)
    {
      bool right_foot = (bool) feet.at(i);
      visualization_msgs::Marker next_marker = right_foot? base_right_marker : base_left_marker;
      next_marker.pose = steps.at(i); //todo: raise pose z such that the face of the box is on the ground
      next_marker.id = step_visualization.markers.size();
      step_visualization.markers.push_back(next_marker);
    }
  }

  inline void visualizeSteps(std::vector<geometry_msgs::PoseStamped> steps, std::vector<unsigned char> feet, visualization_msgs::MarkerArray& step_visualization, double foot_x_span = 0.256, double foot_y_span = 0.135, double foot_z_span = 0.056)
  {
    std::string frame = steps.at(0).header.frame_id;
    std::vector<geometry_msgs::Pose> unstamped(steps.size());
    for(unsigned int i = 0; i < steps.size(); i++)
    {
      unstamped.at(i) = steps.at(i).pose;
    }

    visualizeSteps(unstamped, feet, step_visualization, frame, foot_x_span, foot_y_span, foot_z_span);
  }
}

#endif //GROUND_PATH_GENERATORS_H
