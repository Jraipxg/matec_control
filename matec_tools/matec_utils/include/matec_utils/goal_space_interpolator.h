#ifndef GOAL_SPACE_INTERPOLATOR_H
#define GOAL_SPACE_INTERPOLATOR_H

#include <ros/ros.h>
#include <deque>
#include <matec_utils/common_functions.h>
#include <matec_msgs/GoalMotion.h>
#include <matec_utils/linear_interpolator.h>
#include <matec_utils/minimum_jerk_interpolator.h>

namespace matec_utils
{
  template<typename InterpolatorType>
  class GoalSpaceInterpolator
  {
  public:
    GoalSpaceInterpolator()
    {
      //set limits to max
      x_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      y_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      z_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      x_min_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      y_min_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      z_min_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      R_min_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      P_min_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      Y_min_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      x_max_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      y_max_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      z_max_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      R_max_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      P_max_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
      Y_max_interpolator.setLimits(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
    }

    static double timeForMax(std::vector<geometry_msgs::PoseStamped> initials_in_stable, std::vector<geometry_msgs::PoseStamped> finals_in_stable, matec_msgs::GoalMotion motion, double max_linear_accel, double max_angular_accel)
    {
      double min_motion_duration = motion.segment_duration;
      for(unsigned int i = 0; i < initials_in_stable.size(); i++)
      {
        boost::this_thread::interruption_point();
        matec_utils::Matrix4 stableTstart = matec_utils::poseStampedToMatrix(initials_in_stable.at(i));
        matec_utils::Matrix4 stableTend = matec_utils::poseStampedToMatrix(finals_in_stable.at(i));
        matec_utils::Matrix4 endTstart = stableTend.inverse() * stableTstart;
        matec_utils::Vector6 goal_space_error;
        poseInGoalRegion(endTstart, motion.final_goal_regions.at(i), goal_space_error);
        double goal_space_position_error = std::sqrt((double) (goal_space_error(3) * goal_space_error(3) + goal_space_error(4) * goal_space_error(4) + goal_space_error(5) * goal_space_error(5)));
        double goal_space_orientation_error = std::sqrt((double) (goal_space_error(0) * goal_space_error(0) + goal_space_error(1) * goal_space_error(1) + goal_space_error(2) * goal_space_error(2)));

        min_motion_duration = std::max(min_motion_duration, InterpolatorType::timeForMax(0, fabs(goal_space_position_error), motion.max_linear_velocity, max_linear_accel));
        min_motion_duration = std::max(min_motion_duration, InterpolatorType::timeForMax(0, fabs(goal_space_orientation_error), motion.max_angular_velocity, max_angular_accel));
      }
      return min_motion_duration;
    }

    void loadTrajectory(geometry_msgs::PoseStamped initial_in_stable, matec_msgs::GoalRegion initial_region, geometry_msgs::PoseStamped final_in_stable, matec_msgs::GoalRegion final_region, double duration)
    {
      x_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_in_stable.pose.position.x), matec_utils::ViaPoint(ros::Time(duration), final_in_stable.pose.position.x));
      y_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_in_stable.pose.position.y), matec_utils::ViaPoint(ros::Time(duration), final_in_stable.pose.position.y));
      z_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_in_stable.pose.position.z), matec_utils::ViaPoint(ros::Time(duration), final_in_stable.pose.position.z));
      quaternion_interpolator.loadSimpleTrajectory(matec_utils::QuaternionViaPoint(ros::Time(0), initial_in_stable.pose.orientation), matec_utils::QuaternionViaPoint(ros::Time(duration), final_in_stable.pose.orientation));

      x_min_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.x.min), matec_utils::ViaPoint(ros::Time(duration), final_region.x.min));
      y_min_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.y.min), matec_utils::ViaPoint(ros::Time(duration), final_region.y.min));
      z_min_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.z.min), matec_utils::ViaPoint(ros::Time(duration), final_region.z.min));
      R_min_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.R.min), matec_utils::ViaPoint(ros::Time(duration), final_region.R.min));
      P_min_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.P.min), matec_utils::ViaPoint(ros::Time(duration), final_region.P.min));
      Y_min_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.Y.min), matec_utils::ViaPoint(ros::Time(duration), final_region.Y.min));

      x_max_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.x.max), matec_utils::ViaPoint(ros::Time(duration), final_region.x.max));
      y_max_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.y.max), matec_utils::ViaPoint(ros::Time(duration), final_region.y.max));
      z_max_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.z.max), matec_utils::ViaPoint(ros::Time(duration), final_region.z.max));
      R_max_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.R.max), matec_utils::ViaPoint(ros::Time(duration), final_region.R.max));
      P_max_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.P.max), matec_utils::ViaPoint(ros::Time(duration), final_region.P.max));
      Y_max_interpolator.loadSimpleTrajectory(matec_utils::ViaPoint(ros::Time(0), initial_region.Y.max), matec_utils::ViaPoint(ros::Time(duration), final_region.Y.max));
    }

    void getInterpolatedState(double interpolated_time, matec_msgs::GoalRegion& interpolated_region)
    {
      matec_utils::State state;
      interpolated_region.goal_frame.header.stamp = ros::Time(0);

      quaternion_interpolator.getInterpolatedState(ros::Time(interpolated_time), interpolated_region.goal_frame.pose.orientation);

      x_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.goal_frame.pose.position.x = state.position;

      y_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.goal_frame.pose.position.y = state.position;

      z_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.goal_frame.pose.position.z = state.position;

      x_min_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.x.min = state.position;

      y_min_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.y.min = state.position;

      z_min_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.z.min = state.position;

      R_min_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.R.min = state.position;

      P_min_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.P.min = state.position;

      Y_min_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.Y.min = state.position;

      x_max_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.x.max = state.position;

      y_max_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.y.max = state.position;

      z_max_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.z.max = state.position;

      R_max_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.R.max = state.position;

      P_max_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.P.max = state.position;

      Y_max_interpolator.getInterpolatedState(ros::Time(interpolated_time), state);
      interpolated_region.Y.max = state.position;
    }

  protected:
    InterpolatorType x_interpolator;
    InterpolatorType y_interpolator;
    InterpolatorType z_interpolator;
    matec_utils::QuaternionInterpolator<InterpolatorType> quaternion_interpolator;
    InterpolatorType x_min_interpolator;
    InterpolatorType y_min_interpolator;
    InterpolatorType z_min_interpolator;
    InterpolatorType R_min_interpolator;
    InterpolatorType P_min_interpolator;
    InterpolatorType Y_min_interpolator;
    InterpolatorType x_max_interpolator;
    InterpolatorType y_max_interpolator;
    InterpolatorType z_max_interpolator;
    InterpolatorType R_max_interpolator;
    InterpolatorType P_max_interpolator;
    InterpolatorType Y_max_interpolator;
  };

}

#endif //GOAL_SPACE_INTERPOLATOR_H
