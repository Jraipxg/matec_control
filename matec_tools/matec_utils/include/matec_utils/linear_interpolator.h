#ifndef LINEAR_INTERPOLATOR_H
#define LINEAR_INTERPOLATOR_H

#include "interpolator.h"

namespace matec_utils
{
  class LinearInterpolator: public Interpolator
  {
  public:
    LinearInterpolator() :
        Interpolator()
    {

    }
    static double timeForMaxVelocity(double xi, double xf, double max_velocity)
    {
      return fabs(xf - xi) / max_velocity;
    }

    static double timeForMax(double xi, double xf, double max_velocity, double max_acceleration)
    {
      return timeForMaxVelocity(xi, xf, max_velocity);
    }

    double timeForMaxVelocity(double xi, double xf)
    {
      return timeForMaxVelocity(xi, xf, m_max_velocity);
    }

    double timeForMax(double xi, double xf)
    {
      return timeForMaxVelocity(xi, xf);
    }

    void initializeTrajectory(ViaPoint start_state)
    {
      m_traj.clear();
      start_state.finish_time = ros::Time(0);
      m_traj.push_back(start_state);
    }

    void addVia(ViaPoint via, double duration)
    {
      WTFASSERT(m_traj.size() != 0);
      WTFASSERT(duration >= 0.0);
//      duration = std::max(duration, timeForMax(m_traj.at(m_traj.size() - 1).target_state.position, via.target_state.position));
      via.finish_time = m_traj.at(m_traj.size() - 1).finish_time + ros::Duration(duration);

      //calculate vel/acc if possible
      m_traj.at(m_traj.size() - 1).target_state.velocity = (via.target_state.position - m_traj.at(m_traj.size() - 1).target_state.position) / duration;
      if(m_traj.size() >= 2)
      {
        m_traj.at(m_traj.size() - 2).target_state.acceleration = (m_traj.at(m_traj.size() - 1).target_state.velocity - m_traj.at(m_traj.size() - 2).target_state.velocity) / (m_traj.at(m_traj.size() - 1).finish_time.toSec() - m_traj.at(m_traj.size() - 2).finish_time.toSec());
      }

      m_traj.push_back(via);
      m_have_valid_trajectory = true;
      m_last_slice_index = 1;
    }

    void loadTrajectory(Trajectory& traj, bool populate_non_position_fields = true)
    {
//      m_traj = traj;
//      if(!populate_non_position_fields)
//      {
//        return;
//      }
//      else //assume only positions and times have been set in the trajectory
//      {
//        //adjust timestamps
//        for(unsigned int i = 1; i < m_traj.size(); i++)
//        {
//          double original_slice_time = traj[i].finish_time.toSec() - traj[i - 1].finish_time.toSec();
//          double slice_time = original_slice_time;
//          double min_slice_time = timeForMaxVelocity(m_traj[i - 1].target_state.position, m_traj[i].target_state.position);
//          if((min_slice_time - slice_time) > 1.0e-6)
//          {
//            WTFASSERT(min_slice_time > slice_time);
//            ROS_ERROR("Changed duration of segment %d from %g to %g to meet max velocity constraints (%gs over the limit)!", i, slice_time, min_slice_time, min_slice_time - slice_time);
//            slice_time = min_slice_time;
//            WTFASSERT(slice_time >= 0.0);
//          }
//          if(slice_time == 0)
//          {
//            ROS_ERROR("SLICE TIME ZERO from idx %d to %d", (int ) i - 1, (int ) i);
//          }
//          m_traj[i].finish_time = m_traj[i - 1].finish_time + ros::Duration(slice_time);
//        }
//
//        //adjust velocities
//        for(unsigned int i = 0; i < m_traj.size() - 1; i++)
//        {
//          double dp = (m_traj[i + 1].target_state.position - m_traj[i].target_state.position);
//          double dt = (m_traj[i + 1].finish_time - m_traj[i].finish_time).toSec();
//          WTFASSERT(dt > 0.0);
//          m_traj[i].target_state.velocity = dp / dt;
//        }
//
//        //adjust accelerations
//        for(unsigned int i = 0; i < m_traj.size() - 1; i++)
//        {
//          double dv = (m_traj[i + 1].target_state.velocity - m_traj[i].target_state.velocity);
//          double dt = (m_traj[i + 1].finish_time - m_traj[i].finish_time).toSec();
//          WTFASSERT(dt > 0.0);
//          m_traj[i].target_state.acceleration = dv / dt;
//          if(fabs(m_traj[i].target_state.acceleration) > 0.5)
//          {
//            std::cerr << " a: " << m_traj[i].target_state.acceleration << "dv:" << dv << " dt:" << dt << std::endl;
//          }
//        }
//        m_traj[m_traj.size() - 1].target_state.acceleration = 0.0;
//      }
//
//      m_have_valid_trajectory = true;
//      m_last_slice_index = 1; //m_traj[0] is the start state
    }

    double loadSimpleTrajectory(ViaPoint initial, ViaPoint final)
    {
      m_traj.clear();
      m_traj.push_back(initial);
      m_traj.push_back(final);

      double slice_time = (m_traj[1].finish_time - m_traj[0].finish_time).toSec();
      double min_slice_time = timeForMaxVelocity(m_traj[0].target_state.position, m_traj[1].target_state.position, m_max_velocity);
      if((min_slice_time - slice_time) > 0.000001)
      {
        ROS_WARN("Changed duration from %g to %g to meet max velocity constraints (%gs over the limit)!", slice_time, min_slice_time, min_slice_time - slice_time);
        slice_time = min_slice_time;
        m_traj[1].finish_time = m_traj[0].finish_time + ros::Duration(slice_time);
      }

      m_have_valid_trajectory = true;
      m_last_slice_index = 1; //m_traj[0] is the start state

      return slice_time;
    }

    bool getInterpolatedState(ros::Time time, State& state)
    {
      if(!m_have_valid_trajectory)
      {
        return false;
      }

      //get current trajectory slice (assumes that finish_times are ordered)
      ViaPoint from_via;
      ViaPoint to_via;
      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
      {
        if(time.toSec() <= m_traj.at(i).finish_time.toSec() && time.toSec() >= m_traj.at(i - 1).finish_time.toSec()) //found the slice!
        {
          from_via = m_traj.at(i - 1);
          to_via = m_traj.at(i);

          m_last_slice_index = i;

//          ROS_INFO("Chose slice %d (time %g->%g) based on current time %g", i, from_via.finish_time.toSec(), to_via.finish_time.toSec(), time.toSec());
          break;
        }
        else if(i == (m_traj.size() - 1)) //ran out of slices!
        {
//          ROS_INFO("Trajectory complete at time %g (trajectory bounded by %g,%g)", time.toSec(), m_traj.at(0).finish_time.toSec(), m_traj.at(m_traj.size()-1).finish_time.toSec());
          m_have_valid_trajectory = false;
          return false;
        }
      }

      //figure out where we should be
      double dt = (time - from_via.finish_time).toSec();
      double slice_time = (to_via.finish_time - from_via.finish_time).toSec();
      double time_fraction = dt / slice_time;
      state.position = (1 - time_fraction) * from_via.target_state.position + (time_fraction) * to_via.target_state.position;
      state.velocity = (1 - time_fraction) * from_via.target_state.velocity + (time_fraction) * to_via.target_state.velocity;
      state.acceleration = (1 - time_fraction) * from_via.target_state.acceleration + (time_fraction) * to_via.target_state.acceleration;

//      ROS_INFO("dt:%g, st:%g, tf:%g, p:%g, v:%g, a:%g", dt, slice_time, time_fraction, state.position, state.velocity, state.acceleration);

      return true;
    }
  };
}

#endif //LINEAR_INTERPOLATOR_H
