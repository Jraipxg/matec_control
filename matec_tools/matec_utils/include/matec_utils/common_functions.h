#ifndef COMMON_FUNCTIONS_H
#define COMMON_FUNCTIONS_H

#include <ros/ros.h>
#include <kdl/tree.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <tf_conversions/tf_kdl.h>

#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <matec_msgs/GoalRegion.h>

#include <cmath>

#define WTFASSERT(assertion) if(!(assertion)){std::cerr << "ASSERTION FAILED AT LINE " << __LINE__ << " OF FILE " << __FILE__ << std::endl; std::stringstream ss; ss << "kill -9 " << getpid(); int stupid = system(ss.str().c_str()); stupid=stupid;}

namespace matec_utils
{
  typedef Eigen::Matrix<double, 2, 1> Vector2;
  typedef Eigen::Matrix<double, 3, 1> Vector3;
  typedef Eigen::Matrix<double, 4, 1> Vector4;
  typedef Eigen::Matrix<double, 6, 1> Vector6;
  typedef Eigen::Matrix<double, 10, 1> Vector10;
  typedef Eigen::Matrix<double, 11, 1> Vector11;
  typedef Eigen::Matrix<double, 3, 3> Matrix3;
  typedef Eigen::Matrix<double, 4, 4> Matrix4;
  typedef Eigen::Matrix<double, 6, 6> Matrix6;
  typedef Eigen::Matrix<double, 6, 10> Matrix610;
  typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> Matrix;
  typedef Eigen::Matrix<double, Eigen::Dynamic, 1> Vector;

  template<typename V> //V must be an eigen vector type
  inline V oneAugmentedZero() //todo: come up with a less silly name
  {
    V vector = V::Zero();
    vector(vector.rows() - 1) = 1.0;
    return vector;
  }

  inline double fixAngle(double angle)
  {
    return (angle > M_PI)? (angle - 2.0 * M_PI) : ((angle < -M_PI)? (angle + 2.0 * M_PI) : angle);
  }

  inline double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  template<typename V>
  inline V vectorClamp(V a, V b, V c)
  {
    V clamped;

    for(unsigned int i = 0; i < clamped.rows(); i++)
    {
      clamped(i) = clamp(a(i), b(i), c(i));
    }

    return clamped;
  }

  inline double sign(double val)
  {
    return (val == 0.0)? 0.0 : ((val < 0)? -1.0 : 1.0);
  }

  inline double sat(double val, double boundary_layer = 1e-3)
  {
    return (boundary_layer == 0)? sign(val) : clamp(val / boundary_layer, -1.0, 1.0);
  }

  inline double max3(double a, double b, double c)
  {
    return std::max(a, std::max(b, c));
  }

  inline double min3(double a, double b, double c)
  {
    return std::min(a, std::min(b, c));
  }

  inline double angleAverage(double a1, double a2)
  {
    return atan2((sin(a1) + sin(a2)) / 2.0, (cos(a1) + cos(a2)) / 2.0);
  }

  inline double angleAdd(double a1, double a2) //returns a1 + a2, wrapped
  {
    return atan2(sin(a1 + a2), cos(a1 + a2));
  }

  inline double angleSub(double a1, double a2) //returns a1 - a2, wrapped
  {
    return atan2(sin(a1 - a2), cos(a1 - a2));
  }

  //returns a real value between 0 and 1
  inline long double uniformRandom()
  {
    return ((long double) rand()) / ((long double) RAND_MAX);
  }

  //returns a real value between -1 and 1
  inline long double uniformRandomCentered()
  {
    return 2 * ((long double) (rand() - (((long double) RAND_MAX) / 2.0))) / ((long double) RAND_MAX);
  }

  inline std::pair<long double, long double> uniformRandomCircle()
  {
    std::pair<long double, long double> coordinates;
    long double t = 2 * M_PI * uniformRandom();
    long double u = uniformRandom() + uniformRandom();
    long double r = u > 1? 2 - u : u;
    coordinates.first = r * cos(t);
    coordinates.second = r * sin(t);
    return coordinates;
  }

  //box-muller transform method
#define NORMAL_RANDOM_USE_BASIC_FORM
  inline long double normalRandom(long double mean, long double stddev)
  {
    long double z0;
    static long double z1 = 0.0;
    static bool z1_cached = 0;
    if(!z1_cached)
    {
#ifdef NORMAL_RANDOM_USE_BASIC_FORM
      //normal form
      long double r = sqrt(-2.0 * log(uniformRandom()));
      long double t = 2 * M_PI * uniformRandom();
      z0 = r * cos(t);
      z1 = r * sin(t);

#else
      //polar form
      long double x = uniformRandomCentered();
      long double y = uniformRandomCentered();
      long double r = x * x + y * y;
      while(r == 0.0 || r > 1.0)
      {
        x = uniformRandomCentered();
        y = uniformRandomCentered();
        r = x * x + y * y;
      }

      long double d = sqrt(-2.0 * log(r) / r);
      z0 = x * d;
      z1 = y * d;
#endif
//      double result = z0 * stddev + mean;
      z1_cached = true;
      return z0 * stddev + mean;
    }
    else
    {
      z1_cached = false;
      return z1 * stddev + mean;
    }
  }

  inline void quaternionToRPY(tf::Quaternion quat, double& roll, double& pitch, double& yaw)
  {
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
  }

  inline void quaternionToRPY(geometry_msgs::Quaternion quat, double& roll, double& pitch, double& yaw)
  {
    tf::Quaternion tf_quat;
    tf::quaternionMsgToTF(quat, tf_quat);
    quaternionToRPY(tf_quat, roll, pitch, yaw);
  }

  inline KDL::JntArray vectorToJntArray(std::vector<double> vec)
  {
    KDL::JntArray jnt_array(vec.size());
    for(unsigned int i = 0; i < vec.size(); i++)
    {
      jnt_array(i) = vec[i];
    }
    return jnt_array;
  }

  inline std::vector<double> jntArrayToVector(KDL::JntArray jnt_array)
  {
    std::vector<double> vec;
    for(unsigned int r = 0; r < jnt_array.rows(); r++)
    {
      for(unsigned int c = 0; c < jnt_array.columns(); c++)
      {
        vec.push_back(jnt_array(r, c));
      }
    }
    return vec;
  }

  inline Eigen::MatrixXd kdlJacobianToEigen(KDL::Jacobian jacobian)
  {
    Eigen::MatrixXd jac = Eigen::MatrixXd(jacobian.rows(), jacobian.columns());
    for(unsigned int i = 0; i < jacobian.rows(); i++) // 6 rows: x, y, z, r, p, y
    {
      for(unsigned int j = 0; j < jacobian.columns(); j++) //3 cols
      {
        jac(i, j) = jacobian(i, j);
      }
    }

    return jac;
  }

  inline Matrix3 cross(Vector3 v)
  {
    Matrix3 m;
    m << 0, -v(2), v(1), v(2), 0, -v(0), -v(1), v(0), 0;
    return m;
  }

  inline Matrix6 cross(Vector6 sv)
  {
    Matrix6 m;

    Vector3 w = sv.topRows(3);
    Vector3 v = sv.bottomRows(3);
    m.topLeftCorner(3, 3) = cross(w);
    m.bottomLeftCorner(3, 3) = cross(v);
    m.topRightCorner(3, 3) = Matrix3::Zero();
    m.bottomRightCorner(3, 3) = m.topLeftCorner(3, 3);

    return m;
  }

  inline Vector3 kdlToEigen(KDL::Vector v)
  {
    return Vector3(v.x(), v.y(), v.z());
  }

  inline Matrix3 kdlToEigen(KDL::Rotation rot)
  {
    Matrix3 m;
    for(unsigned int i = 0; i < 3; i++)
    {
      for(unsigned int j = 0; j < 3; j++)
      {
        m(i, j) = rot(i, j);
      }
    }
    return m;
  }

  inline Matrix3 kdlToEigen(KDL::RotationalInertia rot)
  {
    Matrix3 m;
    for(unsigned int i = 0; i < 9; i++)
    {
      m(i) = rot.data[i];
    }
    return m;
  }

  inline Matrix6 constructInertia(double m, Vector3 r, Matrix3 Ic)
  {
    Matrix6 inertia;

    inertia.topLeftCorner(3, 3) = Ic - m * cross(r) * cross(r);
    inertia.topRightCorner(3, 3) = m * cross(r);
    inertia.bottomLeftCorner(3, 3) = -m * cross(r);
    inertia.bottomRightCorner(3, 3) = m * Matrix3::Identity(3, 3);

    return inertia;
  }

  inline Matrix6 kdlToEigen(KDL::RigidBodyInertia kdl_inertia)
  {
    Matrix6 inertia;

    Matrix3 J = kdlToEigen(kdl_inertia.getRotationalInertia());
    Vector3 r = kdlToEigen(kdl_inertia.getCOG());
    double m = kdl_inertia.getMass();
    Matrix3 Ic = J - m * cross(r) * cross(r).transpose();

    return constructInertia(m, r, Ic);
  }

  inline Matrix6 kdlToEigen(KDL::Frame frame)
  {
    Matrix6 trans;

    trans.topLeftCorner(3, 3) = kdlToEigen(frame.M);
    trans.bottomLeftCorner(3, 3) = cross(kdlToEigen(frame.p)) * trans.topLeftCorner(3, 3);
    trans.topRightCorner(3, 3) = Matrix3::Zero();
    trans.bottomRightCorner(3, 3) = trans.topLeftCorner(3, 3);

    return trans;
  }

  inline Vector4 pointToVector(geometry_msgs::Point point)
  {
    Vector4 vec;
    vec << point.x, point.y, point.z, 1.0;
    return vec;
  }

  inline geometry_msgs::Point vectorToPoint(Vector4 vec)
  {
    geometry_msgs::Point point;
    point.x = vec(0);
    point.y = vec(1);
    point.z = vec(2);
    return point;
  }

  inline Vector4 pointStampedToVector(geometry_msgs::PointStamped point)
  {
    return pointToVector(point.point);
  }

  inline geometry_msgs::PointStamped vectorToPointStamped(Vector4 vec, std::string frame_id, ros::Time stamp = ros::Time(0))
  {
    geometry_msgs::PointStamped point;
    point.point = vectorToPoint(vec);
    point.header.frame_id = frame_id;
    point.header.stamp = stamp;

    return point;
  }

  inline Matrix4 pureTranslation(double dx, double dy, double dz)
  {
    Matrix4 mat = Matrix4::Identity();
    mat.topRightCorner(3, 1) << dx, dy, dz;
    return mat;
  }

  inline Matrix4 pureRotation(Eigen::Quaterniond quat)
  {
    Matrix4 mat = Matrix4::Identity();
    mat.topLeftCorner(3, 3) = quat.toRotationMatrix();
    return mat;
  }

  inline Matrix4 pureRotation(geometry_msgs::Quaternion quat)
  {
    return pureRotation(Eigen::Quaterniond(quat.w, quat.x, quat.y, quat.z));
  }

  inline Matrix4 pureRotation(double roll, double pitch, double yaw)
  {
    return pureRotation(tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw));
  }

  inline Matrix4 poseToMatrix(geometry_msgs::Pose pose)
  {
    Matrix4 mat = Matrix4::Identity();
    mat.topLeftCorner(3, 3) = Eigen::Quaterniond(pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z).toRotationMatrix();
    mat.topRightCorner(3, 1) << pose.position.x, pose.position.y, pose.position.z;
    return mat;
  }

  inline geometry_msgs::Pose matrixToPose(Matrix4 mat)
  {
    Matrix3 E = mat.topLeftCorner(3, 3);
    Eigen::Quaterniond quat(E);
    Vector3 r = mat.topRightCorner(3, 1);

    geometry_msgs::Pose pose;
    pose.position.x = r(0);
    pose.position.y = r(1);
    pose.position.z = r(2);
    pose.orientation.w = quat.w();
    pose.orientation.x = quat.x();
    pose.orientation.y = quat.y();
    pose.orientation.z = quat.z();

    return pose;
  }

  inline Matrix4 poseStampedToMatrix(geometry_msgs::PoseStamped pose)
  {
    return poseToMatrix(pose.pose);
  }

  inline geometry_msgs::PoseStamped matrixToPoseStamped(Matrix4 mat, std::string frame_id, ros::Time stamp = ros::Time(0))
  {
    geometry_msgs::PoseStamped pose;
    pose.pose = matrixToPose(mat);
    pose.header.frame_id = frame_id;
    pose.header.stamp = stamp;

    return pose;
  }

  inline bool transformTwist(std::string target_frame, std::string source_frame, ros::Time time, tf::TransformListener& tf_listener, Vector6 twist_in, Vector6& twist_out)
  {
    if(!tf_listener.canTransform(target_frame, source_frame, time))
    {
      ROS_ERROR("Twist transform between %s and %s at time %g failed!", target_frame.c_str(), source_frame.c_str(), time.toSec());
      return false;
    }

    tf::StampedTransform trans;
    tf_listener.lookupTransform(target_frame, source_frame, time, trans);

    KDL::Rotation rot;
    tf::quaternionTFToKDL(trans.getRotation(), rot);

    KDL::Vector p;
    tf::vectorTFToKDL(trans.getOrigin(), p);

    Matrix6 adjoint;
    adjoint.topLeftCorner(3, 3) = kdlToEigen(rot);
    adjoint.bottomLeftCorner(3, 3) = cross(kdlToEigen(p)) * adjoint.topLeftCorner(3, 3);
    adjoint.topRightCorner(3, 3) = Matrix3::Zero();
    adjoint.bottomRightCorner(3, 3) = adjoint.topLeftCorner(3, 3);

    twist_out = adjoint * twist_in;

    return true;
  }

  inline bool transformTwist(std::string target_frame, tf::TransformListener& tf_listener, geometry_msgs::TwistStamped twist_in, geometry_msgs::TwistStamped& twist_out)
  {
    Vector6 vel_in, vel_out;
    vel_in << twist_in.twist.linear.x, twist_in.twist.linear.y, twist_in.twist.linear.z, twist_in.twist.angular.x, twist_in.twist.angular.y, twist_in.twist.angular.z;

    if(!transformTwist(target_frame, twist_in.header.frame_id, twist_in.header.stamp, tf_listener, vel_in, vel_out))
    {
      return false;
    }

    twist_out.twist.linear.x = vel_out(0);
    twist_out.twist.linear.y = vel_out(1);
    twist_out.twist.linear.z = vel_out(2);
    twist_out.twist.angular.x = vel_out(3);
    twist_out.twist.angular.y = vel_out(4);
    twist_out.twist.angular.z = vel_out(5);

    return true;
  }

  inline Vector6 twistToVector(geometry_msgs::Twist& twist)
  {
    Vector6 vector;
    vector(0) = twist.angular.x;
    vector(1) = twist.angular.y;
    vector(2) = twist.angular.z;
    vector(3) = twist.linear.x;
    vector(4) = twist.linear.y;
    vector(5) = twist.linear.z;
    return vector;
  }

  inline Vector6 twistToVector(geometry_msgs::TwistStamped& twist)
  {
    return twistToVector(twist.twist);
  }

  inline geometry_msgs::Twist vectorToTwist(Vector6 vector)
  {
    geometry_msgs::Twist twist;
    twist.angular.x = vector(0);
    twist.angular.y = vector(1);
    twist.angular.z = vector(2);
    twist.linear.x = vector(3);
    twist.linear.y = vector(4);
    twist.linear.z = vector(5);
    return twist;
  }

  inline geometry_msgs::TwistStamped vectorToTwistStamped(Vector6 vector, std::string frame_id, ros::Time stamp = ros::Time(0))
  {
    geometry_msgs::TwistStamped twist;
    twist.twist = vectorToTwist(vector);
    twist.header.frame_id = frame_id;
    twist.header.stamp = stamp;

    return twist;
  }

  inline Vector6 wrenchToVector(geometry_msgs::Wrench& wrench)
  {
    Vector6 vector;
    vector(0) = wrench.torque.x;
    vector(1) = wrench.torque.y;
    vector(2) = wrench.torque.z;
    vector(3) = wrench.force.x;
    vector(4) = wrench.force.y;
    vector(5) = wrench.force.z;
    return vector;
  }

  inline Vector6 wrenchToVector(geometry_msgs::WrenchStamped& wrench)
  {
    return wrenchToVector(wrench.wrench);
  }

  inline geometry_msgs::Wrench vectorToWrench(Vector6 vector)
  {
    geometry_msgs::Wrench wrench;
    wrench.torque.x = vector(0);
    wrench.torque.y = vector(1);
    wrench.torque.z = vector(2);
    wrench.force.x = vector(3);
    wrench.force.y = vector(4);
    wrench.force.z = vector(5);
    return wrench;
  }

  inline geometry_msgs::WrenchStamped vectorToWrenchStamped(Vector6 vector, std::string frame_id, ros::Time stamp = ros::Time(0))
  {
    geometry_msgs::WrenchStamped wrench;
    wrench.wrench = vectorToWrench(vector);
    wrench.header.frame_id = frame_id;
    wrench.header.stamp = stamp;

    return wrench;
  }

  inline Vector stdVectorToEigenVector(std::vector<double> std_vector)
  {
    Vector eigen_vector = Vector(std_vector.size());
    for(unsigned int i = 0; i < std_vector.size(); i++)
    {
      eigen_vector(i) = std_vector.at(i);
    }
    return eigen_vector;
  }

  inline std::vector<double> eigenVectorToStdVector(Vector eigen_vector)
  {
    std::vector<double> std_vector(eigen_vector.rows());
    for(unsigned int i = 0; i < std_vector.size(); i++)
    {
      std_vector.at(i) = eigen_vector(i);
    }
    return std_vector;
  }

  inline tf::Transform matrixToTransform(Matrix4 matrix)
  {
    Eigen::Matrix3d eigen_rot = matrix.topLeftCorner(3, 3);
    tf::Matrix3x3 tf_rot;
    tf::matrixEigenToTF(eigen_rot, tf_rot);

    Eigen::Vector3d eigen_pos = matrix.topRightCorner(3, 1);
    tf::Vector3 tf_pos;
    tf::vectorEigenToTF(eigen_pos, tf_pos);

    return tf::Transform(tf_rot, tf_pos);
  }

  inline Matrix4 transformToMatrix(tf::Transform transform)
  {
    Eigen::Matrix3d eigen_rot;
    tf::matrixTFToEigen(tf::Matrix3x3(transform.getRotation()), eigen_rot);

    Eigen::Vector3d eigen_pos;
    tf::vectorTFToEigen(transform.getOrigin(), eigen_pos);

    Matrix4 matrix = Matrix4::Identity();
    matrix.topLeftCorner(3, 3) = eigen_rot;
    matrix.topRightCorner(3, 1) = eigen_pos;

    return matrix;
  }

  inline Matrix4 stampedTransformToMatrix(tf::StampedTransform transform)
  {
    return transformToMatrix(transform);
  }

  inline tf::StampedTransform matrixToStampedTransform(Matrix4 matrix, std::string parent, std::string child, ros::Time stamp = ros::Time(0))
  {
    return tf::StampedTransform(matrixToTransform(matrix), stamp, parent, child);
  }

  inline matec_utils::Matrix6 motionTransformFromAffine(matec_utils::Matrix4 affine)
  {
    matec_utils::Matrix3 E = affine.topLeftCorner(3, 3);
    matec_utils::Vector3 r = -E.inverse() * affine.topRightCorner(3, 1);

    matec_utils::Matrix6 motion = matec_utils::Matrix6::Identity();
    motion.topLeftCorner(3, 3) = E;
    // motion.topRightCorner(3, 3) = Matrix3::Zero();
    motion.bottomLeftCorner(3, 3) = -E * matec_utils::cross(r);
    motion.bottomRightCorner(3, 3) = E;

    return motion;
  }

  inline matec_utils::Matrix6 transformInertia(matec_utils::Matrix4 iTj, matec_utils::Matrix6 inertia_j)
  {
    matec_utils::Matrix6 jXi = motionTransformFromAffine(iTj).inverse(); //TODO: make sure we're going the right direction
    return jXi.transpose() * inertia_j * jXi;
  }

  inline tf::StampedTransform poseToStampedTransform(geometry_msgs::Pose pose, std::string frame_id, std::string child_frame_id, ros::Time timestamp = ros::Time(0))
  {
    return tf::StampedTransform(tf::Transform(tf::Quaternion(pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w), tf::Vector3(pose.position.x, pose.position.y, pose.position.z)), timestamp, frame_id, child_frame_id);
  }

  inline tf::StampedTransform poseStampedToStampedTransform(geometry_msgs::PoseStamped pose, std::string child_frame_id)
  {
    return poseToStampedTransform(pose.pose, pose.header.frame_id, child_frame_id, pose.header.stamp);
  }

  inline geometry_msgs::PoseStamped stampedTransformToPoseStamped(tf::StampedTransform trans)
  {
    geometry_msgs::PoseStamped pose;

    pose.header.frame_id = trans.frame_id_;
    pose.header.stamp = trans.stamp_;
    pose.pose.position.x = trans.getOrigin().x();
    pose.pose.position.y = trans.getOrigin().y();
    pose.pose.position.z = trans.getOrigin().z();
    pose.pose.orientation.w = trans.getRotation().w();
    pose.pose.orientation.x = trans.getRotation().x();
    pose.pose.orientation.y = trans.getRotation().y();
    pose.pose.orientation.z = trans.getRotation().z();

    return pose;
  }

  inline bool quaternionValid(geometry_msgs::Quaternion quat)
  {
    return std::isfinite(quat.x) && std::isfinite(quat.y) && std::isfinite(quat.z) && std::isfinite(quat.z) && (fabs(1.0 - sqrt(quat.x * quat.x + quat.y * quat.y + quat.z * quat.z + quat.w * quat.w)) < 1e-6);
  }

  inline void goalRegionToRPYxyz(matec_msgs::GoalRegion& goal_region, matec_utils::Vector6& rpyxyz_min, matec_utils::Vector6& rpyxyz_max)
  {
    rpyxyz_min << goal_region.R.min, goal_region.P.min, goal_region.Y.min, goal_region.x.min, goal_region.y.min, goal_region.z.min;
    rpyxyz_max << goal_region.R.max, goal_region.P.max, goal_region.Y.max, goal_region.x.max, goal_region.y.max, goal_region.z.max;
  }

  inline void matrixToRPY(matec_utils::Matrix3 rot, double&roll, double&pitch, double&yaw)
  {
    double epsilon = 1E-12;
    pitch = atan2((double) -rot(2, 0), sqrt((double) (rot(0, 0) * rot(0, 0) + rot(1, 0) * rot(1, 0))));
    if(fabs(pitch) > (M_PI / 2.0 - epsilon))
    {
      yaw = atan2((double) -rot(0, 1), (double) rot(1, 1));
      roll = 0.0;
    }
    else
    {
      roll = atan2((double) rot(2, 1), (double) rot(2, 2));
      yaw = atan2((double) rot(1, 0), (double) rot(0, 0));
    }
  }

  inline void poseToRPYxyz(matec_utils::Matrix4& pose, matec_utils::Vector6& rpyxyz)
  {
    matec_utils::Vector3 r = pose.topRightCorner(3, 1);
    matec_utils::Matrix3 E = pose.topLeftCorner(3, 3);

//    Eigen::AngleAxis<double> angle_axis(E);
//    double R = fixAngle(angle_axis.angle() * angle_axis.axis().x());
//    double P = fixAngle(angle_axis.angle() * angle_axis.axis().y());
//    double Y = fixAngle(angle_axis.angle() * angle_axis.axis().z());


    double R,P,Y;
    matrixToRPY(E,R,P,Y);

    rpyxyz << R, P, Y, r(0), r(1), r(2);
  }

  //assumes that the pose is specified in the goal frame
  inline bool poseInGoalRegion(matec_utils::Matrix4& pose, matec_msgs::GoalRegion& goal_region, matec_utils::Vector6& error)
  {
    matec_utils::Vector6 rpyxyz, rpyxyz_min, rpyxyz_max;
    poseToRPYxyz(pose, rpyxyz);
    goalRegionToRPYxyz(goal_region, rpyxyz_min, rpyxyz_max);

    matec_utils::Vector6 rpyxyz_clamped = matec_utils::vectorClamp<matec_utils::Vector6>(rpyxyz, rpyxyz_min, rpyxyz_max);
    error = rpyxyz_clamped - rpyxyz;

    return (error == matec_utils::Vector6::Zero());
  }

  inline void extractRPY(Matrix3 rot, double& roll, double& pitch, double& yaw)
  {
    Vector3 backwards = rot.eulerAngles(2, 1, 0);
    roll = backwards(2);
    pitch = backwards(1);
    yaw = backwards(0);
  }

  //assumes that f1 and f2 are expressed w.r.t. the same frame (common)
  //returns a twist in the common frame that takes you from f1 to f2 in dt seconds
  inline matec_utils::Vector6 frameTwist(matec_utils::Matrix4 commonTf1, matec_utils::Matrix4 commonTf2, double dt, matec_utils::Vector6 weights = matec_utils::Vector6::Ones())
  {
    matec_utils::Matrix4 delta = commonTf1.inverse() * commonTf2;
    Eigen::AngleAxis<double> angle_axis((matec_utils::Matrix3) delta.topLeftCorner(3, 3));

    matec_utils::Vector6 twist; //expressed here in f1 coordinates
//    twist(0) = weights(0) * fixAngle(angle_axis.angle() * angle_axis.axis().x()) / dt;
//    twist(1) = weights(1) * fixAngle(angle_axis.angle() * angle_axis.axis().y()) / dt;
//    twist(2) = weights(2) * fixAngle(angle_axis.angle() * angle_axis.axis().z()) / dt;
    double R,P,Y;
    matrixToRPY((matec_utils::Matrix3)delta.topLeftCorner(3,3),R,P,Y);
    twist(0) = weights(0) * R / dt;
    twist(1) = weights(1) * P / dt;
    twist(2) = weights(2) * Y / dt;
    twist(3) = weights(3) * delta(0, 3) / dt;
    twist(4) = weights(4) * delta(1, 3) / dt;
    twist(5) = weights(5) * delta(2, 3) / dt;

    //transform back to common
    twist.topRows(3) = commonTf1.topLeftCorner(3, 3) * twist.topRows(3);
    twist.bottomRows(3) = commonTf1.topLeftCorner(3, 3) * twist.bottomRows(3);

    return twist;
  }

  template<typename T>
  void printVector(std::vector<T> vector)
  {
    std::cerr << "[";
    for(unsigned int i = 0; i < vector.size(); i++)
    {
      if(i != 0)
      {
        std::cerr << ", ";
      }
      std::cerr << vector[i];
    }
    std::cerr << "]";
  }
}
;
#endif //COMMON_FUNCTIONS_H
