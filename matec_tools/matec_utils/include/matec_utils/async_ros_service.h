#ifndef ASYNC_ROS_SERVICE
#define ASYNC_ROS_SERVICE

#include <ros/ros.h>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/type_traits.hpp>

namespace matec_utils
{
  template<typename T>
  class AsyncRosService
  {
  public:
    typedef boost::function<void(typename T::Response&)> CallbackType;
    AsyncRosService(std::string server_name, bool print_status = true)
    {
      m_nh = ros::NodeHandle("~");
      m_server_name = server_name;
      m_print_status = print_status;
      m_callback = boost::bind(&AsyncRosService::internalCallback, this, _1);
      m_waiting_for_callback = false;
      m_success = false;
    }

    AsyncRosService(std::string server_name, CallbackType callback, bool print_status = true)
    {
      m_nh = ros::NodeHandle("~");
      m_server_name = server_name;
      m_print_status = print_status;
      m_callback = callback;
      m_waiting_for_callback = false;
      m_success = false;
    }

    void call(typename T::Request request, typename T::Response& response, double timeout = 1.0)
    {
      if(m_waiting_for_callback)
      {
        cancel();
      }
      m_request = request;
      m_waiting_for_callback = true;
      m_success = false;
      m_call_thread = new boost::thread(boost::bind(&AsyncRosService::callThread, this));

      if(timeout == 0.0)
      {
        return;
      }
      else
      {
        boost::posix_time::ptime timeout_time = boost::get_system_time() + boost::posix_time::milliseconds(1000 * timeout);
        while(ros::ok() && m_waiting_for_callback)
        {
          if(timeout < 0)
          {
            if(m_print_status)
            {
              ROS_WARN_THROTTLE(1.0, "%s: Waiting for completion of service call to %s...", m_nh.getNamespace().c_str(), m_server_name.c_str());
            }
          }
          else if(boost::get_system_time() < timeout_time)
          {
            if(m_print_status)
            {
              ROS_WARN_THROTTLE(1.0, "%s: Waiting for completion of service call to %s with timeout %gs...", m_nh.getNamespace().c_str(), m_server_name.c_str(), timeout);
            }
          }
          else //timeout expired!
          {
            if(m_print_status)
            {
              ROS_WARN_THROTTLE(1.0, "%s: Service call to %s timed out after %gs!", m_nh.getNamespace().c_str(), m_server_name.c_str(), timeout);
            }
            break;
          }
        }
      }
    }

    void cancel()
    {
      m_call_thread->interrupt();
      m_call_thread->detach();
      delete m_call_thread;
    }

    bool waitingForCallback()
    {
      return m_waiting_for_callback;
    }

    T::Request getLastRequest()
    {
      return m_request;
    }

    T::Response getLastResponse()
    {
      return m_response;
    }

  private:
    ros::NodeHandle m_nh;
    std::string m_server_name;
    bool m_print_status;
    CallbackType m_callback;
    boost::thread* m_call_thread;
    typename T::Request m_request;
    typename T::Response m_response;
    bool m_success;
    bool m_waiting_for_callback;

    void internalCallback(typename T::Response& response)
    {
      m_response = response;
    }

    void callThread()
    {
      while(ros::ok() && !ros::service::exists(m_server_name, false))
      {
        if(m_print_status)
        {
          ROS_WARN_THROTTLE(1.0, "%s: Waiting for service at %s", m_nh.getNamespace().c_str(), m_server_name.c_str());
        }
        boost::this_thread::interruption_point();
      }

      m_success = ros::service::call(m_server_name, m_request, m_response);
      m_callback(m_response);
    }
  };
}

#endif //ASYNC_ROS_SERVICE
