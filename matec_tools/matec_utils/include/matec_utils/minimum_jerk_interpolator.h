#ifndef MINIMUM_JERK_INTERPOLATOR_H
#define MINIMUM_JERK_INTERPOLATOR_H

#include "interpolator.h"

namespace matec_utils
{
  class MinimumJerkInterpolator: public Interpolator
  {
  public:
    MinimumJerkInterpolator() :
        Interpolator()
    {
    }

    static double timeForMaxVelocity(double xi, double xf, double max_velocity)
    {
      //t_max = T/2
      //vel = (xf - xi) * 60 * (t4 / 2.0 / T5 - t3 / T4 + t2 / 2.0 / T3)
      //max_velocity = (xf - xi) * 60 * (0.03125/T - 1/(8T) + 1/(8T))
      //max_velocity = (xf - xi) * 1.875/T
      return 1.875 * fabs(xf - xi) / max_velocity;
    }

    static double timeForMaxAcceleration(double xi, double xf, double max_acceleration)
    {
      //jerk = (xf - xi) * (360.0 * t2 / T^5 - 360 * t / T^4 + 60 / T^3) = 0
      //6*t_max^2/(T^2) - 6*t_max/T + 1 = 0
      //t_max = ((3-sqrt(3))*T/6.0)
      //accel = (xf - xi) * (120 * t3 / T^5 - 180 * t2 / T^4 + 60 * t / T^3)
      //max_acceleration = (xf - xi) * (1.13249/T^2 - 8.03848/T^2 + 12.6795/T^2)
      //max_acceleration = (xf - xi) * 5.77351/T^2
      return sqrt(5.77351 * fabs(xf - xi) / max_acceleration);
    }

    static double timeForMax(double xi, double xf, double max_velocity, double max_acceleration)
    {
      return std::max(timeForMaxVelocity(xi, xf, max_velocity), timeForMaxAcceleration(xi, xf, max_acceleration));
    }

    double timeForMaxVelocity(double xi, double xf)
    {
      return timeForMaxVelocity(xi, xf, m_max_velocity);
    }

    double timeForMaxAcceleration(double xi, double xf)
    {
      return timeForMaxAcceleration(xi, xf, m_max_acceleration);
    }

    double timeForMax(double xi, double xf)
    {
      return std::max(timeForMaxVelocity(xi, xf), timeForMaxAcceleration(xi, xf));
    }

    void loadTrajectory(Trajectory& traj, bool populate_non_position_fields = true)
    {
      m_traj.clear();
      m_traj.push_back(traj[0]);
      m_traj.push_back(traj[traj.size() - 1]);

      if(traj.size() != 2)
      {
        ROS_WARN("IGNORING INTERMEDIATE VIAPOINTS AND SKIPPING TO THE END!"); //TODO: handle more than one viapoint
      }

      double slice_time = (m_traj[1].finish_time - m_traj[0].finish_time).toSec();
      double min_slice_time = timeForMaxVelocity(m_traj[0].target_state.position, m_traj[1].target_state.position, m_max_velocity);
      if((min_slice_time - slice_time) > 0.000001)
      {
        ROS_WARN("Changed duration from %g to %g to meet max velocity constraints (%gs over the limit)!", slice_time, min_slice_time, min_slice_time - slice_time);
        slice_time = min_slice_time;
        m_traj[1].finish_time = m_traj[0].finish_time + ros::Duration(slice_time);
        traj[1].finish_time = traj[0].finish_time + ros::Duration(slice_time);
      }

      m_have_valid_trajectory = true;
      m_last_slice_index = 1; //m_traj[0] is the start state
    }

    double loadSimpleTrajectory(ViaPoint initial, ViaPoint final)
    {
      m_traj.clear();
      m_traj.push_back(initial);
      m_traj.push_back(final);

      double slice_time = (m_traj[1].finish_time - m_traj[0].finish_time).toSec();
      double min_slice_time = timeForMaxVelocity(m_traj[0].target_state.position, m_traj[1].target_state.position, m_max_velocity);
      if((min_slice_time - slice_time) > 0.000001)
      {
        ROS_WARN("Changed duration from %g to %g to meet max velocity constraints (%gs over the limit)!", slice_time, min_slice_time, min_slice_time - slice_time);
        slice_time = min_slice_time;
        m_traj[1].finish_time = m_traj[0].finish_time + ros::Duration(slice_time);
      }

      m_have_valid_trajectory = true;
      m_last_slice_index = 1; //m_traj[0] is the start state

      return slice_time;
    }

    bool getInterpolatedState(ros::Time time, State& state)
    {
      if(!m_have_valid_trajectory)
      {
        return false;
      }

      //get current trajectory slice (assumes that finish_times are ordered)
      ViaPoint from_via;
      ViaPoint to_via;
      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
      {
        if(time < m_traj[i].finish_time) //found the slice!
        {
          from_via = m_traj[i - 1];
          to_via = m_traj[i];
        }
        else if(i == (m_traj.size() - 1)) //ran out of slices!
        {
          ROS_DEBUG("Trajectory complete!");
          m_have_valid_trajectory = false;
          state = m_traj[m_traj.size() - 1].target_state;
          return false;
        }
      }

      //figure out where we should be
      double xi = from_via.target_state.position;
      double xf = to_via.target_state.position;

      double t = (time - from_via.finish_time).toSec();
      double t2 = t * t;
      double t3 = t2 * t;
      double t4 = t3 * t;
      double t5 = t4 * t;
      double T = (to_via.finish_time - from_via.finish_time).toSec();
      double T3 = T * T * T;
      double T4 = T3 * T;
      double T5 = T4 * T;
//      double jerk = (xf - xi) * (360.0 / T5 * t2 - 360 / T4 * t + 60 / T3);
      state.acceleration = (xf - xi) * (120 * t3 / T5 - 180 * t2 / T4 + 60 * t / T3);
      state.velocity = (xf - xi) * 60 * (t4 / 2.0 / T5 - t3 / T4 + t2 / 2.0 / T3);
      state.position = (xf - xi) * (6 * t5 / T5 - 15 * t4 / T4 + 10 * t3 / T3) + xi;

      return true;
    }
  };
}
#endif //MINIMUM_JERK_INTERPOLATOR_H
