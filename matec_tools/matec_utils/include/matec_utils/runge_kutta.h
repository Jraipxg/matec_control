#ifndef RUNGE_KUTTA_H
#define RUNGE_KUTTA_H

#include "matec_utils/common_functions.h"

namespace matec_utils
{
//R-K coefficients
#define C4_1 (25.0/216.0)
#define C4_3 (1408.0/2565.0)
#define C4_4 (2197.0/4104.0)
#define C4_5 (-1.0/5.0)

#define C5_1 (16.0/135.0)
#define C5_3 (6656.0/12825.0)
#define C5_4 (28561.0/56430.0)
#define C5_5 (-9.0/50.0)
#define C5_6 (2.0/55.0)

#define CK2_h (1.0/4.0)
#define CK2_1 (1.0/4.0)

#define CK3_h (3.0/8.0)
#define CK3_1 (3.0/32.0)
#define CK3_2 (9.0/32.0)

#define CK4_h (12.0/13.0)
#define CK4_1 (1932.0/2197.0)
#define CK4_2 (-7200.0/2197.0)
#define CK4_3 (7296.0/2197.0)

#define CK5_h (1.0/1.0)
#define CK5_1 (439.0/216.0)
#define CK5_2 (-8.0/1.0)
#define CK5_3 (3680.0/513.0)
#define CK5_4 (-845.0/4104.0)

#define CK6_h (1.0/2.0)
#define CK6_1 (-8.0/27.0)
#define CK6_2 (2.0/1.0)
#define CK6_3 (-3544.0/2565.0)
#define CK6_4 (1859.0/4104.0)
#define CK6_5 (-11.0/40.0)

  template<typename StateType>
  StateType integrateFixedStep(double t, StateType& x, double h, boost::function<StateType(double t, StateType x)> f, boost::function<double(StateType a, StateType b)> f_err, double& error)
  {
    StateType K1 = h * f(t, x);
    StateType K2 = h * f(t + CK2_h * h, x + CK2_1 * K1);
    StateType K3 = h * f(t + CK3_h * h, x + CK3_1 * K1 + CK3_2 * K2);
    StateType K4 = h * f(t + CK4_h * h, x + CK4_1 * K1 + CK4_2 * K2 + CK4_3 * K3);
    StateType K5 = h * f(t + CK5_h * h, x + CK5_1 * K1 + CK5_2 * K2 + CK5_3 * K3 + CK5_4 * K4);
    StateType K6 = h * f(t + CK6_h * h, x + CK6_1 * K1 + CK6_2 * K2 + CK6_3 * K3 + CK6_4 * K4 + CK6_5 * K5);

    StateType rk4 = x + C4_1 * K1 + C4_3 * K3 + C4_4 * K4 + C4_5 * K5;
    StateType rk5 = x + C5_1 * K1 + C5_3 * K3 + C5_4 * K4 + C5_5 * K5 + C5_6 * K6;
    error = f_err(rk4, rk5);

    return rk5;
  }

  //num_substeps will be modified to provide a good initial number of substeps for the next iteration
  template<typename StateType>
  StateType integrateAdaptiveStep(double t, StateType& x, unsigned int& num_substeps, double T, boost::function<StateType(double t, StateType x)> f, boost::function<double(StateType a, StateType b)> f_err, double error_min = 1e-10, double error_max = 1e-7, unsigned int absolute_max_substeps = 10)
  {
    StateType next_state;
    double error = 0.0;
    while(num_substeps <= absolute_max_substeps)
    {
      double h = T / ((double) num_substeps);
      next_state = x;
      for(unsigned int i = 0; i < num_substeps; i++)
      {
        next_state = integrateFixedStep(t + h * i, next_state, h, f, f_err, error);
        if(num_substeps < absolute_max_substeps && error > error_max) //if we're on the last one, just let it run through
        {
          num_substeps++;
//          std::cerr << "Bad error " << error << "! Increasing number of substeps to " << num_substeps << "!" << std::endl;
          break;
        }
      }

////        if(e > e_max)
////        {
////          h /= 2.0;
////        }
////        else
////        {
////          k++;
////          t = t + h;
////          x = rk5;
////          if(e < e_min)
////          {
////            h *= 2.0;
////          }
////        }
////
////        if(h < h_min)
////        {
////          h = h_min;
////        }
////        else if(h > h_max)
////        {
////          h = h_max;
////        }

      if(error <= error_max) //this iteration was good enough!
      {
        if(error >= error_min && num_substeps >= 2)
        {
          num_substeps--;
//          std::cerr << "Good error " << error << "! Decreasing number of substeps to " << num_substeps << "!" << std::endl;
        }
        return next_state;
      }
      else if(num_substeps == absolute_max_substeps) //error still isn't good enough, but it's time to give up
      {
        //    std::cerr << "Failed to converge!" << std::endl;
        return next_state;
      }
    }
    return next_state;
  }

//  template<typename StateType>
//  class RungeKutta
//  {
//  public:
//    typedef boost::function<double(StateType a, StateType b)> ErrorFunction;
//    typedef boost::function<void(double t, StateType x)> IntegrableFunction;
//
//    void integrateFixedStep(double t, StateType x, double h, boost::function<void(double t, StateType x)> f, StateType& rk4, StateType& rk5)
//    {
//      StateType K1 = h * f(t, x);
//      StateType K2 = h * f(t + CK2_h * h, x + CK2_1 * K1);
//      StateType K3 = h * f(t + CK3_h * h, x + CK3_1 * K1 + CK3_2 * K2);
//      StateType K4 = h * f(t + CK4_h * h, x + CK4_1 * K1 + CK4_2 * K2 + CK4_3 * K3);
//      StateType K5 = h * f(t + CK5_h * h, x + CK5_1 * K1 + CK5_2 * K2 + CK5_3 * K3 + CK5_4 * K4);
//      StateType K6 = h * f(t + CK6_h * h, x + CK6_1 * K1 + CK6_2 * K2 + CK6_3 * K3 + CK6_4 * K4 + CK6_5 * K5);
//
//      rk4 = x + C4_1 * K1 + C4_3 * K3 + C4_4 * K4 + C4_5 * K5;
//      rk5 = x + C5_1 * K1 + C5_3 * K3 + C5_4 * K4 + C5_5 * K5 + C5_6 * K6;
//    }
//
////    StateType integrate(double t, StateType x, IntegrableFunction f, ErrorFunction err, double T = 0.001)
////    {
////      //todo: param
////
////      double h = T;
////      int k = 0;
////      while(k < n_max)
////      {
////        StateType rk4, rk5;
////        integrateFixedStep(t, x, h, f, rk4, rk5);
////
////        double e = fabs(rk4 - rk5); //type problems
////        if(e > e_max)
////        {
////          h /= 2.0;
////        }
////        else
////        {
////          k++;
////          t = t + h;
////          x = rk5;
////          if(e < e_min)
////          {
////            h *= 2.0;
////          }
////        }
////
////        if(h < h_min)
////        {
////          h = h_min;
////        }
////        else if(h > h_max)
////        {
////          h = h_max;
////        }
////      }
////    }
//
//  private:
//    double h_min = 0.0001;
//    double h_max = 1.0;
//    double e_min = 1e-12;
//    double e_max = 1e-3;
//    int n_max = 5;
//  };

}
#endif //RUNGE_KUTTA_H
