#include "gravity_compensation/gravity_compensation.h"

namespace gravity_compensation
{
  GravityCompensation::GravityCompensation(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 1.0);

    matec_utils::blockOnSMJointNames(m_joint_names);
    initModel();
    
    m_joint_command_pub.advertise("/joint_commands");
    m_compensation_pub.advertise("/gravity_compensation");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&GravityCompensation::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_combined_command_sub.subscribe("/combined_dynamics_command");
  }

  GravityCompensation::~GravityCompensation()
  {
  }

  void GravityCompensation::initModel()
  {
    while(!m_nh.hasParam("/robot_description") && ros::ok())
    {
      ROS_WARN_THROTTLE(0.5, "%s waiting for /robot_description parameter!", m_nh.getNamespace().c_str());
    }

    m_urdf_model.initParam("/robot_description");
    m_tree.load(m_joint_names, m_urdf_model, false);
    std::vector<dynamics_tree::DynamicsTreeNode::DynamicsDirection> inverse_dynamics_direction_config(m_joint_names.size(), dynamics_tree::DynamicsTreeNode::INVERSE_DYNAMICS);
    std::vector<dynamics_tree::ExternalWrenchFrame> external_wrench_frame_config(m_joint_names.size(), dynamics_tree::NO_FRAME);
    m_tree.configureDynamics(inverse_dynamics_direction_config, external_wrench_frame_config);
  }

  void GravityCompensation::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    matec_msgs::DynamicsCommand combined_command;
    matec_msgs::Odometry odom;
    matec_msgs::WrenchArray wrenches;
    if(!m_root_link_odom_sub.getCurrentMessage(odom))
    {
      ROS_WARN_THROTTLE(0.5, "%s: No odometry received!", m_nh.getNamespace().c_str());
      return;
    }
    if(!m_combined_command_sub.getCurrentMessage(combined_command))
    {
      ROS_WARN_THROTTLE(0.5, "%s: No command received!", m_nh.getNamespace().c_str());
      return;
    }

    matec_msgs::FullJointStates joint_commands;
    joint_commands = combined_command.feedforward;

    std::vector<double> compensation_torques(combined_command.feedforward.torque.size(), 0.0);
    std::vector<matec_utils::Vector6> external_wrenches(m_joint_names.size(), matec_utils::Vector6::Zero()); //TODO: buffer
    m_tree.hybridDynamics(odom, msg.position, combined_command.feedforward.velocity, combined_command.feedforward.acceleration, compensation_torques, external_wrenches);
    assert(compensation_torques.size() == combined_command.feedforward.torque.size());

    for(unsigned int i = 0; i < compensation_torques.size(); i++)
    {
//      joint_commands.position[i] += combined_command.feedback.position[i];
//      joint_commands.velocity[i] += combined_command.feedback.velocity[i];
      joint_commands.acceleration[i] += combined_command.feedback.acceleration[i];
      joint_commands.torque[i] += combined_command.feedback.torque[i] + compensation_torques[i];
//      joint_commands.compliance[i] += combined_command.feedback.compliance[i];
    }

    joint_commands.header.stamp = msg.header.stamp;
    m_joint_command_pub.publish(joint_commands);
  }

  void GravityCompensation::stop()
  {
    matec_msgs::FullJointStates joint_commands;
    joint_commands.velocity.resize(m_joint_names.size(), 0.0);
    joint_commands.acceleration.resize(m_joint_names.size(), 0.0);
    joint_commands.torque.resize(m_joint_names.size(), 0.0);
    joint_commands.header.stamp = ros::Time::now();
    m_joint_command_pub.publish(joint_commands);
  }

  void GravityCompensation::spin()
  {
    ROS_INFO("GravityCompensation started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
    stop();
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "gravity_compensation");
  ros::NodeHandle nh("~");

  gravity_compensation::GravityCompensation node(nh);
  node.spin();

  return 0;
}
