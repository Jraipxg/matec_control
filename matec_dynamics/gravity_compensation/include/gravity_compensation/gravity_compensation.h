#ifndef GRAVITY_COMPENSATION_H
#define GRAVITY_COMPENSATION_H

#include <ros/ros.h>
#include <boost/thread.hpp>
#include <time.h>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/StringArray.h>
#include <matec_msgs/DynamicsCommand.h>
#include <matec_msgs/DataVector.h>
#include <matec_msgs/WrenchArray.h>
#include <matec_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/JointState.h>

#include <tf/tf.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_dynamics_tree/dynamics_tree.h"

namespace gravity_compensation
{
  class GravityCompensation
  {
  public:
    GravityCompensation(const ros::NodeHandle& nh);
    ~GravityCompensation();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    urdf::Model m_urdf_model;
    dynamics_tree::DynamicsTree m_tree;

    boost::mutex m_mutex;

    std::vector<std::string> m_joint_names;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_command_pub;
    shared_memory_interface::Publisher<matec_msgs::DataVector> m_compensation_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<matec_msgs::Odometry> m_root_link_odom_sub;
    shared_memory_interface::Subscriber<matec_msgs::DynamicsCommand> m_combined_command_sub;

    void initModel();
    void stop();

    void convertRootLinkOdometry(matec_msgs::Odometry odom);

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //GRAVITY_COMPENSATION_H
