#ifndef ADAPTIVE_HYDRAULIC_CONTROLLER_H
#define ADAPTIVE_HYDRAULIC_CONTROLLER_H

#include "low_level_control/low_level_controller.h"
#include "matec_utils/rolling_buffer.h"

namespace low_level_control
{
  class AdaptiveHydraulicController: public LowLevelController
  {
  public:
    AdaptiveHydraulicController() :
        LowLevelController()
    {
      m_pm = matec_utils::ParameterManager::getInstance();
      reset();
    }

    void reset()
    {
      m_q_tilde = 0.0;
      m_q_dot_tilde = 0.0;
      m_q_dot_dot_tilde = 0.0;
      m_augmented_q_dot_desired = 0.0;
      m_augmented_q_dot_tilde = 0.0;
      m_i_accumulator = 0.0;
      m_gravity_torque = 0.0;
      m_feedforward = 0.0;
      m_feedback = 0.0;
      m_com_est = (m_node && m_node->supported_mass != 0.0)? (matec_utils::Vector3) m_node->supported_com.topRows(3) : matec_utils::Vector3::Zero();
    }

    void configure(std::string actuator_name, double frequency, ActuatorState min_limits, ActuatorState max_limits, dynamics_tree::DynamicsTree& tree)
    {
      LowLevelController::configure(actuator_name, frequency, min_limits, max_limits, tree);
      m_nh = ros::NodeHandle("~");
      configureParams();

      m_inertial_gravity << 0.0, 0.0, -9.80665;
      m_axis = m_node->axis.topRows(3); //todo: support prismatic joints
      m_link_name = m_node->link_name;
    }

    double process(ActuatorState current, ActuatorState desired, dynamics_tree::DynamicsTree& tree)
    {
      if(!m_enabled())
      {
        reset();
        return 0.0;
      }

      if(!m_last_state_valid)
      {
        m_last_state = current;
        m_last_state_valid = true;
      }

      //errors
      m_q_tilde = desired.q - current.q;
      m_q_dot_tilde = desired.q_dot - current.q_dot;
      m_q_dot_dot_tilde = desired.q_dot_dot - current.q_dot_dot;
      m_augmented_q_dot_desired = desired.q_dot + m_lambda() * m_q_tilde;
      m_augmented_q_dot_tilde = m_augmented_q_dot_desired - current.q_dot;

      //feedforward terms
      matec_utils::Vector3 link_gravity = matec_utils::Vector3::Zero();
      matec_utils::Vector3 planar_gravity = matec_utils::Vector3::Zero();
      matec_utils::Vector3 planar_com_est = matec_utils::Vector3::Zero();
      tree.transformVector(m_link_name, tree.getInertialFrameName(), m_inertial_gravity, link_gravity);
      planar_gravity = link_gravity - (link_gravity.dot(m_axis)) / (m_axis.squaredNorm()) * m_axis; //todo: cache axis part or figure out if unnecessary
      planar_com_est = m_com_est - (m_com_est.dot(m_axis)) / (m_axis.squaredNorm()) * m_axis; //todo: cache axis part or figure out if unnecessary
      m_gravity_torque = -m_node->supported_mass * m_axis.transpose() * (planar_com_est.cross(planar_gravity));

//      std::cerr << "supported mass: " << m_node->supported_mass << std::endl;
//      std::cerr << "supported mass com: " << m_node->supported_com.transpose() << std::endl;
//      std::cerr << "com_est: " << m_com_est.transpose() << std::endl;
//      std::cerr << "planar_com_est: " << planar_com_est.transpose() << std::endl;
//      std::cerr << "planar_gravity: " << planar_gravity.transpose() << std::endl;
//      std::cerr << "m_gravity_torque: " << m_gravity_torque << std::endl;

      m_feedforward = (desired.q_dot < 0.0)? (desired.q_dot * m_neg_vel_ff()) : (desired.q_dot * m_pos_vel_ff());
//      m_feedforward += (desired.q_dot_dot < 0.0)? (desired.q_dot_dot * m_neg_acc_ff()) : (desired.q_dot_dot * m_pos_acc_ff());
      m_feedforward += (m_gravity_torque < 0.0)? (m_neg_torque_ff() * m_gravity_torque) : (m_pos_torque_ff() * m_gravity_torque);
      m_feedforward += (fabs(current.q_dot) < m_f_v_thresh())? (m_f_break() * matec_utils::sign(desired.q_dot)) : (0.0);

      //feedback terms
      m_i_accumulator = (fabs(m_q_tilde) <= m_i_liveband())? matec_utils::clamp(m_i_accumulator + m_q_tilde, -m_i_clamp(), m_i_clamp()) : 0.0;
      m_feedback = m_kick() * matec_utils::sat(m_augmented_q_dot_desired, m_boundary());
      m_feedback += m_kp() * m_q_tilde;
      m_feedback += m_kv() * m_q_dot_tilde;
      m_feedback += m_kr() * m_augmented_q_dot_tilde;
      m_feedback += m_ki() * m_i_accumulator;

      //adapt
      if(desired.q_dot == 0.0 && desired.q_dot_dot == 0.0) //we're supposed to be stopped, probably a good time for adaptation
      {
//        matec_utils::Vector3 grav_normal = planar_gravity.cross(m_axis);
//        matec_utils::Vector3 com_normal = m_com_est.cross(m_axis);
//        matec_utils::Vector3 rotation_vector = grav_normal - (grav_normal.dot(com_normal)) / (com_normal.squaredNorm()) * com_normal;
        matec_utils::Vector3 com_est_dot = m_beta() * (m_node->supported_com.topRows(3) - m_com_est);
        if(planar_com_est.norm() != 0.0)
        {
          com_est_dot += m_gamma_mag() * (planar_com_est / planar_com_est.norm()) * ((matec_utils::sign(m_q_tilde) == matec_utils::sign(m_gravity_torque))? 1 : -1);
        }
        //        com_est_dot -= m_gamma_rot() * (rotation_vector / rotation_vector.norm()) * (increase_magnitude? 1 : -1);
        m_com_est += com_est_dot * m_dt;
      }

      m_last_state = current;
      return matec_utils::clamp(m_feedforward + m_feedback, -m_max_command(), m_max_command());
    }

    std::vector<std::pair<std::string, double> > controllerState()
    {
      std::vector<std::pair<std::string, double> > state_vector;

      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/p_err", m_q_tilde));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/v_err", m_q_dot_tilde));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/a_err", m_q_dot_dot_tilde));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/augmented_q_dot_desired", m_augmented_q_dot_desired));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/augmented_q_dot_tilde", m_augmented_q_dot_tilde));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/i_accumulator", m_i_accumulator));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/gravity_torque", m_gravity_torque));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/feedback", m_feedback));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/feedforward", m_feedforward));

      return state_vector;
    }

  private:
    ros::NodeHandle m_nh;
    std::string m_link_name;
    matec_utils::Vector3 m_inertial_gravity;
    matec_utils::Vector3 m_axis;
    matec_utils::Vector3 m_com_est;

    matec_utils::Parameter<bool> m_enabled;
    matec_utils::Parameter<double> m_kp;
    matec_utils::Parameter<double> m_kv;
    matec_utils::Parameter<double> m_kr;
    matec_utils::Parameter<double> m_ki;
    matec_utils::Parameter<double> m_i_clamp;
    matec_utils::Parameter<double> m_i_liveband;
    matec_utils::Parameter<double> m_lambda;
    matec_utils::Parameter<double> m_kick;
    matec_utils::Parameter<double> m_boundary;
    matec_utils::Parameter<double> m_f_break;
    matec_utils::Parameter<double> m_f_v_thresh;
    matec_utils::Parameter<double> m_pos_vel_ff;
    matec_utils::Parameter<double> m_neg_vel_ff;
    matec_utils::Parameter<double> m_pos_torque_ff;
    matec_utils::Parameter<double> m_neg_torque_ff;
    matec_utils::Parameter<double> m_max_command;

    matec_utils::Parameter<double> m_gamma_mag;
    matec_utils::Parameter<double> m_gamma_rot;
    matec_utils::Parameter<double> m_beta;

    double m_q_tilde;
    double m_q_dot_tilde;
    double m_q_dot_dot_tilde;
    double m_augmented_q_dot_desired;
    double m_augmented_q_dot_tilde;
    double m_i_accumulator;
    double m_gravity_torque;
    double m_feedback;
    double m_feedforward;

    boost::shared_ptr<matec_utils::ParameterManager> m_pm;
    void configureParams()
    {
      std::string control_base = "/control/" + m_actuator_name + "/";
      m_pm->param(control_base + "enabled", false, m_enabled);
      m_pm->param(control_base + "kp", 0.0, m_kp);
      m_pm->param(control_base + "kv", 0.0, m_kv);
      m_pm->param(control_base + "kr", 0.0, m_kr);
      m_pm->param(control_base + "ki", 0.0, m_ki);
      m_pm->param(control_base + "i_clamp", 0.0, m_i_clamp);
      m_pm->param(control_base + "i_liveband", 0.0, m_i_liveband);
      m_pm->param(control_base + "lambda", 0.0, m_lambda);
      m_pm->param(control_base + "kick", 0.0, m_kick);
      m_pm->param(control_base + "boundary", 0.0, m_boundary);
      m_pm->param(control_base + "pos_vel_ff", 0.0, m_pos_vel_ff);
      m_pm->param(control_base + "neg_vel_ff", 0.0, m_neg_vel_ff);
      m_pm->param(control_base + "pos_torque_ff", 0.0, m_pos_torque_ff);
      m_pm->param(control_base + "neg_torque_ff", 0.0, m_neg_torque_ff);
      m_pm->param(control_base + "f_break", 0.0, m_f_break);
      m_pm->param(control_base + "f_v_thresh", 0.0, m_f_v_thresh);
      m_pm->param(control_base + "gamma_mag", 0.0, m_gamma_mag);
      m_pm->param(control_base + "gamma_rot", 0.0, m_gamma_rot);
      m_pm->param(control_base + "beta", 0.0, m_beta);

      std::string joint_dynamics_base = "/joint_dynamics/" + m_actuator_name + "/";
      m_pm->param(joint_dynamics_base + "limits/cmd", 0.0, m_max_command);
    }
  };
}
#endif //ADAPTIVE_HYDRAULIC_CONTROLLER_H
