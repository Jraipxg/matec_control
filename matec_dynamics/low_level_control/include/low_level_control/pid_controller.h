#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

#include "low_level_control/low_level_controller.h"

namespace low_level_control
{
  class PIDController: public LowLevelController
  {
  public:
    PIDController() :
        LowLevelController()
    {
      m_pm = matec_utils::ParameterManager::getInstance();
      reset();
    }

    void reset()
    {
      m_q_tilde = 0;
      m_q_dot_tilde = 0;
      m_q_dot_dot_tilde = 0;
    }

    void configure(std::string actuator_name, double frequency, ActuatorState min_limits, ActuatorState max_limits, dynamics_tree::DynamicsTree& tree)
    {
      LowLevelController::configure(actuator_name, frequency, min_limits, max_limits, tree);
      m_nh = ros::NodeHandle("~");
    }

    double process(ActuatorState current, ActuatorState desired, dynamics_tree::DynamicsTree& tree)
    {
      if(!m_last_state_valid)
      {
        m_last_state = current;
        m_last_state_valid = true;
      }

      m_q_tilde = desired.q - current.q;
      m_q_dot_tilde = desired.q_dot - current.q_dot;
      m_q_dot_dot_tilde = desired.q_dot_dot - current.q_dot_dot;

      m_i_accumulator = (fabs(m_q_tilde) <= m_i_liveband())? matec_utils::clamp(m_i_accumulator + m_q_tilde, -m_i_clamp(), m_i_clamp()) : 0.0;
      double feedback = m_kp() * m_q_tilde + m_kv() * m_q_dot_tilde + m_ka() * m_q_dot_dot_tilde + m_ki() * m_i_accumulator; //TODO: add time scaling?

      m_last_state = current;
      return feedback;
    }

    std::vector<std::pair<std::string, double> > controllerState()
    {
      std::vector<std::pair<std::string, double> > state_vector;

      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/abs_p_err", fabs(m_q_tilde)));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/abs_v_err", fabs(m_q_dot_tilde)));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/abs_a_err", fabs(m_q_dot_dot_tilde)));
      state_vector.push_back(std::pair<std::string, double>(m_actuator_name + "/i_accumulator", m_i_accumulator));

      return state_vector;
    }

  private:
    ros::NodeHandle m_nh;
    matec_utils::Parameter<double> m_kp;
    matec_utils::Parameter<double> m_kv;
    matec_utils::Parameter<double> m_ka;
    matec_utils::Parameter<double> m_ki;
    matec_utils::Parameter<double> m_i_clamp;
    matec_utils::Parameter<double> m_i_liveband;
    double m_i_accumulator;

    double m_q_tilde;
    double m_q_dot_tilde;
    double m_q_dot_dot_tilde;

    boost::shared_ptr<matec_utils::ParameterManager> m_pm;
    void updateParams()
    {
      std::string base = "/pid_gains/" + m_actuator_name + "/";
      m_pm->param(base + "kp", 0.0, m_kp);
      m_pm->param(base + "kv", 0.0, m_kv);
      m_pm->param(base + "ka", 0.0, m_ka);
      m_pm->param(base + "ki", 0.0, m_ki);
      m_pm->param(base + "i_clamp", 0.0, m_i_clamp);
      m_pm->param(base + "i_liveband", 0.0, m_i_liveband);
    }
  };
}
#endif //PID_CONTROLLER_H
