#ifndef LOW_LEVEL_CONTROLLER_H
#define LOW_LEVEL_CONTROLLER_H

#include "matec_msgs/FullJointStates.h"
#include "matec_utils/common_functions.h"
#include "matec_utils/common_initialization_components.h"
#include "matec_dynamics_tree/dynamics_tree.h"
#include <matec_utils/parameter_helper.h>

namespace low_level_control
{
  class ActuatorState
  {
  public:
    enum Component
    {
      POSITION, //q
      VELOCITY, //q_dot
      ACCELERATION, //q_dot_dot
      JERK, //q_dot_dot_dot
      TORQUE, //tau
      NUM_FEEDBACK_TYPES
    };

    double time;
    double q;
    double q_dot;
    double q_dot_dot;
    double q_dot_dot_dot;
    double tau;

    ActuatorState(double time_ = 0.0, double q_ = 0.0, double q_dot_ = 0.0, double q_dot_dot_ = 0.0, double q_dot_dot_dot_ = 0.0, double tau_ = 0.0)
    {
      time = time_;
      q = q_;
      q_dot = q_dot_;
      q_dot_dot = q_dot_dot_;
      q_dot_dot_dot = q_dot_dot_dot_;
      tau = tau_;
    }

    ActuatorState(matec_msgs::FullJointStates& joint_states, unsigned int joint_idx)
    {
      time = joint_states.header.stamp.toSec();
      q = (joint_idx < joint_states.position.size())? joint_states.position.at(joint_idx) : 0.0;
      q_dot = (joint_idx < joint_states.velocity.size())? joint_states.velocity.at(joint_idx) : 0.0;
      q_dot_dot = (joint_idx < joint_states.acceleration.size())? joint_states.acceleration.at(joint_idx) : 0.0;
      q_dot_dot_dot = 0.0;
      tau = (joint_idx < joint_states.torque.size())? joint_states.torque.at(joint_idx) : 0.0;
    }

    void set(matec_msgs::FullJointStates& joint_states, unsigned int joint_idx)
    {
      joint_states.position.at(joint_idx) = q;
      joint_states.velocity.at(joint_idx) = q_dot;
      joint_states.acceleration.at(joint_idx) = q_dot_dot;
      joint_states.torque.at(joint_idx) = tau;
    }

    void clamp(ActuatorState& min_limits, ActuatorState& max_limits)
    {
      q = matec_utils::clamp(q, min_limits.q, max_limits.q);
      q_dot = matec_utils::clamp(q_dot, min_limits.q_dot, max_limits.q_dot);
      q_dot_dot = matec_utils::clamp(q_dot_dot, min_limits.q_dot_dot, max_limits.q_dot_dot);
      q_dot_dot_dot = matec_utils::clamp(q_dot_dot_dot, min_limits.q_dot_dot_dot, max_limits.q_dot_dot_dot);
      tau = matec_utils::clamp(tau, min_limits.tau, max_limits.tau);
    }
  };

  class LowLevelController
  {
  public:
    LowLevelController()
    {
    }
    ~LowLevelController()
    {
    }
//    virtual ~LowLevelController() = 0;

    virtual void configure(std::string actuator_name, double frequency, ActuatorState min_limits, ActuatorState max_limits, dynamics_tree::DynamicsTree& tree)
    {
      m_node = tree.getNodeByJoint(actuator_name);
      m_actuator_name = actuator_name;
      m_frequency = frequency;
      m_dt = 1.0 / m_frequency;
      m_min_limits = min_limits;
      m_max_limits = max_limits;

      m_last_state_valid = false;
    }

    virtual double process(ActuatorState current, ActuatorState desired, dynamics_tree::DynamicsTree& tree) = 0;

    virtual std::vector<std::pair<std::string, double> > controllerState() = 0;

    virtual void reset() = 0;

  protected:
    boost::shared_ptr<dynamics_tree::DynamicsTreeNode> m_node;
    std::string m_actuator_name;
    double m_frequency;
    double m_dt;
    ActuatorState m_min_limits;
    ActuatorState m_max_limits;

    ActuatorState m_last_state;
    bool m_last_state_valid;
  };
}
#endif //LOW_LEVEL_CONTROLLER_H
