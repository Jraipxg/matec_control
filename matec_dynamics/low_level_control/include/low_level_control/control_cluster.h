#ifndef CONTROL_CLUSTER_H
#define CONTROL_CLUSTER_H

#include "low_level_control/low_level_controller.h"
#include "low_level_control/pid_controller.h"
#include "estimators/sliding_mode_observer.h"
#include "matec_msgs/Odometry.h"
#include "matec_msgs/StampedDouble.h"

namespace low_level_control
{
  template<typename O, typename C>
  class ControlCluster
  {
  public:
    ControlCluster()
    {

    }

    void reset()
    {
      for(unsigned int i = 0; i < m_controllers.size(); i++)
      {
        m_controllers.at(i).reset();
      }
    }

    void configure(double frequency, double controller_state_publish_frequency)
    {
      matec_utils::blockOnROSJointNames(m_joint_names);

      urdf::Model urdf_model;
      urdf_model.initParam("/robot_description");

      m_graph.loadFromURDF(m_joint_names, urdf_model, false);
      m_graph.spawnDynamicsTree(urdf_model.getRoot()->name, false, m_tree);

      m_controllers.resize(m_joint_names.size());
      for(unsigned int i = 0; i < m_controllers.size(); i++)
      {
        //todo: use augmented joint properties yaml
        ActuatorState min, max;
        if(urdf_model.getJoint(m_joint_names.at(i)) && urdf_model.getJoint(m_joint_names.at(i))->limits)
        {
          min.q = urdf_model.getJoint(m_joint_names.at(i))->limits->lower;
          max.q = urdf_model.getJoint(m_joint_names.at(i))->limits->upper;
          min.q_dot = -urdf_model.getJoint(m_joint_names.at(i))->limits->velocity;
          max.q_dot = urdf_model.getJoint(m_joint_names.at(i))->limits->velocity;
          min.q_dot_dot = -std::numeric_limits<double>::max();
          max.q_dot_dot = std::numeric_limits<double>::max();
          min.tau = -urdf_model.getJoint(m_joint_names.at(i))->limits->effort;
          max.tau = urdf_model.getJoint(m_joint_names.at(i))->limits->effort;
        }
        else
        {
          //todo: warn no limits
          min.q = -std::numeric_limits<double>::max();
          max.q = std::numeric_limits<double>::max();
          min.q_dot = -std::numeric_limits<double>::max();
          max.q_dot = std::numeric_limits<double>::max();
          min.q_dot_dot = -std::numeric_limits<double>::max();
          max.q_dot_dot = std::numeric_limits<double>::max();
          min.tau = -std::numeric_limits<double>::max();
          max.tau = std::numeric_limits<double>::max();
        }

        m_controllers.at(i).configure(m_joint_names.at(i), frequency, min, max, m_tree);
      }

      m_observer.configure(frequency, urdf_model.getRoot()->name, false);
    }

    void process(matec_msgs::FullJointStates current, matec_msgs::Odometry root_link_odom, matec_msgs::FullJointStates desired, matec_msgs::FullJointStates& estimate, std::vector<double>& feedback)
    {
      m_tree.kinematics(desired.position, root_link_odom);
      m_tree.computeSupportedCentersOfMassRecursive(m_tree.getRootNode());
      current.acceleration = desired.acceleration; //todo: param
      estimate = current; //m_observer.process(current, root_link_odom);
      feedback.resize(current.joint_indices.size());
      for(unsigned int i = 0; i < current.joint_indices.size(); i++)
      {
        feedback[i] = m_controllers.at(i).process(ActuatorState(estimate, i), ActuatorState(desired, i), m_tree);
      }
    }

    void publishControllerStates()
    {
      ros::Time now_time = ros::Time::now();
      for(unsigned int i = 0; i < m_controllers.size(); i++)
      {
        std::vector<std::pair<std::string, double> > state_vector = m_controllers[i].controllerState();

        for(unsigned int j = 0; j < state_vector.size(); j++)
        {
          std::string topic = "/controller_states/" + state_vector[j].first;
          if(m_publishers.find(topic) == m_publishers.end())
          {
            ros::NodeHandle nh("~");
            m_publishers[topic] = nh.advertise<matec_msgs::StampedDouble>(topic, 1, true);
          }
          matec_msgs::StampedDouble msg;
          msg.header.stamp = now_time;
          msg.data = state_vector[j].second;
          m_publishers[topic].publish(msg);
        }
      }
    }

  private:
    std::vector<std::string> m_joint_names;
    std::vector<C> m_controllers;
    O m_observer;
    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;

    std::map<std::string, ros::Publisher> m_publishers;
  };
}

#endif //CONTROL_CLUSTER_H
