#ifndef GOAL_SPACE_INTERPOLATOR_H
#define GOAL_SPACE_INTERPOLATOR_H

#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread/mutex.hpp>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/GoalSpaceCarrot.h>
#include <matec_msgs/StringArray.h>
#include <matec_actions/GoalSpaceAction.h>

#include "matec_utils/common_functions.h"
#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/ManageGoalSpaceInterpolator.h"
#include "matec_msgs/AwakenBehavior.h"

#include "matec_utils/minimum_jerk_interpolator.h"

namespace goal_space_interpolator
{
  typedef actionlib::ActionServer<matec_actions::GoalSpaceAction> Server;

  class GoalSpaceInterpolator
  {
  public:
    GoalSpaceInterpolator(const ros::NodeHandle& nh);
    ~GoalSpaceInterpolator();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_publish_rate;
    std::string m_behavior_name;
    std::string m_goal_frame;
    bool m_mobile_goal_frame;
    double m_max_linear_vel;
    double m_max_angular_vel;
    double m_linear_tare_buffer;
    double m_angular_tare_buffer;

    bool m_awaiting_management;
    bool m_behavior_awake;

    Server m_server;
    Server::GoalHandle m_goal_handle;

    tf::TransformListener m_tf_listener;

    ros::ServiceServer m_manage_service_server;

    ros::Time m_goal_start_time;

    std::vector<std::pair<matec_utils::MinimumJerkInterpolator, matec_utils::MinimumJerkInterpolator> > m_interpolators;

    shared_memory_interface::Publisher<matec_msgs::GoalSpaceCarrot> m_carrot_pub;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    matec_msgs::GoalSpaceCarrot m_last_command;

    boost::mutex m_mutex;

    void goalCallback(Server::GoalHandle goal_handle);
    void cancelCallback(Server::GoalHandle goal_handle);
    void cancelCurrentGoal();
    void setupGoal();
    bool goalActive();
    void finishGoal(bool success);

    void initializeCommand();
    void calculateNextCommand();

    bool managementCallback(matec_msgs::ManageGoalSpaceInterpolator::Request& req, matec_msgs::ManageGoalSpaceInterpolator::Response& res);
  };
}
#endif //GOAL_SPACE_INTERPOLATOR_H
