#include "goal_space_interpolator/goal_space_interpolator.h"

namespace goal_space_interpolator
{
  GoalSpaceInterpolator::GoalSpaceInterpolator(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "server", boost::bind(&GoalSpaceInterpolator::goalCallback, this, _1), boost::bind(&GoalSpaceInterpolator::cancelCallback, this, _1), false)
  {
    m_nh.param("publish_rate", m_publish_rate, 250.0);
    m_nh.param("behavior", m_behavior_name, std::string(m_nh.getNamespace()));
    m_nh.param("goal_frame", m_goal_frame, std::string("global"));
    m_nh.param("mobile_goal_frame", m_mobile_goal_frame, false);
    m_nh.param("linear_buffer", m_linear_tare_buffer, 0.01);
    m_nh.param("angular_buffer", m_angular_tare_buffer, 0.01);

    m_nh.param("max_linear_vel", m_max_linear_vel, 0.1);
    m_nh.param("max_angular_vel", m_max_angular_vel, 0.1);

    m_interpolators.resize(6); //x,y,z,R,P,Y
    for(unsigned int i = 0; i < m_interpolators.size(); i++)
    {
      double max_vel = (i < 3)? m_max_linear_vel : m_max_angular_vel;
      m_interpolators[i].first.setLimits(max_vel, std::numeric_limits<double>::max());
      m_interpolators[i].second.setLimits(max_vel, std::numeric_limits<double>::max());
    }

    initializeCommand();

    m_carrot_pub.advertise("carrot");

    m_behavior_awake = false;
    m_awaiting_management = false;
    m_manage_service_server = m_nh.advertiseService("manage", &GoalSpaceInterpolator::managementCallback, this);

    m_server.start();
  }

  GoalSpaceInterpolator::~GoalSpaceInterpolator()
  {

  }

  void GoalSpaceInterpolator::initializeCommand()
  {
    m_last_command.x_importance = 0.0;
    m_last_command.y_importance = 0.0;
    m_last_command.z_importance = 0.0;
    m_last_command.R_importance = 0.0;
    m_last_command.P_importance = 0.0;
    m_last_command.Y_importance = 0.0;
  }

  bool GoalSpaceInterpolator::managementCallback(matec_msgs::ManageGoalSpaceInterpolator::Request& req, matec_msgs::ManageGoalSpaceInterpolator::Response& res)
  {
    if(!m_tf_listener.canTransform(m_goal_frame, req.new_setpoint.header.frame_id, ros::Time(0)))
    {
      ROS_ERROR("%s: Management failed due to nonexistant transform between %s and %s", m_nh.getNamespace().c_str(), m_goal_frame.c_str(), req.new_setpoint.header.frame_id.c_str());
      return false;
    }

    geometry_msgs::PoseStamped goal_centroid;
    m_tf_listener.transformPose(m_goal_frame, req.new_setpoint, goal_centroid);

    double x = goal_centroid.pose.position.x;
    double y = goal_centroid.pose.position.y;
    double z = goal_centroid.pose.position.z;
    double R, P, Y;
    matec_utils::quaternionToRPY(goal_centroid.pose.orientation, R, P, Y);

    initializeCommand();
    m_last_command.x.min = x - m_linear_tare_buffer;
    m_last_command.x.max = x + m_linear_tare_buffer;
    m_last_command.y.min = y - m_linear_tare_buffer;
    m_last_command.y.max = y + m_linear_tare_buffer;
    m_last_command.z.min = z - m_linear_tare_buffer;
    m_last_command.z.max = z + m_linear_tare_buffer;
    m_last_command.R.min = R - m_angular_tare_buffer;
    m_last_command.R.max = R + m_angular_tare_buffer;
    m_last_command.P.min = P - m_angular_tare_buffer;
    m_last_command.P.max = P + m_angular_tare_buffer;
    m_last_command.Y.min = Y - m_angular_tare_buffer;
    m_last_command.Y.max = Y + m_angular_tare_buffer;

    m_carrot_pub.publish(m_last_command);

    m_awaiting_management = false;

    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return true;
  }

  inline bool validComponent(double importance, matec_msgs::ValueRange range)
  {
    return (range.min < range.max) && (importance >= 0.0);
  }

  void GoalSpaceInterpolator::goalCallback(Server::GoalHandle goal_handle)
  {
    bool x_ok = validComponent(goal_handle.getGoal()->goal.x_importance, goal_handle.getGoal()->goal.x);
    bool y_ok = validComponent(goal_handle.getGoal()->goal.y_importance, goal_handle.getGoal()->goal.y);
    bool z_ok = validComponent(goal_handle.getGoal()->goal.z_importance, goal_handle.getGoal()->goal.z);
    bool R_ok = validComponent(goal_handle.getGoal()->goal.R_importance, goal_handle.getGoal()->goal.R);
    bool P_ok = validComponent(goal_handle.getGoal()->goal.P_importance, goal_handle.getGoal()->goal.P);
    bool Y_ok = validComponent(goal_handle.getGoal()->goal.Y_importance, goal_handle.getGoal()->goal.Y);
    if(!(x_ok && y_ok && z_ok && R_ok && P_ok && Y_ok))
    {
      ROS_ERROR("%s: Rejecting goal due to invalid goal region specification!", m_nh.getNamespace().c_str());
      goal_handle.setRejected();
      return;
    }

    cancelCurrentGoal();
    m_goal_handle = goal_handle;
    m_goal_handle.setAccepted();

    matec_utils::awakenBehavior(m_behavior_name, m_behavior_awake);

    if(m_behavior_awake)
    {
      setupGoal();
      ROS_INFO("%s: Starting trajectory!", m_nh.getNamespace().c_str());
    }
    else
    {
      m_awaiting_management = true;
    }
  }

  void GoalSpaceInterpolator::cancelCallback(Server::GoalHandle goal_handle)
  {
    cancelCurrentGoal();
  }

  void GoalSpaceInterpolator::cancelCurrentGoal()
  {
    if(m_goal_handle.getGoal().get()) //goal doesn't actually exist, so no need to cancel
    {
      actionlib_msgs::GoalStatus status = m_goal_handle.getGoalStatus(); //cancel if current goal is pending, recalling, active, or preempting
      if(status.status == actionlib_msgs::GoalStatus::PENDING || status.status == actionlib_msgs::GoalStatus::RECALLING || status.status == actionlib_msgs::GoalStatus::ACTIVE || status.status == actionlib_msgs::GoalStatus::PREEMPTING)
      {
        m_goal_handle.setCanceled();
        m_goal_handle = Server::GoalHandle();
      }
    }

    for(unsigned int i = 0; i < m_interpolators.size(); i++)
    {
      m_interpolators[i].first.cancelTrajectory();
      m_interpolators[i].second.cancelTrajectory();
    }
  }

  bool GoalSpaceInterpolator::goalActive()
  {
    return (bool) m_goal_handle.getGoal();
  }

  void GoalSpaceInterpolator::finishGoal(bool success)
  {
    if(success)
    {
      ROS_INFO("%s: Finished interpolation!", m_nh.getNamespace().c_str());
      matec_actions::GoalSpaceFeedback feedback;
      feedback.progress = 1.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setSucceeded();
      m_goal_handle = Server::GoalHandle();
    }
    else
    {
      ROS_ERROR("%s: Interpolation failed!", m_nh.getNamespace().c_str());
      matec_actions::GoalSpaceFeedback feedback;
      feedback.progress = 0.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setAborted();
      m_goal_handle = Server::GoalHandle();
    }
  }

  bool getMinMax(int component_idx, matec_msgs::GoalSpaceCarrot goal, std::pair<double, double>& min_max)
  {
    switch(component_idx)
    {
    case 0:
      min_max = std::pair<double, double>(goal.x.min, goal.x.max);
      break;
    case 1:
      min_max = std::pair<double, double>(goal.y.min, goal.y.max);
      break;
    case 2:
      min_max = std::pair<double, double>(goal.z.min, goal.z.max);
      break;
    case 3:
      min_max = std::pair<double, double>(goal.R.min, goal.R.max);
      break;
    case 4:
      min_max = std::pair<double, double>(goal.P.min, goal.P.max);
      break;
    case 5:
      min_max = std::pair<double, double>(goal.Y.min, goal.Y.max);
      break;
    default:
      return false;
    }
    return true;
  }

  bool setMinMax(int component_idx, std::pair<double, double> min_max, matec_msgs::GoalSpaceCarrot& goal)
  {
    switch(component_idx)
    {
    case 0:
      goal.x.min = min_max.first;
      goal.x.max = min_max.second;
      break;
    case 1:
      goal.y.min = min_max.first;
      goal.y.max = min_max.second;
      break;
    case 2:
      goal.z.min = min_max.first;
      goal.z.max = min_max.second;
      break;
    case 3:
      goal.R.min = min_max.first;
      goal.R.max = min_max.second;
      break;
    case 4:
      goal.P.min = min_max.first;
      goal.P.max = min_max.second;
      break;
    case 5:
      goal.Y.min = min_max.first;
      goal.Y.max = min_max.second;
      break;
    default:
      return false;
    }
    return true;
  }

  void GoalSpaceInterpolator::setupGoal()
  {
    boost::mutex::scoped_lock lock(m_mutex);

    ros::Duration goal_duration = m_goal_handle.getGoal()->duration;
    matec_msgs::GoalSpaceCarrot target = m_goal_handle.getGoal()->goal;

    matec_utils::ViaPoint start;
    start.finish_time = ros::Time::now();
    matec_utils::ViaPoint end;
    end.finish_time = start.finish_time + goal_duration;

    for(unsigned int i = 0; i < m_interpolators.size(); i++)
    {
      std::pair<double, double> current_min_max, target_min_max;
      getMinMax(i, m_last_command, current_min_max);
      getMinMax(i, target, target_min_max);

      //min
      matec_utils::Trajectory min_traj;
      start.target_state.position = current_min_max.first;
      end.target_state.position = target_min_max.first;
      min_traj.push_back(start);
      min_traj.push_back(end);
      m_interpolators[i].first.loadTrajectory(min_traj);

      //max
      matec_utils::Trajectory max_traj;
      start.target_state.position = current_min_max.second;
      end.target_state.position = target_min_max.second;
      max_traj.push_back(start);
      max_traj.push_back(end);
      m_interpolators[i].second.loadTrajectory(max_traj);
    }

    m_last_command.x_importance = target.x_importance;
    m_last_command.y_importance = target.y_importance;
    m_last_command.z_importance = target.z_importance;
    m_last_command.R_importance = target.R_importance;
    m_last_command.P_importance = target.P_importance;
    m_last_command.Y_importance = target.Y_importance;
  }

  void GoalSpaceInterpolator::calculateNextCommand()
  {
    boost::mutex::scoped_lock lock(m_mutex);

    ros::Time command_time = ros::Time::now();
    for(unsigned int i = 0; i < m_interpolators.size(); i++)
    {
      matec_utils::State min, max;
      m_interpolators[i].first.getInterpolatedState(command_time, min);
      m_interpolators[i].second.getInterpolatedState(command_time, max);
      setMinMax(i, std::pair<double, double>(min.position, max.position), m_last_command);
    }

    m_carrot_pub.publish(m_last_command);
  }

  void GoalSpaceInterpolator::spin()
  {
    ROS_INFO("GoalSpaceInterpolator started.");
    ros::Rate loop_rate(m_publish_rate);
    while(ros::ok())
    {
      if(goalActive())
      {
        if(m_behavior_awake) //ready to go!
        {
          calculateNextCommand();
        }
        else if(!m_awaiting_management) //not ready to go, but either got the management signal back or haven't sent it yet
        {
          if(matec_utils::awakenBehavior(m_behavior_name, m_behavior_awake)) //make sure the behavior is ready
          {
            if(m_behavior_awake) //behavior is now ready to go, configure the trajectories
            {
              setupGoal();
              ROS_INFO("%s: Starting trajectory!", m_nh.getNamespace().c_str());
            }
            else
            {
              m_awaiting_management = true;
            }
          }
        }
      }

      loop_rate.sleep();
      ros::spinOnce();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "goal_space_interpolator");
  ros::NodeHandle nh("~");

  goal_space_interpolator::GoalSpaceInterpolator node(nh);
  node.spin();

  return 0;
}
