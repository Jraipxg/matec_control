#ifndef JOINT_SPACE_INTERPOLATOR_H
#define JOINT_SPACE_INTERPOLATOR_H

#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread/mutex.hpp>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/StringArray.h>

#include "matec_utils/minimum_jerk_interpolator.h"
#include "matec_utils/linear_interpolator.h"

#include "matec_utils/common_initialization_components.h"
#include "matec_utils/parameter_helper.h"
#include "matec_msgs/ManageJointInterpolator.h"
#include "matec_msgs/AwakenBehavior.h"
#include "matec_msgs/MATECState.h"

namespace joint_space_interpolator
{
  typedef actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction> Server;

  class JointSpaceInterpolator
  {
  public:
    JointSpaceInterpolator(const ros::NodeHandle& nh);
    ~JointSpaceInterpolator();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_publish_rate;
    std::string m_behavior_name;
    double m_feedback_period;
    bool m_enforce_linear_vel_limits;

    bool m_linear_mode;

    bool m_sent_awaken;
    bool m_awaiting_management;
    bool m_behavior_awake;

    Server m_server;
    Server::GoalHandle m_goal_handle;
    std::vector<unsigned char> m_goal_joint_subset;

    ros::Time m_goal_start_time;
    ros::Time m_last_feedback_time;

    ros::ServiceServer m_manage_service_server;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_carrot_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<matec_msgs::MATECState> m_matec_state_sub;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    boost::shared_ptr<matec_utils::ParameterManager> m_pm;
    std::vector<matec_utils::Parameter<double> > m_max_joint_velocities;
    std::vector<matec_utils::Parameter<double> > m_max_joint_accelerations;

    std::vector<matec_utils::MinimumJerkInterpolator> m_minimum_jerk_interpolators;
    std::vector<matec_utils::LinearInterpolator> m_linear_interpolators;
    matec_msgs::FullJointStates m_last_command;

    boost::mutex m_mutex;

    void goalCallback(Server::GoalHandle goal_handle);
    void cancelCallback(Server::GoalHandle goal_handle);
    void cancelCurrentGoal();
    void setupGoal();
    void shutdown();
    bool goalActive();

    bool checkGoalJoints(std::vector<std::string> goal_joints);

    void finishGoal(bool success);
    void calculateNextCommand(matec_msgs::FullJointStates& command);
    void interpolate();

    bool managementCallback(matec_msgs::ManageJointInterpolator::Request& req, matec_msgs::ManageJointInterpolator::Response& res);
  };
}
#endif //JOINT_SPACE_INTERPOLATOR_H
