#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>
#include <matec_msgs/Tare.h>

#include "matec_utils/common_initialization_components.h"

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_joint");
  ros::NodeHandle nh;

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  unsigned int joint_idx = -1;
  double target_position = 0;
  double duration = 5.0;
  std::string interpolator_server_name;
  if(argc == 1)
  {
    for(unsigned int i = 0; i < joint_names.size(); i++)
    {
      cout << i << ": " << joint_names[i] << endl;
    }
    cout << "Enter joint index: ";
    cin >> joint_idx; //TODO: add checks
    cout << "Enter the desired position: ";
    cin >> target_position;
    cout << "Enter the desired duration: ";
    cin >> duration;
    cout << "Enter interpolator server name (empty for base_joint_interpolator): ";
    getline(cin, interpolator_server_name);
  }
  else if(argc == 4)
  {
    joint_idx = atoi(argv[1]);
    target_position = atof(argv[2]);
    duration = atof(argv[3]);
    interpolator_server_name = "base_joint_interpolator";
  }
  else if(argc == 5)
  {
    joint_idx = atoi(argv[1]);
    target_position = atof(argv[2]);
    duration = atof(argv[3]);
    interpolator_server_name = argv[4];
  }
  else
  {
    cout << "Args: [joint_idx] [target_position] [duration] [interpolator_server_name (optional)]\n";
    return 0;
  }

  if(interpolator_server_name.length() == 0)
  {
    interpolator_server_name = "base_joint_interpolator";
  }

  if(joint_idx >= joint_names.size())
  {
    cout << "Invalid joint index!";
    return 0;
  }

  std::string server_name = "/" + interpolator_server_name + "/server";
  std::cerr << "Connecting to joint server at " << server_name << std::endl;
  actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> client(nh, server_name, true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");

  control_msgs::FollowJointTrajectoryGoal goal;
  goal.trajectory.header.stamp = ros::Time::now();
  goal.trajectory.joint_names.push_back(joint_names[joint_idx]);

  trajectory_msgs::JointTrajectoryPoint point;
  point.positions.push_back(target_position);
  point.time_from_start = ros::Duration(duration);
  goal.trajectory.points.push_back(point);

  // Start the trajectory
//  if(ros::service::exists("/tare", false))
//  {
//    matec_msgs::Tare tare;
//    ros::service::call("/tare", tare);
//  }

  client.sendGoal(goal);

  client.waitForResult(ros::Duration(1.0));

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

