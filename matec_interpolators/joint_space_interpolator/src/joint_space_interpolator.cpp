#include "joint_space_interpolator/joint_space_interpolator.h"

namespace joint_space_interpolator
{
  JointSpaceInterpolator::JointSpaceInterpolator(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "server", boost::bind(&JointSpaceInterpolator::goalCallback, this, _1), boost::bind(&JointSpaceInterpolator::cancelCallback, this, _1), false)
  {
    m_nh.param("publish_rate", m_publish_rate, 250.0);
    m_nh.param("behavior", m_behavior_name, std::string(m_nh.getNamespace()));
    m_nh.param("feedback_period", m_feedback_period, 0.5);
    m_nh.param("enforce_linear_vel_limits", m_enforce_linear_vel_limits, false);

    matec_utils::blockOnSMJointNames(m_joint_names);

    m_pm = matec_utils::ParameterManager::getInstance();
    m_max_joint_velocities.resize(m_joint_names.size());
    m_max_joint_accelerations.resize(m_joint_names.size());
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      std::string base = "/joint_dynamics/" + m_joint_names.at(i) + "/limits/";
      m_pm->param(base + "vel", 0.3, m_max_joint_velocities.at(i));
      m_pm->param(base + "acc", 1.0, m_max_joint_accelerations.at(i));
    }

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints, m_joint_names))
    {
      ROS_ERROR("%s: Must specify at least one controlled joint!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("%s: couldn't generate joint mapping! Shutting down!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }

    m_last_command.position.resize(m_controlled_joints.size());
    m_last_command.velocity.resize(m_controlled_joints.size());
    m_last_command.acceleration.resize(m_controlled_joints.size());
    m_linear_interpolators.resize(m_controlled_joints.size());
    m_minimum_jerk_interpolators.resize(m_controlled_joints.size());

    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      m_last_command.joint_indices.push_back(m_joint_map[i]);
    }

    m_joint_states_sub.subscribe("/joint_states");
    m_matec_state_sub.subscribe("/matec_state");
    m_joint_carrot_pub.advertise("carrot");

    m_behavior_awake = false;
    m_awaiting_management = false;
    m_manage_service_server = m_nh.advertiseService("manage", &JointSpaceInterpolator::managementCallback, this);

    m_server.start();
  }

  JointSpaceInterpolator::~JointSpaceInterpolator()
  {

  }

  bool JointSpaceInterpolator::managementCallback(matec_msgs::ManageJointInterpolator::Request& req, matec_msgs::ManageJointInterpolator::Response& res)
  {
    if(goalActive() && m_behavior_awake && m_sent_awaken)
    {
      cancelCurrentGoal();
    }

    for(unsigned int i = 0; i < req.new_setpoint.joint_indices.size(); i++)
    {
      unsigned int control_idx = std::find(m_joint_map.begin(), m_joint_map.end(), req.new_setpoint.joint_indices[i]) - m_joint_map.begin();
      if(control_idx < m_controlled_joints.size())
      {
        m_last_command.position[control_idx] = req.new_setpoint.position[i];
        m_last_command.velocity[control_idx] = 0.0;
        m_last_command.acceleration[control_idx] = 0.0;
      }
      else
      {
        ROS_WARN("%s: Tare requested for uncontrolled joint %s", m_nh.getNamespace().c_str(), m_joint_names[req.new_setpoint.joint_indices[i]].c_str());
      }
    }
    m_joint_carrot_pub.publish(m_last_command);
    m_awaiting_management = false;

    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return true;
  }

  bool JointSpaceInterpolator::checkGoalJoints(std::vector<std::string> goal_joints)
  {
    for(unsigned int i = 0; i < goal_joints.size(); i++)
    {
      if(std::find(m_controlled_joints.begin(), m_controlled_joints.end(), goal_joints[i]) == m_controlled_joints.end())
      {
        ROS_ERROR("%s: Goal contained joint %s that we aren't supposed to control!", m_nh.getNamespace().c_str(), goal_joints[i].c_str());
        return false;
      }
    }

    return true;
  }

  void JointSpaceInterpolator::goalCallback(Server::GoalHandle goal_handle)
  {
    ROS_INFO("%s: Got goal with %d points!", m_nh.getNamespace().c_str(), (int ) goal_handle.getGoal()->trajectory.points.size());
    if(goal_handle.getGoal()->trajectory.joint_names.size() == 0)
    {
      ROS_ERROR("%s: Trajectory had no joints!", m_nh.getNamespace().c_str());
      goal_handle.setRejected();
      return;
    }
    if(goal_handle.getGoal()->trajectory.points.size() == 0)
    {
      ROS_ERROR("%s: Trajectory had no points!", m_nh.getNamespace().c_str());
      goal_handle.setRejected();
      return;
    }

    if(!checkGoalJoints(goal_handle.getGoal()->trajectory.joint_names))
    {
      goal_handle.setRejected();
      return;
    }

    cancelCurrentGoal();
    m_goal_handle = goal_handle;
    m_goal_handle.setAccepted();

    matec_utils::namesToIndices(m_joint_names, goal_handle.getGoal()->trajectory.joint_names, m_goal_joint_subset);
    m_sent_awaken = false;
    m_awaiting_management = false;

    ROS_INFO("%s: Done with goal callback!", m_nh.getNamespace().c_str());
  }

  void JointSpaceInterpolator::cancelCallback(Server::GoalHandle goal_handle)
  {
    cancelCurrentGoal();
    shutdown();
  }

  void JointSpaceInterpolator::cancelCurrentGoal()
  {
    if(m_goal_handle.getGoal().get())
    {
      m_goal_handle.setCanceled();
      m_goal_handle = Server::GoalHandle();
    }

    for(unsigned int i = 0; i < m_minimum_jerk_interpolators.size(); i++)
    {
      m_minimum_jerk_interpolators[i].cancelTrajectory();
    }

    for(unsigned int i = 0; i < m_linear_interpolators.size(); i++)
    {
      m_linear_interpolators[i].cancelTrajectory();
    }
  }

  void JointSpaceInterpolator::shutdown()
  {
    //stop everything that we were doing
  }

  bool JointSpaceInterpolator::goalActive()
  {
    return (bool) m_goal_handle.getGoal();
  }

  void JointSpaceInterpolator::setupGoal()
  {
    std::vector<std::string> joint_names = m_goal_handle.getGoal()->trajectory.joint_names;

    matec_utils::printVector(joint_names);

    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      m_linear_interpolators[i].setLimits(m_max_joint_velocities.at(m_joint_map[i])(), m_max_joint_accelerations.at(m_joint_map[i])());
      m_minimum_jerk_interpolators[i].setLimits(m_max_joint_velocities.at(m_joint_map[i])(), m_max_joint_accelerations.at(m_joint_map[i])());
    }

    boost::mutex::scoped_lock lock(m_mutex);
    if(m_goal_handle.getGoal()->trajectory.points.size() == 1)
    {
      ROS_INFO("%s: Interpolating smoothly", m_nh.getNamespace().c_str());
      m_linear_mode = false;

      std::vector<double> start_positions(m_goal_handle.getGoal()->trajectory.joint_names.size());
      std::vector<double> end_positions(m_goal_handle.getGoal()->trajectory.joint_names.size());
      double duration = m_goal_handle.getGoal()->trajectory.points.at(0).time_from_start.toSec();
      for(unsigned int i = 0; i < m_goal_handle.getGoal()->trajectory.joint_names.size(); i++)
      {
        unsigned int interpolator_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), m_goal_handle.getGoal()->trajectory.joint_names.at(i)) - m_controlled_joints.begin();
        if(interpolator_idx >= m_controlled_joints.size())
        {
          ROS_ERROR("%s: Bad joint name!", m_nh.getNamespace().c_str());
          finishGoal(false);
          return;
        }
        start_positions.at(i) = m_last_command.position.at(interpolator_idx);
        end_positions.at(i) = m_goal_handle.getGoal()->trajectory.points.at(0).positions.at(i);
        duration = std::max(duration, m_minimum_jerk_interpolators[interpolator_idx].timeForMax(start_positions.at(i), end_positions.at(i)));
      }

      for(unsigned int i = 0; i < m_goal_handle.getGoal()->trajectory.joint_names.size(); i++)
      {
        unsigned int interpolator_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), m_goal_handle.getGoal()->trajectory.joint_names.at(i)) - m_controlled_joints.begin();
        matec_utils::ViaPoint start = matec_utils::ViaPoint(ros::Time(0), start_positions.at(i), 0.0);
        matec_utils::ViaPoint end = matec_utils::ViaPoint(ros::Time(duration), end_positions.at(i), 0.0);
        m_minimum_jerk_interpolators.at(interpolator_idx).loadSimpleTrajectory(start, end);
      }
    }
    else
    {
      ROS_INFO("%s: Interpolating linearly", m_nh.getNamespace().c_str());
      m_linear_mode = true;

      //initialize trajectories
      //todo: make this less stupid
      double last_time = 0.0;
      for(unsigned int i = 0; i < m_goal_handle.getGoal()->trajectory.joint_names.size(); i++)
      {
        unsigned int interpolator_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), m_goal_handle.getGoal()->trajectory.joint_names.at(i)) - m_controlled_joints.begin();
        if(interpolator_idx >= m_controlled_joints.size())
        {
          ROS_ERROR("%s: Bad joint name!", m_nh.getNamespace().c_str());
          finishGoal(false);
          return;
        }
        double pos = (m_goal_handle.getGoal()->trajectory.points.at(0).positions.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(0).positions.at(i) : 0.0;
        double vel = (m_goal_handle.getGoal()->trajectory.points.at(0).velocities.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(0).velocities.at(i) : 0.0;
        double acc = (m_goal_handle.getGoal()->trajectory.points.at(0).accelerations.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(0).accelerations.at(i) : 0.0;
        m_linear_interpolators.at(interpolator_idx).initializeTrajectory(matec_utils::ViaPoint(ros::Time(0), pos, vel, acc));
      }

      //add in the goal points
      //todo: make this less stupid
      for(unsigned int v = 1; v < m_goal_handle.getGoal()->trajectory.points.size(); v++)
      {
        double time = m_goal_handle.getGoal()->trajectory.points.at(v).time_from_start.toSec();
        double duration = time - last_time;

        if(m_enforce_linear_vel_limits)
        {
          for(unsigned int i = 0; i < m_goal_handle.getGoal()->trajectory.joint_names.size(); i++)
          {
            unsigned int interpolator_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), m_goal_handle.getGoal()->trajectory.joint_names.at(i)) - m_controlled_joints.begin();
            if(interpolator_idx >= m_controlled_joints.size())
            {
              ROS_ERROR("%s: Bad joint name!", m_nh.getNamespace().c_str());
              finishGoal(false);
              return;
            }
            double last_pos = (m_goal_handle.getGoal()->trajectory.points.at(v - 1).positions.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(v - 1).positions.at(i) : 0.0;
            double pos = (m_goal_handle.getGoal()->trajectory.points.at(v).positions.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(v).positions.at(i) : 0.0;
            duration = std::max(duration, m_linear_interpolators.at(interpolator_idx).timeForMax(last_pos, pos));
          }
        }

        //todo: make this less stupid
        for(unsigned int i = 0; i < m_goal_handle.getGoal()->trajectory.joint_names.size(); i++)
        {
          unsigned int interpolator_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), m_goal_handle.getGoal()->trajectory.joint_names.at(i)) - m_controlled_joints.begin();
          if(interpolator_idx >= m_controlled_joints.size())
          {
            ROS_ERROR("%s: Bad joint name!", m_nh.getNamespace().c_str());
            finishGoal(false);
            return;
          }
          double pos = (m_goal_handle.getGoal()->trajectory.points.at(v).positions.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(v).positions.at(i) : 0.0;
          double vel = (m_goal_handle.getGoal()->trajectory.points.at(v).velocities.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(v).velocities.at(i) : 0.0;
          double acc = (m_goal_handle.getGoal()->trajectory.points.at(v).accelerations.size() > i)? m_goal_handle.getGoal()->trajectory.points.at(v).accelerations.at(i) : 0.0;
          m_linear_interpolators.at(interpolator_idx).addVia(matec_utils::ViaPoint(ros::Time(0), pos, vel, acc), duration);
        }

        last_time = time;
      }
    }
    m_goal_start_time = ros::Time::now();
    m_last_feedback_time = ros::Time::now();
  }

  void JointSpaceInterpolator::finishGoal(bool success)
  {
    if(success)
    {
      ROS_INFO("%s: Finished trajectory!", m_nh.getNamespace().c_str());
      control_msgs::FollowJointTrajectoryFeedback feedback;
      //feedback.progress = 1.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setSucceeded();
      m_goal_handle = Server::GoalHandle();
    }
    else
    {
      ROS_ERROR("%s: Trajectory following failed!", m_nh.getNamespace().c_str());
      control_msgs::FollowJointTrajectoryFeedback feedback;
      //feedback.progress = 0.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setAborted();
      m_goal_handle = Server::GoalHandle();
    }
  }

  void JointSpaceInterpolator::calculateNextCommand(matec_msgs::FullJointStates& command)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    ros::Time command_time = ros::Time(ros::Duration(ros::Time::now() - m_goal_start_time).toSec());

    bool all_done = true;
    if(m_linear_mode)
    {
      for(unsigned int joint = 0; joint < m_linear_interpolators.size(); joint++)
      {
        if(!m_linear_interpolators[joint].hasTrajectory())
        {
          command.velocity[joint] = 0;
          command.acceleration[joint] = 0;
          continue;
        }
        all_done = false;

        matec_utils::State state;
        if(m_linear_interpolators[joint].getInterpolatedState(command_time, state))
        {
          command.position[joint] = state.position;
          command.velocity[joint] = state.velocity;
          command.acceleration[joint] = state.acceleration;
        }
      }
    }
    else
    {
      for(unsigned int joint = 0; joint < m_minimum_jerk_interpolators.size(); joint++)
      {
        if(!m_minimum_jerk_interpolators[joint].hasTrajectory())
        {
          command.velocity[joint] = 0;
          command.acceleration[joint] = 0;
          continue;
        }
        all_done = false;

        matec_utils::State state;
        if(m_minimum_jerk_interpolators[joint].getInterpolatedState(command_time, state))
        {
          command.position[joint] = state.position;
          command.velocity[joint] = state.velocity;
          command.acceleration[joint] = state.acceleration;
        }
      }
    }
    command.joint_indices = m_joint_map;

    if(goalActive() && all_done)
    {
      finishGoal(true);
    }
    else
    {
      if((ros::Time::now() - m_last_feedback_time).toSec() > m_feedback_period)
      {
        m_last_feedback_time = ros::Time::now();
        control_msgs::FollowJointTrajectoryFeedback feedback;
        feedback.header.stamp = command_time;
        m_goal_handle.publishFeedback(feedback);
      }
    }
  }

  void JointSpaceInterpolator::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_publish_rate);
    while(ros::ok())
    {
      loop_rate.sleep();
      ros::spinOnce();

      if(goalActive())
      {
        if(!m_sent_awaken) //haven't sent awaken signal yet!
        {
          matec_msgs::MATECState matec_state;
          if(!m_matec_state_sub.getCurrentMessage(matec_state))
          {
            ROS_ERROR_THROTTLE(1.0, "%s: Couldn't get matec state!", m_nh.getNamespace().c_str());
            continue;
          }

          if(matec_state.transitioning_behaviors.size() != 0)
          {
            ROS_WARN_THROTTLE(1.0, "%s: Waiting for behaviors to stop transitioning...", m_nh.getNamespace().c_str());
            continue;
          }

          ROS_INFO("%s: Attempting to wake up behavior!", m_nh.getNamespace().c_str());
          if(matec_utils::awakenBehavior(m_behavior_name, m_behavior_awake, m_goal_joint_subset)) //make sure the behavior is ready
          {
            m_sent_awaken = true;
            if(m_behavior_awake) //behavior is already ready to go, configure the trajectories
            {
              setupGoal();
              ROS_INFO("%s: Starting Trajectory!", m_nh.getNamespace().c_str());
            }
            else
            {
              m_awaiting_management = true;
            }
          }
        }
        else if(!m_behavior_awake && !m_awaiting_management) //we sent the awaken signal, got management back, and are now ready to set up the goal
        {
          m_behavior_awake = true;
          setupGoal();
          ROS_INFO("%s: Starting Trajectory!", m_nh.getNamespace().c_str());
        }
        else if(m_behavior_awake) //ready to go!
        {
          calculateNextCommand(m_last_command);
          m_joint_carrot_pub.publish(m_last_command);
        }
        else
        {
          ROS_ERROR_THROTTLE(1.0, "%s: WEIRD STATE DETECTED!", m_nh.getNamespace().c_str());
        }
      }
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "joint_space_interpolator");
  ros::NodeHandle nh("~");

  joint_space_interpolator::JointSpaceInterpolator node(nh);
  node.spin();

  return 0;
}
