#include "wrench_sentinel/wrench_sentinel.h"

namespace wrench_sentinel
{
  WrenchSentinel::WrenchSentinel(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);

    m_wrenches_sub.subscribe("/wrenches",boost::bind(&WrenchSentinel::wrenchCallback, this, _1));
  }

  WrenchSentinel::~WrenchSentinel()
  {
  }

  void WrenchSentinel::wrenchCallback(matec_msgs::WrenchArray& wrenches)
  {
    //todo: figure out events
  }

  void WrenchSentinel::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "wrench_sentinel");
  ros::NodeHandle nh("~");

  wrench_sentinel::WrenchSentinel node(nh);
  node.spin();

  return 0;
}
