#ifndef WRENCH_SENTINEL_H
#define WRENCH_SENTINEL_H

#include <ros/ros.h>
#include <matec_utils/common_initialization_components.h>
#include <matec_utils/common_functions.h>
#include <matec_msgs/WrenchArray.h>
 
namespace wrench_sentinel
{
  class WrenchSentinel
  {
  public:
    WrenchSentinel(const ros::NodeHandle& nh);
    ~WrenchSentinel();
    void spin();
 
  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    shared_memory_interface::Subscriber<matec_msgs::WrenchArray> m_wrenches_sub;
    shared_memory_interface::Publisher<matec_msgs::WrenchArray> m_state_pub;

    void wrenchCallback(matec_msgs::WrenchArray& wrenches);
  };
}
#endif //WRENCH_SENTINEL_H
