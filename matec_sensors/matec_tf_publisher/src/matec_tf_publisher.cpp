#include "matec_tf_publisher/matec_tf_publisher.h"

namespace matec_tf_publisher
{
  MATECTFPublisher::MATECTFPublisher(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("publish_frequency", m_publish_frequency, 50.0);
    m_nh.param("inertial_frame", m_gravitic_frame, std::string("ground"));
    m_nh.param("canonical_position_source", m_canonical_position_source, std::string("raw_odometry"));
    m_nh.param("canonical_orientation_source", m_canonical_orientation_source, std::string("raw_odometry"));
    m_nh.param("output_odometry_frame", m_output_odometry_frame, std::string("global"));

    //options are min, max, avg
    m_nh.param("height_definition_type", m_height_definition_type, std::string("min"));

    //joints
    matec_utils::blockOnSMJointNames(m_joint_names);
    m_all_joint_names = m_joint_names;

    while(!m_nh.hasParam("/robot_description"))
    {
      ROS_WARN_THROTTLE(1.0, "%s waiting for robot description", m_nh.getNamespace().c_str());
    }
    m_urdf_model.initParam("/robot_description");

    generateStaticTransforms(m_urdf_model.getRoot(), m_static_transforms);

    m_all_joint_states.header.stamp = ros::Time(0);
    for(unsigned int i = 0; i < m_all_joint_names.size(); i++)
    {
      m_all_joint_states.acceleration.push_back(0.0);
      m_all_joint_states.compliance.push_back(0.0);
      m_all_joint_states.joint_indices.push_back(i);
      m_all_joint_states.position.push_back(0.0);
      m_all_joint_states.torque.push_back(0.0);
      m_all_joint_states.velocity.push_back(0.0);
    }
    m_joint_states_sub.subscribe("/joint_states");

    //tree construction
    m_graph.loadFromURDF(m_all_joint_names, m_urdf_model);
    m_graph.spawnDynamicsTree(m_urdf_model.getRoot()->name, false, m_tree);

    //auxiliary ros joint state topics
    std::string auxiliary_joint_state_topic_string;
    m_nh.param("ros_joint_state_topics", auxiliary_joint_state_topic_string, std::string(""));
    m_auxiliary_ros_joint_state_topics = matec_utils::parameterStringToStringVector(auxiliary_joint_state_topic_string);
    m_auxiliary_joint_state_subs.resize(m_auxiliary_ros_joint_state_topics.size());
    for(unsigned int i = 0; i < m_auxiliary_ros_joint_state_topics.size(); i++)
    {
      m_auxiliary_joint_state_subs.at(i) = m_nh.subscribe(m_auxiliary_ros_joint_state_topics.at(i), 5, &MATECTFPublisher::auxiliaryJointStateCallback, this);
    }

    //odometry
    std::string sm_odometry_topic_string;
    m_nh.param("sm_odometry_topics", sm_odometry_topic_string, std::string("[/raw_odometry]"));
    m_sm_odometry_topics = matec_utils::parameterStringToStringVector(sm_odometry_topic_string);
    m_root_link_odom_pub.advertise("/root_link_odom");
    m_odom_subs.resize(m_sm_odometry_topics.size());
    for(unsigned int i = 0; i < m_sm_odometry_topics.size(); i++)
    {
      m_odom_subs.at(i).subscribe(m_sm_odometry_topics.at(i));
    }
    m_canonicalToutput = matec_utils::Matrix4::Identity();

    std::string height_definition_frames_string;
    m_nh.param("height_definition_frames", height_definition_frames_string, std::string("[l_sole, r_sole]"));
    m_height_definition_frames = matec_utils::parameterStringToStringVector(height_definition_frames_string);

    //tare service
    m_tare_srv = m_nh.advertiseService("/reset_odometry", &MATECTFPublisher::tareCallback, this);
  }

  MATECTFPublisher::~MATECTFPublisher()
  {
  }

  void MATECTFPublisher::auxiliaryJointStateCallback(const sensor_msgs::JointStateConstPtr& msg)
  {
    if(msg->name.size() != msg->position.size())
    {
      ROS_ERROR("%s: Auxiliary joint state message had size mismatch between names and positions", m_nh.getNamespace().c_str());
      return;
    }
    for(unsigned int i = 0; i < msg->name.size(); i++)
    {
      unsigned int idx = std::find(m_all_joint_names.begin(), m_all_joint_names.end(), msg->name.at(i)) - m_all_joint_names.begin();
      if(idx >= m_all_joint_names.size())
      {
        ROS_ERROR("%s: Auxiliary joint state message contained bad joint name %s", m_nh.getNamespace().c_str(), msg->name.at(i).c_str());
        return;
      }
      m_all_joint_states.position.at(idx) = msg->position.at(i);
    }
  }

  bool MATECTFPublisher::tareCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
  {
    ROS_INFO("%s: Taring odometry", m_nh.getNamespace().c_str());
    tf::StampedTransform pos_trans, ori_trans;
    m_tf_listener.lookupTransform(m_canonical_position_source, m_gravitic_frame, ros::Time(0), pos_trans);
    m_tf_listener.lookupTransform(m_canonical_orientation_source, m_gravitic_frame, ros::Time(0), ori_trans);
    m_canonicalToutput = matec_utils::transformToMatrix(tf::Transform(ori_trans.getRotation(), pos_trans.getOrigin()));

    return true;
  }

  void MATECTFPublisher::generateStaticTransforms(boost::shared_ptr<const urdf::Link> link, std::vector<geometry_msgs::TransformStamped>& static_transforms)
  {
    if(link->parent_joint)
    {
      if(link->parent_joint->type == urdf::Joint::FIXED)
      {
        geometry_msgs::TransformStamped fixed_transform;

        fixed_transform.transform.translation.x = link->parent_joint->parent_to_joint_origin_transform.position.x;
        fixed_transform.transform.translation.y = link->parent_joint->parent_to_joint_origin_transform.position.y;
        fixed_transform.transform.translation.z = link->parent_joint->parent_to_joint_origin_transform.position.z;

        fixed_transform.transform.rotation.w = link->parent_joint->parent_to_joint_origin_transform.rotation.w;
        fixed_transform.transform.rotation.x = link->parent_joint->parent_to_joint_origin_transform.rotation.x;
        fixed_transform.transform.rotation.y = link->parent_joint->parent_to_joint_origin_transform.rotation.y;
        fixed_transform.transform.rotation.z = link->parent_joint->parent_to_joint_origin_transform.rotation.z;

        fixed_transform.child_frame_id = link->name;
        fixed_transform.header.frame_id = link->parent_joint->parent_link_name;
        fixed_transform.header.stamp = ros::Time::now();

        static_transforms.push_back(fixed_transform);
      }
      else
      {
        if(std::find(m_all_joint_names.begin(), m_all_joint_names.end(), link->parent_joint->name) == m_all_joint_names.end())
        {
          //add joints that are not fixed, but also not controlled by matec
          m_all_joint_names.push_back(link->parent_joint->name);
        }
      }
    }

    for(unsigned int i = 0; i < link->child_links.size(); i++)
    {
      generateStaticTransforms(link->child_links.at(i), static_transforms);
    }
  }

  void MATECTFPublisher::publish()
  {
    matec_msgs::FullJointStates matec_joint_states;
    if(!m_joint_states_sub.getCurrentMessage(matec_joint_states))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for joint states...", m_nh.getNamespace().c_str());
      return;
    }
    if(matec_joint_states.position.size() != m_joint_names.size() || matec_joint_states.joint_indices.size() != m_joint_names.size())
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for correct joint states...", m_nh.getNamespace().c_str());
      return;
    }
    //copy in the matec joint positions
    for(unsigned int i = 0; i < matec_joint_states.position.size(); i++)
    {
      m_all_joint_states.position.at(i) = matec_joint_states.position.at(i);
    }

    matec_msgs::Odometry canonical_odometry;
    bool have_canonical_position = false;
    bool have_canonical_orientation = false;
    double position_time = 0.0;
    double orientation_time = 0.0;
    for(unsigned int i = 0; i < m_odom_subs.size(); i++)
    {
      matec_msgs::Odometry odom;
      if(!m_odom_subs.at(i).getCurrentMessage(odom))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Waiting for odometry %s", m_nh.getNamespace().c_str(), m_sm_odometry_topics.at(i).c_str());
        return;
      }

      tf::StampedTransform odometryTroot;
      odometryTroot.setOrigin(tf::Vector3(odom.pose.position.x, odom.pose.position.y, odom.pose.position.z));
      tf::Quaternion quat;
      tf::quaternionMsgToTF(odom.pose.orientation, quat);
      odometryTroot.setRotation(quat);
      odometryTroot.frame_id_ = odom.header.frame_id;
      odometryTroot.child_frame_id_ = m_urdf_model.getRoot()->name; //todo: don't assume that odometry points to the root frame
      odometryTroot.stamp_ = odom.header.stamp;
      tf::StampedTransform rootTodometry(odometryTroot.inverse(), odometryTroot.stamp_, odometryTroot.child_frame_id_, odometryTroot.frame_id_);
      m_broadcaster.sendTransform(rootTodometry);

      if(odom.header.frame_id == m_canonical_position_source)
      {
        canonical_odometry.pose.position = odom.pose.position;
        canonical_odometry.velocity.linear = odom.velocity.linear;
        canonical_odometry.acceleration.linear = odom.acceleration.linear;
        position_time = odom.header.stamp.toSec();
        have_canonical_position = true;
      }
      if(odom.header.frame_id == m_canonical_orientation_source)
      {
        canonical_odometry.pose.orientation = odom.pose.orientation;
        canonical_odometry.velocity.angular = odom.velocity.angular;
        canonical_odometry.acceleration.angular = odom.acceleration.angular;
        orientation_time = odom.header.stamp.toSec();
        have_canonical_orientation = true;
      }
    }
    if(!have_canonical_position || !have_canonical_orientation)
    {
      canonical_odometry = matec_msgs::Odometry();
      canonical_odometry.pose.orientation.w = 1.0;
      ROS_WARN_THROTTLE(1.0, "%s: Didn't get definition for canonical odometry from frames %s and %s!", m_nh.getNamespace().c_str(), m_canonical_position_source.c_str(), m_canonical_orientation_source.c_str());
      return;
    }

    if(position_time == 0.0)
    {
      position_time = orientation_time;
    }
    if(orientation_time == 0.0)
    {
      orientation_time = position_time;
    }
    double output_odom_time = (position_time + orientation_time) / 2.0;

    matec_utils::Matrix4 outputTroot = m_canonicalToutput.inverse() * matec_utils::poseToMatrix(canonical_odometry.pose);
    matec_msgs::Odometry output_odometry;
    output_odometry.pose = matec_utils::matrixToPose(outputTroot);
    output_odometry.header.frame_id = m_output_odometry_frame;
    output_odometry.header.stamp = ros::Time(output_odom_time);
    output_odometry.velocity = canonical_odometry.velocity; //todo: transform velocities and such if we care
    output_odometry.acceleration = canonical_odometry.acceleration; //todo: transform velocities and such if we care
    m_root_link_odom_pub.publish(output_odometry);

    tf::StampedTransform odometryTroot;
    odometryTroot.setOrigin(tf::Vector3(output_odometry.pose.position.x, output_odometry.pose.position.y, output_odometry.pose.position.z));
    tf::Quaternion quat;
    tf::quaternionMsgToTF(output_odometry.pose.orientation, quat);
    odometryTroot.setRotation(quat);
    odometryTroot.frame_id_ = output_odometry.header.frame_id;
    odometryTroot.child_frame_id_ = m_urdf_model.getRoot()->name; //todo: don't assume that odometry points to the root frame
    odometryTroot.stamp_ = output_odometry.header.stamp;
    tf::StampedTransform rootTodometry(odometryTroot.inverse(), odometryTroot.stamp_, odometryTroot.child_frame_id_, odometryTroot.frame_id_);
    m_broadcaster.sendTransform(rootTodometry);

    //joint transforms
    m_tree.kinematics(m_all_joint_states.position, output_odometry);
    for(unsigned int i = 0; i < m_all_joint_names.size(); i++)
    {
      boost::shared_ptr<dynamics_tree::DynamicsTreeNode> node = m_tree.getNodeByIndex(i);
      if(node && node->parent)
      {
        matec_utils::Matrix4 matrix = node->iTip.inverse();
        tf::StampedTransform trans = matec_utils::matrixToStampedTransform(matrix, node->parent->link_name, node->link_name, matec_joint_states.header.stamp);
        m_broadcaster.sendTransform(trans);
      }
    }

    //inertial transform
    double height = 0.0;
    if(m_height_definition_frames.size() != 0)
    {
      if(m_height_definition_type == "min")
      {
        height = std::numeric_limits<double>::max();
      }
      else if(m_height_definition_type == "max")
      {
        height = -std::numeric_limits<double>::max();
      }
      else if(m_height_definition_type == "avg")
      {
        height = 0.0;
      }
      else
      {
        ROS_ERROR("%s: BAD HEIGHT DEFINITION TYPE \"%s\"", m_nh.getNamespace().c_str(), m_height_definition_type.c_str());
      }

      unsigned int num_definition_frames = m_height_definition_frames.size();
      for(unsigned int i = 0; i < m_height_definition_frames.size(); i++)
      {
        matec_utils::Matrix4 odometryTdef_frame;
        if(!m_tree.lookupTransform(m_output_odometry_frame, m_height_definition_frames.at(i), odometryTdef_frame))
        {
          ROS_WARN_THROTTLE(1.0, "%s: Couldn't find transform between frames %s and %s", m_nh.getNamespace().c_str(), m_output_odometry_frame.c_str(), m_height_definition_frames.at(i).c_str());
          if(num_definition_frames == 1) //prevent div by zero during average
          {
            height = 0.0;
            break;
          }
          else
          {
            num_definition_frames--;
            continue;
          }
        }

        double def_frame_height = odometryTdef_frame(2, 3);
        if(m_height_definition_type == "min")
        {
          height = std::min(height, def_frame_height);
        }
        else if(m_height_definition_type == "max")
        {
          height = std::max(height, def_frame_height);
        }
        else if(m_height_definition_type == "avg")
        {
          height += def_frame_height;
        }
      }
      if(m_height_definition_type == "avg")
      {
        height /= (double) m_height_definition_frames.size();
      }
    }

    tf::StampedTransform inertial;
    inertial.frame_id_ = m_output_odometry_frame;
    inertial.child_frame_id_ = m_gravitic_frame;
    inertial.stamp_ = output_odometry.header.stamp;
    inertial.setOrigin(tf::Vector3(output_odometry.pose.position.x, output_odometry.pose.position.y, height));
    inertial.setRotation(tf::createQuaternionFromRPY(0, 0, tf::getYaw(odometryTroot.getRotation())));
    m_broadcaster.sendTransform(inertial);
  }

  void MATECTFPublisher::spin()
  {
    ROS_INFO("%s: started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_publish_frequency);
    while(ros::ok())
    {
      publish();

      //todo: switch over to using actual static transforms
      for(unsigned int i = 0; i < m_static_transforms.size(); i++)
      {
        m_static_transforms.at(i).header.stamp = ros::Time::now();
      }
      m_broadcaster.sendTransform(m_static_transforms);

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "matec_tf_publisher");
  ros::NodeHandle nh("~");

  matec_tf_publisher::MATECTFPublisher node(nh);
  node.spin();

  return 0;
}
