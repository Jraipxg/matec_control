#ifndef MATEC_TF_PUBLISHER
#define MATEC_TF_PUBLISHER

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/tfMessage.h>

#include <matec_utils/common_functions.h>
#include <matec_utils/common_initialization_components.h>
#include <matec_dynamics_tree/dynamics_graph.h>

#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <sensor_msgs/JointState.h>

namespace matec_tf_publisher
{
  class MATECTFPublisher
  {
  public:
    MATECTFPublisher(const ros::NodeHandle& nh);
    ~MATECTFPublisher();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_publish_frequency;
    std::string m_gravitic_frame;
    std::string m_output_odometry_frame;
    std::string m_canonical_position_source;
    std::string m_canonical_orientation_source;

    std::string m_height_definition_type;
    std::vector<std::string> m_height_definition_frames;
    std::vector<std::string> m_sm_odometry_topics;
    std::vector<std::string> m_auxiliary_ros_joint_state_topics;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_all_joint_names;
    matec_msgs::FullJointStates m_all_joint_states;

    std::vector<geometry_msgs::TransformStamped> m_static_transforms;
    std::vector<ros::Publisher> m_static_broadcasters;

    ros::ServiceServer m_tare_srv;
    matec_utils::Matrix4 m_canonicalToutput;

    tf::TransformListener m_tf_listener;
    tf::TransformBroadcaster m_broadcaster;

    shared_memory_interface::Publisher<matec_msgs::Odometry> m_root_link_odom_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    std::vector<shared_memory_interface::Subscriber<matec_msgs::Odometry> > m_odom_subs;

    std::vector<ros::Subscriber> m_auxiliary_joint_state_subs;

    urdf::Model m_urdf_model;
    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;

    void publish();
    void generateStaticTransforms(boost::shared_ptr<const urdf::Link> link, std::vector<geometry_msgs::TransformStamped>& static_transforms);

    bool tareCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    void auxiliaryJointStateCallback(const sensor_msgs::JointStateConstPtr& msg);
  };
}

#endif //MATEC_TF_PUBLISHER
