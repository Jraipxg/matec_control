#include "estimators/rolling_buffer.h"
#include "stdio.h"
#include <iostream>

using namespace estimators;

void printBuffer(RollingBuffer<ScalarSample>& buffer)
{
  std::cerr << "[";
  for(unsigned int i = 0; i < buffer.size(); i++)
  {
    if(i != 0)
    {
      std::cerr << ", ";
    }
    std::cerr << buffer(i).value;
  }
  std::cerr << "]\n";
}

int main(int argc, char **argv)
{
  RollingBuffer<ScalarSample> buffer(10);

  for(unsigned int i = 0; i < 100; i++)
  {
    buffer.push(ScalarSample(i,0));
    printBuffer(buffer);
  }

  return 0;
}
