#include "ros/ros.h"
#include "matec_msgs/Float64Stamped.h"

#include "estimators/adaptive_window_estimator.h"
#include "estimators/time_difference_estimator.h"

double pos(double dt, double magnitude, double frequency)
{
  return magnitude * sin(2.0 * M_PI * frequency * dt);
}

double vel(double dt, double magnitude, double frequency)
{
  return (magnitude * cos(2.0 * M_PI * frequency * dt)) * (2 * M_PI * frequency);
}

double acc(double dt, double magnitude, double frequency)
{
  return -magnitude * sin(2.0 * M_PI * frequency * dt) * (4 * M_PI * frequency * M_PI * frequency);
}

int main(int argc, char **argv)
{
  srand(time(NULL));

  ros::init(argc, argv, "estimator_tester");
  ros::NodeHandle nh("~");
  while(ros::Time::now().toSec() == 0)
  {

  }

  ros::Publisher true_position_pub = nh.advertise<matec_msgs::Float64Stamped>("/true_position", 1, false);
  ros::Publisher true_velocity_pub = nh.advertise<matec_msgs::Float64Stamped>("/true_velocity", 1, false);
  ros::Publisher true_acceleration_pub = nh.advertise<matec_msgs::Float64Stamped>("/true_acceleration", 1, false);

  ros::Publisher noisy_position_pub = nh.advertise<matec_msgs::Float64Stamped>("/noisy_position", 1, false);

  ros::Publisher adaptive_estimated_position_pub = nh.advertise<matec_msgs::Float64Stamped>("/estimated_position", 1, false);
  ros::Publisher adaptive_estimated_velocity_pub = nh.advertise<matec_msgs::Float64Stamped>("/estimated_velocity", 1, false);

  ros::Publisher baseline_estimated_position_pub = nh.advertise<matec_msgs::Float64Stamped>("/baseline_estimated_position", 1, false);
  ros::Publisher baseline_estimated_velocity_pub = nh.advertise<matec_msgs::Float64Stamped>("/baseline_estimated_velocity", 1, false);

  unsigned int num_signals = 1;
  double magnitude = 1.0;
  double frequency = 1.0;
  double noise_magnitude = 0.1;
  double loop_frequency = 1000;

  matec_msgs::Float64Stamped true_position, true_velocity, true_acceleration, noisy_position, baseline_estimated_position, baseline_estimated_velocity, adaptive_estimated_position, adaptive_estimated_velocity;

  std::vector<estimators::AdaptiveWindowEstimator> adaptive_estimators;
  std::vector<estimators::TimeDifferenceEstimator> baseline_estimators;
  adaptive_estimators.resize(num_signals, estimators::AdaptiveWindowEstimator(50, noise_magnitude * 2, loop_frequency));
  baseline_estimators.resize(num_signals, estimators::TimeDifferenceEstimator(5.0));

  ros::Rate loop_rate(loop_frequency);
  ros::Time start = ros::Time::now();
  while(ros::ok())
  {
    ros::Time current = ros::Time::now();
    double dt = (current - start).toSec();

    for(unsigned int i = 0; i < num_signals; i++)
    {
      true_position.data = pos(dt, magnitude, frequency);
      true_velocity.data = vel(dt, magnitude, frequency);
      true_acceleration.data = acc(dt, magnitude, frequency);
      double random_noise = noise_magnitude * (2.0 * ((double) rand() / (double) RAND_MAX) - 1);
      noisy_position.data = true_position.data + random_noise;

      adaptive_estimators[i].process(estimators::ScalarSample(noisy_position.data, dt), adaptive_estimated_position.data, adaptive_estimated_velocity.data);
      baseline_estimators[i].process(estimators::ScalarSample(noisy_position.data, dt), baseline_estimated_position.data, baseline_estimated_velocity.data);
    }

    true_position.header.stamp = current;
    true_velocity.header.stamp = current;
    true_acceleration.header.stamp = current;
    noisy_position.header.stamp = current;
    adaptive_estimated_position.header.stamp = current;
    adaptive_estimated_velocity.header.stamp = current;
    baseline_estimated_position.header.stamp = current;
    baseline_estimated_velocity.header.stamp = current;

    true_position_pub.publish(true_position);
    true_velocity_pub.publish(true_velocity);
    true_acceleration_pub.publish(true_acceleration);
    noisy_position_pub.publish(noisy_position);
    adaptive_estimated_position_pub.publish(adaptive_estimated_position);
    adaptive_estimated_velocity_pub.publish(adaptive_estimated_velocity);
    baseline_estimated_position_pub.publish(baseline_estimated_position);
    baseline_estimated_velocity_pub.publish(baseline_estimated_velocity);

    loop_rate.sleep();
  }

  return 0;
}
