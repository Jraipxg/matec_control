#include "estimators/sliding_mode_observer_node.h"

namespace estimators
{
  SlidingModeObserverNode::SlidingModeObserverNode(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 1.0);
    m_observer.configure(1000.0,"",false);

    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_estimated_joint_states_pub.advertise("/estimated_joint_states");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&SlidingModeObserverNode::sensorCallback, this, _1));
  }

  SlidingModeObserverNode::~SlidingModeObserverNode()
  {

  }

  void SlidingModeObserverNode::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    matec_msgs::Odometry root_link_odom;
    if(!m_root_link_odom_sub.getCurrentMessage(root_link_odom))
    {
      ROS_WARN_THROTTLE(1.0, "%s: Waiting for root link odometry", m_nh.getNamespace().c_str());
      return;
    }
    m_estimate = m_observer.process(msg, root_link_odom);
    m_estimated_joint_states_pub.publish(m_estimate);
  }

  void SlidingModeObserverNode::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "sliding_mode_observer");
  ros::NodeHandle nh("~");

  estimators::SlidingModeObserverNode node(nh);
  node.spin();

  return 0;
}
