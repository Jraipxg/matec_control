#ifndef TIME_DIFFERENCE_ESTIMATOR_H
#define TIME_DIFFERENCE_ESTIMATOR_H

#include "rolling_buffer.h"
#include "median_filter.h"
#include "matec_utils/biquad.h"

namespace estimators
{
  class TimeDifferenceEstimator
  {
  public:
    TimeDifferenceEstimator(double filter_frequency) :
        m_buffer(2)
    {
      m_velocity_filter.setBiquad(bq_type_lowpass, filter_frequency / 1000.0, 0.7071, 6);
    }

    ~TimeDifferenceEstimator()
    {
    }

    void process(ScalarSample sample, double& position, double& velocity)
    {
      ScalarSample filtered_sample = sample;
      filtered_sample.time = sample.time;
      m_buffer.push(filtered_sample);

      if(m_buffer.size() == 1)
      {
        position = m_buffer(0).value;
        return;
      }

      position = filtered_sample.value;
      velocity = (m_buffer(0).value - m_buffer(1).value) / (m_buffer(0).time - m_buffer(1).time);
      velocity = m_velocity_filter.process(velocity);
    }

  private:
    RollingBuffer<ScalarSample> m_buffer;
    Biquad m_velocity_filter;
  };
}

#endif //TIME_DIFFERENCE_ESTIMATOR_H
