#ifndef MEDIAN_FILTER_H
#define MEDIAN_FILTER_H

#include "rolling_buffer.h"

namespace estimators
{
  template<typename Container, typename Comp = std::less<typename Container::value_type> >
  class MedianFilter
  {
  public:
    MedianFilter(unsigned int size) :
        m_buffer(size)
    {

    }

    ~MedianFilter()
    {
    }

    Container process(Container sample, Comp comp = Comp())
    {
      m_buffer.push(sample);

      std::vector<Container> sortable_buffer(m_buffer.data()->begin(), m_buffer.data()->end());
      std::sort(sortable_buffer.begin(), sortable_buffer.end(), comp);
      assert(sortable_buffer.size() != 0);

      bool odd = sortable_buffer.size() & 1;
      if(odd)
      {
        return sortable_buffer[floor(sortable_buffer.size() / 2.0)];
      }
      else
      {
        unsigned int idx_1 = sortable_buffer.size() / 2;
        return sortable_buffer[idx_1] + sortable_buffer[idx_1 + 1];
      }
    }

  private:
    RollingBuffer<Container> m_buffer;
  };
}

#endif //MEDIAN_FILTER_H
