#ifndef SLIDING_MODE_OBSERVER_H
#define SLIDING_MODE_OBSERVER_H

#include <ros/ros.h>

#include <matec_utils/common_initialization_components.h>
#include <matec_utils/common_functions.h>
#include <matec_utils/parameter_helper.h>
#include <matec_dynamics_tree/dynamics_graph.h>
#include <matec_msgs/FullJointStates.h>

namespace estimators
{
  class SlidingModeObserver
  {
  public:
    SlidingModeObserver()
    {
      m_configured = false;
      m_pm = matec_utils::ParameterManager::getInstance();
    }

    void configure(double sensing_frequency, std::string stable_frame = "", bool calculate_feedforward = true)
    {
      m_nh = ros::NodeHandle("~");
      m_dt = 1.0 / sensing_frequency;
      m_calculate_feedforward = calculate_feedforward;
      matec_utils::blockOnSMJointNames(m_joint_names);

      m_a1.resize(m_joint_names.size());
      m_a2.resize(m_joint_names.size());
      m_a3.resize(m_joint_names.size());
      m_k1.resize(m_joint_names.size());
      m_k2.resize(m_joint_names.size());
      m_k3.resize(m_joint_names.size());
      m_kff.resize(m_joint_names.size());
      configureParams();

      if(m_calculate_feedforward)
      {
        urdf::Model urdf_model;
        urdf_model.initParam("/robot_description");

        if(stable_frame.length() == 0)
        {
          stable_frame = urdf_model.root_link_->name;
        }

        m_graph.loadFromURDF(m_joint_names, urdf_model, false); //TODO: break out floating base param
        m_graph.spawnDynamicsTree(stable_frame, false, m_tree);
        std::vector<dynamics_tree::DynamicsTreeNode::DynamicsDirection> dynamics_direction_config(m_joint_names.size(), dynamics_tree::DynamicsTreeNode::FORWARD_DYNAMICS);
        std::vector<dynamics_tree::ExternalWrenchFrame> external_wrench_frame_config(m_joint_names.size(), dynamics_tree::NO_FRAME);
        m_tree.configureDynamics(dynamics_direction_config, external_wrench_frame_config);
        m_external_wrenches.resize(m_joint_names.size(), matec_utils::Vector6::Zero());
      }

      m_configured = true;
    }

    matec_msgs::FullJointStates process(matec_msgs::FullJointStates& joint_states, matec_msgs::Odometry& root_link_odom)
    {
      if(!m_configured)
      {
        ROS_ERROR("%s: sliding mode observer process called before configuration!", m_nh.getNamespace().c_str());
        return joint_states;
      }

      if(m_estimate.joint_indices.size() == 0)
      {
        //assume that the first call has the true states
        m_estimate.joint_indices = joint_states.joint_indices;
        m_estimate.position = joint_states.position;
        m_estimate.velocity.resize(m_estimate.joint_indices.size(), 0.0);
        m_estimate.acceleration.resize(m_estimate.joint_indices.size(), 0.0);
        m_estimate.torque.resize(m_estimate.joint_indices.size(), 0.0);
        m_estimate.compliance.resize(m_estimate.joint_indices.size(), 0.0);
      }
      m_estimate.header.stamp = joint_states.header.stamp;
      m_estimate.torque = joint_states.torque;
      m_estimate.compliance = joint_states.compliance;

      std::vector<double> predicted_accelerations;
      if(m_calculate_feedforward)
      {
        m_tree.hybridDynamics(root_link_odom, m_estimate.position, m_estimate.velocity, predicted_accelerations, m_estimate.torque, m_external_wrenches);
      }
      else
      {
        if(joint_states.acceleration.size() == m_estimate.acceleration.size())
        {
          predicted_accelerations = joint_states.acceleration;
        }
        else
        {
          predicted_accelerations.resize(m_estimate.joint_indices.size(), 0.0);
        }
      }

      std::vector<double> feedforward_acceleration(m_estimate.acceleration.size(), 0.0);
      if(joint_states.acceleration.size() == m_estimate.acceleration.size())
      {
        feedforward_acceleration = joint_states.acceleration;
      }


      for(unsigned int i = 0; i < m_estimate.joint_indices.size(); i++)
      {
        double x1 = joint_states.position[i];
        double x1_hat = m_estimate.position[i];
        double x2_hat = m_estimate.velocity[i];
        double x3_hat = m_estimate.acceleration[i];

        double x1_tilde = x1_hat - x1;

        double x1_hat_dot = -m_a1.at(i)() * x1_tilde + x2_hat - m_k1.at(i)() * matec_utils::sat(x1_tilde);
        double x2_hat_dot = -m_a2.at(i)() * x1_tilde + x3_hat - m_k2.at(i)() * matec_utils::sat(x1_tilde);
        double x3_hat_dot = -m_a3.at(i)() * x1_tilde - m_kff.at(i)() * (m_estimate.acceleration[i] - feedforward_acceleration[i]) + predicted_accelerations[i] - m_k3.at(i)() * matec_utils::sat(x1_tilde);

        m_estimate.position[i] += m_dt * x1_hat_dot;
        m_estimate.velocity[i] += m_dt * x2_hat_dot;
        m_estimate.acceleration[i] += m_dt * x3_hat_dot;
      }

      return m_estimate;
    }

  private:
    ros::NodeHandle m_nh;
    bool m_calculate_feedforward;
    double m_dt;
    std::vector<matec_utils::Parameter<double> > m_a1;
    std::vector<matec_utils::Parameter<double> > m_a2;
    std::vector<matec_utils::Parameter<double> > m_a3;
    std::vector<matec_utils::Parameter<double> > m_k1;
    std::vector<matec_utils::Parameter<double> > m_k2;
    std::vector<matec_utils::Parameter<double> > m_k3;
    std::vector<matec_utils::Parameter<double> > m_kff;
    bool m_configured;

    boost::shared_ptr<matec_utils::ParameterManager> m_pm;
    void configureParams()
    {
      for(unsigned int i = 0; i < m_joint_names.size(); i++)
      {
          //{a1: 1, a2: 10, a3: 10, k1: 1, k2: 50, k3: 300, kff: 10}
        std::string base = "/smo/" + m_joint_names.at(i) + "/";
        m_pm->param(base + "a1", 1.0, m_a1.at(i));
        m_pm->param(base + "a2", 10.0, m_a2.at(i));
        m_pm->param(base + "a3", 10.0, m_a3.at(i));
        m_pm->param(base + "k1", 1.0, m_k1.at(i));
        m_pm->param(base + "k2", 50.0, m_k2.at(i));
        m_pm->param(base + "k3", 300.0, m_k3.at(i));
        m_pm->param(base + "kff", 10.0, m_kff.at(i));
      }
    }

    std::vector<std::string> m_joint_names;
    matec_msgs::FullJointStates m_estimate;

    dynamics_tree::DynamicsGraph m_graph;
    dynamics_tree::DynamicsTree m_tree;
    std::vector<matec_utils::Vector6> m_external_wrenches;
  };
}

#endif //SLIDING_MODE_OBSERVER_H
