#ifndef SLIDING_MODE_OBSERVER_NODE_H
#define SLIDING_MODE_OBSERVER_NODE_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>

#include <matec_utils/common_initialization_components.h>
#include <matec_utils/common_functions.h>
#include <matec_msgs/FullJointStates.h>
#include <estimators/sliding_mode_observer.h>

namespace estimators
{
  class SlidingModeObserverNode
  {
  public:
    SlidingModeObserverNode(const ros::NodeHandle& nh);
    ~SlidingModeObserverNode();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    SlidingModeObserver m_observer;
    matec_msgs::FullJointStates m_estimate;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_estimated_joint_states_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<matec_msgs::Odometry> m_root_link_odom_sub;

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}

#endif //SLIDING_MODE_OBSERVER_NODE_H
