#include "planned_manipulation/planned_manipulation_targeter.h"

namespace planned_manipulation
{
  /*
   ['back_bkz', 'back_bky', 'back_bkx', 'neck_ay',
   'l_leg_hpz', 'l_leg_hpx', 'l_leg_hpy', 'l_leg_kny', 'l_leg_aky', 'l_leg_akx',
   'r_leg_hpz', 'r_leg_hpx', 'r_leg_hpy', 'r_leg_kny', 'r_leg_aky', 'r_leg_akx',

   'l_arm_shz', 'l_arm_shx', 'l_arm_ely', 'l_arm_elx', 'l_arm_uwy', 'l_arm_mwx', 'l_arm_lwy', 'r_arm_shz', 'r_arm_shx', 'r_arm_ely', 'r_arm_elx', 'r_arm_uwy', 'r_arm_mwx', 'r_arm_lwy']

   stand prep position: [0.0009728074073791504, 0.00036388635635375977, -0.00035750865936279297, 0.006988020613789558,
   -0.0012077093124389648, 0.047927580773830414, -0.9680576324462891, 1.808952808380127, -0.8131875395774841, -0.05107739940285683,
   0.00044667720794677734, -0.045057736337184906, -0.970180332660675, 1.8076224327087402, -0.813373327255249, 0.04530243203043938,
   -0.3087518960237503, -1.1568483114242554, 1.9012644812464714, 0.3160879388451576, -1.8762502670288086, -1.8363667726516724, -1.1611275672912598, 0.2997724711894989, 1.141485571861267, 1.8904161974787712, -0.3139977753162384, 0.555397093296051, -0.16164325177669525, -0.09146390110254288]

   stand position: [0.0005373954772949219, 0.00012099742889404297, -0.0004216432571411133, 0.007173776160925627, 0.0001779794692993164, 0.030633889138698578, -0.49552488327026367, 0.9093780517578125, -0.43186333775520325, -0.035480961203575134, -0.0005813837051391602, -0.02851404994726181, -0.4948579967021942, 0.9017395973205566, -0.43322765827178955, 0.028302326798439026, -0.3122992366552353, -1.1585739850997925, 1.9012644812464714, 0.3160879388451576, -1.876346230506897, -1.8362709283828735, -1.1612235307693481, 0.3052372634410858, 1.139088749885559, 1.8907037302851677, -0.31380602717399597, 0.5553012490272522, -0.16154737770557404, -0.09146390110254288]

   manipulate position: [0.0011486411094665527, -0.0002986788749694824, -0.0006783008575439453, 0.007107862737029791,
   -0.0014450550079345703, 0.02946861833333969, -0.6136269569396973, 1.1486845016479492, -0.5482787489891052, -0.03522927686572075,
   0.00044667720794677734, -0.029138408601284027, -0.6112165451049805, 1.1331772804260254, -0.5462814569473267, 0.0375833585858345,

   -0.3123951107263565, -1.1585739850997925, 1.9011686369776726, 0.3106231465935707, -1.876346230506897, -1.8361750841140747, -1.1612235307693481, 0.30504533648490906, 1.1372672319412231, 1.8907037302851677, -0.3139977753162384, 0.5553012490272522, -0.16154737770557404, -0.09146390110254288]

   nominal pelvis height in manipulate: 0.847
   */

  PlannedManipulationTargeter::PlannedManipulationTargeter(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "server", boost::bind(&PlannedManipulationTargeter::goalCallback, this, _1), boost::bind(&PlannedManipulationTargeter::cancelCallback, this, _1), false)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("ground_frame", m_ground_frame, std::string("ground"));
    m_nh.param("global_frame", m_global_frame, std::string("global"));
    m_nh.param("posed_link", m_root_link, std::string("pelvis")); //todo: read urdf root link
    m_nh.param("nominal_z", m_nominal_z, 0.847);
    m_nh.param("planning_timeout", m_planning_timeout, 1.0);
    m_nh.param("auto_visualize", m_auto_visualize, true);

    m_nh.param("min_x_centroid", m_x.min, -1.2);
    m_nh.param("max_x_centroid", m_x.max, -0.3);
    m_nh.param("min_y_centroid", m_y.min, -0.6);
    m_nh.param("max_y_centroid", m_y.max, 0.6);
    m_nh.param("min_Y_offset", m_Y.min, -0.6);
    m_nh.param("max_Y_offset", m_Y.max, 0.6);

    m_nh.param("x_variance", m_x_variance, 0.1);
    m_nh.param("y_variance", m_y_variance, 0.15);
    m_nh.param("Y_variance", m_Y_variance, 0.2);

    m_nh.param("visualization_locomotion_velocity", m_visualization_locomotion_velocity, 0.2);

    m_tf_listener.reset(new tf::TransformListener(ros::Duration(30.0)));

    matec_utils::blockOnROSJointNames(m_joint_names);

    m_standard_initial_configuration.resize(m_joint_names.size(), 0.0);

    //todo: don't hardcode
    setInitialConfigurationPosition("l_arm_shz", 0.8);
    setInitialConfigurationPosition("l_arm_shx", -0.75);
    setInitialConfigurationPosition("l_arm_ely", 2.15);
    setInitialConfigurationPosition("l_arm_elx", 2.15);
    setInitialConfigurationPosition("l_arm_uwy", 0.925);
    setInitialConfigurationPosition("l_arm_mwx", 0.0);
    setInitialConfigurationPosition("l_arm_lwy", 0.0);
    setInitialConfigurationPosition("r_arm_shz", -0.8);
    setInitialConfigurationPosition("r_arm_shx", 0.75);
    setInitialConfigurationPosition("r_arm_ely", 2.15);
    setInitialConfigurationPosition("r_arm_elx", -2.15);
    setInitialConfigurationPosition("r_arm_uwy", 0.925);
    setInitialConfigurationPosition("r_arm_mwx", 0.0);
    setInitialConfigurationPosition("r_arm_lwy", 0.0);
    setInitialConfigurationPosition("l_leg_hpz", 0);
    setInitialConfigurationPosition("l_leg_hpx", 0.02946861833333969);
    setInitialConfigurationPosition("l_leg_hpy", -0.6136269569396973);
    setInitialConfigurationPosition("l_leg_kny", 1.1486845016479492);
    setInitialConfigurationPosition("l_leg_aky", -0.5482787489891052);
    setInitialConfigurationPosition("l_leg_akx", -0.03522927686572075);
    setInitialConfigurationPosition("r_leg_hpz", 0);
    setInitialConfigurationPosition("r_leg_hpx", -0.029138408601284027);
    setInitialConfigurationPosition("r_leg_hpy", -0.6112165451049805);
    setInitialConfigurationPosition("r_leg_kny", 1.1331772804260254);
    setInitialConfigurationPosition("r_leg_aky", -0.5462814569473267);
    setInitialConfigurationPosition("r_leg_akx", 0.0375833585858345);

    //prepare base marker
    m_base_marker.action = visualization_msgs::Marker::ADD;
    m_base_marker.frame_locked = true;
    m_base_marker.header.stamp = ros::Time(0);
    m_base_marker.header.frame_id = m_global_frame;
    m_base_marker.ns = "valid_planned_manipulation_targets";
    m_base_marker.color.r = 1.0;
    m_base_marker.color.g = 0.4;
    m_base_marker.color.b = 0.0;
    m_base_marker.scale.x = 0.25;
    m_base_marker.scale.y = 0.25;
    m_base_marker.scale.z = 0.5;
    m_base_marker.type = visualization_msgs::Marker::CYLINDER;

//
//          if(time_slice < m_plan.trajectory.points.size())
//          {
//            joint_trajectories.trajectory.points.push_back(m_plan.trajectory.points.at(time_slice));
//
//            matec_msgs::Prophecy prophecy;
//            prophecy.header.stamp = ros::Time(0) + m_plan.trajectory.points.at(time_slice).time_from_start;
//            prophecy.joint_positions = getJointPositions(time_slice);
//            prophecy.root_link_pose.pose = m_initial_root_link_odom.pose; //todo: update when we have pose movement simulation
//            prophecy.root_link_pose.header = m_initial_root_link_odom.header;
//            prophecies.push_back(prophecy);
//          }
//        }
//      }

    m_client = new PlannedManipulationClient(m_nh, "/planned_manipulation/server", true);

    while(!m_client->isServerConnected() && ros::ok())
    {
      ROS_WARN_THROTTLE(2.0, "%s: Waiting for the planned_manipulation actionlib server to start...", m_nh.getNamespace().c_str());
      ros::spinOnce();
    }

    m_target_markers_pub = m_nh.advertise<visualization_msgs::MarkerArray>("valid_target_markers", 1, true);

    m_target_poses.header.stamp = ros::Time(0);
    m_target_poses.header.frame_id = m_global_frame;
    m_target_poses_pub = m_nh.advertise<geometry_msgs::PoseArray>("valid_targets", 1, true);

    m_attempted_poses.header.stamp = ros::Time(0);
    m_attempted_poses.header.frame_id = m_global_frame;
    m_attempted_poses_pub = m_nh.advertise<geometry_msgs::PoseArray>("attempted_targets", 1, true);

    m_global_centroid_pub = m_nh.advertise<geometry_msgs::PoseStamped>("centroid", 1, true);
    m_best_pose_pub = m_nh.advertise<geometry_msgs::PoseStamped>("best", 1, true);

    m_cmd_sub = m_nh.subscribe("/smi/joint_commands", 1, &PlannedManipulationTargeter::commandCallback, this);

    m_first_test = false;
    m_second_test = false;
    m_ready_for_next_test = false;
    m_server.start();

//    m_execute_srv = m_nh.advertiseService("execute", &PlannedManipulationTargeter::executeCallback, this);
//    m_visualize_srv = m_nh.advertiseService("visualize", &PlannedManipulationTargeter::visualizeCallback, this);
  }

  PlannedManipulationTargeter::~PlannedManipulationTargeter()
  {

  }

  void PlannedManipulationTargeter::commandCallback(const matec_msgs::FullJointStates::ConstPtr& msg)
  {
    m_current_commanded_configuration = msg->position;
  }

  std::string getPlanName(unsigned int i)
  {
    std::stringstream ss;
    ss << "targeter_test_plan_" << i;
    return ss.str();
  }

  bool PlannedManipulationTargeter::setInitialConfigurationPosition(std::string joint_name, double position)
  {
    unsigned int idx = std::find(m_joint_names.begin(), m_joint_names.end(), joint_name) - m_joint_names.begin();
    if(idx == m_joint_names.size())
    {
      return false;
    }

    m_standard_initial_configuration.at(idx) = position;
    return true;
  }

  void PlannedManipulationTargeter::computeFitness()
  {
    //compute time fitnesses and update markers
    for(unsigned int i = 0; i < m_target_markers.markers.size(); i++)
    {
      m_target_markers.markers.at(i).action = visualization_msgs::Marker::DELETE;
    }
    m_target_markers_pub.publish(m_target_markers);
    m_target_markers.markers.clear();
    m_fitnesses.clear();

    double min_solve_time = *std::min_element(m_solve_times.begin(), m_solve_times.end());
    double max_solve_time = *std::max_element(m_solve_times.begin(), m_solve_times.end());
    double min_alpha = 0.4;
    double max_alpha = 1.0;

    for(unsigned int i = 0; i < m_target_poses.poses.size(); i++)
    {
      double time_fitness = 1.0;
      if(min_solve_time != max_solve_time)
      {
        time_fitness = 1.0 - (m_solve_times.at(i) - min_solve_time) / (max_solve_time - min_solve_time);
      }
      m_fitnesses.push_back(time_fitness);
      double alpha = (max_alpha - min_alpha) * (time_fitness) + min_alpha;
      m_base_marker.pose = m_target_poses.poses.at(i);
      addAxisMarkers(m_base_marker, m_target_markers, alpha);
      m_target_markers_pub.publish(m_target_markers);
    }
    if(m_goal_handle.getGoal()->num_solutions != 0 && m_target_poses.poses.size() >= m_goal_handle.getGoal()->num_solutions)
    {
      finish(true, true, true);
    }

    //todo: deal with multiple clusters

    //compute weighted average pose
    double total_fitness = std::accumulate(m_fitnesses.begin(), m_fitnesses.end(), 0.0);
    m_stance_guess.header.frame_id = m_global_frame;
    m_stance_guess.header.stamp = ros::Time(0);
    m_stance_guess.pose.position.x = 0.0;
    m_stance_guess.pose.position.y = 0.0;
    m_stance_guess.pose.position.z = 0.0;
    double avg_sin = 0.0;
    double avg_cos = 0.0;
    for(unsigned int i = 0; i < m_target_poses.poses.size(); i++)
    {
      m_stance_guess.pose.position.x += m_fitnesses.at(i) * m_target_poses.poses.at(i).position.x;
      m_stance_guess.pose.position.y += m_fitnesses.at(i) * m_target_poses.poses.at(i).position.y;
      m_stance_guess.pose.position.z += m_fitnesses.at(i) * m_target_poses.poses.at(i).position.z;
      avg_sin += m_fitnesses.at(i) * sin(tf::getYaw(m_target_poses.poses.at(i).orientation));
      avg_cos += m_fitnesses.at(i) * cos(tf::getYaw(m_target_poses.poses.at(i).orientation));
    }
    m_stance_guess.pose.position.x /= total_fitness;
    m_stance_guess.pose.position.y /= total_fitness;
    m_stance_guess.pose.position.z /= total_fitness;
    avg_sin /= total_fitness;
    avg_cos /= total_fitness;
    double avg_yaw = atan2(avg_sin, avg_cos);
    m_stance_guess.pose.orientation = tf::createQuaternionMsgFromYaw(avg_yaw);
    m_have_guess = true;

    //snap best pose to nearest actual solution
    double min_dist = std::numeric_limits<double>::max();
    for(unsigned int i = 0; i < m_target_poses.poses.size(); i++)
    {
      double dx = m_stance_guess.pose.position.x - m_target_poses.poses.at(i).position.x;
      double dy = m_stance_guess.pose.position.y - m_target_poses.poses.at(i).position.y;
      double dist = sqrt(dx * dx + dy * dy);
      if(dist < min_dist)
      {
        min_dist = dist;
        m_best_plan = getPlanName(i);
        m_best_pose.pose = m_target_poses.poses.at(i);
      }
    }
    m_best_pose.header = m_stance_guess.header;
    m_best_pose_pub.publish(m_best_pose);
  }

  void PlannedManipulationTargeter::feedbackCallback(const matec_actions::PlannedManipulationFeedbackConstPtr &feedback)
  {
    if(!m_goal_handle.getGoal())
    {
      return;
    }

    if(feedback->planning_complete)
    {
      geometry_msgs::PoseStamped ground_sample = m_goal.initial_pose;
      ground_sample.pose.position.z = 0.0;
      geometry_msgs::PoseStamped global_sample;
      ground_sample.header.stamp = ros::Time(0);
      m_tf_listener->transformPose(m_global_frame, ground_sample, global_sample);

      if(feedback->planning_progress == 1.0)
      {
        m_solve_times.push_back((ros::Time::now() - m_current_test_start_time).toSec());

        ROS_DEBUG("%s: Found valid pose!", m_nh.getNamespace().c_str());
        m_target_poses.poses.push_back(global_sample.pose);
        m_target_poses_pub.publish(m_target_poses);

        computeFitness();

        if(m_first_test)
        {
          ROS_INFO("Current state worked!");
          finish(true, false, false);
        }
        if(m_second_test)
        {
          ROS_INFO("Initial guess worked!");
          finish(true, true, true);
        }
      }
      else if(!m_first_test)
      {
        ROS_DEBUG("%s: Pose was invalid!", m_nh.getNamespace().c_str());

        if(feedback->planning_progress > m_closest_plan_so_far_percent && m_target_poses.poses.size() == 0)
        {
          ROS_INFO_STREAM("Replacing with better plan: " << (feedback->planning_progress*100.0) << "% at\n" << global_sample);
          m_stance_guess = global_sample;
          m_stance_guess.header.stamp = ros::Time(0);
          m_closest_plan_so_far_percent = feedback->planning_progress;
        }
      }
      m_ready_for_next_test = true;
    }
  }

  void PlannedManipulationTargeter::doneCallback(const actionlib::SimpleClientGoalState &state, const matec_actions::PlannedManipulationResultConstPtr &result)
  {
    //todo handle rejected
    if((state == actionlib::SimpleClientGoalState::SUCCEEDED))
    {
    }
    else
    {
    }
  }

  void PlannedManipulationTargeter::testSample(geometry_msgs::PoseStamped sample, std::vector<double> initial_configuration)
  {
    geometry_msgs::PoseStamped global_sample;
    sample.header.stamp = ros::Time(0);
    m_tf_listener->transformPose(m_global_frame, sample, global_sample);

    m_attempted_poses.poses.push_back(global_sample.pose);
    m_attempted_poses_pub.publish(m_attempted_poses);

    m_goal.plan_name = getPlanName(m_target_poses.poses.size());
    m_goal.initial_pose = global_sample;
    m_goal.initial_joint_positions = initial_configuration;

    m_current_test_start_time = ros::Time::now();
    m_ready_for_next_test = false;
    m_client->sendGoal(m_goal, boost::bind(&PlannedManipulationTargeter::doneCallback, this, _1, _2), Client::SimpleActiveCallback(), boost::bind(&PlannedManipulationTargeter::feedbackCallback, this, _1));
  }

  void PlannedManipulationTargeter::findTargetPoses()
  {
    ROS_DEBUG("%s: Trying new pose", m_nh.getNamespace().c_str());

    geometry_msgs::PoseStamped sample;
    sample.header.frame_id = m_ground_frame;

    if(m_have_guess)
    {
      //todo: give up on user specified guess if it fails
      geometry_msgs::PoseStamped ground_guess;
      m_stance_guess.header.stamp = ros::Time(0);
      m_tf_listener->transformPose(m_ground_frame, m_stance_guess, ground_guess);
      double x_ground = ground_guess.pose.position.x;
      double y_ground = ground_guess.pose.position.y;
      double Y_ground = tf::getYaw(ground_guess.pose.orientation);

      double dx_guess = m_first_test? 0.0 : matec_utils::normalRandom(0, m_x_variance); //todo: variance better?
      double dy_guess = m_first_test? 0.0 : matec_utils::normalRandom(0, m_y_variance);
      double dY_guess = m_first_test? 0.0 : matec_utils::normalRandom(0, m_Y_variance);

      double dx_ground = dx_guess * cos(Y_ground) - dy_guess * sin(Y_ground);
      double dy_ground = dx_guess * sin(Y_ground) + dy_guess * cos(Y_ground);
      sample.pose.position.x = x_ground + dx_ground;
      sample.pose.position.y = y_ground + dy_ground;
      sample.pose.position.z = m_nominal_z;

      sample.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0, 0, matec_utils::angleAdd(Y_ground, dY_guess));
    }
    else
    {
      geometry_msgs::PoseStamped ground_centroid;
      m_global_centroid.header.stamp = ros::Time(0);
      m_tf_listener->transformPose(m_ground_frame, m_global_centroid, ground_centroid);
      double x_ground = ground_centroid.pose.position.x;
      double y_ground = ground_centroid.pose.position.y;
      double Y_ground = tf::getYaw(ground_centroid.pose.orientation);

      double dx_centroid = (m_x.max - m_x.min) * matec_utils::uniformRandom() + m_x.min;
      double dy_centroid = (m_y.max - m_y.min) * matec_utils::uniformRandom() + m_y.min;
      double dx_ground = dx_centroid * cos(Y_ground) - dy_centroid * sin(Y_ground);
      double dy_ground = dx_centroid * sin(Y_ground) + dy_centroid * cos(Y_ground);

      sample.pose.position.x = x_ground + dx_ground;
      sample.pose.position.y = y_ground + dy_ground;
      sample.pose.position.z = m_nominal_z;

      double sample_Y = atan2(-dy_ground, -dx_ground) + (m_Y.max - m_Y.min) * matec_utils::uniformRandom() + m_Y.min; //modulate yaw w.r.t. target view
      sample.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0, 0, sample_Y);
    }

    testSample(sample, m_standard_initial_configuration);
  }

  void PlannedManipulationTargeter::goalCallback(TargeterServer::GoalHandle goal_handle)
  {
    srand(time(NULL));
    ROS_INFO("%s: Got goal!", m_nh.getNamespace().c_str());
    cancel();
    m_closest_plan_so_far_percent = 0.0;
    m_have_guess = false;

    m_target_poses.poses.clear();
    m_target_poses_pub.publish(m_target_poses);
    m_attempted_poses.poses.clear();
    m_attempted_poses_pub.publish(m_attempted_poses);
    m_solve_times.clear();
    m_fitnesses.clear();
    for(unsigned int i = 0; i < m_target_markers.markers.size(); i++)
    {
      m_target_markers.markers.at(i).action = visualization_msgs::Marker::DELETE;
    }
    m_target_markers_pub.publish(m_target_markers);
    m_target_markers.markers.clear();

    m_goal_handle = goal_handle;
    m_goal_handle.setAccepted();
    m_goal = m_goal_handle.getGoal()->goal;

    //compute the path centroid
    unsigned int num_averaged = 0;
    m_global_centroid.header.frame_id = m_global_frame;
    m_global_centroid.header.stamp = ros::Time(0);
    m_global_centroid.pose.position.x = 0.0;
    m_global_centroid.pose.position.y = 0.0;
    m_global_centroid.pose.position.z = 0.0;
    double avg_sin = 0.0;
    double avg_cos = 0.0;
    for(unsigned int i = 0; i < m_goal.segments.size(); i++)
    {
//      for(unsigned int j = 0; j < m_goal.segments.at(i).initial_goal_regions.size(); j++)
//      {
//        geometry_msgs::PoseStamped global_pose;
//        m_goal.segments.at(i).initial_goal_regions.at(j).goal_frame.header.stamp = ros::Time(0);
//        m_tf_listener->transformPose(m_global_frame, m_goal.segments.at(i).initial_goal_regions.at(j).goal_frame, global_pose);
//        m_global_centroid.pose.position.x += global_pose.pose.position.x;
//        m_global_centroid.pose.position.y += global_pose.pose.position.y;
//        m_global_centroid.pose.position.z += global_pose.pose.position.z;
//        avg_sin += sin(tf::getYaw(global_pose.pose.orientation));
//        avg_cos += cos(tf::getYaw(global_pose.pose.orientation));
//        num_averaged++;
//      }
      for(unsigned int j = 0; j < m_goal.segments.at(i).final_goal_regions.size(); j++)
      {
        geometry_msgs::PoseStamped global_pose;
        m_goal.segments.at(i).final_goal_regions.at(j).goal_frame.header.stamp = ros::Time(0);
        m_tf_listener->transformPose(m_global_frame, m_goal.segments.at(i).final_goal_regions.at(j).goal_frame, global_pose);
        m_global_centroid.pose.position.x += global_pose.pose.position.x;
        m_global_centroid.pose.position.y += global_pose.pose.position.y;
        m_global_centroid.pose.position.z += global_pose.pose.position.z;
        avg_sin += sin(tf::getYaw(global_pose.pose.orientation));
        avg_cos += cos(tf::getYaw(global_pose.pose.orientation));
        num_averaged++;
      }
    }
    m_global_centroid.pose.position.x /= (double) num_averaged;
    m_global_centroid.pose.position.y /= (double) num_averaged;
    m_global_centroid.pose.position.z /= (double) num_averaged;
    avg_sin /= (double) num_averaged;
    avg_cos /= (double) num_averaged;
    double avg_yaw = atan2(avg_sin, avg_cos);
    m_global_centroid.pose.orientation = tf::createQuaternionMsgFromYaw(avg_yaw);
    m_global_centroid_pub.publish(m_global_centroid);

    //modify the goal for probing
    m_goal.allow_incomplete_planning = false;

    m_goal.initial_pose.header.stamp = ros::Time(0);
    m_goal.initial_pose.header.frame_id = m_global_frame;
    m_goal.initial_pose_link = m_root_link;

    m_goal.initial_joint_positions = m_standard_initial_configuration;
    m_goal.execute_on_plan = false;
    m_goal.visualize_on_plan = m_auto_visualize;

    if(matec_utils::quaternionValid(m_goal_handle.getGoal()->stance_guess.pose.orientation))
    {
      m_stance_guess = m_goal_handle.getGoal()->stance_guess;
      m_have_guess = true;
      ROS_INFO("Using provided stance guess");
    }

    tf::StampedTransform trans;
    m_tf_listener->lookupTransform(m_global_frame, "pelvis", ros::Time(0), trans);
    geometry_msgs::PoseStamped current_root_pose = matec_utils::stampedTransformToPoseStamped(trans);
    testSample(current_root_pose, m_current_commanded_configuration);
    m_first_test = true;
  }

  void PlannedManipulationTargeter::cancelCallback(TargeterServer::GoalHandle goal_handle)
  {
    ROS_INFO("%s: Goal cancelled!", m_nh.getNamespace().c_str());
    cancel();
  }

  void PlannedManipulationTargeter::cancel()
  {
    if(m_goal_handle.getGoal())
    {
      m_goal_handle.setCanceled();
      m_goal_handle = TargeterServer::GoalHandle();
    }
  }

  void PlannedManipulationTargeter::finish(bool success, bool need_to_move, bool standard_configuration)
  {
    if(!m_goal_handle.getGoal())
    {
      return;
    }
    if(success)
    {
      ROS_INFO("%s: Finished PlannedManipulationTargeter!", m_nh.getNamespace().c_str());
      matec_actions::PlannedManipulationTargeterResult result;
      result.need_to_move = need_to_move;
      result.best_initial_configuration = standard_configuration? m_standard_initial_configuration : m_current_commanded_configuration;
      result.best_stance = m_best_pose;
      result.best_plan_name = m_best_plan;
      m_goal_handle.setSucceeded(result);
      m_goal_handle = TargeterServer::GoalHandle();

      if(!need_to_move)
      {
        ROS_INFO_STREAM(m_nh.getNamespace() << ": Best plan was " << m_best_plan << " from current positions");
      }
      else
      {
        ROS_INFO_STREAM(m_nh.getNamespace() << ": Best plan was " << m_best_plan << " using stance:\n" << m_best_pose);
      }
    }
    else
    {
      ROS_INFO("%s: Aborted PlannedManipulationTargeter!", m_nh.getNamespace().c_str());
      m_goal_handle.setAborted();
      m_goal_handle = TargeterServer::GoalHandle();
    }
  }

  void PlannedManipulationTargeter::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      if(!m_client->isServerConnected())
      {
        ROS_WARN_THROTTLE(2.0, "%s: Waiting for the planned_manipulation actionlib server to start...", m_nh.getNamespace().c_str());
        cancel();
      }

      if(m_goal_handle.getGoal())
      {
        if(m_ready_for_next_test)
        {
          if(m_first_test)
          {
            m_first_test = false;
            m_second_test = true;
            ROS_INFO("First test failed! Trying second test");

            //todo: make sure the user actually provided a guess
            //todo: give up on user specified guess if it fails
            geometry_msgs::PoseStamped sample;
            m_stance_guess.header.stamp = ros::Time(0);
            m_tf_listener->transformPose(m_ground_frame, m_stance_guess, sample);
            sample.header.stamp = ros::Time(0);
            sample.pose.position.z = m_nominal_z;
            sample.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0, 0, tf::getYaw(sample.pose.orientation));
            testSample(sample, m_standard_initial_configuration);
          }
          else if(m_second_test)
          {
            ROS_INFO("Second test failed! Searching!");
            m_second_test = false;
          }
          else
          {
            findTargetPoses();
          }
        }
        else
        {
          double elapsed_time = (ros::Time::now() - m_current_test_start_time).toSec();
          if(elapsed_time > std::max(m_planning_timeout, m_goal_handle.getGoal()->sample_timeout))
          {
            ROS_DEBUG("%s: Planning test timed out.", m_nh.getNamespace().c_str());
            m_ready_for_next_test = true;
          }
        }
      }
      loop_rate.sleep();
      ros::spinOnce();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "planned_manipulation_targeter");
  ros::NodeHandle nh("~");

  planned_manipulation::PlannedManipulationTargeter node(nh);
  node.spin();

  return 0;
}
