#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <matec_actions/PlannedManipulationAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

#include "planned_manipulation/goal_region_helper.h"

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  if(argc < 6 || ((argc - 2 - 1) % 3) != 0)
  {
    ROS_ERROR("Need 2+3n arguments: tool_frame, target_frame_id, x1, y1, z1, x2, y2, z2, ...");
    return 0;
  }

  unsigned int num_points = (argc - 2 - 1) / 3;

  ros::init(argc, argv, "test_planned_manipulation");

  ros::NodeHandle nh;

  signal(SIGABRT, &halt);
  signal(SIGTERM, &halt);
  signal(SIGINT, &halt);

  actionlib::SimpleActionClient<matec_actions::PlannedManipulationAction> client(nh, "/planned_manipulation/server", true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");
  std::string tool_frame = argv[1];

  std::vector<geometry_msgs::PoseStamped> targets(num_points);
  for(unsigned int i = 0; i < num_points; i++)
  {
    targets.at(i).pose.position.x = atof(argv[3+3*i]);
    targets.at(i).pose.position.y = atof(argv[4+3*i]);
    targets.at(i).pose.position.z = atof(argv[5+3*i]);
    targets.at(i).pose.orientation.w = 1.0;
    targets.at(i).header.frame_id = argv[2];
    targets.at(i).header.stamp = ros::Time(0);
  }

  double x_tol = 0.01;
  double y_tol = 0.01;
  double z_tol = 0.01;
  double R_tol = M_PI;
  double P_tol = M_PI;
  double Y_tol = M_PI;
  matec_actions::PlannedManipulationGoal goal = planned_manipulation::generateMultiTargetGoal(tool_frame, targets, joint_names, R_tol, P_tol, Y_tol, x_tol, y_tol, z_tol, "pelvis");

  client.sendGoal(goal);
  while(ros::ok() && (client.getState().state_ != actionlib::SimpleClientGoalState::SUCCEEDED) && (client.getState().state_ != actionlib::SimpleClientGoalState::ABORTED))
  {
    ROS_INFO_THROTTLE(1.0, "Waiting for completion...");
  }

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

//**************************
//
//**************************
void halt(int sig)
{
  WTFASSERT(0);
}
