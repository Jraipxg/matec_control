#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <matec_actions/PlannedManipulationAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

#include "planned_manipulation/goal_region_helper.h"

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  if(argc < 10) //first arg is reserved
  {
    ROS_ERROR("Need 9 arguments: tool_frame_1, tool_frame_2, target_frame_id, x1, y1, z1, x2, y2, z2");
    return 0;
  }

  ros::init(argc, argv, "test_planned_manipulation");

  ros::NodeHandle nh;

  signal(SIGABRT, &halt);
  signal(SIGTERM, &halt);
  signal(SIGINT, &halt);

  actionlib::SimpleActionClient<matec_actions::PlannedManipulationAction> client(nh, "/planned_manipulation/server", true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");


  std::string tool_frame_1 = argv[1];
  geometry_msgs::PoseStamped target_1;
  target_1.pose.position.x = atof(argv[4]);
  target_1.pose.position.y = atof(argv[5]);
  target_1.pose.position.z = atof(argv[6]);
  target_1.pose.orientation.w = 1.0;
  target_1.header.frame_id = argv[3];
  target_1.header.stamp = ros::Time(0);

  std::string tool_frame_2 = argv[2];
  geometry_msgs::PoseStamped target_2;
  target_2.pose.position.x = atof(argv[7]);
  target_2.pose.position.y = atof(argv[8]);
  target_2.pose.position.z = atof(argv[9]);
  target_2.pose.orientation.w = 1.0;
  target_2.header.frame_id = argv[3];
  target_2.header.stamp = ros::Time(0);

  std::vector<std::string> tool_frames = {tool_frame_1, tool_frame_2};
  std::vector<geometry_msgs::PoseStamped> targets = {target_1, target_2};

  double x_tol = 0.01;
  double y_tol = 0.01;
  double z_tol = 0.01;
  double R_tol = M_PI;
  double P_tol = M_PI;
  double Y_tol = M_PI;
  matec_actions::PlannedManipulationGoal goal = planned_manipulation::generateMultiToolGoal(tool_frames, targets, joint_names, R_tol, P_tol, Y_tol, x_tol, y_tol, z_tol, "pelvis");

  client.sendGoal(goal);
  while(ros::ok() && (client.getState().state_ != actionlib::SimpleClientGoalState::SUCCEEDED) && (client.getState().state_ != actionlib::SimpleClientGoalState::ABORTED))
  {
    ROS_INFO_THROTTLE(1.0, "Waiting for completion...");
  }

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

//**************************
//
//**************************
void halt(int sig)
{
  WTFASSERT(0);
}
