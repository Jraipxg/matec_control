#include "planned_manipulation/manipulation_plan.h"

namespace planned_manipulation
{
  ManipulationPlan::ManipulationPlan(Server::GoalHandle goal_handle, std::vector<std::string> joint_names, boost::shared_ptr<urdf::Model> urdf_model, boost::shared_ptr<dynamics_tree::DynamicsGraph> graph, boost::shared_ptr<tf::TransformListener> tf_listener, boost::function<void(std::string, double)> self_visualize, boost::function<
      void(std::string)> self_execute)
  {
    srand(time(NULL));
    m_nh = ros::NodeHandle("~");
    m_node_name = m_nh.getNamespace();
    m_goal_handle = goal_handle;
    m_goal_handle.setAccepted();
    m_goal = *m_goal_handle.getGoal();
    m_tf_listener = tf_listener;
    m_planning_complete = false;
    m_planning_thread_active = false;
    m_joint_names = joint_names;

    m_self_visualize_callback = self_visualize;
    m_self_execute_callback = self_execute;
    m_in_callback = false;

    m_auxiliary_feedback_pub = m_nh.advertise<matec_actions::PlannedManipulationFeedback>("feedback", 1, true);

    m_urdf_model = urdf_model;
    m_graph = graph;

    m_max_joint_velocities.resize(m_joint_names.size());
    m_max_joint_accelerations.resize(m_joint_names.size());
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      boost::shared_ptr<const urdf::Joint> joint = m_urdf_model->getJoint(m_joint_names[i]);
      m_joint_mins.push_back(joint->limits->lower);
      m_joint_maxes.push_back(joint->limits->upper);

      std::string base = "/joint_dynamics/" + m_joint_names.at(i) + "/limits/";
      m_nh.param(base + "vel", m_max_joint_velocities.at(i), 0.3);
      m_nh.param(base + "acc", m_max_joint_accelerations.at(i), 1.0);
    }

    m_max_linear_accel = 1.0; //todo: param
    m_max_angular_accel = 0.5;

    ROS_INFO("%s: Created \"%s\" plan", m_node_name.c_str(), m_goal.plan_name.c_str());
  }

  ManipulationPlan::~ManipulationPlan()
  {
    stopPlanningThread();
  }

  void ManipulationPlan::stopPlanningThread()
  {
    while(m_in_callback && ros::ok())
    {
      //wait to cancel if we're in the middle of a callback
      ros::Duration(0.01).sleep();
    }
    if(m_planning_thread_active)
    {
      m_planning_thread->interrupt();
      m_planning_thread->join();
      m_planning_thread_active = false;

      //make sure we're actually dead
      ros::Duration(0.2).sleep();
    }
  }

  void ManipulationPlan::cancel()
  {
    ROS_INFO("%s: Cancelling \"%s\" plan", m_node_name.c_str(), m_goal.plan_name.c_str());
    stopPlanningThread();

    if(active())
    {
      m_goal_handle.setCanceled();
      m_goal_handle = Server::GoalHandle();
    }
    m_planning_complete = false;
    ROS_INFO("%s: Cancelled \"%s\" planning thread", m_node_name.c_str(), m_goal.plan_name.c_str());
  }

  bool ManipulationPlan::active()
  {
    return (bool) m_goal_handle.getGoal();
  }

  bool ManipulationPlan::ready()
  {
    return active() && m_planning_complete;
  }

  void ManipulationPlan::feedback(double execution_progress, bool execution_complete)
  {
    matec_actions::PlannedManipulationFeedback feedback;
    feedback.plan_name = m_goal.plan_name;
    feedback.planning_complete = m_planning_complete;
    feedback.planning_progress = m_planning_progress;
    feedback.execution_progress = execution_progress;
    feedback.execution_complete = execution_complete;
    if(m_goal_handle.getGoal())
    {
      m_goal_handle.publishFeedback(feedback);
      m_auxiliary_feedback_pub.publish(feedback);
    }
  }

  void ManipulationPlan::finish(bool success)
  {
    ROS_INFO("%s: Finishing \"%s\" plan", m_node_name.c_str(), m_goal.plan_name.c_str());
    if(!m_goal_handle.getGoal())
    {
      return;
    }
    if(success)
    {
      ROS_INFO("%s: Finished ManipulationPlan!", m_node_name.c_str());
      feedback(1.0, true);
      m_goal_handle.setSucceeded();
      m_goal_handle = Server::GoalHandle();
    }
    else
    {
      ROS_ERROR("%s: ManipulationPlan failed!", m_node_name.c_str());
      feedback(0.0, false);
      m_goal_handle.setAborted();
      m_goal_handle = Server::GoalHandle();
    }
  }

  void ManipulationPlan::getPlan(control_msgs::FollowJointTrajectoryGoal& plan, std::vector<int>& segment_start_indices)
  {
    plan = m_plan;
    segment_start_indices = m_segment_start_indices;
  }

  //todo add timescale

  void ManipulationPlan::getVisualizations(double visualization_density, visualization_msgs::MarkerArray& goal_regions, control_msgs::FollowJointTrajectoryGoal& joint_trajectories, std::vector<matec_msgs::Prophecy>& prophecies)
  {
    //todo: add movement prophecies
//    matec_msgs::Prophecy start_prophecy;
//    start_prophecy.header.stamp = ros::Time(0);
//    start_prophecy.joint_positions = getJointPositions(0);
//    start_prophecy.root_link_pose.pose = m_initial_root_link_odom.pose; //todo: update when we have pose movement simulation
//    start_prophecy.root_link_pose.header = m_initial_root_link_odom.header;
//    prophecies.push_back(prophecy);

    joint_trajectories.trajectory.header = m_plan.trajectory.header;
    joint_trajectories.trajectory.joint_names = m_plan.trajectory.joint_names;
    joint_trajectories.trajectory.points.clear();
    goal_regions.markers.clear();

    //generate region markers
    visualization_msgs::Marker marker;
    marker.action = visualization_msgs::Marker::ADD;
    marker.frame_locked = true;
    marker.header.stamp = ros::Time(0);
    marker.ns = "plan_regions_visualization";

    unsigned long plan_idx_offset = 0;
    for(unsigned long segment = 0; segment < m_goal.segments.size(); segment++)
    {
      boost::this_thread::interruption_point();
      std::vector<double> segment_initial_positions = getJointPositions(0); //todo: deal with changing joint positions / root link odometries
      matec_msgs::Odometry segment_initial_odometry = m_initial_root_link_odom; //todo: deal with changing joint positions / root link odometries
      marker.header.frame_id = m_initial_root_link_odom.header.frame_id; //goal.segments.at(segment).stable_frame;
      boost::this_thread::interruption_point();
      m_graph->spawnDynamicsTree(m_odom_link, false, m_tree);
      boost::this_thread::interruption_point();
      m_tree.kinematics(segment_initial_positions, segment_initial_odometry);
      boost::this_thread::interruption_point();
      for(unsigned long tool = 0; tool < m_plan_regions.at(segment).size(); tool++)
      {
        boost::this_thread::interruption_point();
        visualization_msgs::Marker last_cube_marker;
        for(unsigned long time_slice = 0; time_slice < m_plan_regions.at(segment).at(tool).size(); time_slice++)
        {
          if((long) time_slice > m_last_good_slices.at(segment))
          {
            marker.color.r = 1.0;
            marker.color.g = 0.4;
            marker.color.b = 0.0;
          }
          else
          {
            marker.color.r = 0.0;
            marker.color.g = 0.5;
            marker.color.b = 0.5;
          }

          matec_msgs::GoalRegion region = m_plan_regions.at(segment).at(tool).at(time_slice);

          geometry_msgs::PoseStamped visualization_pose;
          m_tree.transformPose(marker.header.frame_id, region.goal_frame, visualization_pose);
//          m_tf_listener->transformPose(marker.header.frame_id, region.goal_frame, visualization_pose);

          marker.pose = visualization_pose.pose; //todo: deal with asymmetric goal regions
          marker.scale.x = region.x.max - region.x.min;
          marker.scale.y = region.y.max - region.y.min;
          marker.scale.z = region.z.max - region.z.min;

          double percent_complete = (double) time_slice / (double) m_plan_regions.at(segment).at(tool).size();
          marker.color.a = 0.2 + 0.6 * percent_complete; //0.2 alpha at the beginning to 0.8 alpha at the end

          if((time_slice != 0) && (time_slice != m_plan_regions.at(segment).at(tool).size() - 1))
          {
            //if it's not the first or last slice, make sure we aren't putting them too close together
            double center_dx = last_cube_marker.pose.position.x - marker.pose.position.x;
            double center_dy = last_cube_marker.pose.position.y - marker.pose.position.y;
            double center_dz = last_cube_marker.pose.position.z - marker.pose.position.z;
            double center_dist = sqrt(center_dx * center_dx + center_dy * center_dy + center_dz * center_dz);
            double current_radius = sqrt((marker.scale.x * marker.scale.x + marker.scale.y * marker.scale.y + marker.scale.z * marker.scale.z) / 4.0);
            double last_radius = sqrt((last_cube_marker.scale.x * last_cube_marker.scale.x + last_cube_marker.scale.y * last_cube_marker.scale.y + last_cube_marker.scale.z * last_cube_marker.scale.z) / 4.0);
            double avg_radius = (current_radius + last_radius) / 2.0;

            if(center_dist * visualization_density < avg_radius) //too close! skip it.
            {
              continue;
            }
          }
          boost::this_thread::interruption_point();

          marker.type = visualization_msgs::Marker::CUBE;
          marker.id = goal_regions.markers.size();
          goal_regions.markers.push_back(marker);
          last_cube_marker = marker;

          planned_manipulation::addAxisMarkers(marker, goal_regions);

          if((plan_idx_offset + time_slice) < m_plan.trajectory.points.size())
          {
            joint_trajectories.trajectory.points.push_back(m_plan.trajectory.points.at((plan_idx_offset + time_slice)));

            matec_msgs::Prophecy prophecy;
            prophecy.header.stamp = ros::Time(0) + m_plan.trajectory.points.at((plan_idx_offset + time_slice)).time_from_start;
            prophecy.joint_positions = getJointPositions((plan_idx_offset + time_slice));
            prophecy.root_link_pose.pose = m_initial_root_link_odom.pose; //todo: update when we have pose movement simulation
            prophecy.root_link_pose.header = m_initial_root_link_odom.header;
            prophecies.push_back(prophecy);
          }
        }
        plan_idx_offset += m_plan_regions.at(segment).at(tool).size();
      }
    }
  }

  std::vector<double> ManipulationPlan::getJointPositions(unsigned int plan_idx)
  {
    std::vector<double> positions = m_current_positions;
    for(unsigned int i = 0; i < m_plan.trajectory.joint_names.size(); i++)
    {
      unsigned int matec_idx = std::find(m_joint_names.begin(), m_joint_names.end(), m_plan.trajectory.joint_names.at(i)) - m_joint_names.begin();
      positions.at(matec_idx) = m_plan.trajectory.points.at(plan_idx).positions.at(i);
    }
    return positions;
  }

  std::string ManipulationPlan::findNearestURDFLink(std::string frame)
  {
    boost::shared_ptr<const urdf::Link> link = m_urdf_model->getLink(frame);
    if(link)
    {
      return frame;
    }
    else
    {
      std::string parent;
      while(ros::ok() && !m_tf_listener->getParent(frame, ros::Time(0), parent))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Waiting for tf to find the parent of %s", m_node_name.c_str(), frame.c_str());
      }

      return findNearestURDFLink(parent);
    }
  }

  bool ManipulationPlan::getCurrentOffset(std::string tool_frame, std::string& nearest_urdf_link, matec_utils::Matrix4& nearestTtool)
  {
    //find the closest part of the tree
    nearest_urdf_link = findNearestURDFLink(tool_frame);

    //look up the offset
    if(!m_tf_listener->canTransform(nearest_urdf_link, tool_frame, ros::Time(0)))
    {
      ROS_ERROR("%s: Goal failed due to nonexistent transform between %s and %s", m_node_name.c_str(), nearest_urdf_link.c_str(), tool_frame.c_str());
      return false;
    }
    tf::StampedTransform tf_nearestTtool;
    //targetTsource
    m_tf_listener->lookupTransform(nearest_urdf_link, tool_frame, ros::Time(0), tf_nearestTtool);

    nearestTtool = matec_utils::stampedTransformToMatrix(tf_nearestTtool);

    return true;
  }

  void ManipulationPlan::generateTwistBasisRecursive(int depth, matec_utils::Vector6& current_delta, matec_utils::Matrix4& stableTtool, matec_utils::Matrix4& stableTgoal, matec_utils::Vector6 mins, matec_utils::Vector6 maxes, double dt, std::vector<matec_utils::Vector6>& twist_basis)
  {
    if(depth == 6)
    {
      matec_utils::Matrix4 goalTbound = matec_utils::Matrix4::Identity();
      goalTbound.topRightCorner(3, 1) = current_delta.bottomRows(3);
      tf::Quaternion quat = tf::createQuaternionFromRPY((double) current_delta(0), (double) current_delta(1), (double) current_delta(2));
      goalTbound.topLeftCorner(3, 3) = Eigen::Quaterniond(quat.w(), quat.x(), quat.y(), quat.z()).toRotationMatrix();

      matec_utils::Matrix4 stableTbound = stableTgoal * goalTbound;
      matec_utils::Vector6 twistToBound = matec_utils::frameTwist(stableTtool, stableTbound, dt);
      twist_basis.push_back(twistToBound);
    }
    else
    {
      current_delta(depth) = mins(depth); //min
      generateTwistBasisRecursive(depth + 1, current_delta, stableTtool, stableTgoal, mins, maxes, dt, twist_basis);

      current_delta(depth) = maxes(depth); //max
      generateTwistBasisRecursive(depth + 1, current_delta, stableTtool, stableTgoal, mins, maxes, dt, twist_basis);
    }
  }

  void ManipulationPlan::generateTwistBasis(matec_utils::Matrix4& stableTtool, matec_utils::Matrix4& stableTgoal, matec_msgs::GoalRegion& goal_region, double dt, std::vector<matec_utils::Vector6>& twist_basis)
  {
    matec_utils::Vector6 mins, maxes;
    matec_utils::goalRegionToRPYxyz(goal_region, mins, maxes);

    matec_utils::Vector6 current_delta = matec_utils::Vector6::Zero();
    generateTwistBasisRecursive(0, current_delta, stableTtool, stableTgoal, mins, maxes, dt, twist_basis);
  }

  matec_utils::Vector6 sampleTwistFromBasis(std::vector<matec_utils::Vector6> twist_basis)
  {
    matec_utils::Vector6 twist = matec_utils::Vector6::Zero();
    std::vector<double> rand(twist_basis.size());
    double rand_sum = 0.0;
    for(unsigned int i = 0; i < twist_basis.size(); i++)
    {
      rand.at(i) = matec_utils::uniformRandom();
      rand_sum += rand.at(i);
    }
    for(unsigned int i = 0; i < twist_basis.size(); i++)
    {
      twist += twist_basis.at(i) * rand.at(i) / rand_sum; //convex combination of twist basis vectors
    }
    return twist;
  }

  inline std::string getMobileGoalFrameName(int tool_idx)
  {
    std::stringstream ss;
    ss << "goal_frame_" << tool_idx;
    return ss.str();
  }

  //returns true if all tool poses are within goal regions
  bool ManipulationPlan::checkToolPoses(dynamics_tree::DynamicsTree& tree, std::vector<double> joint_positions, std::vector<matec_msgs::GoalRegion> slice_regions, std::vector<matec_utils::Matrix4>& goalTtools, std::vector<matec_utils::Vector6>& restoration_twists)
  {
    matec_msgs::Odometry odom;
    odom.pose.orientation.w = 1.0; //everything is relative to the tree, don't need real odom
    tree.kinematics(joint_positions, odom);

    goalTtools.resize(slice_regions.size());
    restoration_twists.resize(slice_regions.size());
    bool tools_in_goal_regions = true;

    for(unsigned int tool = 0; tool < slice_regions.size() && ros::ok(); tool++)
    {
      boost::this_thread::interruption_point();
      WTFASSERT(tree.lookupTransform(getMobileGoalFrameName(tool), slice_regions.at(tool).tool_frame, goalTtools.at(tool)));
      if(!matec_utils::poseInGoalRegion(goalTtools.at(tool), slice_regions.at(tool), restoration_twists.at(tool)))
      {
        tools_in_goal_regions = false;
      }
    }

    return tools_in_goal_regions;
  }

  bool ManipulationPlan::tryTwists(std::vector<matec_utils::Vector6> twists, TwistSolutionVariant mode, std::vector<double> initial_positions, std::vector<double> initial_velocities, std::vector<unsigned int> available_joint_indices, std::vector<matec_msgs::GoalRegion> slice_regions, double dt, dynamics_tree::DynamicsTree& tree, std::vector<
      matec_utils::Matrix>& jacobians, std::vector<matec_utils::Matrix>& JTs, std::vector<matec_utils::Matrix>& JJTs, std::vector<double>& final_positions, std::vector<double>& final_velocities, double& score)
  {
    final_positions = initial_positions;
    final_velocities = initial_velocities;
    std::vector<std::vector<double> > tool_joint_velocities(twists.size(), std::vector<double>(available_joint_indices.size(), 0.0));
    for(unsigned int tool = 0; tool < twists.size(); tool++)
    {
      boost::this_thread::interruption_point();

      //calculate joint velocities from each twist
      if(mode == INDEPENDENT_TRANSPOSE)
      {
        //todo: use cached jacobian instead
//        if(!tree.cartesianVelocityToJointVelocities(available_joint_indices, weighted_twist, slice_regions.at(tool).goal_frame.header.frame_id, slice_regions.at(tool).tool_frame, tool_joint_velocities.at(tool)))
//        {
//          return false;
//        }
//        boost::this_thread::interruption_point();

        for(unsigned int i = 0; i < available_joint_indices.size(); i++)
        {
          matec_utils::Vector6 Ji = jacobians.at(tool).col(i);
          matec_utils::Vector6 JJTe = Ji * Ji.transpose() * twists.at(tool);
          double alpha = (double) (twists.at(tool).transpose() * JJTe) / (double) (JJTe.transpose() * JJTe); //possibly wrong
          if(isnan(alpha) || isinf(alpha)) //usually happens when error is zero
          {
            alpha = 0;
          }
          tool_joint_velocities.at(tool).at(i) = alpha * Ji.transpose() * twists.at(tool);
        }
      }
      else
      {
        if(mode == FULL_TRANSPOSE)
        {
          //dQ = J^T * e * (<e, J*J^T*e> / <J*J^T*e, J*J^T*e>)
          matec_utils::Vector JJTe = JJTs.at(tool) * twists.at(tool);
          boost::this_thread::interruption_point();
          matec_utils::Vector velocities = JTs.at(tool) * twists.at(tool) * (twists.at(tool).transpose() * JJTe) / (JJTe.transpose() * JJTe);
          boost::this_thread::interruption_point();

          for(unsigned int i = 0; i < available_joint_indices.size(); i++)
          {
            tool_joint_velocities.at(tool).at(i) = velocities(i);
          }
        }
        if(mode == PSEUDOINVERSE)
        {
          matec_utils::Vector velocities = JTs.at(tool) * JJTs.at(tool).inverse() * twists.at(tool);
          boost::this_thread::interruption_point();
          for(unsigned int i = 0; i < available_joint_indices.size(); i++)
          {
            tool_joint_velocities.at(tool).at(i) = velocities(i);
          }

          for(unsigned int i = 0; i < tool_joint_velocities.at(tool).size(); i++)
          {
            if(isnan(tool_joint_velocities.at(tool).at(i)) || isinf(tool_joint_velocities.at(tool).at(i)))
            {
              std::cerr << "NAN1" << std::endl;
              tool_joint_velocities.at(tool).at(i) = 0.0;
            }
          }
        }
      }

      //limit velocity
      double velocity_scale_factor = 1.0;
      double velocity_scale_factor_no_joint_limits = 1.0;
      for(unsigned int j = 0; j < tool_joint_velocities.at(tool).size() && ros::ok(); j++)
      {
        boost::this_thread::interruption_point();
        unsigned int matec_idx = available_joint_indices.at(j);
//        double max_accel = m_max_joint_accelerations.at(matec_idx);
        double min_vel_from_accel = -std::numeric_limits<double>::max(); //initial_velocities.at(matec_idx) - max_accel * dt;
        double max_vel_from_accel = std::numeric_limits<double>::max(); //initial_velocities.at(matec_idx) + max_accel * dt;
        double max_positive_vel = std::min(matec_utils::clamp(m_max_joint_velocities.at(matec_idx), min_vel_from_accel, max_vel_from_accel), (m_joint_maxes.at(matec_idx) - initial_positions.at(matec_idx)) / dt);
        double max_negative_vel = std::max(matec_utils::clamp(-m_max_joint_velocities.at(matec_idx), min_vel_from_accel, max_vel_from_accel), (m_joint_mins.at(matec_idx) - initial_positions.at(matec_idx)) / dt);
        if(tool_joint_velocities.at(tool).at(j) != 0.0)
        {
          if(tool_joint_velocities.at(tool).at(j) > max_positive_vel)
          {
            velocity_scale_factor = std::min(velocity_scale_factor, fabs(max_positive_vel / tool_joint_velocities.at(tool).at(j)));
          }
          else if(tool_joint_velocities.at(tool).at(j) < max_negative_vel)
          {
            velocity_scale_factor = std::min(velocity_scale_factor, fabs(max_negative_vel / tool_joint_velocities.at(tool).at(j)));
          }
        }

        if(tool_joint_velocities.at(tool).at(j) != 0.0)
        {
          if((tool_joint_velocities.at(tool).at(j) > m_max_joint_velocities.at(matec_idx)) || (tool_joint_velocities.at(tool).at(j) < -m_max_joint_velocities.at(matec_idx)))
          {
            velocity_scale_factor_no_joint_limits = std::min(velocity_scale_factor_no_joint_limits, fabs(m_max_joint_velocities.at(matec_idx) / tool_joint_velocities.at(tool).at(j)));
          }
        }
      }

      if(velocity_scale_factor == 0.0)
      {
        //if we can't move one or more joints because they're stuck against a limit, see if ignoring the problem helps (limits still enforced individually later)
        velocity_scale_factor = velocity_scale_factor_no_joint_limits;
      }

      for(unsigned int j = 0; j < tool_joint_velocities.at(tool).size() && ros::ok(); j++)
      {
        tool_joint_velocities.at(tool).at(j) *= velocity_scale_factor;
      }
    }

    //simulate forward (euler)
    for(unsigned int j = 0; j < available_joint_indices.size() && ros::ok(); j++)
    {
      boost::this_thread::interruption_point();
      unsigned int num_tools_influencing_joint = 0;
      double combined_joint_velocity = 0.0;
      for(unsigned int tool = 0; tool < twists.size(); tool++)
      {
        if(tool_joint_velocities.at(tool).at(j) != 0.0)
        {
          if(isnan(tool_joint_velocities.at(tool).at(j)) || isinf(tool_joint_velocities.at(tool).at(j)))
          {
//            std::cerr << "Twist: " << twists.at(tool).transpose()<<std::endl;
//            std::cerr << "NAN2! JAC:" << std::endl;
//            std::cerr << jacobians.at(tool) << std::endl;
            tool_joint_velocities.at(tool).at(j) = 0.0;
            continue;
          }
          combined_joint_velocity += tool_joint_velocities.at(tool).at(j);
          num_tools_influencing_joint++;
        }
      }

      if(num_tools_influencing_joint == 0)
      {
        continue;
      }

      unsigned int matec_idx = available_joint_indices.at(j);
      final_positions.at(matec_idx) += dt * combined_joint_velocity / (double) num_tools_influencing_joint; //average velocity across all goal regions * time
      final_positions.at(matec_idx) = matec_utils::clamp(final_positions.at(matec_idx), m_joint_mins[matec_idx], m_joint_maxes[matec_idx]);
      final_velocities.at(matec_idx) = (final_positions.at(matec_idx) - initial_positions.at(matec_idx)) / dt;
    }

    //check
    matec_msgs::Odometry odom;
    odom.pose.orientation.w = 1.0; //everything is relative to the tree, don't need real odom
    boost::this_thread::interruption_point();
    tree.kinematics(final_positions, odom); //todo: update when we deal with root movement
    boost::this_thread::interruption_point();

    bool tools_in_goal_regions = true;
    score = 0.0;
    for(unsigned int tool = 0; tool < slice_regions.size() && ros::ok(); tool++)
    {
      boost::this_thread::interruption_point();
      matec_utils::Vector6 err;
      matec_utils::Matrix4 goalTtool;
      tree.lookupTransform(getMobileGoalFrameName(tool), slice_regions.at(tool).tool_frame, goalTtool);

      if(!matec_utils::poseInGoalRegion(goalTtool, slice_regions.at(tool), err))
      {
        tools_in_goal_regions = false;
      }
      score += err.norm(); //todo: weight error norm?
    }

    return tools_in_goal_regions;
  }

  bool ManipulationPlan::tryTwistSolutionVariants(std::vector<matec_utils::Vector6> twists, std::vector<double> initial_positions, std::vector<double> initial_velocities, std::vector<unsigned int> available_joint_indices, std::vector<matec_msgs::GoalRegion> slice_regions, double dt, dynamics_tree::DynamicsTree& tree, std::vector<
      matec_utils::Matrix>& jacobians, std::vector<matec_utils::Matrix>& JTs, std::vector<matec_utils::Matrix>& JJTs, std::vector<double>& final_positions, std::vector<double>& final_velocities, double& score)
  {
    score = std::numeric_limits<double>::max();
    for(unsigned int i = 0; i < NUM_TWIST_SOLUTION_VARIANTS; i++)
    {
      boost::this_thread::interruption_point();
      double candidate_score = std::numeric_limits<double>::max();
      std::vector<double> candidate_positions;
      std::vector<double> candidate_velocities;
      tryTwists(twists, (TwistSolutionVariant) i, initial_positions, initial_velocities, available_joint_indices, slice_regions, dt, tree, jacobians, JTs, JJTs, candidate_positions, candidate_velocities, candidate_score);
      boost::this_thread::interruption_point();

      if(candidate_score < score)
      {
        score = candidate_score;
        final_positions = candidate_positions;
        final_velocities = candidate_velocities;
      }
      if(score == 0.0)
      {
        return true;
      }
    }
    return false;
  }

  bool ManipulationPlan::solveSlice(std::vector<double> initial_positions, std::vector<double> initial_velocities, std::vector<unsigned int> available_joint_indices, std::vector<matec_msgs::GoalRegion> slice_regions, double& slice_dt, dynamics_tree::DynamicsTree& tree, std::vector<double>& final_positions, std::vector<
      double>& final_velocities)
  {
    std::vector<matec_utils::Matrix4> centroidTtools(slice_regions.size(), matec_utils::Matrix4::Identity());
    std::vector<matec_utils::Vector6> candidate_twists(slice_regions.size(), matec_utils::Vector6::Zero());
    std::vector<double> original_initial_positions = initial_positions;
    std::vector<double> original_initial_velocities = initial_velocities;
    std::vector<double> candidate_positions = initial_positions;
    std::vector<double> candidate_velocities = initial_velocities;
    double candidate_score = std::numeric_limits<double>::max();
    std::vector<double> best_positions = initial_positions;
    std::vector<double> best_velocities = initial_velocities;
    double best_score = std::numeric_limits<double>::max();

    //load the current slice into the tree and get initial tool poses
    for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
    {
      tree.updateFixedFrameOffset(getMobileGoalFrameName(tool), matec_utils::poseStampedToMatrix(slice_regions.at(tool).goal_frame).inverse());
    }
    boost::this_thread::interruption_point();
    tree.kinematics(initial_positions, m_initial_root_link_odom); //todo: at least use segment odom
    boost::this_thread::interruption_point();

    //set up weights
    std::vector<matec_utils::Vector6> one_weights(slice_regions.size(), matec_utils::Vector6::Ones());
    std::vector<matec_utils::Vector6> full_weights(slice_regions.size(), matec_utils::Vector6::Ones());
    std::vector<matec_utils::Vector6> position_weights(slice_regions.size(), matec_utils::Vector6::Ones());
    std::vector<matec_utils::Vector6> goal_weights(slice_regions.size(), matec_utils::Vector6::Zero());
    for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
    {
      position_weights.at(tool).topRows(3) = matec_utils::Vector3::Zero();

      //get required slice travel
      matec_utils::Vector6 rpyxyz_goal_tool, rpyxyz_min, rpyxyz_max;
      matec_utils::Matrix4 goalTtool;
      tree.lookupTransform(getMobileGoalFrameName(tool), slice_regions.at(tool).tool_frame, goalTtool);
      matec_utils::poseToRPYxyz(goalTtool, rpyxyz_goal_tool);
      matec_utils::goalRegionToRPYxyz(slice_regions.at(tool), rpyxyz_min, rpyxyz_max);
      double max_weight = 0.0;
      for(unsigned int i = 0; i < 6; i++)
      {
        boost::this_thread::interruption_point();
        if(rpyxyz_goal_tool(i) != 0.0)
        {
          goal_weights.at(tool)(i) = 1.0 - matec_utils::clamp(((double) rpyxyz_max(i) - (double) rpyxyz_min(i)) / (double) rpyxyz_goal_tool(i), 0.0, 1.0);
          max_weight = std::max(max_weight, (double) goal_weights.at(tool)(i));
        }
        else
        {
          goal_weights.at(tool)(i) = 0.0;
        }
      }
      WTFASSERT(max_weight <= 1.0);

      if(max_weight != 0.0 && max_weight != 1.0)
      {
        //scale weights so that at least one is 1.0 and the others are smaller
        for(unsigned int i = 0; i < 6; i++)
        {
          goal_weights.at(tool)(i) = (double) goal_weights.at(tool)(i) / max_weight;
        }
      }
      else
      {
        goal_weights.at(tool) = matec_utils::Vector6::Ones();
      }

      for(unsigned int i = 0; i < 3; i++)
      {
        goal_weights.at(tool)(i) = matec_utils::clamp(1.0 - 0.5 * ((double) rpyxyz_max(i) - (double) rpyxyz_min(i)) / (M_PI), 0.0, 1.0);
      }
      for(unsigned int i = 3; i < 6; i++)
      {
        goal_weights.at(tool)(i) = matec_utils::clamp(1.0 / ((double) rpyxyz_max(i) - (double) rpyxyz_min(i)), 0.0, 1.0);
      }

      //enforce no un-necessary orientation twisting (TODO: find a way to do this generically)
      for(unsigned int i = 0; i < 3; i++)
      {
        if(fabs((double) rpyxyz_max(i) - (double) rpyxyz_min(i)) > (1.9 * M_PI))
        {
          full_weights.at(tool)(i) = 0.0;
          goal_weights.at(tool)(i) = 0.0;
        }
      }
    }

    //solve
    bool success = false;
    unsigned int slice_attempt = 0;
    for(slice_attempt = 0; slice_attempt < m_max_same_slice_attempts; slice_attempt++)
    {
      boost::this_thread::interruption_point();
      initial_positions = best_positions;
      initial_velocities = best_velocities;

      //everything is relative to the tree, don't need real odom
      tree.kinematics(initial_positions, m_initial_root_link_odom); //todo: at least use segment odom
      std::vector<matec_utils::Matrix> jacobians(slice_regions.size());
      std::vector<matec_utils::Matrix> JTs(slice_regions.size());
      std::vector<matec_utils::Matrix> JJTs(slice_regions.size());
      for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
      {
        if(!tree.jacobian(available_joint_indices, slice_regions.at(tool).goal_frame.header.frame_id, slice_regions.at(tool).tool_frame, jacobians.at(tool)))
        {
          return false;
        }
        boost::this_thread::interruption_point();
        JTs.at(tool) = jacobians.at(tool).transpose();
        boost::this_thread::interruption_point();
        JJTs.at(tool) = jacobians.at(tool) * JTs.at(tool);
        boost::this_thread::interruption_point();
      }

      std::vector<matec_utils::Matrix4> stableTtools(slice_regions.size(), matec_utils::Matrix4::Identity());
      std::vector<matec_utils::Matrix4> stableTgoals(slice_regions.size(), matec_utils::Matrix4::Identity());
      for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
      {
        tree.lookupTransform(slice_regions.at(tool).goal_frame.header.frame_id, getMobileGoalFrameName(tool), stableTgoals.at(tool));
        tree.lookupTransform(slice_regions.at(tool).goal_frame.header.frame_id, slice_regions.at(tool).tool_frame, stableTtools.at(tool));
      }

      //generate centroid twists
      for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
      {
        candidate_twists.at(tool) = -matec_utils::frameTwist(stableTgoals.at(tool), stableTtools.at(tool), slice_dt, goal_weights.at(tool));
      }

      //try following intelligently weighted centroid twists
      boost::this_thread::interruption_point();
      if(tryTwistSolutionVariants(candidate_twists, initial_positions, initial_velocities, available_joint_indices, slice_regions, slice_dt, tree, jacobians, JTs, JJTs, candidate_positions, candidate_velocities, candidate_score))
      {
        final_positions = candidate_positions;
        final_velocities = candidate_velocities;
        success = true;
        break;
      }
      else if(candidate_score < best_score)
      {
        best_positions = candidate_positions;
        best_velocities = candidate_velocities;
      }

      //try following the full centroids
      for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
      {
        candidate_twists.at(tool) = -matec_utils::frameTwist(stableTgoals.at(tool), stableTtools.at(tool), slice_dt, full_weights.at(tool));
      }
      boost::this_thread::interruption_point();
      if(tryTwistSolutionVariants(candidate_twists, initial_positions, initial_velocities, available_joint_indices, slice_regions, slice_dt, tree, jacobians, JTs, JJTs, candidate_positions, candidate_velocities, candidate_score))
      {
        final_positions = candidate_positions;
        final_velocities = candidate_velocities;
        success = true;
        break;
      }
      else if(candidate_score < best_score)
      {
        best_positions = candidate_positions;
        best_velocities = candidate_velocities;
      }

      //try following just the centroid positions
      for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
      {
        candidate_twists.at(tool) = -matec_utils::frameTwist(stableTgoals.at(tool), stableTtools.at(tool), slice_dt, position_weights.at(tool));
      }
      boost::this_thread::interruption_point();
      if(tryTwistSolutionVariants(candidate_twists, initial_positions, initial_velocities, available_joint_indices, slice_regions, slice_dt, tree, jacobians, JTs, JJTs, candidate_positions, candidate_velocities, candidate_score))
      {
        final_positions = candidate_positions;
        final_velocities = candidate_velocities;
        success = true;
        break;
      }
      else if(candidate_score < best_score)
      {
        best_positions = candidate_positions;
        best_velocities = candidate_velocities;
      }

      //try minimally correcting the goal-space error
      for(unsigned int tool = 0; tool < slice_regions.size(); tool++)
      {
        matec_utils::Matrix4 goalTtool = stableTgoals.at(tool).inverse() * stableTtools.at(tool);
        matec_utils::poseInGoalRegion(goalTtool, slice_regions.at(tool), candidate_twists.at(tool));
        candidate_twists.at(tool) = candidate_twists.at(tool) / slice_dt;

        candidate_twists.at(tool).topRows(3) = stableTgoals.at(tool).topLeftCorner(3, 3) * candidate_twists.at(tool).topRows(3);
        candidate_twists.at(tool).bottomRows(3) = stableTgoals.at(tool).topLeftCorner(3, 3) * candidate_twists.at(tool).bottomRows(3);
      }
      if(tryTwistSolutionVariants(candidate_twists, initial_positions, initial_velocities, available_joint_indices, slice_regions, slice_dt, tree, jacobians, JTs, JJTs, candidate_positions, candidate_velocities, candidate_score))
      {
        final_positions = candidate_positions;
        final_velocities = candidate_velocities;
        success = true;
        break;
      }
      else if(candidate_score < best_score)
      {
        best_positions = candidate_positions;
        best_velocities = candidate_velocities;
      }

      //come up with a twist basis for every goal region
      boost::this_thread::interruption_point();
      std::vector<std::vector<matec_utils::Vector6> > twist_bases(slice_regions.size());
      for(unsigned int tool = 0; tool < slice_regions.size() && ros::ok(); tool++)
      {
        generateTwistBasis(stableTtools.at(tool), stableTgoals.at(tool), slice_regions.at(tool), slice_dt, twist_bases.at(tool));
      }

      //randomly twist until we find an answer
      unsigned int num_attempts = 0;
      boost::this_thread::interruption_point();
      while(ros::ok() && num_attempts <= m_max_twist_attempts)
      {
        for(unsigned int tool = 0; tool < slice_regions.size() && ros::ok(); tool++)
        {
          boost::this_thread::interruption_point();
          //solve for joint velocities
          candidate_twists.at(tool) = sampleTwistFromBasis(twist_bases[tool]);
        }

        //todo: add weights back in
        boost::this_thread::interruption_point();
        if(tryTwistSolutionVariants(candidate_twists, initial_positions, initial_velocities, available_joint_indices, slice_regions, slice_dt, tree, jacobians, JTs, JJTs, candidate_positions, candidate_velocities, candidate_score))
        {
          final_positions = candidate_positions;
          final_velocities = candidate_velocities;
          success = true;
          break;
        }
        else if(candidate_score < best_score)
        {
          best_positions = candidate_positions;
          best_velocities = candidate_velocities;
        }

        final_positions = best_positions;
        final_velocities = best_velocities;
        num_attempts++;
      }

      if(success)
      {
        break;
      }
    }

    //may need a longer dt than originally specified
    for(unsigned int i = 0; i < original_initial_positions.size(); i++)
    {
      boost::this_thread::interruption_point();
      //todo: add in accel!
      double full_delta = fabs(final_positions.at(i) - original_initial_positions.at(i));
      slice_dt = std::max(slice_dt, full_delta / m_max_joint_velocities.at(i));
    }

    return success;
  }

  double ManipulationPlan::minimumMotionDuration(std::vector<geometry_msgs::PoseStamped> initials_in_stable, std::vector<geometry_msgs::PoseStamped> finals_in_stable, matec_msgs::GoalMotion motion)
  {
    double min_motion_duration = motion.segment_duration;
    for(unsigned int i = 0; i < initials_in_stable.size(); i++)
    {
      boost::this_thread::interruption_point();
      matec_utils::Matrix4 stableTstart = matec_utils::poseStampedToMatrix(initials_in_stable.at(i));
      matec_utils::Matrix4 stableTend = matec_utils::poseStampedToMatrix(finals_in_stable.at(i));
      matec_utils::Matrix4 endTstart = stableTend.inverse() * stableTstart;
      matec_utils::Vector6 goal_space_error;
      matec_utils::poseInGoalRegion(endTstart, motion.final_goal_regions.at(i), goal_space_error);
      double goal_space_position_error = std::sqrt((double) (goal_space_error(3) * goal_space_error(3) + goal_space_error(4) * goal_space_error(4) + goal_space_error(5) * goal_space_error(5)));
      double goal_space_orientation_error = std::sqrt((double) (goal_space_error(0) * goal_space_error(0) + goal_space_error(1) * goal_space_error(1) + goal_space_error(2) * goal_space_error(2)));

      min_motion_duration = std::max(min_motion_duration, matec_utils::MinimumJerkInterpolator::timeForMaxVelocity(0, fabs(goal_space_position_error), motion.max_linear_velocity));
      min_motion_duration = std::max(min_motion_duration, matec_utils::MinimumJerkInterpolator::timeForMaxAcceleration(0, fabs(goal_space_position_error), m_max_linear_accel));
      min_motion_duration = std::max(min_motion_duration, matec_utils::MinimumJerkInterpolator::timeForMaxVelocity(0, fabs(goal_space_orientation_error), motion.max_angular_velocity));
      min_motion_duration = std::max(min_motion_duration, matec_utils::MinimumJerkInterpolator::timeForMaxAcceleration(0, fabs(goal_space_orientation_error), m_max_angular_accel));

      if(min_motion_duration > motion.segment_duration)
      {
        ROS_WARN_STREAM(m_node_name << ": Changed segment duration from " << motion.segment_duration << " to " << min_motion_duration << " due to velocity constraints and frame delta " << goal_space_error.transpose());
      }
    }
    return min_motion_duration;
  }

  bool ManipulationPlan::transformCentroid(std::string stable_frame, std::vector<double> joint_positions, matec_msgs::Odometry odometry, bool attach_to_robot, geometry_msgs::PoseStamped centroid_in, geometry_msgs::PoseStamped& centroid_out)
  {
    //todo: add checks for tf availability
    //move kinematic tree to desired initial state
    boost::this_thread::interruption_point();
    m_graph->spawnDynamicsTree(m_odom_link, false, m_tree);
    boost::this_thread::interruption_point();
    m_tree.kinematics(joint_positions, odometry);
    boost::this_thread::interruption_point();

    //todo: fix sim tf tree

    std::string nearest_urdf_link = findNearestURDFLink(centroid_in.header.frame_id); //todo: don't assume that a urdf link is at the top of the tf tree

    geometry_msgs::PoseStamped intermediate_centroid;
    centroid_in.header.stamp = ros::Time(0);
    if(attach_to_robot)
    {
      m_tf_listener->transformPose(nearest_urdf_link, centroid_in, intermediate_centroid);
    }
    else
    {
      m_tf_listener->transformPose(odometry.header.frame_id, centroid_in, intermediate_centroid);
    }

    m_tree.transformPose(stable_frame, intermediate_centroid, centroid_out);

    return true;
  }

  template<typename InterpolationMethod>
  void ManipulationPlan::interpolateSegment(std::vector<double> segment_initial_joints, matec_msgs::Odometry segment_initial_odometry, matec_msgs::GoalMotion motion, std::vector<std::vector<matec_msgs::GoalRegion> >& interpolated_motions)
  {
    std::vector<geometry_msgs::PoseStamped> initials_in_stable(motion.initial_goal_regions.size());
    std::vector<geometry_msgs::PoseStamped> finals_in_stable(motion.initial_goal_regions.size());
    for(unsigned int i = 0; i < motion.initial_goal_regions.size(); i++)
    {
      boost::this_thread::interruption_point();
      transformCentroid(motion.stable_frame, segment_initial_joints, segment_initial_odometry, motion.initial_goal_regions.at(i).attach_goal_frame_to_robot, motion.initial_goal_regions.at(i).goal_frame, initials_in_stable.at(i));
      transformCentroid(motion.stable_frame, segment_initial_joints, segment_initial_odometry, motion.final_goal_regions.at(i).attach_goal_frame_to_robot, motion.final_goal_regions.at(i).goal_frame, finals_in_stable.at(i));
    }

    double minimum_motion_duration = matec_utils::GoalSpaceInterpolator<InterpolationMethod>::timeForMax(initials_in_stable, finals_in_stable, motion, m_max_linear_accel, m_max_angular_accel);
    if(minimum_motion_duration > motion.segment_duration)
    {
      ROS_WARN_STREAM(m_node_name << ": Changed segment duration from " << motion.segment_duration << " to " << minimum_motion_duration << " due to Cartesian vel / acc constraints");
    }

    unsigned long num_steps = ceil(minimum_motion_duration * m_plan_frequency);
    double segment_duration = num_steps / m_plan_frequency;

    ROS_DEBUG("%s: Expanded segment into %d substeps", m_node_name.c_str(), (int ) num_steps);

    for(unsigned int i = 0; i < motion.initial_goal_regions.size(); i++)
    {
      boost::this_thread::interruption_point();

      matec_msgs::GoalRegion interpolated_region;
      interpolated_region.goal_frame.header.frame_id = motion.stable_frame;
      interpolated_region.tool_frame = motion.initial_goal_regions.at(i).tool_frame;

      matec_utils::GoalSpaceInterpolator<InterpolationMethod> interpolator;
      interpolator.loadTrajectory(initials_in_stable.at(i), motion.initial_goal_regions.at(i), finals_in_stable.at(i), motion.final_goal_regions.at(i), segment_duration);

      std::vector<matec_msgs::GoalRegion> interpolated_motion(num_steps);
      for(unsigned long j = 0; j < num_steps; j++)
      {
        double interpolated_time = j / m_plan_frequency;
        interpolator.getInterpolatedState(interpolated_time, interpolated_region);
        interpolated_motion.at(j) = interpolated_region;
      }

      interpolated_motions.push_back(interpolated_motion);
    }
  }

  bool ManipulationPlan::solveSegment(std::vector<double> segment_initial_joints, matec_msgs::Odometry segment_initial_odometry, matec_msgs::GoalMotion segment, unsigned int segment_idx, std::vector<std::vector<matec_msgs::GoalRegion> > segment_regions)
  {
    //setup tree
    dynamics_tree::DynamicsTree tree;
    m_graph->spawnDynamicsTree(segment.stable_frame, false, tree);

    //make sure the tree has the relevant tool frames
    for(unsigned int tool = 0; tool < segment_regions.size() && ros::ok(); tool++)
    {
      boost::this_thread::interruption_point();
      std::string nearest_urdf_link;
      matec_utils::Matrix4 nearestTtool;

      if(!getCurrentOffset(segment.initial_goal_regions[tool].tool_frame, nearest_urdf_link, nearestTtool))
      {
        return false;
      }
      if(nearest_urdf_link != segment.initial_goal_regions[tool].tool_frame)
      {
        WTFASSERT(tree.addFixedFrame(nearest_urdf_link, segment.initial_goal_regions[tool].tool_frame, nearestTtool.inverse()));
      }

      WTFASSERT(tree.addFixedFrame(segment.stable_frame, getMobileGoalFrameName(tool), matec_utils::Matrix4::Identity())); //mobile goal frame for every tool, adjusted in solveSlice
    }

    //find out what joints we actually need to control
    std::vector<unsigned int> all_available_supporting_joints;
    for(unsigned int tool = 0; tool < segment_regions.size() && ros::ok(); tool++)
    {
      std::vector<unsigned int> desired_joint_indices, available_supporting_joints;
      for(unsigned int i = 0; i < segment.available_joints.size(); i++)
      {
        boost::this_thread::interruption_point();
        unsigned int matec_idx = std::find(m_joint_names.begin(), m_joint_names.end(), segment.available_joints.at(i)) - m_joint_names.begin();
        if(matec_idx == m_joint_names.size())
        {
          ROS_ERROR("%s: Joint %s not found! Ignoring!", m_node_name.c_str(), segment.available_joints.at(i).c_str());
          continue;
        }
        desired_joint_indices.push_back(matec_idx);
      }

      tree.getSupportingSubset(desired_joint_indices, segment.initial_goal_regions[tool].tool_frame, available_supporting_joints);

      for(unsigned int i = 0; i < available_supporting_joints.size(); i++)
      {
        if(std::find(all_available_supporting_joints.begin(), all_available_supporting_joints.end(), available_supporting_joints.at(i)) == all_available_supporting_joints.end())
        {
          all_available_supporting_joints.push_back(available_supporting_joints.at(i));
          m_joint_active.at(available_supporting_joints.at(i)) = true;
        }
      }
    }

    //solve all time points
    std::vector<double> joint_positions = segment_initial_joints;
    std::vector<double> joint_velocities(segment_initial_joints.size(), 0.0); //start at zero velocity for now
    WTFASSERT(joint_positions.size() == m_joint_names.size());
    int print_mod = std::ceil((double) segment_regions.at(0).size() / 20.0);

    unsigned int canonical_size = segment_regions.at(0).size();
    for(unsigned int i = 0; i < segment_regions.size(); i++) //sanity check that all tools have the same number of time slices
    {
      WTFASSERT(segment_regions.at(i).size() == canonical_size);
    }

    unsigned int segment_start_idx = m_plan.trajectory.points.size();
    unsigned int num_consecutive_failures = 0;
    m_last_good_slices.at(segment_idx) = -1;
    bool segment_success = true;
    for(unsigned int time_slice = 0; time_slice < segment_regions.at(0).size(); time_slice++)
    {
      boost::this_thread::interruption_point();
      if(time_slice % print_mod == 0)
      {
        ROS_INFO_THROTTLE(1.0, "%s: Working on slice %d / %d!", m_node_name.c_str(), (int ) time_slice, (int ) segment_regions.at(0).size());

        m_planning_progress = ((double) segment_idx + (double) time_slice / (double) segment_regions.at(0).size()) / (double) m_goal.segments.size();
        feedback();
      }

      std::vector<matec_msgs::GoalRegion> slice_regions;
      for(unsigned int tool = 0; tool < segment_regions.size(); tool++)
      {
        boost::this_thread::interruption_point();
        slice_regions.push_back(segment_regions.at(tool).at(time_slice)); //todo: make it so we don't have to do this weird inversion
      }

      double slice_time = m_plan_dt; // * (num_consecutive_failures + 1); //time of this slice might be extended if we are making up for past failure

      if(solveSlice(joint_positions, joint_velocities, all_available_supporting_joints, slice_regions, slice_time, tree, joint_positions, joint_velocities))
      {
        double slice_time_dilation = slice_time / (m_plan_dt); // * (num_consecutive_failures + 1));
        if(slice_time_dilation > m_segment_time_dilations.at(segment_idx))
        {
          ROS_DEBUG("%s: Time dilated to %g at segment %d, slice %d", m_node_name.c_str(), slice_time_dilation, (int ) segment_idx, (int ) time_slice);
          m_segment_time_dilations.at(segment_idx) = slice_time_dilation;
        }

        trajectory_msgs::JointTrajectoryPoint point;
        point.positions.resize(m_plan.trajectory.joint_names.size());
        for(unsigned int i = 0; i < m_plan.trajectory.joint_names.size(); i++)
        {
          boost::this_thread::interruption_point();
          unsigned int matec_idx = std::find(m_joint_names.begin(), m_joint_names.end(), m_plan.trajectory.joint_names.at(i)) - m_joint_names.begin();
          point.positions.at(i) = joint_positions.at(matec_idx);
          point.time_from_start = m_plan.trajectory.points.at(m_plan.trajectory.points.size() - 1).time_from_start + ros::Duration(m_plan_dt);
        }

        //interpolate linearly through failures
        if(num_consecutive_failures > 0)
        {
          trajectory_msgs::JointTrajectoryPoint last_good_point = m_plan.trajectory.points.at(m_plan.trajectory.points.size() - 1);
          for(unsigned int i = 1; i <= num_consecutive_failures; i++)
          {
            trajectory_msgs::JointTrajectoryPoint interpolated_point = last_good_point;
            for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
            {
              boost::this_thread::interruption_point();
              interpolated_point.positions.at(j) += (point.positions.at(j) - last_good_point.positions.at(j)) * (double) i / (double) num_consecutive_failures;
            }
            interpolated_point.time_from_start = m_plan.trajectory.points.at(m_plan.trajectory.points.size() - 1).time_from_start + ros::Duration(m_plan_dt);

            m_plan.trajectory.points.push_back(interpolated_point);
          }
          num_consecutive_failures = 0;
        }
        else
        {
          m_plan.trajectory.points.push_back(point);
        }
        m_last_good_slices.at(segment_idx) = time_slice;
      }
      else
      {
        num_consecutive_failures++;

        if(num_consecutive_failures > m_goal.num_acceptable_consecutive_failures)
        {
          ROS_ERROR("%s: Planning failed at time slice %d / %d!", m_node_name.c_str(), (int ) time_slice, (int ) segment_regions.at(0).size());
          segment_success = false;
          break;
        }
        else
        {
          ROS_WARN("%s: Planning failed at time slice %d / %d! Trying the next one (%d failures remaining)!", m_node_name.c_str(), (int ) time_slice, (int ) segment_regions.at(0).size(), (int ) (m_goal.num_acceptable_consecutive_failures - num_consecutive_failures));
        }
      }
    }

    //rescale durations if we ended early
    unsigned int num_valid_segment_slices = m_plan.trajectory.points.size() - segment_start_idx;
    if(!segment_success && num_valid_segment_slices >= 1)
    {
      //need to completely re-profile this segment
      double max_velocity_scale = 1.0; //todo: param
      double min_velocity_scale = 0.1;
      for(unsigned int i = segment_start_idx; i < m_plan.trajectory.points.size(); i++)
      {
        unsigned int slice = i - segment_start_idx;
        double velocity_scale = min_velocity_scale + (max_velocity_scale - min_velocity_scale) * (1.0 - (double) slice / (double) (num_valid_segment_slices));
        double new_duration = m_plan_dt;
        for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
        {
          boost::this_thread::interruption_point();
          //todo: cache matec idx searches
          unsigned int matec_idx = std::find(m_joint_names.begin(), m_joint_names.end(), m_plan.trajectory.joint_names.at(j)) - m_joint_names.begin();
          double delta = fabs(m_plan.trajectory.points.at(i).positions.at(j) - m_plan.trajectory.points.at(i - 1).positions.at(j));
          double max_vel = velocity_scale * m_max_joint_velocities.at(matec_idx);
          new_duration = std::max(new_duration, delta / max_vel);
        }
        m_plan.trajectory.points.at(i).time_from_start = m_plan.trajectory.points.at(i - 1).time_from_start + ros::Duration(new_duration);
      }
    }

    return segment_success;
  }

  void ManipulationPlan::planningThread()
  {
    m_planning_thread_active = true;
    ROS_INFO("%s: Starting \"%s\" plan", m_node_name.c_str(), m_goal.plan_name.c_str());
    m_planning_complete = false;
    m_planning_progress = 0.0;
    m_last_good_slices.resize(m_goal.segments.size(), -1);

    m_plan_regions.clear();
    m_plan_regions.resize(m_goal.segments.size()); //[segment][tool][time]
    m_segment_time_dilations.clear();
    m_segment_time_dilations.resize(m_goal.segments.size(), 1.0);

    //attempt to solve
    bool success = true;
    for(unsigned int i = 0; i < m_goal.segments.size(); i++)
    {
      m_segment_start_indices.push_back(m_plan.trajectory.points.size() - 1);

      ROS_INFO_THROTTLE(1.0, "%s: Working on segment %d/%d", m_node_name.c_str(), (int ) i, (int ) m_goal.segments.size());
      std::vector<double> segment_initial_joints = getJointPositions(m_plan.trajectory.points.size() - 1);
      matec_msgs::Odometry segment_initial_odometry = m_initial_root_link_odom; //todo: simulate forward based on stable frame

      if(m_goal.segments.at(i).interpolation_method == matec_msgs::GoalMotion::LINEAR)
      {
        interpolateSegment<matec_utils::LinearInterpolator>(segment_initial_joints, segment_initial_odometry, m_goal.segments.at(i), m_plan_regions.at(i)); //todo: visualize the remainder of the path even if a segment fails
      }
      else //todo: support other interpolation methods
      {
        interpolateSegment<matec_utils::MinimumJerkInterpolator>(segment_initial_joints, segment_initial_odometry, m_goal.segments.at(i), m_plan_regions.at(i)); //todo: visualize the remainder of the path even if a segment fails
      }

      if(success && !solveSegment(segment_initial_joints, segment_initial_odometry, m_goal.segments.at(i), i, m_plan_regions.at(i)))
      {
        success = false;
      }
    }

    if(m_plan.trajectory.points.size() != 1)
    {
      //clean plan
      for(unsigned int i = 0; i < m_joint_active.size(); i++)
      {
        if(!m_joint_active.at(i)) //joint doesn't move, remove it if it's there
        {
          std::string stationary_joint_name = m_joint_names.at(i);
          unsigned int stationary_joint_idx = std::find(m_plan.trajectory.joint_names.begin(), m_plan.trajectory.joint_names.end(), stationary_joint_name) - m_plan.trajectory.joint_names.begin();
          if(stationary_joint_idx != m_plan.trajectory.joint_names.size())
          {
            //joint doesn't move but is in the plan
            m_plan.trajectory.joint_names.erase(m_plan.trajectory.joint_names.begin() + stationary_joint_idx);
            for(unsigned int j = 0; j < m_plan.trajectory.points.size(); j++)
            {
              boost::this_thread::interruption_point();
              m_plan.trajectory.points.at(j).positions.erase(m_plan.trajectory.points.at(j).positions.begin() + stationary_joint_idx);
            }
          }
        }
      }

      //smooth plan
      ROS_INFO("%s: Smoothing plan", m_node_name.c_str());
//    //look through plan, if n and n+2 are closer than n and n+1, n+1 <= lerp(n,n+2)
//    int smooth_count = 0;
//    for(unsigned int i = 1; i < m_plan.trajectory.points.size() - 1; i++)
//    {
//      bool replace = true;
//      for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
//      {
//        double dist1 = m_plan.trajectory.points.at(i - 1).positions.at(j) - m_plan.trajectory.points.at(i).positions.at(j);
//        double dist2 = m_plan.trajectory.points.at(i - 1).positions.at(j) - m_plan.trajectory.points.at(i + 1).positions.at(j);
//        if(fabs(dist1) < fabs(dist2))
//        {
//          replace = false;
//          break;
//        }
//      }
//
//      if(replace)
//      {
//        for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
//        {
//          m_plan.trajectory.points.at(i).positions.at(j) = (m_plan.trajectory.points.at(i - 1).positions.at(j) + m_plan.trajectory.points.at(i + 1).positions.at(j)) / 2.0;
//        }
//        smooth_count++;
//      }
//    }

      //low-pass filter
      double filter_frequency = 5;
      double max_allowable_filtration_error = 1e-8;

      //initialize filters
      std::vector<Biquad> lpfs(m_plan.trajectory.joint_names.size(), Biquad(bq_type_lowpass, filter_frequency / m_plan_frequency, 0.7071, 6));
      while(ros::ok())
      {
        bool all_ok = true;
        for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
        {
          boost::this_thread::interruption_point();
          double filtered_pos = lpfs.at(j).process(m_plan.trajectory.points.at(0).positions.at(j));
          if(fabs(m_plan.trajectory.points.at(0).positions.at(j) - filtered_pos) > max_allowable_filtration_error)
          {
            all_ok = false;
          }
        }
        if(all_ok)
        {
          break;
        }
      }

      //filter
      for(unsigned int i = 1; i < m_plan.trajectory.points.size() - 1; i++)
      {
        for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
        {
          boost::this_thread::interruption_point();
          m_plan.trajectory.points.at(i).positions.at(j) = lpfs.at(j).process(m_plan.trajectory.points.at(i).positions.at(j));
        }
      }

      //final adjustment
      std::vector<double> last_positions = m_plan.trajectory.points.at(m_plan.trajectory.points.size() - 1).positions;
      m_plan.trajectory.points.pop_back();
      while(ros::ok())
      {
        bool all_ok = true;
        std::vector<double> filtered_positions(last_positions.size());
        for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
        {
          boost::this_thread::interruption_point();
          filtered_positions.at(j) = lpfs.at(j).process(last_positions.at(j));
          if(fabs(last_positions.at(j) - filtered_positions.at(j)) > max_allowable_filtration_error)
          {
            all_ok = false;
          }
        }
        if(all_ok)
        {
          break;
        }
        else
        {
          trajectory_msgs::JointTrajectoryPoint point;
          point.positions = filtered_positions;
          point.time_from_start = m_plan.trajectory.points.at(m_plan.trajectory.points.size() - 1).time_from_start + ros::Duration(m_plan_dt);
          m_plan.trajectory.points.push_back(point);
        }
      }

      //todo: add checks for goal-space validity
      //todo: target high vel/acc points

      //re-evaluate time
//    double scaled_dt = m_plan_dt;
//    for(unsigned int i = 1; i < m_plan.trajectory.points.size(); i++)
//    {
//      for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
//      {
//        unsigned int matec_idx = std::find(m_joint_names.begin(), m_joint_names.end(), m_plan.trajectory.joint_names[j]) - m_joint_names.begin();
//        scaled_dt = std::max(scaled_dt, (m_plan.trajectory.points.at(i).positions.at(j) - m_plan.trajectory.points.at(i - 1).positions.at(j)) / m_max_joint_velocities[matec_idx]);
//        //todo: accel constraints?
//      }
//    }
//
//    for(unsigned int i = 1; i < m_plan.trajectory.points.size(); i++)
//    {
//      m_plan.trajectory.points.at(i).time_from_start = m_plan.trajectory.points.at(i-1).time_from_start + ros::Duration(scaled_dt);
//    }

      //generate vel/acc
      boost::this_thread::interruption_point();
      m_plan.trajectory.points.at(0).velocities.resize(m_plan.trajectory.joint_names.size(), 0.0);
      m_plan.trajectory.points.at(0).accelerations.resize(m_plan.trajectory.joint_names.size(), 0.0);
      for(unsigned int i = 1; i < m_plan.trajectory.points.size(); i++)
      {
        m_plan.trajectory.points.at(i).velocities.resize(m_plan.trajectory.joint_names.size(), 0.0);
        m_plan.trajectory.points.at(i).accelerations.resize(m_plan.trajectory.joint_names.size(), 0.0);
        double dt = (m_plan.trajectory.points.at(i).time_from_start - m_plan.trajectory.points.at(i - 1).time_from_start).toSec();
        for(unsigned int j = 0; j < m_plan.trajectory.joint_names.size(); j++)
        {
          boost::this_thread::interruption_point();
          double dp = (m_plan.trajectory.points.at(i).positions.at(j) - m_plan.trajectory.points.at(i - 1).positions.at(j));
          m_plan.trajectory.points.at(i).velocities.at(j) = dp / dt;
          double dv = (m_plan.trajectory.points.at(i).velocities.at(j) - m_plan.trajectory.points.at(i - 1).velocities.at(j));
          m_plan.trajectory.points.at(i).accelerations.at(j) = dv / dt;
        }
      }
    }

    if(success || m_goal.allow_incomplete_planning)
    {
      ROS_INFO("%s: Planning complete!", m_node_name.c_str());
      m_planning_progress = 1.0;
    }
    else
    {
      ROS_ERROR("%s: Planning failed!", m_node_name.c_str());
    }

    m_planning_complete = true;
    feedback();

    if(m_goal.visualize_on_plan)
    {
//      matec_msgs::VisualizeManipulationPlan srv;
//      srv.request.plan_names.push_back(m_goal.plan_name);
//      srv.request.plan_visualization_density = m_goal.plan_visualization_density;
      m_in_callback = true;
      boost::this_thread::interruption_point();
      m_self_visualize_callback(m_goal.plan_name, m_goal.plan_visualization_density);
//      ros::service::call("~visualize", srv);
      boost::this_thread::interruption_point();
      m_in_callback = false;
    }

    if(success && m_goal.execute_on_plan)
    {
      m_in_callback = true;
//      matec_msgs::ExecuteManipulationPlan srv;
//      srv.request.plan_names.push_back(m_goal.plan_name);
      boost::this_thread::interruption_point();
      m_self_execute_callback(m_goal.plan_name);
//      ros::service::call("~execute", srv);
      boost::this_thread::interruption_point();
      m_in_callback = false;
    }
  }

  bool ManipulationPlan::plan(std::vector<double> current_positions, std::vector<double> last_commanded_positions, matec_msgs::Odometry current_root_link_odom, double plan_frequency, unsigned int max_twist_attempts, unsigned int max_same_slice_attempts)
  {
    ROS_INFO("%s: Initializing \"%s\" plan", m_node_name.c_str(), m_goal.plan_name.c_str());
    m_plan_frequency = plan_frequency;
    m_plan_dt = 1.0 / m_plan_frequency;
    m_max_twist_attempts = max_twist_attempts;
    m_max_same_slice_attempts = max_same_slice_attempts;

    if(m_goal.initial_joint_positions.size() != current_positions.size())
    {
      m_current_positions = current_positions;
      m_last_commanded_positions = last_commanded_positions;
    }
    else
    {
      m_current_positions = m_goal.initial_joint_positions;
      m_last_commanded_positions = m_goal.initial_joint_positions;
    }

    //set up initial root link odometry
    if(m_goal.initial_pose_link.size() == 0)
    {
      m_initial_root_link_odom = current_root_link_odom;
      m_odom_link = m_urdf_model->getRoot()->name;
    }
    else
    {
      m_initial_root_link_odom.header = m_goal.initial_pose.header;
      m_initial_root_link_odom.pose = m_goal.initial_pose.pose;
      m_odom_link = m_goal.initial_pose_link;
    }
    m_initial_root_link_odom.velocity.linear.x = 0;
    m_initial_root_link_odom.velocity.linear.y = 0;
    m_initial_root_link_odom.velocity.linear.z = 0;
    m_initial_root_link_odom.velocity.angular.x = 0;
    m_initial_root_link_odom.velocity.angular.y = 0;
    m_initial_root_link_odom.velocity.angular.z = 0;
    m_initial_root_link_odom.acceleration.linear.x = 0;
    m_initial_root_link_odom.acceleration.linear.y = 0;
    m_initial_root_link_odom.acceleration.linear.z = 0;
    m_initial_root_link_odom.acceleration.angular.x = 0;
    m_initial_root_link_odom.acceleration.angular.y = 0;
    m_initial_root_link_odom.acceleration.angular.z = 0;

    if(!initializePlan())
    {
      return false;
    }

//    if(m_planning_thread)
//    {
//      pthread_cancel(m_planning_thread->native_handle());
//      m_planning_thread->detach();
//    }
    m_planning_thread.reset(new boost::thread(boost::bind(&ManipulationPlan::planningThread, this)));

    return true;
  }

  bool ManipulationPlan::initializePlan()
  {
    //clear out old plan
    m_plan.trajectory.joint_names.clear();
    m_plan.trajectory.points.clear();
    m_joint_active.clear();
    m_joint_active.resize(m_joint_names.size(), false);

    //gather the superset of joint names
    std::set<std::string> plan_joint_names;
    for(unsigned int i = 0; i < m_goal.segments.size() && ros::ok(); i++)
    {
      for(unsigned int j = 0; j < m_goal.segments[i].available_joints.size() && ros::ok(); j++)
      {
        if(std::find(m_joint_names.begin(), m_joint_names.end(), m_goal.segments[i].available_joints.at(j)) == m_joint_names.end())
        {
          ROS_ERROR("%s: Joint %s not found! Ignoring!", m_node_name.c_str(), m_goal.segments[i].available_joints.at(j).c_str());
          continue;
        }
        plan_joint_names.insert(m_goal.segments[i].available_joints.at(j));
      }
    }
    std::copy(plan_joint_names.begin(), plan_joint_names.end(), std::back_inserter(m_plan.trajectory.joint_names));

    //get initial joint configuration for all relevant joints
    trajectory_msgs::JointTrajectoryPoint initial_point;
    std::vector<unsigned int> plan_idx_to_matec;
    for(unsigned int i = 0; i < m_plan.trajectory.joint_names.size() && ros::ok(); i++)
    {
      unsigned int matec_idx = std::find(m_joint_names.begin(), m_joint_names.end(), m_plan.trajectory.joint_names[i]) - m_joint_names.begin();
      if(matec_idx == m_joint_names.size())
      {
        ROS_ERROR("%s: Couldn't find joint name %s in matec!", m_node_name.c_str(), m_plan.trajectory.joint_names[i].c_str());
        return false;
      }

      initial_point.positions.push_back(m_last_commanded_positions.at(matec_idx));
      plan_idx_to_matec.push_back(matec_idx);
    }
    initial_point.time_from_start = ros::Duration(0);
    m_plan.trajectory.points.push_back(initial_point);

    return true;
  }
}
