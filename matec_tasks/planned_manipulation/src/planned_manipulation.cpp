#include "planned_manipulation/planned_manipulation.h"

namespace planned_manipulation
{
  PlannedManipulation::PlannedManipulation(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "server", boost::bind(&PlannedManipulation::goalCallback, this, _1), boost::bind(&PlannedManipulation::cancelCallback, this, _1), false)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("plan_output_frequency", m_plan_output_frequency, 250.0);
    m_nh.param("max_twist_attempts", m_max_twist_attempts, 10);
    m_nh.param("max_same_slice_attempts", m_max_same_slice_attempts, 25);
    m_plan_dt = 1.0 / m_plan_output_frequency;

    m_tf_listener.reset(new tf::TransformListener(ros::Duration(30.0)));

    matec_utils::blockOnROSJointNames(m_joint_names);

    while(!m_nh.hasParam("/robot_description"))
    {
      ROS_WARN_THROTTLE(1.0, "%s waiting for robot description", m_nh.getNamespace().c_str());
    }
    m_urdf_model.reset(new urdf::Model());
    m_urdf_model->initParam("/robot_description");
    m_graph.reset(new dynamics_tree::DynamicsGraph());
    m_graph->loadFromParameterServer(m_joint_names, false);

    m_have_joint_command = false;
    m_have_odometry = false;
    m_joint_command_sub = m_nh.subscribe("/smi/joint_commands", 2, &PlannedManipulation::jointCommandCallback, this);
    m_joint_states_sub = m_nh.subscribe("/smi/joint_states", 2, &PlannedManipulation::jointStatesCallback, this);
    m_odometry_sub = m_nh.subscribe("/smi/root_link_odom", 2, &PlannedManipulation::odometryCallback, this);

    m_goal_regions_visualization_pub = m_nh.advertise<visualization_msgs::MarkerArray>("plan_regions_visual", 1, true);
    m_plan_visualization_pub = m_nh.advertise<control_msgs::FollowJointTrajectoryGoal>("plan_visual", 1, true);

    std::string joint_server_name;
    m_nh.param("server_name", joint_server_name, std::string("/base_joint_interpolator/server"));
    m_client = new Client(m_nh, joint_server_name, true);

    while(!m_client->isServerConnected() && ros::ok())
    {
      ROS_WARN_THROTTLE(2.0, "%s: Waiting for the %s actionlib server to start...", m_nh.getNamespace().c_str(), joint_server_name.c_str());
      ros::spinOnce();
    }
    m_server.start();

    m_get_srv = m_nh.advertiseService("get_plan", &PlannedManipulation::getCallback, this);
    m_execute_srv = m_nh.advertiseService("execute", &PlannedManipulation::executeCallback, this);
    m_visualize_srv = m_nh.advertiseService("visualize", &PlannedManipulation::visualizeCallback, this);
  }

  PlannedManipulation::~PlannedManipulation()
  {

  }

  void PlannedManipulation::jointCommandCallback(const matec_msgs::FullJointStates::ConstPtr msg)
  {
    m_last_joint_command = *msg;
    m_have_joint_command = true;
  }

  void PlannedManipulation::jointStatesCallback(const matec_msgs::FullJointStates::ConstPtr msg)
  {
    m_joint_states = *msg;
    m_have_joint_states = true;
  }

  void PlannedManipulation::odometryCallback(const matec_msgs::Odometry::ConstPtr msg)
  {
    m_latest_odometry = *msg;
    m_have_odometry = true;
  }

  bool PlannedManipulation::mergePlansParallel(bool assume_synched, control_msgs::FollowJointTrajectoryGoal other_plan, control_msgs::FollowJointTrajectoryGoal& plan)
  {
    if(plan.trajectory.joint_names.size() == 0 && plan.trajectory.points.size() == 0)
    {
      //empty plan, just copy the other plan in
      plan = other_plan;
      return true;
    }

    //check compatibility and load new joint names
    std::vector<unsigned int> other_plan_index_map;
    for(unsigned int i = 0; i < other_plan.trajectory.joint_names.size(); i++)
    {
      if(std::find(plan.trajectory.joint_names.begin(), plan.trajectory.joint_names.end(), other_plan.trajectory.joint_names.at(i)) != plan.trajectory.joint_names.end())
      {
        //overlapping joints
        return false;
      }
      else
      {
        other_plan_index_map.push_back(plan.trajectory.joint_names.size());
        plan.trajectory.joint_names.push_back(other_plan.trajectory.joint_names.at(i));
      }
    }

    if(assume_synched)
    {
      //merge plans, assuming time steps are the same
      unsigned int new_length = std::max(plan.trajectory.points.size(), other_plan.trajectory.points.size());
      plan.trajectory.points.resize(new_length, plan.trajectory.points.at(plan.trajectory.points.size() - 1)); //extend the trajectory if necessary, keeping the last joint configuration constant
      for(unsigned int i = 0; i < new_length; i++)
      {
        if(i < other_plan.trajectory.points.size())
        {
          plan.trajectory.points.at(i).positions.insert(plan.trajectory.points.at(i).positions.end(), other_plan.trajectory.points.at(i).positions.begin(), other_plan.trajectory.points.at(i).positions.end());
        }
        else
        {
          plan.trajectory.points.at(i).positions.insert(plan.trajectory.points.at(i).positions.end(), other_plan.trajectory.points.at(other_plan.trajectory.points.size() - 1).positions.begin(), other_plan.trajectory.points.at(other_plan.trajectory.points.size() - 1).positions.end());
        }
      }

      //fix time stamps
      plan.trajectory.points.at(0).time_from_start = ros::Duration(0);
      for(unsigned int i = 1; i < plan.trajectory.points.size(); i++)
      {
        plan.trajectory.points.at(i).time_from_start = plan.trajectory.points.at(i - 1).time_from_start + ros::Duration(m_plan_dt);
      }
    }
    else
    {
      unsigned int plan_idx = 1;
      unsigned int other_plan_idx = 1;
      control_msgs::FollowJointTrajectoryGoal new_plan;
      new_plan.trajectory.header = plan.trajectory.header;
      new_plan.trajectory.joint_names = plan.trajectory.joint_names;

      //set up first point
      trajectory_msgs::JointTrajectoryPoint new_point;
      new_point.positions = plan.trajectory.points.at(0).positions;
      new_point.positions.insert(new_point.positions.end(), other_plan.trajectory.points.at(0).positions.begin(), other_plan.trajectory.points.at(0).positions.end());
      new_point.time_from_start = ros::Duration(0);
      new_plan.trajectory.points.push_back(new_point);

      //merge, assuming both plans are sorted by time
      while(ros::ok() && plan_idx < plan.trajectory.points.size() && other_plan_idx < other_plan.trajectory.points.size())
      {
        if(plan.trajectory.points.at(plan_idx).time_from_start < other_plan.trajectory.points.at(other_plan_idx).time_from_start)
        {
          for(unsigned int i = 0; i < plan.trajectory.points.at(plan_idx).positions.size(); i++)
          {
            new_point.positions.at(i) = plan.trajectory.points.at(plan_idx).positions.at(i);
          }
          new_point.time_from_start = plan.trajectory.points.at(plan_idx).time_from_start;
          plan_idx++;
        }
        else
        {
          for(unsigned int i = 0; i < other_plan.trajectory.points.at(other_plan_idx).positions.size(); i++)
          {
            new_point.positions.at(other_plan_index_map.at(i)) = other_plan.trajectory.points.at(other_plan_idx).positions.at(i);
          }
          new_point.time_from_start = other_plan.trajectory.points.at(other_plan_idx).time_from_start;
          other_plan_idx++;
        }
        new_plan.trajectory.points.push_back(new_point);
      }

      //copy in the last bits if necessary
      for(; plan_idx < plan.trajectory.points.size(); plan_idx++)
      {
        for(unsigned int i = 0; i < plan.trajectory.points.at(plan_idx).positions.size(); i++)
        {
          new_point.positions.at(i) = plan.trajectory.points.at(plan_idx).positions.at(i);
        }
        new_point.time_from_start = plan.trajectory.points.at(plan_idx).time_from_start;
        new_plan.trajectory.points.push_back(new_point);
      }

      for(; other_plan_idx < other_plan.trajectory.points.size(); other_plan_idx++)
      {
        for(unsigned int i = 0; i < other_plan.trajectory.points.at(other_plan_idx).positions.size(); i++)
        {
          new_point.positions.at(other_plan_index_map.at(i)) = other_plan.trajectory.points.at(other_plan_idx).positions.at(i);
        }
        new_point.time_from_start = other_plan.trajectory.points.at(other_plan_idx).time_from_start;
        new_plan.trajectory.points.push_back(new_point);
      }
    }

    return true;
  }

  bool PlannedManipulation::mergePlansSeries(bool assume_synched, control_msgs::FollowJointTrajectoryGoal other_plan, control_msgs::FollowJointTrajectoryGoal& plan)
  {
    if(plan.trajectory.joint_names.size() == 0 && plan.trajectory.points.size() == 0)
    {
      //empty plan, just copy the other plan in
      plan = other_plan;
      return true;
    }

    //find the superset of joint names
    std::vector<bool> joint_already_in_plan;
    std::vector<unsigned int> other_plan_index_map;
    for(unsigned int i = 0; i < other_plan.trajectory.joint_names.size(); i++)
    {
      unsigned int idx = std::find(plan.trajectory.joint_names.begin(), plan.trajectory.joint_names.end(), other_plan.trajectory.joint_names.at(i)) - plan.trajectory.joint_names.begin();
      if(idx == plan.trajectory.joint_names.size())
      {
        plan.trajectory.joint_names.push_back(other_plan.trajectory.joint_names.at(i));
        joint_already_in_plan.push_back(false);
      }
      else
      {
        joint_already_in_plan.push_back(true);
      }
      other_plan_index_map.push_back(idx);
    }

    //merge plans
    //fill any any missing joint data in the original plan
    unsigned int original_size = plan.trajectory.points.size();
    for(unsigned int i = 0; i < plan.trajectory.points.size(); i++)
    {
      plan.trajectory.points.at(i).positions.resize(plan.trajectory.joint_names.size());
      for(unsigned int j = 0; j < other_plan_index_map.size(); j++)
      {
        if(!joint_already_in_plan.at(j)) //need to extend with other_plan's first set of positions
        {
          plan.trajectory.points.at(i).positions.at(other_plan_index_map.at(j)) = other_plan.trajectory.points.at(0).positions.at(j);
        }
      }
    }

    //extend the trajectory, keeping the last joint configuration constant
    unsigned int new_length = plan.trajectory.points.size() + other_plan.trajectory.points.size();
    plan.trajectory.points.resize(new_length, plan.trajectory.points.at(plan.trajectory.points.size() - 1));

    //fill in the real data for the new segment
    for(unsigned int i = original_size; i < plan.trajectory.points.size(); i++)
    {
      for(unsigned int j = 0; j < other_plan_index_map.size(); j++)
      {
        plan.trajectory.points.at(i).positions.at(other_plan_index_map.at(j)) = other_plan.trajectory.points.at(i - original_size).positions.at(j);
      }
      plan.trajectory.points.at(i).time_from_start += other_plan.trajectory.points.at(i - original_size).time_from_start;
    }

    return true;
  }

  bool PlannedManipulation::executeCallback(matec_msgs::ExecuteManipulationPlan::Request &req, matec_msgs::ExecuteManipulationPlan::Response &res)
  {
    control_msgs::FollowJointTrajectoryGoal full_plan, individual_plan;
    for(unsigned int i = 0; i < req.plan_names.size(); i++)
    {
      if(m_plans.find(req.plan_names.at(i)) == m_plans.end())
      {
        ROS_ERROR("%s: Tried to use a non-existant plan %s!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
        return false;
      }
      if(!m_plans[req.plan_names.at(i)]->ready())
      {
        ROS_ERROR("%s: Tried to use plan %s that wasn't done planning!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
        return false;
      }
      std::vector<int> segment_start_indices;
      m_plans[req.plan_names.at(i)]->getPlan(individual_plan, segment_start_indices);

      if(req.plans_in_parallel)
      {
        if(!mergePlansParallel(true, individual_plan, full_plan))
        {
          ROS_ERROR("%s: Plan %s was not compatible with the other plans!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
          return false;
        }
      }
      else
      {
        if(!mergePlansSeries(true, individual_plan, full_plan))
        {
          ROS_ERROR("%s: Plan %s was not compatible with the other plans!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
          return false;
        }
      }
    }

    //execute
    m_active_plan_names = req.plan_names;
    m_active_plan_goal = full_plan;
    ROS_INFO("%s: Executing plan!", m_nh.getNamespace().c_str());
    m_client->cancelAllGoals();
    m_client->sendGoal(full_plan, boost::bind(&PlannedManipulation::doneCallback, this, _1, _2), Client::SimpleActiveCallback(), boost::bind(&PlannedManipulation::feedbackCallback, this, _1));

    return true;
  }

  bool PlannedManipulation::getCallback(matec_msgs::GetManipulationPlan::Request &req, matec_msgs::GetManipulationPlan::Response &res)
  {
    control_msgs::FollowJointTrajectoryGoal full_plan, individual_plan;
    if(m_plans.find(req.plan_name) == m_plans.end())
    {
      ROS_ERROR("%s: Tried to use a non-existant plan %s!", m_nh.getNamespace().c_str(), req.plan_name.c_str());
      return false;
    }
    if(!m_plans[req.plan_name]->ready())
    {
      ROS_ERROR("%s: Tried to use plan %s that wasn't done planning!", m_nh.getNamespace().c_str(), req.plan_name.c_str());
      return false;
    }
    m_plans[req.plan_name]->getPlan(res.plan, res.segment_start_indices);

    return true;
  }

  void PlannedManipulation::selfVisualizeCallback(std::string plan_name, double visualization_density)
  {
    matec_msgs::VisualizeManipulationPlan srv;
    srv.request.plan_names.push_back(plan_name);
    srv.request.plan_visualization_density = visualization_density;
    visualizeCallback(srv.request, srv.response);
  }

  void PlannedManipulation::selfExecuteCallback(std::string plan_name)
  {
    matec_msgs::ExecuteManipulationPlan srv;
    srv.request.plan_names.push_back(plan_name);
    executeCallback(srv.request, srv.response);
  }

  void PlannedManipulation::feedbackCallback(const control_msgs::FollowJointTrajectoryFeedbackConstPtr &feedback)
  {
    double full_plan_time = m_active_plan_goal.trajectory.points.at(m_active_plan_goal.trajectory.points.size() - 1).time_from_start.toSec();
    double execution_progress = feedback->header.stamp.toSec() / full_plan_time;
    for(unsigned int i = 0; i < m_active_plan_names.size(); i++)
    {
      m_plans[m_active_plan_names.at(i)]->feedback(execution_progress);
    }
  }

  void PlannedManipulation::doneCallback(const actionlib::SimpleClientGoalState &state, const control_msgs::FollowJointTrajectoryResultConstPtr &result)
  {
    for(unsigned int i = 0; i < m_active_plan_names.size(); i++)
    {
      if((state == actionlib::SimpleClientGoalState::SUCCEEDED))
      {
        m_plans[m_active_plan_names.at(i)]->finish(true);
      }
      else
      {
        m_plans[m_active_plan_names.at(i)]->finish(false);
      }

      m_plans.erase(m_active_plan_names.at(i));
    }
    m_active_plan_names.clear();
  }

  bool PlannedManipulation::visualizeCallback(matec_msgs::VisualizeManipulationPlan::Request &req, matec_msgs::VisualizeManipulationPlan::Response &res)
  {
    visualization_msgs::MarkerArray full_regions, individual_regions;
    control_msgs::FollowJointTrajectoryGoal full_plan, individual_plan;
    std::vector<matec_msgs::Prophecy> full_prophecies, individual_prophecies;
    for(unsigned int i = 0; i < req.plan_names.size(); i++)
    {
      if(m_plans.find(req.plan_names.at(i)) == m_plans.end())
      {
        ROS_ERROR("%s: Tried to use a non-existant plan %s!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
        return false;
      }
      if(!m_plans[req.plan_names.at(i)]->ready())
      {
        ROS_ERROR("%s: Tried to use plan %s that wasn't done planning!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
        return false;
      }
      m_plans[req.plan_names.at(i)]->getVisualizations(req.plan_visualization_density, individual_regions, individual_plan, individual_prophecies);

      //just merge the markers directly since they aren't ordered
      full_regions.markers.insert(full_regions.markers.end(), individual_regions.markers.begin(), individual_regions.markers.end());

      if(req.plans_in_parallel)
      {
        if(!mergePlansParallel(false, individual_plan, full_plan))
        {
          ROS_ERROR("%s: Plan %s was not compatible with the other plans!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
          return false;
        }
      }
      else
      {
        if(!mergePlansSeries(false, individual_plan, full_plan))
        {
          ROS_ERROR("%s: Plan %s was not compatible with the other plans!", m_nh.getNamespace().c_str(), req.plan_names.at(i).c_str());
          return false;
        }
      }
    }
    //todo: merge prophecies appropriately
    full_prophecies = individual_prophecies;

    //todo: add initial configuration prophecies

//    geometry_msgs::PoseStamped zero_pose_ground, initial_pose_global;
//    zero_pose_ground.header.stamp = ros::Time(0);
//    zero_pose_ground.header.frame_id = m_ground_frame;
//    zero_pose_ground.pose.orientation.w = 1.0;
//    m_tf_listener->transformPose(m_global_frame, zero_pose_ground, initial_pose_global);
//
//    geometry_msgs::PoseStamped final_pose_global;
//    m_tf_listener->transformPose(m_global_frame, pm_goal.initial_pose, final_pose_global);
//    new_goal = pm_goal;
//    new_goal.initial_pose = final_pose_global;
//
//    matec_msgs::Prophecy start_prophecy, move_prophecy, manipulate_prophecy;
//    start_prophecy.header.stamp = ros::Time(0);
//    start_prophecy.root_link_pose = initial_pose_global;
//    start_prophecy.joint_positions = m_standard_initial_configuration;
//
//    double travel_x = initial_pose_global.pose.position.x - final_pose_global.pose.position.x;
//    double travel_y = initial_pose_global.pose.position.y - final_pose_global.pose.position.y;
//    double travel_time = sqrt(travel_x * travel_x + travel_y * travel_y) / m_visualization_locomotion_velocity;
//    move_prophecy = start_prophecy;
//    move_prophecy.header.stamp = ros::Time(travel_time);
//    move_prophecy.root_link_pose = final_pose_global;

    //clear old visualizations
    for(unsigned int i = 0; i < m_last_goal_region_visualization.markers.size(); i++)
    {
      m_last_goal_region_visualization.markers.at(i).action = visualization_msgs::Marker::DELETE;
    }
    m_goal_regions_visualization_pub.publish(m_last_goal_region_visualization);

    //publish new ones
    m_plan_visualization_pub.publish(full_plan);
    m_goal_regions_visualization_pub.publish(full_regions);
    m_last_goal_region_visualization = full_regions;

    res.goal_regions = full_regions;
    res.joint_trajectories = full_plan;

    //invoke prophecy
    if(ros::service::exists("/prophet/add_prophecies", false))
    {
      std_srvs::Empty erase_prophecies;
      ros::service::call("/prophet/erase_prophecies", erase_prophecies);

      matec_msgs::AddProphecies add_prophecies;
      add_prophecies.request.prophecies = full_prophecies;
      ros::service::call("/prophet/add_prophecies", add_prophecies);
    }

    return true;
  }

  inline bool validComponent(matec_msgs::ValueRange range)
  {
    return (range.min < range.max);
  }

  void PlannedManipulation::goalCallback(Server::GoalHandle goal_handle)
  {
    ROS_INFO("%s: Got planned manipulation goal with %d segments", m_nh.getNamespace().c_str(), (int ) goal_handle.getGoal()->segments.size());

    if(!m_have_joint_command || !m_have_joint_states)
    {
      ROS_ERROR("%s: Rejecting goal because we haven't received joint positions yet!", m_nh.getNamespace().c_str());
      goal_handle.setRejected();
      return;
    }

    if(!m_have_odometry)
    {
      ROS_ERROR("%s: Rejecting goal because we haven't received odometry yet!", m_nh.getNamespace().c_str());
      goal_handle.setRejected();
      return;
    }

    if(goal_handle.getGoal()->initial_pose_link.size() != 0)
    {
      if(!m_urdf_model->getLink(goal_handle.getGoal()->initial_pose_link))
      {
        ROS_ERROR("%s: Rejecting goal because initial_pose_link %s was not in the urdf!", m_nh.getNamespace().c_str(), goal_handle.getGoal()->initial_pose_link.c_str());
        goal_handle.setRejected();
        return;
      }
      if(goal_handle.getGoal()->initial_pose.pose.orientation.x == 0.0 && goal_handle.getGoal()->initial_pose.pose.orientation.y == 0.0 && goal_handle.getGoal()->initial_pose.pose.orientation.z == 0.0 && goal_handle.getGoal()->initial_pose.pose.orientation.w == 0.0)
      {
        ROS_ERROR("%s: Rejecting goal because initial_pose's quatnernion was bad!", m_nh.getNamespace().c_str());
        goal_handle.setRejected();
        return;
      }
    }

    if(goal_handle.getGoal()->initial_joint_positions.size() != 0 && goal_handle.getGoal()->initial_joint_positions.size() != m_joint_names.size())
    {
      ROS_ERROR("%s: Rejecting goal because initial_joint_positions is the wrong size!", m_nh.getNamespace().c_str());
      goal_handle.setRejected();
      return;
    }

    for(unsigned int i = 0; i < goal_handle.getGoal()->segments.size(); i++)
    {
      if(goal_handle.getGoal()->segments[i].initial_goal_regions.size() != goal_handle.getGoal()->segments[i].final_goal_regions.size())
      {
        ROS_ERROR("%s: Rejecting goal due to mismatch in initial / target regions!", m_nh.getNamespace().c_str());
        goal_handle.setRejected();
        return;
      }

      if(goal_handle.getGoal()->segments[i].interpolation_method != matec_msgs::GoalMotion::MINIMUM_JERK && goal_handle.getGoal()->segments[i].interpolation_method != matec_msgs::GoalMotion::LINEAR && goal_handle.getGoal()->segments[i].interpolation_method != matec_msgs::GoalMotion::DIRECT)
      {
        ROS_ERROR("%s: Rejecting goal due to invalid interpolation method!", m_nh.getNamespace().c_str());
        goal_handle.setRejected();
        return;
      }

      for(unsigned int j = 0; j < goal_handle.getGoal()->segments[i].initial_goal_regions.size(); j++)
      {
        if(!validComponent(goal_handle.getGoal()->segments[i].initial_goal_regions[j].x) || !validComponent(goal_handle.getGoal()->segments[i].initial_goal_regions[j].y) || !validComponent(goal_handle.getGoal()->segments[i].initial_goal_regions[j].z) || !validComponent(goal_handle.getGoal()->segments[i].initial_goal_regions[j].R) || !validComponent(goal_handle.getGoal()->segments[i].initial_goal_regions[j].P) || !validComponent(goal_handle.getGoal()->segments[i].initial_goal_regions[j].Y))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid goal region specification!", m_nh.getNamespace().c_str());
          goal_handle.setRejected();
          return;
        }

        if(!m_tf_listener->frameExists(goal_handle.getGoal()->segments[i].initial_goal_regions[j].goal_frame.header.frame_id))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid goal space frame!", m_nh.getNamespace().c_str());
          goal_handle.setRejected();
          return;
        }

        if(!m_tf_listener->frameExists(goal_handle.getGoal()->segments[i].initial_goal_regions[j].tool_frame))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid tool frame!", m_nh.getNamespace().c_str());
          goal_handle.setRejected();
          return;
        }

        if(!m_tf_listener->canTransform(goal_handle.getGoal()->segments[i].initial_goal_regions[j].goal_frame.header.frame_id, goal_handle.getGoal()->segments[i].final_goal_regions[j].goal_frame.header.frame_id, ros::Time(0)))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid transform between goal frames %s and %s!", m_nh.getNamespace().c_str(), goal_handle.getGoal()->segments[i].initial_goal_regions[j].goal_frame.header.frame_id.c_str(), goal_handle.getGoal()->segments[i].final_goal_regions[j].goal_frame.header.frame_id.c_str());
          goal_handle.setRejected();
          return;
        }

        if(!validComponent(goal_handle.getGoal()->segments[i].final_goal_regions[j].x) || !validComponent(goal_handle.getGoal()->segments[i].final_goal_regions[j].y) || !validComponent(goal_handle.getGoal()->segments[i].final_goal_regions[j].z) || !validComponent(goal_handle.getGoal()->segments[i].final_goal_regions[j].R) || !validComponent(goal_handle.getGoal()->segments[i].final_goal_regions[j].P) || !validComponent(goal_handle.getGoal()->segments[i].final_goal_regions[j].Y))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid goal region specification!", m_nh.getNamespace().c_str());
          goal_handle.setRejected();
          return;
        }

        if(!m_tf_listener->frameExists(goal_handle.getGoal()->segments[i].final_goal_regions[j].goal_frame.header.frame_id))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid goal space frame!", m_nh.getNamespace().c_str());
          goal_handle.setRejected();
          return;
        }

        if(!m_tf_listener->frameExists(goal_handle.getGoal()->segments[i].final_goal_regions[j].tool_frame))
        {
          ROS_ERROR("%s: Rejecting goal due to invalid tool frame!", m_nh.getNamespace().c_str());
          goal_handle.setRejected();
          return;
        }
      }

      if(!m_tf_listener->frameExists(goal_handle.getGoal()->segments[i].stable_frame))
      {
        ROS_ERROR("%s: Rejecting goal due to invalid stable frame!", m_nh.getNamespace().c_str());
        goal_handle.setRejected();
        return;
      }

      if(goal_handle.getGoal()->segments[i].available_joints.size() == 0 || goal_handle.getGoal()->segments[i].available_joints.size() > m_joint_names.size())
      {
        ROS_ERROR("%s: Rejecting goal due to incorrectly specified available joints!", m_nh.getNamespace().c_str());
        goal_handle.setRejected();
        return;
      }
    }

    if(m_plans.find(goal_handle.getGoal()->plan_name) != m_plans.end())
    {
      m_plans[goal_handle.getGoal()->plan_name]->cancel();
    }
    m_plans[goal_handle.getGoal()->plan_name].reset(new ManipulationPlan(goal_handle, m_joint_names, m_urdf_model, m_graph, m_tf_listener, boost::bind(&PlannedManipulation::selfVisualizeCallback, this, _1, _2), boost::bind(&PlannedManipulation::selfExecuteCallback, this, _1)));
    m_plans[goal_handle.getGoal()->plan_name]->plan(m_joint_states.position, m_last_joint_command.position, m_latest_odometry, m_plan_output_frequency, m_max_twist_attempts, m_max_same_slice_attempts); //spawns a new thread, should return immediately
  }

  void PlannedManipulation::cancelCallback(Server::GoalHandle goal_handle)
  {
    m_active_plan_names.clear();
    for(std::map<std::string, boost::shared_ptr<ManipulationPlan> >::iterator iter = m_plans.begin(); iter != m_plans.end(); iter++)
    {
      iter->second->cancel();
    }
    m_plans.clear();

//    if(ros::service::exists("/prophet/erase_prophecies", false))
//    {
//      std_srvs::Empty erase_prophecies;
//      ros::service::call("/prophet/erase_prophecies", erase_prophecies);
//    }
  }

  void PlannedManipulation::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      loop_rate.sleep();
      ros::spinOnce();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "planned_manipulation");
  ros::NodeHandle nh("~");

  planned_manipulation::PlannedManipulation node(nh);
  node.spin();

  return 0;
}
