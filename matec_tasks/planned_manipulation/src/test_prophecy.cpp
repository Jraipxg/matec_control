#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <matec_actions/PlannedManipulationAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

#include "planned_manipulation/goal_region_helper.h"

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  if(argc < 15) //first arg is reserved
  {
    ROS_ERROR("Need 14 arguments: tool_frame, frame_id, target_x, target_y, target_z, target_R, target_P, target_Y, pelvis_x, pelvis_y, pelvis_z, pelvis_R, pelvis_P, pelvis_Y");
    return 0;
  }

  ros::init(argc, argv, "test_planned_manipulation");

  ros::NodeHandle nh;

  signal(SIGABRT, &halt);
  signal(SIGTERM, &halt);
  signal(SIGINT, &halt);

  actionlib::SimpleActionClient<matec_actions::PlannedManipulationAction> client(nh, "/planned_manipulation/server", true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  std::vector<std::string> joint_names;
  matec_utils::blockOnROSJointNames(joint_names);

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");

  std::string tool_frame = argv[1];

  geometry_msgs::PoseStamped target;
  target.pose.position.x = atof(argv[3]);
  target.pose.position.y = atof(argv[4]);
  target.pose.position.z = atof(argv[5]);
  target.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(atof(argv[6]), atof(argv[7]), atof(argv[8]));
  target.header.frame_id = argv[2];
  target.header.stamp = ros::Time(0);

  std::string initial_pose_link = "pelvis";
  std::vector<double> initial_joint_configuration;
  geometry_msgs::PoseStamped initial_pose;
  initial_pose.header = target.header;
  initial_pose.pose.position.x = atof(argv[9]);
  initial_pose.pose.position.y = atof(argv[10]);
  initial_pose.pose.position.z = atof(argv[11]);
  initial_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(atof(argv[12]), atof(argv[13]), atof(argv[14]));

  double x_tol = 0.01;
  double y_tol = 0.01;
  double z_tol = 0.01;
  double R_tol = M_PI;
  double P_tol = M_PI;
  double Y_tol = M_PI;
  matec_actions::PlannedManipulationGoal goal = planned_manipulation::generatePropheticSingleTargetGoal(tool_frame, target, joint_names, initial_pose_link, initial_pose, initial_joint_configuration, R_tol, P_tol, Y_tol, x_tol, y_tol, z_tol, "pelvis");
  goal.execute_on_plan = false;

  ros::Publisher target_pub = nh.advertise<geometry_msgs::PoseStamped>("/test_planned_manipulation_target", 1, true);

  client.sendGoal(goal);
  while(ros::ok() && (client.getState().state_ != actionlib::SimpleClientGoalState::SUCCEEDED) && (client.getState().state_ != actionlib::SimpleClientGoalState::ABORTED))
  {
    ROS_INFO_THROTTLE(1.0, "Waiting for completion...");
    target_pub.publish(target);
  }

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

//**************************
//
//**************************
void halt(int sig)
{
  WTFASSERT(0);
}
