#ifndef PLANNED_MANIPULATION_H
#define PLANNED_MANIPULATION_H

#include "planned_manipulation/manipulation_plan.h"

namespace planned_manipulation
{
  class PlannedManipulation
  {
  public:
    PlannedManipulation(const ros::NodeHandle& nh);
    ~PlannedManipulation();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    double m_plan_output_frequency;
    double m_plan_dt;
    int m_max_twist_attempts;
    int m_max_same_slice_attempts;

    ros::ServiceServer m_get_srv;
    ros::ServiceServer m_execute_srv;
    ros::ServiceServer m_visualize_srv;

    std::vector<std::string> m_active_plan_names;
    control_msgs::FollowJointTrajectoryGoal m_active_plan_goal;

    Server m_server;
    Client* m_client;
    std::map<std::string, boost::shared_ptr<ManipulationPlan> > m_plans;

    boost::shared_ptr<urdf::Model> m_urdf_model;
    boost::shared_ptr<dynamics_tree::DynamicsGraph> m_graph;

    ros::Subscriber m_joint_command_sub;
    matec_msgs::FullJointStates m_last_joint_command;
    bool m_have_joint_command;

    ros::Subscriber m_joint_states_sub;
    matec_msgs::FullJointStates m_joint_states;
    bool m_have_joint_states;

    ros::Subscriber m_odometry_sub;
    matec_msgs::Odometry m_latest_odometry;
    bool m_have_odometry;

    boost::shared_ptr<tf::TransformListener> m_tf_listener;

    std::vector<std::string> m_joint_names;

    //visualization
    visualization_msgs::MarkerArray m_last_goal_region_visualization;
    ros::Publisher m_goal_regions_visualization_pub;
    ros::Publisher m_plan_visualization_pub;

    void goalCallback(Server::GoalHandle goal_handle);
    void cancelCallback(Server::GoalHandle goal_handle);

    bool mergePlansParallel(bool assume_synched, control_msgs::FollowJointTrajectoryGoal other_plan, control_msgs::FollowJointTrajectoryGoal& plan);
    bool mergePlansSeries(bool assume_synched, control_msgs::FollowJointTrajectoryGoal other_plan, control_msgs::FollowJointTrajectoryGoal& plan);

    void selfVisualizeCallback(std::string plan_name, double visualization_density);
    void selfExecuteCallback(std::string plan_name);

    void jointCommandCallback(const matec_msgs::FullJointStates::ConstPtr msg);
    void jointStatesCallback(const matec_msgs::FullJointStates::ConstPtr msg);
    void odometryCallback(const matec_msgs::Odometry::ConstPtr msg);
    void feedbackCallback(const control_msgs::FollowJointTrajectoryFeedbackConstPtr &feedback);
    void doneCallback(const actionlib::SimpleClientGoalState &state, const control_msgs::FollowJointTrajectoryResultConstPtr &result);
    bool getCallback(matec_msgs::GetManipulationPlan::Request &req, matec_msgs::GetManipulationPlan::Response &res);
    bool executeCallback(matec_msgs::ExecuteManipulationPlan::Request &req, matec_msgs::ExecuteManipulationPlan::Response &res);
    bool visualizeCallback(matec_msgs::VisualizeManipulationPlan::Request &req, matec_msgs::VisualizeManipulationPlan::Response &res);
  };
}

#endif //PLANNED_MANIPULATION_H
