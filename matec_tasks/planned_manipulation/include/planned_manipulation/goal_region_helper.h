#ifndef GOAL_REGION_HELPER_H
#define GOAL_REGION_HELPER_H

#include <ros/ros.h>
#include "matec_actions/PlannedManipulationAction.h"
#include <matec_utils/common_functions.h>
#include <matec_utils/common_initialization_components.h>

namespace planned_manipulation
{
  inline void populateSymmetricGoalRegion(matec_msgs::GoalRegion& region, double x_tol = 0.01, double y_tol = 0.01, double z_tol = 0.01, double R_tol = M_PI, double P_tol = M_PI, double Y_tol = M_PI)
  {
    region.x.min = -x_tol;
    region.x.max = x_tol;
    region.y.min = -y_tol;
    region.y.max = y_tol;
    region.z.min = -z_tol;
    region.z.max = z_tol;
    region.R.min = -R_tol;
    region.R.max = R_tol;
    region.P.min = -P_tol;
    region.P.max = P_tol;
    region.Y.min = -Y_tol;
    region.Y.max = Y_tol;
  }

  inline matec_actions::PlannedManipulationGoal generateSingleTargetGoal(std::string tool_frame, geometry_msgs::PoseStamped target, std::vector<std::string> available_joints, double R_tol = M_PI, double P_tol = M_PI / 45.0, double Y_tol = M_PI / 45.0, double x_tol = 0.01, double y_tol = 0.01, double z_tol = 0.01, std::string stable_frame = "pelvis")
  {
    matec_actions::PlannedManipulationGoal goal;
    goal.allow_incomplete_planning = true;
    goal.num_acceptable_consecutive_failures = 5;
    goal.plan_visualization_density = 1.5;
    goal.execute_on_plan = true;
    goal.visualize_on_plan = true;

    matec_msgs::GoalMotion motion;
    motion.available_joints = available_joints;
    motion.max_angular_velocity = 0.25; //todo: param
    motion.max_linear_velocity = 0.075; //todo: param
    motion.stable_frame = stable_frame; //todo: param
    motion.segment_duration = 0.0; //todo: param

    matec_msgs::GoalRegion final_region, initial_region;
    final_region.tool_frame = tool_frame;
    final_region.x.min = -x_tol;
    final_region.x.max = x_tol;
    final_region.y.min = -y_tol;
    final_region.y.max = y_tol;
    final_region.z.min = -z_tol;
    final_region.z.max = z_tol;
    final_region.R.min = -R_tol;
    final_region.R.max = R_tol;
    final_region.P.min = -P_tol;
    final_region.P.max = P_tol;
    final_region.Y.min = -Y_tol;
    final_region.Y.max = Y_tol;
    initial_region = final_region;

    final_region.goal_frame = target;
    final_region.goal_frame.header.stamp = ros::Time(0);
    motion.final_goal_regions.push_back(final_region);

    initial_region.goal_frame.header.frame_id = tool_frame;
    initial_region.goal_frame.header.stamp = ros::Time(0);
    initial_region.goal_frame.pose.orientation.w = 1.0;
    motion.initial_goal_regions.push_back(initial_region);

    goal.segments.push_back(motion);

    return goal;
  }

  inline matec_actions::PlannedManipulationGoal generatePropheticSingleTargetGoal(std::string tool_frame, geometry_msgs::PoseStamped target, std::vector<std::string> available_joints, std::string initial_pose_link, geometry_msgs::PoseStamped initial_pose, std::vector<double> initial_joint_configuration, double R_tol = M_PI, double P_tol = M_PI / 45.0, double Y_tol = M_PI / 45.0, double x_tol = 0.01, double y_tol = 0.01, double z_tol = 0.01, std::string stable_frame = "pelvis")
  {
    matec_actions::PlannedManipulationGoal goal = generateSingleTargetGoal(tool_frame, target, available_joints, R_tol, P_tol, Y_tol, x_tol, y_tol, z_tol, stable_frame);
    goal.segments.at(0).initial_goal_regions.at(0).attach_goal_frame_to_robot = true;
    goal.segments.at(0).final_goal_regions.at(0).attach_goal_frame_to_robot = false;
    goal.initial_joint_positions = initial_joint_configuration;
    goal.initial_pose = initial_pose;
    goal.initial_pose_link = initial_pose_link;

    return goal;
  }

  inline matec_actions::PlannedManipulationGoal generateMultiTargetGoal(std::string tool_frame, std::vector<geometry_msgs::PoseStamped> targets, std::vector<std::string> available_joints, double R_tol = M_PI, double P_tol = M_PI / 45.0, double Y_tol = M_PI / 45.0, double x_tol = 0.01, double y_tol = 0.01, double z_tol = 0.01, std::string stable_frame = "pelvis")
  {
    matec_actions::PlannedManipulationGoal goal;
    goal.allow_incomplete_planning = true;
    goal.num_acceptable_consecutive_failures = 5;
    goal.plan_visualization_density = 1.5;
    goal.execute_on_plan = true;
    goal.visualize_on_plan = true;

    matec_msgs::GoalMotion motion;
    motion.available_joints = available_joints;
    motion.max_angular_velocity = 0.25; //todo: param
    motion.max_linear_velocity = 0.075; //todo: param
    motion.stable_frame = stable_frame; //todo: param
    motion.segment_duration = 0.0; //todo: param
    motion.initial_goal_regions.resize(1);
    motion.final_goal_regions.resize(1);

    motion.initial_goal_regions.at(0).goal_frame.header.frame_id = tool_frame;
    motion.initial_goal_regions.at(0).goal_frame.header.stamp = ros::Time(0);
    motion.initial_goal_regions.at(0).goal_frame.pose = geometry_msgs::Pose();
    motion.initial_goal_regions.at(0).goal_frame.pose.orientation.w = 1.0;
    planned_manipulation::populateSymmetricGoalRegion(motion.initial_goal_regions.at(0), x_tol, y_tol, z_tol, R_tol, P_tol, Y_tol);
    motion.initial_goal_regions.at(0).attach_goal_frame_to_robot = true;
    motion.initial_goal_regions.at(0).tool_frame = tool_frame;

    motion.final_goal_regions.at(0).goal_frame = targets.at(0);
    planned_manipulation::populateSymmetricGoalRegion(motion.final_goal_regions.at(0), x_tol, y_tol, z_tol, R_tol, P_tol, Y_tol);
    motion.final_goal_regions.at(0).attach_goal_frame_to_robot = false;
    motion.final_goal_regions.at(0).tool_frame = tool_frame;

    goal.segments.push_back(motion);

    for(unsigned int i = 1; i < targets.size(); i++)
    {
      motion.initial_goal_regions.at(0) = motion.final_goal_regions.at(0);
      motion.final_goal_regions.at(0).goal_frame = targets.at(i);
      motion.final_goal_regions.at(0).goal_frame.header.stamp = ros::Time(0);

      goal.segments.push_back(motion);
    }

    return goal;
  }

  inline matec_actions::PlannedManipulationGoal generateMultiToolGoal(std::vector<std::string> tool_frames, std::vector<geometry_msgs::PoseStamped> targets, std::vector<std::string> available_joints, double R_tol = M_PI, double P_tol = M_PI / 45.0, double Y_tol = M_PI / 45.0, double x_tol = 0.01, double y_tol = 0.01, double z_tol = 0.01, std::string stable_frame = "pelvis")
  {
    WTFASSERT(tool_frames.size() == targets.size());

    matec_actions::PlannedManipulationGoal goal;
    goal.allow_incomplete_planning = true;
    goal.num_acceptable_consecutive_failures = 5;
    goal.plan_visualization_density = 1.5;
    goal.execute_on_plan = true;
    goal.visualize_on_plan = true;

    matec_msgs::GoalMotion motion;
    motion.available_joints = available_joints;
    motion.max_angular_velocity = 0.25; //todo: param
    motion.max_linear_velocity = 0.075; //todo: param
    motion.stable_frame = stable_frame; //todo: param
    motion.segment_duration = 0.0; //todo: param
    motion.initial_goal_regions.resize(targets.size());
    motion.final_goal_regions.resize(targets.size());

    matec_msgs::GoalRegion region_template;
    region_template.x.min = -x_tol;
    region_template.x.max = x_tol;
    region_template.y.min = -y_tol;
    region_template.y.max = y_tol;
    region_template.z.min = -z_tol;
    region_template.z.max = z_tol;
    region_template.R.min = -R_tol;
    region_template.R.max = R_tol;
    region_template.P.min = -P_tol;
    region_template.P.max = P_tol;
    region_template.Y.min = -Y_tol;
    region_template.Y.max = Y_tol;

    for(unsigned int i = 0; i < targets.size(); i++)
    {
      region_template.tool_frame = tool_frames.at(i);
      region_template.goal_frame.header.frame_id = tool_frames.at(i);
      region_template.goal_frame.header.stamp = ros::Time(0);
      region_template.goal_frame.pose = geometry_msgs::Pose();
      region_template.goal_frame.pose.orientation.w = 1.0;
      motion.initial_goal_regions.at(i) = region_template;

      region_template.goal_frame = targets.at(i);
      region_template.goal_frame.header.stamp = ros::Time(0);
      motion.final_goal_regions.at(i) = region_template;
    }

    goal.segments.push_back(motion);

    return goal;
  }
}

#endif //GOAL_REGION_HELPER_H
