#ifndef PLANNED_MANIPULATION_TARGETER_H
#define PLANNED_MANIPULATION_TARGETER_H

#include "planned_manipulation_common.h"
#include "matec_actions/PlannedManipulationTargeterAction.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Pose2D.h"
#include <numeric>

namespace planned_manipulation
{
  typedef actionlib::ActionServer<matec_actions::PlannedManipulationTargeterAction> TargeterServer;
  typedef actionlib::SimpleActionClient<matec_actions::PlannedManipulationAction> PlannedManipulationClient;

  class PlannedManipulationTargeter
  {
  public:
    PlannedManipulationTargeter(const ros::NodeHandle& nh);
    ~PlannedManipulationTargeter();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_global_frame;
    std::string m_ground_frame;
    std::string m_root_link;
    double m_nominal_z;
    double m_visualization_locomotion_velocity;
    bool m_auto_visualize;
    double m_planning_timeout;
    double m_x_variance;
    double m_y_variance;
    double m_Y_variance;

    ros::Subscriber m_cmd_sub;

    bool m_have_guess;
    geometry_msgs::PoseStamped m_stance_guess;

    visualization_msgs::Marker m_base_marker;
    visualization_msgs::MarkerArray m_target_markers;
    ros::Publisher m_target_markers_pub;

    geometry_msgs::PoseArray m_target_poses;
    ros::Publisher m_target_poses_pub;

    geometry_msgs::PoseArray m_attempted_poses;
    ros::Publisher m_attempted_poses_pub;

    geometry_msgs::PoseStamped m_global_centroid;
    ros::Publisher m_global_centroid_pub;

    geometry_msgs::PoseStamped m_best_pose;
    std::string m_best_plan;
    ros::Publisher m_best_pose_pub;

    double m_closest_plan_so_far_percent;


    std::vector<double> m_solve_times;
    std::vector<double> m_fitnesses;

    ros::ServiceServer m_execute_srv;
    ros::ServiceServer m_visualize_srv;

    matec_msgs::ValueRange m_x;
    matec_msgs::ValueRange m_y;
    matec_msgs::ValueRange m_Y;

    std::vector<double> m_standard_initial_configuration;
    std::vector<double> m_current_commanded_configuration;

    TargeterServer m_server;
    PlannedManipulationClient* m_client;
    TargeterServer::GoalHandle m_goal_handle;
    matec_actions::PlannedManipulationGoal m_goal;
    double m_global_centroid_yaw;

    boost::shared_ptr<tf::TransformListener> m_tf_listener;
    std::vector<std::string> m_joint_names;

    bool m_first_test; //current positions
    bool m_second_test; //stance guess
    ros::Time m_current_test_start_time;
    bool m_ready_for_next_test;

    void goalCallback(TargeterServer::GoalHandle goal_handle);
    void cancelCallback(TargeterServer::GoalHandle goal_handle);

    void cancel();
    void finish(bool success, bool need_to_move, bool standard_configuration);

    void computeFitness();

    bool setInitialConfigurationPosition(std::string joint_name, double position);
    void testSample(geometry_msgs::PoseStamped global_sample, std::vector<double> initial_configuration);
    void findTargetPoses();

    void commandCallback(const matec_msgs::FullJointStates::ConstPtr& msg);
    void feedbackCallback(const matec_actions::PlannedManipulationFeedbackConstPtr &feedback);
    void doneCallback(const actionlib::SimpleClientGoalState &state, const matec_actions::PlannedManipulationResultConstPtr &result);
  };
}

#endif //PLANNED_MANIPULATION_TARGETER_H
