#ifndef MANIPULATION_PLAN_H
#define MANIPULATION_PLAN_H

#include "planned_manipulation/planned_manipulation_common.h"
#include "matec_utils/biquad.h"
#include "matec_utils/goal_space_interpolator.h"

namespace planned_manipulation
{
  enum TwistSolutionVariant
  {
    INDEPENDENT_TRANSPOSE,
    FULL_TRANSPOSE,
    PSEUDOINVERSE,
    NUM_TWIST_SOLUTION_VARIANTS
  };

  class ManipulationPlan
  {
  public:
    ManipulationPlan(Server::GoalHandle goal_handle, std::vector<std::string> joint_names, boost::shared_ptr<urdf::Model> urdf_model, boost::shared_ptr<dynamics_tree::DynamicsGraph> graph, boost::shared_ptr<tf::TransformListener> tf_listener, boost::function<void(std::string, double)> self_visualize, boost::function<
        void(std::string)> self_execute);
    ~ManipulationPlan();

    bool plan(std::vector<double> current_positions, std::vector<double> last_commanded_positions, matec_msgs::Odometry current_root_link_odom, double plan_frequency, unsigned int max_twist_attempts, unsigned int max_same_slice_attempts);
    bool active();
    bool ready();
    void feedback(double execution_progress = 0.0, bool execution_complete = false);
    void finish(bool success);
    void cancel();

    void getPlan(control_msgs::FollowJointTrajectoryGoal& plan, std::vector<int>& segment_start_indices);
    void getVisualizations(double visualization_density, visualization_msgs::MarkerArray& goal_regions, control_msgs::FollowJointTrajectoryGoal& joint_trajectories, std::vector<matec_msgs::Prophecy>& prophecies);

  private:
    ros::NodeHandle m_nh;
    std::string m_node_name;
    boost::shared_ptr<urdf::Model> m_urdf_model;
    boost::shared_ptr<dynamics_tree::DynamicsGraph> m_graph;
    boost::shared_ptr<tf::TransformListener> m_tf_listener;
    boost::shared_ptr<boost::thread> m_planning_thread;
    Server::GoalHandle m_goal_handle;
    matec_actions::PlannedManipulationGoal m_goal;
    dynamics_tree::DynamicsTree m_tree;
    control_msgs::FollowJointTrajectoryGoal m_plan;
    std::vector<std::vector<std::vector<matec_msgs::GoalRegion> > > m_plan_regions; //[segment][tool][time]
    std::vector<bool> m_joint_active;
    bool m_planning_thread_active;
    bool m_planning_complete;
    double m_planning_progress;
    double m_max_linear_accel;
    double m_max_angular_accel;
    boost::function<void(std::string, double)> m_self_visualize_callback;
    boost::function<void(std::string)> m_self_execute_callback;
    bool m_in_callback;

    std::vector<int> m_segment_start_indices;

    ros::Publisher m_auxiliary_feedback_pub;

    unsigned int m_max_twist_attempts;
    unsigned int m_max_same_slice_attempts;

    double m_plan_frequency;
    double m_plan_dt;
    std::vector<double> m_segment_time_dilations;

    std::vector<double> m_current_positions;
    std::vector<double> m_last_commanded_positions;
    std::vector<std::string> m_joint_names;
    matec_msgs::Odometry m_initial_root_link_odom;
    std::string m_odom_link;
    std::vector<long> m_last_good_slices;

    std::vector<double> m_joint_mins;
    std::vector<double> m_joint_maxes;

    boost::shared_ptr<matec_utils::ParameterManager> m_pm;
    std::vector<double> m_max_joint_velocities;
    std::vector<double> m_max_joint_accelerations;

    void stopPlanningThread();

    double minimumMotionDuration(std::vector<geometry_msgs::PoseStamped> initials_in_stable, std::vector<geometry_msgs::PoseStamped> finals_in_stable, matec_msgs::GoalMotion motion);
    std::string findNearestURDFLink(std::string frame);
    bool getCurrentOffset(std::string tool_frame, std::string& nearest_urdf_link, matec_utils::Matrix4& tool_offset);
    std::vector<double> getJointPositions(unsigned int plan_idx);
    bool transformCentroid(std::string stable_frame, std::vector<double> joint_positions, matec_msgs::Odometry odometry, bool attach_to_robot, geometry_msgs::PoseStamped centroid_in, geometry_msgs::PoseStamped& centroid_out);

    void generateTwistBasisRecursive(int depth, matec_utils::Vector6& current_delta, matec_utils::Matrix4& stableTtool, matec_utils::Matrix4& stableTgoal, matec_utils::Vector6 mins, matec_utils::Vector6 maxes, double dt, std::vector<matec_utils::Vector6>& twist_basis);
    void generateTwistBasis(matec_utils::Matrix4& stableTtool, matec_utils::Matrix4& stableTgoal, matec_msgs::GoalRegion& goal_region, double dt, std::vector<matec_utils::Vector6>& twist_basis);
    bool tryTwists(std::vector<matec_utils::Vector6> twists, TwistSolutionVariant mode, std::vector<double> initial_positions, std::vector<double> initial_velocities, std::vector<unsigned int> available_joint_indices, std::vector<
        matec_msgs::GoalRegion> slice_regions, double dt, dynamics_tree::DynamicsTree& tree, std::vector<matec_utils::Matrix>& jacobians, std::vector<matec_utils::Matrix>& JTs, std::vector<matec_utils::Matrix>& JJTs, std::vector<double>& final_positions, std::vector<double>& final_velocities, double& score);
    bool tryTwistSolutionVariants(std::vector<matec_utils::Vector6> twists, std::vector<double> initial_positions, std::vector<double> initial_velocities, std::vector<unsigned int> available_joint_indices, std::vector<matec_msgs::GoalRegion> slice_regions, double dt, dynamics_tree::DynamicsTree& tree, std::vector<
        matec_utils::Matrix>& jacobians, std::vector<matec_utils::Matrix>& JTs, std::vector<matec_utils::Matrix>& JJTs, std::vector<double>& final_positions, std::vector<double>& final_velocities, double& score);
    bool checkToolPoses(dynamics_tree::DynamicsTree& tree, std::vector<double> joint_positions, std::vector<matec_msgs::GoalRegion> slice_regions, std::vector<matec_utils::Matrix4>& goalTtools, std::vector<matec_utils::Vector6>& restoration_twists);
    bool solveSlice(std::vector<double> initial_positions, std::vector<double> initial_velocities, std::vector<unsigned int> available_joint_indices, std::vector<matec_msgs::GoalRegion> slice_regions, double& slice_dt, dynamics_tree::DynamicsTree& tree, std::vector<double>& final_positions, std::vector<
        double>& final_velocities);
    bool solveSegment(std::vector<double> segment_initial_joints, matec_msgs::Odometry segment_initial_odometry, matec_msgs::GoalMotion segment, unsigned int segment_idx, std::vector<std::vector<matec_msgs::GoalRegion> > segment_regions);
    bool initializePlan();

    template<typename InterpolationMethod>
    void interpolateSegment(std::vector<double> segment_initial_joints, matec_msgs::Odometry segment_initial_odometry, matec_msgs::GoalMotion motion, std::vector<std::vector<matec_msgs::GoalRegion> >& interpolated_motions);

    void planningThread();
  };
}

#endif //MANIPULATION_PLAN_H
