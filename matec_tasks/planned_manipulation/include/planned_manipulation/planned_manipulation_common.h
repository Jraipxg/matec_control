#ifndef PLANNED_MANIPULATION_COMMON_H
#define PLANNED_MANIPULATION_COMMON_H

#include <ros/ros.h>

#include <actionlib/server/action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread/mutex.hpp>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/StringArray.h>
#include <matec_actions/PlannedManipulationAction.h>
#include <control_msgs/FollowJointTrajectoryAction.h>

#include "matec_utils/common_functions.h"
#include "matec_utils/common_initialization_components.h"
#include "matec_utils/parameter_helper.h"
#include "matec_utils/minimum_jerk_interpolator.h"
#include "matec_utils/quaternion_interpolator.h"

#include "matec_dynamics_tree/dynamics_graph.h"

#include "visualization_msgs/MarkerArray.h"
#include "matec_msgs/ExecuteManipulationPlan.h"
#include "matec_msgs/VisualizeManipulationPlan.h"
#include "matec_msgs/GetManipulationPlan.h"
#include <matec_msgs/AddProphecies.h>
#include <std_srvs/Empty.h>

namespace planned_manipulation
{
  typedef actionlib::ActionServer<matec_actions::PlannedManipulationAction> Server;
  typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> Client;

  inline void addAxisMarkers(visualization_msgs::Marker base_marker, visualization_msgs::MarkerArray& marker_array, double alpha=1.0) //base_marker must have the right pose and other properties you want set
  {
    tf::Transform pose_trans = tf::Transform(tf::Quaternion(base_marker.pose.orientation.x, base_marker.pose.orientation.y, base_marker.pose.orientation.z, base_marker.pose.orientation.w), tf::Vector3(base_marker.pose.position.x, base_marker.pose.position.y, base_marker.pose.position.z));
    tf::StampedTransform pose_trans_stamped = tf::StampedTransform(pose_trans, ros::Time(0), base_marker.header.frame_id, "pose");

    tf::StampedTransform roll_trans_stamped = tf::StampedTransform(tf::Transform(tf::createQuaternionFromRPY(0.0, M_PI / 2.0, 0.0), tf::Vector3(base_marker.scale.x / 4.0, 0.0, 0.0)), ros::Time(0), "pose", "roll");
    tf::StampedTransform pitch_trans_stamped = tf::StampedTransform(tf::Transform(tf::createQuaternionFromRPY(-M_PI / 2.0, 0.0, 0.0), tf::Vector3(0.0, base_marker.scale.y / 4.0, 0.0)), ros::Time(0), "pose", "pitch");
    tf::StampedTransform yaw_trans_stamped = tf::StampedTransform(tf::Transform(tf::createQuaternionFromRPY(0.0, 0.0, 0.0), tf::Vector3(0.0, 0.0, base_marker.scale.z / 4.0)), ros::Time(0), "pose", "yaw");

    tf::Transformer transformer;
    transformer.setTransform(pose_trans_stamped);
    transformer.setTransform(roll_trans_stamped);
    transformer.setTransform(pitch_trans_stamped);
    transformer.setTransform(yaw_trans_stamped);

    tf::StampedTransform marker_roll_trans, marker_pitch_trans, marker_yaw_trans;
    transformer.lookupTransform(base_marker.header.frame_id, "roll", ros::Time(0), marker_roll_trans);
    transformer.lookupTransform(base_marker.header.frame_id, "pitch", ros::Time(0), marker_pitch_trans);
    transformer.lookupTransform(base_marker.header.frame_id, "yaw", ros::Time(0), marker_yaw_trans);

    geometry_msgs::PoseStamped marker_roll_pose = matec_utils::stampedTransformToPoseStamped(marker_roll_trans);
    geometry_msgs::PoseStamped marker_pitch_pose = matec_utils::stampedTransformToPoseStamped(marker_pitch_trans);
    geometry_msgs::PoseStamped marker_yaw_pose = matec_utils::stampedTransformToPoseStamped(marker_yaw_trans);

    base_marker.type = visualization_msgs::Marker::CYLINDER;
    base_marker.color.a = alpha;

    double box_x = base_marker.scale.x / 2.0;
    double box_y = base_marker.scale.y / 2.0;
    double box_z = base_marker.scale.z / 2.0;
    double cylinder_diameter_scale = 0.1;

    base_marker.id = marker_array.markers.size();
    base_marker.pose = marker_roll_pose.pose;
    base_marker.color.r = 1.0;
    base_marker.color.g = 0.0;
    base_marker.color.b = 0.0;
    base_marker.scale.x = cylinder_diameter_scale * (box_y + box_z) / 2.0;
    base_marker.scale.y = cylinder_diameter_scale * (box_y + box_z) / 2.0;
    base_marker.scale.z = box_x;
    marker_array.markers.push_back(base_marker);

    base_marker.id = marker_array.markers.size();
    base_marker.pose = marker_pitch_pose.pose;
    base_marker.color.r = 0.0;
    base_marker.color.g = 1.0;
    base_marker.color.b = 0.0;
    base_marker.scale.x = cylinder_diameter_scale * (box_x + box_z) / 2.0;
    base_marker.scale.y = cylinder_diameter_scale * (box_x + box_z) / 2.0;
    base_marker.scale.z = box_y;
    marker_array.markers.push_back(base_marker);

    base_marker.id = marker_array.markers.size();
    base_marker.pose = marker_yaw_pose.pose;
    base_marker.color.r = 0.0;
    base_marker.color.g = 0.0;
    base_marker.color.b = 1.0;
    base_marker.scale.x = cylinder_diameter_scale * (box_x + box_y) / 2.0;
    base_marker.scale.y = cylinder_diameter_scale * (box_x + box_y) / 2.0;
    base_marker.scale.z = box_z;
    marker_array.markers.push_back(base_marker);
  }
}

#endif //PLANNED_MANIPULATION_COMMON_H
