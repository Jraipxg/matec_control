#!/usr/bin/env python
from xml.dom import minidom
class Segment:
  def __init__(self, duration, joint_names, joint_positions):
    self.duration = duration
    self.joint_names = joint_names
    self.joint_positions = joint_positions

  def __repr__(self):
    out = ""
    out = "<segment duration=\"" + str(self.duration) + "\">\n"
    for [name, position] in zip(self.joint_names, self.joint_positions): #todo: don't assume len(joint_names)==len(joint_positions)
        out += "  <joint name=\"" + name + "\" position=\"" + str(position) + "\"/>\n"
    out += "</segment>\n"
    return out

class Script:
  def __repr__(self):
    out = ""
    for segment in self.segments:
      out += str(segment)
    return out

  def loadFromFile(self, file_path, script_name):
    self.name = script_name
    xmldoc = minidom.parse(file_path)
    segments_xml = xmldoc.getElementsByTagName('segment')
    self.num_segments = len(segments_xml)
    self.segments = []
    for s in segments_xml:
      duration = float(s.attributes['duration'].value)
      joints_xml = s.getElementsByTagName('joint')
      joint_names = []
      joint_positions = []
      for j in joints_xml:
        joint_names.append(j.attributes['name'].value)
        joint_positions.append(float(j.attributes['position'].value))
      self.segments.append(Segment(duration, joint_names, joint_positions))

  def writeToFile(self, script_directory):
    filename = script_directory + "/" + self.name + ".xml"
    with open(filename, 'w') as doc:
        doc.write("<?xml version=\"1.0\"?>\n")
        doc.write(str(self))
