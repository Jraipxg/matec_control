#!/usr/bin/env python
#Written by Frank Weng

class SAML:
    class Action:
        class Command:
            def __init__(self, joints=[], data=[], effort=[], duration=0):
                self.joints = joints
                self.data = data
                self.effort = effort
                self.duration = duration
                if effort == [] and data != []:
                    self.effort = [0]*len(data)

            def __repr__(self):
                out = "\t<command>\n\t\t<joints=\""
                for j in self.joints:
                    out += str(j) + ' '
                out += "\"/>\n\t\t<data=\""
                for d in self.data:
                    out += str(d) + ' '
                out += "\"/>\n\t\t<effort=\""
                for e in self.effort:
                    out += str(e) + ' '
                out += "\"/>\n\t\t<duration=\""
                out += str(self.duration)
                out += "\"/>\n\t</command>\n"
                return out

        def __init__(self, name="", commands=[]):
            self.name = name
            self.commands = list(commands)

        def __repr__(self):
            out = "<action>\n\t<name=\"" + self.name +"\">\n"
            for c in self.commands:
                out += str(c)
            out += "</action>\n"
            return out
    def __init__(self, actions=[]):
        self.actions = list(actions)
    def __repr__(self):
        out = ""
        for a in self.actions:
            out += str(a)
        return out

def write(file_name, saml):
    with open(file_name, 'w') as doc:
        doc.write(str(saml))

def parse(file_name):
    actions = list()
    with open(file_name, 'r') as doc:
        lines = doc.readlines()
        for line in lines:
            if "<action>" in line:
                action = SAML.Action()
            elif "</action>" in line:
                actions.append(action)
            elif "<name=" in line:
                action.name = line.split('"')[1].rstrip().lstrip()
            elif "<command>" in line:
                command = SAML.Action.Command()
            elif "<joints=" in line:
                command.joints = line.split('"')[1].rstrip().lstrip().split(' ')
            elif "<data=" in line:
                command.data = [float(x) for x in line.split('"')[1].rstrip().lstrip().split(' ')]
            elif "<effort=" in line:
                command.effort = [float(x) for x in line.split('"')[1].rstrip().lstrip().split(' ')]
            elif "<duration=" in line:
                command.duration = float(line.split('"')[1].rstrip().lstrip())
            elif "</command>" in line:
                action.commands.append(command)
            else:
                print "What's this? ", line
    return SAML(actions)

