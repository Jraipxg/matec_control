#!/usr/bin/env python
import roslib; roslib.load_manifest('joint_script_executor')
import rospy
from script_parser import Script, Segment
import actionlib
import matec_actions.msg
from matec_msgs.srv import *
import control_msgs.msg
import trajectory_msgs.msg
import os
from os import listdir
from os.path import isfile, join

class JointScriptExecutor(object):
  def __init__(self, name):
    script_directory = rospy.get_param('~script_directory', '../scripts/atlas')
    relative_script_directory = os.path.dirname(os.path.realpath(__file__)) + "/" + script_directory
    if(os.path.exists(script_directory)):
      script_files = [f for f in listdir(script_directory) if f.endswith(".xml")]
    else:
      if(os.path.exists(relative_script_directory)):
        script_files = [f for f in listdir(relative_script_directory) if f.endswith(".xml")]
        script_directory = relative_script_directory
      else:
        rospy.logerr(rospy.get_name().lstrip('/') + ": Couldn't find script files at " + script_directory + " or " + relative_script_directory + "!")
        return
    self.script_names = [os.path.splitext(filename)[0] for filename in script_files]
    self.script_directory = script_directory
    self.scripts = []
    for [filename, script_name] in zip(script_files, self.script_names):
      script = Script()
      script.loadFromFile(script_directory + '/' + filename, script_name)
      self.scripts.append(("_".join(script_name.lower().split()),script))
    self.scripts_dict = dict(self.scripts)
      #print "Loaded script " + script_name + ":\n" + str(script)

    rospy.loginfo(rospy.get_name().lstrip('/') + ": Loaded joint scripts: " + str(self.script_names))

    server_name = rospy.get_param('~server_name', '/base_joint_interpolator/server')
    self.client = actionlib.SimpleActionClient(server_name, control_msgs.msg.FollowJointTrajectoryAction)
    print rospy.get_name().lstrip('/') + ": Waiting for interpolator server at " + server_name + "..."
    self.client.wait_for_server()
    print rospy.get_name().lstrip('/') + ": Connected to interpolator server!"

    self.action_server = actionlib.SimpleActionServer("~execute_action_script", matec_actions.msg.ExecuteScriptAction, execute_cb=self.actionCallback, auto_start=False)
    self.action_server.start()

    self.explicit_action_server = actionlib.SimpleActionServer("~execute_explicit_action_script", matec_actions.msg.ExecuteExplicitScriptAction, execute_cb=self.explicitActionCallback, auto_start=False)
    self.explicit_action_server.start()

    self.service_server = rospy.Service('~execute_script', ExecuteScript, self.serviceCallback)

    self.list_service_server = rospy.Service('~list_scripts', ListScripts, self.listCallback)
    self.lookup_service_server = rospy.Service('~lookup_script', LookupScript, self.lookupCallback)
    self.save_service_server = rospy.Service('~save_script', SaveScript, self.saveCallback)

    print rospy.get_name().lstrip('/') + " initialization complete!"

  def executeScript(self, trajectory_goals):
    for goal in trajectory_goals:
      self.client.send_goal(goal)
      self.client.wait_for_result()

  def lookupScript(self, script_name):
    trajectory_goals = []
    script_name = "_".join(script_name.lower().split())
    if script_name in self.scripts_dict:
    	script = self.scripts_dict[script_name]
    	print "Found script " + script.name;
        for segment in script.segments:
          trajectory_goal = control_msgs.msg.FollowJointTrajectoryGoal()
          trajectory_goal.trajectory.joint_names = segment.joint_names
          point = trajectory_msgs.msg.JointTrajectoryPoint()
          point.time_from_start = rospy.Duration(segment.duration)
          for position in segment.joint_positions:
            point.positions.append(position)
            point.velocities.append(0.)
          trajectory_goal.trajectory.points.append(point)
          trajectory_goals.append(trajectory_goal)
    else:
    	print "Couldn't find script", script_name
    
    return trajectory_goals

  def serviceCallback(self, req):
    trajectory_goals = self.lookupScript(req.script_name)
    if len(trajectory_goals) > 0:
      self.executeScript(trajectory_goals)
      return ExecuteScriptResponse()
    else:
      print rospy.get_name().lstrip('/') + ": Script " + req.script_name + " not found!"
      return None

  def listCallback(self, req):
    res = ListScriptsResponse()
    res.available_script_names = [" ".join(x.capitalize() for x in script.split('_')) for script in sorted(self.script_names)]
    return res

  def lookupCallback(self, req):
    return LookupScriptResponse(self.lookupScript(req.script_name))

  def saveCallback(self, req):
    script = Script()
    script.segments = []
    for seg in req.trajectory:
      if len(seg.trajectory.points) != 1 or len(seg.trajectory.joint_names) != len(seg.trajectory.points[0].positions):
        print rospy.get_name().lstrip('/') + ": Malformed SaveScripts request", req
        return None
      point = seg.trajectory.points[0]
      script.segments.append(Segment(point.time_from_start.to_sec(), seg.trajectory.joint_names, point.positions))
    script.name = req.script_name
    script.writeToFile(self.script_directory)
    self.script_names.append(req.script_name)
    self.scripts_dict[req.script_name] = script
    return SaveScriptResponse()

  def actionCallback(self, script_goal):
    trajectory_goals = self.lookupScript(script_goal.script_name)
    if len(trajectory_goals) > 0:
      self.executeScript(trajectory_goals)
      self.action_server.set_succeeded()
    else:
      print rospy.get_name().lstrip('/') + ": Script " + script_goal.script_name + " not found!"
      self.action_server.set_aborted()

  def explicitActionCallback(self, explicit_script_goal):
    self.executeScript(explicit_script_goal.goals)
    self.explicit_action_server.set_succeeded()


if __name__ == '__main__':
  rospy.init_node('joint_script_executor')
  JointScriptExecutor(rospy.get_name().lstrip('/'))
  rospy.spin()
