#include "joint_space_control/joint_space_control.h"

namespace joint_space_control
{
  JointPositionControl::JointPositionControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("interpolator", m_interpolator, std::string(m_nh.getNamespace()));
    m_nh.param("base_behavior", m_base_behavior, false);

    m_nh.param("default_kp", m_default_kp, 0.0);
    m_nh.param("default_kv", m_default_kv, 0.0);
    m_nh.param("default_ki", m_default_ki, 0.0);
    m_nh.param("default_i_clamp", m_default_i_clamp, 0.0);
    m_nh.param("default_i_liveband", m_default_i_liveband, 0.0);
    m_nh.param("default_i_accumulator", m_default_i_accumulator, 0.0);
    m_nh.param("default_compliance", m_default_compliance, 0.5);

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints, m_joint_names))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointPositionControl couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }
    initializeCommand();

    m_muted = true;
    matec_utils::blockOnBehaviorRegistration(m_nh.getNamespace(), m_base_behavior);

    m_kps.resize(m_controlled_joints.size(), m_default_kp);
    m_kvs.resize(m_controlled_joints.size(), m_default_kv);
    m_kis.resize(m_controlled_joints.size(), m_default_ki);
    m_i_clamps.resize(m_controlled_joints.size(), m_default_i_clamp);
    m_i_livebands.resize(m_controlled_joints.size(), m_default_i_liveband);
    m_i_accumulators.resize(m_controlled_joints.size(), m_default_i_accumulator);
    m_compliances.resize(m_controlled_joints.size(), m_default_compliance);

    m_error_pub = m_nh.advertise<matec_msgs::FullJointStates>("errors", 1, true);
    m_total_error_pub = m_nh.advertise<std_msgs::Float64>("total_joint_error_degrees", 1, true);
    m_max_error_pub = m_nh.advertise<std_msgs::Float64>("max_joint_error_degrees", 1, true);

    m_behavior_pub.advertise("behavior_command");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&JointPositionControl::sensorCallback, this, _1));
    m_joint_carrot_sub.subscribe("/" + m_interpolator + "/carrot");

    m_manage_service_server = m_nh.advertiseService("manage", &JointPositionControl::managementCallback, this);
    m_awaken_service_server = m_nh.advertiseService("awaken", &JointPositionControl::awakenCallback, this);

    if(m_base_behavior) //auto-wake base behaviors on startup
    {
      matec_msgs::AwakenBehavior awaken;
      awakenCallback(awaken.request, awaken.response);
    }

    getGains();
  }

  JointPositionControl::~JointPositionControl()
  {

  }

  void JointPositionControl::initializeCommand()
  {
    m_command.feedforward.joint_indices = m_joint_map;
    m_command.feedforward.position.resize(m_controlled_joints.size(), 0.0);
    m_command.feedforward.velocity.resize(m_controlled_joints.size(), 0.0);
    m_command.feedforward.acceleration.resize(m_controlled_joints.size(), 0.0);
    m_command.feedforward.torque.resize(m_controlled_joints.size(), 0.0);
    m_command.feedforward.compliance.resize(m_controlled_joints.size(), 0.0);

    m_command.feedback.joint_indices = m_joint_map;
    m_command.feedback.acceleration.resize(m_controlled_joints.size(), 0.0);
  }

  bool JointPositionControl::awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res)
  {
    ROS_INFO("%s: Waking up...", m_nh.getNamespace().c_str());
    std::vector<unsigned char> required_actuators;
    if(req.actuators.size() != 0)
    {
      required_actuators = req.actuators;
    }
    else
    {
      required_actuators = m_joint_map;
    }

    res.awake = true;
    for(unsigned int i = 0; i < required_actuators.size(); i++)
    {
      if(std::find(m_owned_joints.begin(), m_owned_joints.end(), m_joint_names[required_actuators[i]]) == m_owned_joints.end())
      {
        ROS_INFO("%s: Need control of joint %s!", m_nh.getNamespace().c_str(), m_joint_names[required_actuators[i]].c_str());
        res.awake = false;
      }
    }

    if(res.awake)
    {
      ROS_INFO("%s: Already awake!", m_nh.getNamespace().c_str());
      return true;
    }
    else
    {
      ROS_INFO("%s: Requesting ownership of missing joints!", m_nh.getNamespace().c_str());
      return matec_utils::requestOwnership(required_actuators);
    }
  }

  //TODO: implement sleep, reject if base behavior

  bool JointPositionControl::managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res)
  {
    ROS_INFO("%s: Got management signal!", m_nh.getNamespace().c_str());
    boost::mutex::scoped_lock lock(m_mutex);
    ROS_INFO("%s: Got through mutex!", m_nh.getNamespace().c_str());
    matec_utils::indicesToNames(m_joint_names, req.owned_joints, m_owned_joints);

    for(unsigned int i = 0; i < req.newly_owned_joints.size(); i++)
    {
      unsigned int control_idx = std::find(m_joint_map.begin(), m_joint_map.end(), req.newly_owned_joints[i]) - m_joint_map.begin();
      if(control_idx < m_joint_map.size())
      {
        m_command.feedforward.position[control_idx] = req.last_command.position[req.newly_owned_joints[i]];
        m_command.feedforward.velocity[control_idx] = 0;
        m_command.feedforward.acceleration[control_idx] = 0;
      }
      else
      {
        ROS_WARN("%s: Tare requested for uncontrolled joint %s", m_nh.getNamespace().c_str(), m_joint_names[req.newly_owned_joints[i]].c_str());
      }
    }
    m_behavior_pub.publish(m_command);

    //forward to interpolator
    bool success = true;
    std::string interpolator_management = "/" + matec_utils::stripLeadingSlash(m_interpolator) + "/manage";
    if(ros::service::exists(interpolator_management, false))
    {
      matec_msgs::ManageJointInterpolator manage_joint_interpolator;
      manage_joint_interpolator.request.new_setpoint = m_command.feedforward;
      ROS_INFO("%s: Calling interpolator management!", m_nh.getNamespace().c_str());
      success = ros::service::call(interpolator_management, manage_joint_interpolator.request, manage_joint_interpolator.response);
      ROS_INFO("%s: Done calling interpolator management!", m_nh.getNamespace().c_str());
    }
    else
    {
      ROS_WARN_THROTTLE(1.0, "%s: Interpolator (%s) management service not found at %s!", m_nh.getNamespace().c_str(), m_interpolator.c_str(), interpolator_management.c_str());
      success = false;
    }

    if(req.owned_joints.size() == 0)
    {
      m_muted = true;
    }
    else
    {
      m_muted = false;
    }

    res.release_remaining_joints = false;
    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return success;
  }

  void JointPositionControl::getGains()
  {
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/p", m_kps[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/v", m_kvs[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/i", m_kis[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/i_clamp", m_i_clamps[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/i_liveband", m_i_livebands[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/compliance", m_compliances[i]);
    }
  }

  double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  void JointPositionControl::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    matec_utils::behaviorHeartbeat(); //TODO: param
    if(m_muted)
    {
      return; //TODO: fully unsubscribe so we don't waste processing
    }

    boost::mutex::scoped_lock lock(m_mutex);
    matec_msgs::FullJointStates carrot;
    if(!m_joint_carrot_sub.getCurrentMessage(carrot))
    {
      ROS_WARN_THROTTLE(1.0, "%s: No carrot received (%s)!", m_nh.getNamespace().c_str(), m_nh.resolveName("/" + m_interpolator + "/carrot").c_str());
      return;
    }

    if(carrot.position.size() != m_controlled_joints.size())
    {
      ROS_ERROR("%s: Received position control carrot that was the wrong size (got %d, expected %d)", m_nh.getNamespace().c_str(), (int ) carrot.position.size(), (int ) m_controlled_joints.size());
      return;
    }

//    matec_msgs::FullJointStates error;
//    std_msgs::Float64 total_error;
//    std_msgs::Float64 max_error;
//    std_msgs::Int32 worst_joint;
//    total_error.data = 0;
//    max_error.data = 0;
//    worst_joint.data = -1;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      int msg_idx = m_joint_map[i];
      unsigned int carrot_idx;

      if(carrot.joint_indices.size() == 0)
      {
        ROS_ERROR("%s: Interpolator %s did not provide joint indices!", m_nh.getNamespace().c_str(), m_interpolator.c_str());
        return;
      }
      else
      {
        carrot_idx = std::find(carrot.joint_indices.begin(), carrot.joint_indices.end(), msg_idx) - carrot.joint_indices.begin();
        if(carrot_idx == carrot.joint_indices.size())
        {
          ROS_ERROR("%s: Carrot did not provide a setpoint for joint %s (%d)!", m_nh.getNamespace().c_str(), m_joint_names[msg_idx].c_str(), msg_idx);
          return;
        }
      }

//      ROS_INFO("%s=>sm %d, carrot %d, jpc %d", m_joint_names[msg_idx].c_str(), msg_idx, carrot_idx, i);

      double pos_error = carrot.position[carrot_idx] - msg.position[msg_idx];
      double vel_error = carrot.velocity[carrot_idx] - msg.velocity[msg_idx];

      if(fabs(pos_error) <= m_i_livebands[i])
      {
        m_i_accumulators[i] = clamp(m_i_accumulators[i] + pos_error, -m_i_clamps[i], m_i_clamps[i]);
      }
      else
      {
        m_i_accumulators[i] = 0;
      }

      double feedback = m_kps[i] * pos_error + m_kvs[i] * vel_error + m_kis[i] * m_i_accumulators[i];
      m_command.feedback.acceleration[i] = feedback;
      m_command.feedforward.acceleration[i] = carrot.acceleration[carrot_idx];
      m_command.feedforward.velocity[i] = carrot.velocity[carrot_idx];
      m_command.feedforward.position[i] = carrot.position[carrot_idx];
      m_command.feedforward.compliance[i] = m_compliances[i];
//      error.position.push_back(pos_error);
//      error.velocity.push_back(vel_error);
//      error.acceleration.push_back(feedback);

//      total_error.data += fabs(pos_error) * 180.0 / M_PI;
//      if(fabs(pos_error * 180.0 / M_PI) > max_error.data)
//      {
//        max_error.data = fabs(pos_error * 180.0 / M_PI);
//        worst_joint.data = msg_idx;
//      }
    }

    m_behavior_pub.publish(m_command);
//    m_error_pub.publish(error);
//    m_total_error_pub.publish(total_error);
//    m_max_error_pub.publish(max_error);
  }

  void JointPositionControl::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "joint_space_control");
  ros::NodeHandle nh("~");

  joint_space_control::JointPositionControl node(nh);
  node.spin();

  return 0;
}
