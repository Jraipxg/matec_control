#ifndef joint_space_control_H
#define joint_space_control_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/StringArray.h>
#include <matec_msgs/DynamicsCommand.h>
#include <boost/thread.hpp>

#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>

#include "matec_utils/common_functions.h"
#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/ManageBehavior.h"
#include "matec_msgs/ManageJointInterpolator.h"
#include "matec_msgs/RegisterBehavior.h"
#include "matec_msgs/AwakenBehavior.h"
#include "matec_msgs/RequestJointOwnershipChange.h"

namespace joint_space_control
{
  class JointPositionControl
  {
  public:
    JointPositionControl(const ros::NodeHandle& nh);
    ~JointPositionControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_interpolator;
    bool m_base_behavior;
    double m_default_kp;
    double m_default_kv;
    double m_default_ki;
    double m_default_i_clamp;
    double m_default_i_liveband;
    double m_default_i_accumulator;
    double m_default_compliance;

    bool m_muted;

    ros::Publisher m_error_pub;
    ros::Publisher m_total_error_pub;
    ros::Publisher m_max_error_pub;
    ros::Publisher m_worst_joint_pub;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_owned_joints;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    matec_msgs::DynamicsCommand m_command;

    ros::ServiceServer m_manage_service_server;
    ros::ServiceServer m_awaken_service_server;

    std::vector<double> m_kps;
    std::vector<double> m_kvs;
    std::vector<double> m_kis;
    std::vector<double> m_i_clamps;
    std::vector<double> m_i_livebands;
    std::vector<double> m_i_accumulators;
    std::vector<double> m_compliances;

    boost::mutex m_mutex;

    shared_memory_interface::Publisher<matec_msgs::DynamicsCommand> m_behavior_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_carrot_sub;

    void getGains();
    void initializeCommand();

//    void tare(std::vector<int> subset = std::vector<int>());
    bool managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res);
    bool awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res);

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //joint_space_control_H
