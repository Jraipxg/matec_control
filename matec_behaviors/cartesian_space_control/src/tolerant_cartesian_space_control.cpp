#include "cartesian_space_control/tolerant_cartesian_space_control.h"

namespace cartesian_space_control
{
  TolerantCartesianPoseControl::TolerantCartesianPoseControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_muted = true;

    srand(time(NULL));
    matec_utils::blockOnSMJointNames(m_joint_names);
    matec_utils::blockOnBehaviorRegistration(m_nh.getNamespace());
    matec_utils::blockOnROSTime();

    m_nh.param("loop_rate", m_loop_rate, 2.0);
    m_nh.param("interpolator", m_interpolator, std::string(m_nh.getNamespace()));

    m_nh.param("controlled_frame", m_controlled_frame, std::string(""));

    if(m_controlled_frame.length() == 0)
    {
      ROS_FATAL("%s: controlled_frame must be specified for TolerantCartesianPoseControl to work!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints, m_joint_names))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointPositionControl couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }

    initModel();
    findNearestActuatedLink(m_controlled_frame);
    findLongestControlChain();
    m_chain_base_frame = getChainBase(m_chain);

    double position_filter_frequency = 100.0; //todo: param
    double velocity_filter_frequency = 100.0; //todo: param
    m_position_filters.resize(m_chain.getNrOfJoints(), Biquad(bq_type_lowpass, position_filter_frequency / 1000.0, 0.7071, 6));
    m_velocity_filters.resize(m_chain.getNrOfJoints(), Biquad(bq_type_lowpass, velocity_filter_frequency / 1000.0, 0.7071, 6));

    m_velocity_median_filters.resize(m_chain.getNrOfJoints(), estimators::MedianFilter<double, std::less<double> >(3));

    matec_utils::getFPVector(m_nh, "controlled_joint_weights", m_controlled_joints.size(), 1.0, m_controlled_joint_weights);
    m_cartesian_weights.resize(6, 0.0);
    unpackPositionTolerance(Config::CENTIMETER);
    unpackOrientationTolerance(Config::SPHERE);
    m_fwd_vel_solver = new KDL::ChainFkSolverVel_recursive(m_chain);
    m_fwd_pos_solver = new KDL::ChainFkSolverPos_recursive(m_chain);
    m_inv_vel_solver = new KDL::ChainIkSolverVel_wdls(m_chain);
    configureWeights();

    m_field_names.push_back("p_x");
    m_field_names.push_back("p_y");
    m_field_names.push_back("p_z");
    m_field_names.push_back("r_x");
    m_field_names.push_back("r_y");
    m_field_names.push_back("r_z");
    m_kps.resize(m_field_names.size(), 0.0);
    m_kvs.resize(m_field_names.size(), 0.0);
    m_kis.resize(m_field_names.size(), 0.0);
    m_i_clamps.resize(m_field_names.size(), 0.0);
    m_i_livebands.resize(m_field_names.size(), 0.0);
    m_i_accumulators.resize(m_field_names.size(), 0.0);
    getGains();

    m_current_pub = m_nh.advertise<geometry_msgs::PoseStamped>("measured_cartesian_pose", 1, true);
    m_current_commanded_pub = m_nh.advertise<geometry_msgs::PoseStamped>("commanded_cartesian_pose", 1, true);
    m_target_pub = m_nh.advertise<geometry_msgs::PoseStamped>("target_cartesian_pose", 1, true);

    m_first_callback = true;
    m_last_deltas = KDL::JntArray(m_chain.getNrOfJoints());
    m_last_positions = KDL::JntArray(m_chain.getNrOfJoints());
    m_last_velocities = KDL::JntArray(m_chain.getNrOfJoints());

    m_manage_service_server = m_nh.advertiseService("manage", &TolerantCartesianPoseControl::managementCallback, this);
    m_awaken_service_server = m_nh.advertiseService("awaken", &TolerantCartesianPoseControl::awakenCallback, this);

    m_command_pub.advertise("behavior_command");
    m_pose_carrot_sub.subscribe("/" + m_interpolator + "/carrot");
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&TolerantCartesianPoseControl::sensorCallback, this, _1));

    m_config_server = m_nh.advertiseService("configure", &TolerantCartesianPoseControl::configureCallback, this);

    if(m_base_behavior) //auto-wake base behaviors on startup
    {
      matec_msgs::AwakenBehavior awaken;
      awakenCallback(awaken.request, awaken.response);
    }
  }

  TolerantCartesianPoseControl::~TolerantCartesianPoseControl()
  {

  }

  void TolerantCartesianPoseControl::initializeCommand()
  {
    m_command.feedforward.joint_indices = m_joint_map;
    m_command.feedforward.position.resize(m_controlled_joints.size());
    m_command.feedforward.velocity.resize(m_controlled_joints.size());
    m_command.feedforward.acceleration.resize(m_controlled_joints.size());
    m_command.feedforward.torque.resize(m_controlled_joints.size());
  }

  bool TolerantCartesianPoseControl::awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res)
  {
    std::vector<unsigned char> required_actuators;
    if(req.actuators.size() != 0)
    {
      required_actuators = req.actuators;
    }
    else
    {
      required_actuators = m_joint_map;
    }

    res.awake = true;
    for(unsigned int i = 0; i < required_actuators.size(); i++)
    {
      if(std::find(m_owned_joints.begin(), m_owned_joints.end(), m_joint_names[required_actuators[i]]) == m_owned_joints.end())
      {
        ROS_INFO("%s: Need joint %s! Requesting ownership!", m_nh.getNamespace().c_str(), m_joint_names[required_actuators[i]].c_str());
        res.awake = false;
      }
    }

    if(res.awake)
    {
      return true;
    }
    else
    {
      return matec_utils::requestOwnership(required_actuators);
    }
  }

  //TODO: implement sleep, reject if base behavior

  bool TolerantCartesianPoseControl::managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    matec_utils::indicesToNames(m_joint_names, req.owned_joints, m_owned_joints);

    if(m_owned_joints.size() == m_controlled_joints.size())
    {
      for(unsigned int i = 0; i < req.newly_owned_joints.size(); i++)
      {
        unsigned int control_idx = std::find(m_joint_map.begin(), m_joint_map.end(), req.newly_owned_joints[i]) - m_joint_map.begin();
        if(control_idx < m_controlled_joints.size())
        {
          m_last_positions(control_idx) = req.last_command.position[req.newly_owned_joints[i]];
          m_last_velocities(control_idx) = 0.0;
        }
        else
        {
          ROS_WARN("Tare requested for uncontrolled joint %s", m_joint_names[req.newly_owned_joints[i]].c_str());
        }
      }
      send(m_last_positions, m_last_velocities);
      res.release_remaining_joints = false;
    }
    else if(m_owned_joints.size() != 0)
    {
      ROS_WARN("%s: Lost control of too many joints! Giving up control of the rest!", m_nh.getNamespace().c_str());
      res.release_remaining_joints = true;
      return true;
    }

    //forward to interpolator
    bool success = true;
    std::string interpolator_management = "/" + m_interpolator + "/manage";
    if(ros::service::exists(interpolator_management, false))
    {
      matec_msgs::ManageCartesianInterpolator manage_cartesian_interpolator;
      KDL::Frame commanded_frame;
      m_fwd_pos_solver->JntToCart(m_last_positions, commanded_frame);
      tf::poseKDLToMsg(commanded_frame, manage_cartesian_interpolator.request.new_setpoint.pose);
      manage_cartesian_interpolator.request.new_setpoint.header.frame_id = m_chain_base_frame;
      manage_cartesian_interpolator.request.new_setpoint.header.stamp = ros::Time::now();
      success = ros::service::call(interpolator_management, manage_cartesian_interpolator.request, manage_cartesian_interpolator.response);
    }
    else
    {
      ROS_WARN_THROTTLE(1.0, "%s: Interpolator (%s) management service not found!", m_nh.getNamespace().c_str(), m_interpolator.c_str());
      success = false;
    }

    if(m_owned_joints.size() != m_controlled_joints.size())
    {
      //TODO: just change weights if we still have some joints left?
      m_muted = true;
    }
    else
    {
      m_muted = false;
    }

    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return success;
  }

  void TolerantCartesianPoseControl::findNearestActuatedLink(std::string frame)
  {
    boost::shared_ptr<const urdf::Link> link = m_urdf_model.getLink(frame);
    if(link)
    {
      m_last_controlled_link = frame;
    }
    else
    {
      std::string parent;
      while(ros::ok() && !m_tf_listener.getParent(frame, ros::Time(0), parent))
      {
        ROS_WARN_THROTTLE(1.0, "%s is waiting for tf to find the parent of %s", m_nh.getNamespace().c_str(), frame.c_str());
      }

      findNearestActuatedLink(parent);
    }
  }

  bool TolerantCartesianPoseControl::configureCallback(matec_msgs::ConfigureCartesianControl::Request& req, matec_msgs::ConfigureCartesianControl::Response& res)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    if(req.joint_weights.size() == m_controlled_joints.size())
    {
      m_controlled_joint_weights = req.joint_weights;
    }

    unpackPositionTolerance(req.allowable_position_variance);
    unpackOrientationTolerance(req.allowable_angle_variance);

    configureWeights();
    return true;
  }

  void TolerantCartesianPoseControl::unpackPositionTolerance(int preset)
  {
    switch(preset)
    {
    case Config::USE_CURRENT_POSITION_VARIANCE:
    {
      return;
    }
    case Config::MILLIMETER:
    {
      m_tolerance.x = 0.001;
      m_tolerance.y = 0.001;
      m_tolerance.z = 0.001;
      m_cartesian_weights[0] = 1.0;
      m_cartesian_weights[1] = 1.0;
      m_cartesian_weights[2] = 1.0;
      return;
    }
    case Config::CENTIMETER:
    {
      m_tolerance.x = 0.01;
      m_tolerance.y = 0.01;
      m_tolerance.z = 0.01;
      m_cartesian_weights[0] = 1.0;
      m_cartesian_weights[1] = 1.0;
      m_cartesian_weights[2] = 1.0;
      return;
    }
    case Config::DECIMETER:
    {
      m_tolerance.x = 0.1;
      m_tolerance.y = 0.1;
      m_tolerance.z = 0.1;
      m_cartesian_weights[0] = 0.1;
      m_cartesian_weights[1] = 0.1;
      m_cartesian_weights[2] = 0.1;
      return;
    }
    default:
    {
      ROS_ERROR("Unknown position preset requested!");
      return;
    }
    }
  }

  void TolerantCartesianPoseControl::unpackOrientationTolerance(int preset)
  {
    switch(preset)
    {
    case Config::USE_CURRENT_ANGLE_VARIANCE:
    {
      return;
    }
    case Config::FULL_CYLINDER:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::HALF_CYLINDER:
    {
      m_tolerance.roll = M_PI / 2;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.01;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::QUARTER_CYLINDER:
    {
      m_tolerance.roll = M_PI / 4;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.05;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::EIGHTH_CYLINDER:
    {
      m_tolerance.roll = M_PI / 8;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::SIXTEENTH_CYLINDER:
    {
      m_tolerance.roll = M_PI / 16;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::SIXTEENTH_CONE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 16;
      m_tolerance.yaw = M_PI / 16;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::EIGHTH_CONE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 8;
      m_tolerance.yaw = M_PI / 8;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::QUARTER_CONE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 4;
      m_tolerance.yaw = M_PI / 4;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.05;
      m_cartesian_weights[5] = 0.05;
      return;
    }
    case Config::HEMISPHERE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 2;
      m_tolerance.yaw = M_PI / 2;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.01;
      m_cartesian_weights[5] = 0.01;
      return;
    }
    case Config::SPHERE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI;
      m_tolerance.yaw = M_PI;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.0;
      m_cartesian_weights[5] = 0.0;
      return;
    }
    case Config::EXACT_ANGLE:
    {
      m_tolerance.roll = EPSILON_ANGLE;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::SIXTEENTH:
    {
      m_tolerance.roll = M_PI / 16;
      m_tolerance.pitch = M_PI / 16;
      m_tolerance.yaw = M_PI / 16;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::EIGHTH:
    {
      m_tolerance.roll = M_PI / 8;
      m_tolerance.pitch = M_PI / 8;
      m_tolerance.yaw = M_PI / 8;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::QUARTER:
    {
      m_tolerance.roll = M_PI / 4;
      m_tolerance.pitch = M_PI / 4;
      m_tolerance.yaw = M_PI / 4;
      m_cartesian_weights[3] = 0.05;
      m_cartesian_weights[4] = 0.05;
      m_cartesian_weights[5] = 0.05;
      return;
    }
    case Config::HALF:
    {
      m_tolerance.roll = M_PI / 2;
      m_tolerance.pitch = M_PI / 2;
      m_tolerance.yaw = M_PI / 2;
      m_cartesian_weights[3] = 0.01;
      m_cartesian_weights[4] = 0.01;
      m_cartesian_weights[5] = 0.01;
      return;
    }
    default:
    {
      ROS_ERROR("Unknown orientation preset requested!");
      return;
    }
    }
  }

  void TolerantCartesianPoseControl::configureWeights()
  {
    //figure out what parts of the chain we can actually use and weight the solver accordingly
    Eigen::MatrixXd cartesian_weight_matrix = Eigen::MatrixXd::Identity(6, 6);

    if(m_cartesian_weights.size() == 6)
    {
      for(unsigned int i = 0; i < 6; i++)
      {
        cartesian_weight_matrix(i, i) = m_cartesian_weights[i];
      }
    }

    Eigen::MatrixXd joint_weight_matrix = Eigen::MatrixXd::Identity(m_chain.getNrOfJoints(), m_chain.getNrOfJoints());
    for(unsigned int i = 0; i < m_chain.getNrOfSegments(); i++) //possibly need to decrease this range by 1
    {
      std::string joint_name = m_chain.getSegment(i).getJoint().getName();
      ROS_DEBUG("Processing segment %d with joint %s", i, joint_name.c_str());

      unsigned int controlled_joint_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), joint_name) - m_controlled_joints.begin();
      if(controlled_joint_idx == m_controlled_joints.size())
      {
        joint_weight_matrix(i, i) = 0; //weight 0 ==> don't move the joint
        ROS_DEBUG("Joint %s is not controllable.", joint_name.c_str());
      }
      else
      {
        assert(controlled_joint_idx < m_controlled_joints.size());
        assert(m_controlled_joints.size() == m_controlled_joint_weights.size());
        joint_weight_matrix(i, i) = m_controlled_joint_weights[controlled_joint_idx];
        ROS_DEBUG("Joint %s is controllable.", joint_name.c_str());
      }
    }
    m_inv_vel_solver->setWeightJS(joint_weight_matrix);
    m_inv_vel_solver->setWeightTS(cartesian_weight_matrix);
  }

  void TolerantCartesianPoseControl::findLongestControlChain()
  {
    //NOTE: currently assuming that all controlled joints are in the same chain!
    unsigned int longest_chain_length = 0;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      if(std::find(m_joint_names.begin(), m_joint_names.end(), m_controlled_joints[i]) == m_joint_names.end())
      {
        ROS_FATAL("Tried to control a joint that the SMI didn't know about! Shutting down!");
        ros::shutdown();
      }

      std::string parent_frame = m_urdf_model.getJoint(m_controlled_joints[i])->parent_link_name;

      KDL::Chain chain;
      m_tree.getChain(parent_frame, m_last_controlled_link, chain);

      if(chain.getNrOfJoints() > longest_chain_length)
      {
        longest_chain_length = chain.getNrOfJoints();
        m_chain = chain;
      }
    }

    ROS_INFO("Using chain of %d joints from %s to %s", m_chain.getNrOfJoints(), m_chain.getSegment(0).getJoint().getName().c_str(), m_chain.getSegment(m_chain.getNrOfSegments() - 1).getJoint().getName().c_str());
  }

  bool TolerantCartesianPoseControl::addControlledFrameOffsetToChain(KDL::Chain& chain)
  {
    tf::StampedTransform controlled_frame_offset;
    if(m_tf_listener.canTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0)) && m_tf_listener.canTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0)))
    {
      m_tf_listener.lookupTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0), controlled_frame_offset);
    }
    else
    {
      ROS_ERROR("Couldn't find transform from the last controllable link (%s) and the controlled frame (%s)", m_last_controlled_link.c_str(), m_controlled_frame.c_str());
      return false;
    }

    KDL::Frame fixed_offset;
    tf::transformTFToKDL(controlled_frame_offset.inverse(), fixed_offset);
    KDL::Segment fixedSegment = KDL::Segment(m_controlled_frame, KDL::Joint(m_controlled_frame + "_fixed", KDL::Joint::None), fixed_offset);
    chain.addSegment(fixedSegment);

    return true;
  }

  void TolerantCartesianPoseControl::getGains()
  {
    m_nh.param("gains/center_spring", m_center_spring, 0.0);
    for(unsigned int i = 0; i < m_field_names.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/p", m_kps[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/v", m_kvs[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i", m_kis[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i_clamp", m_i_clamps[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i_liveband", m_i_livebands[i]);
    }

    m_nh.param("max_joint_velocity", m_max_velocity, 0.5);
    m_nh.param("num_whole_range_attempts", m_num_whole_range_attempts, 25);
    m_nh.param("num_best_attempts", m_num_best_attempts, 25);
  }

  double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  double sign(double v)
  {
    if(v > 0.0)
    {
      return 1.0;
    }
    else if(v < 0.0)
    {
      return -1.0;
    }
    else
    {
      return 0.0;
    }
  }

  std::vector<double> extractPoseVector(geometry_msgs::PoseStamped pose)
  {
    double roll, pitch, yaw;
    tf::Quaternion quat;
    tf::quaternionMsgToTF(pose.pose.orientation, quat);
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

    std::vector<double> vector; //should match m_field_names
    vector.push_back(pose.pose.position.x);
    vector.push_back(pose.pose.position.y);
    vector.push_back(pose.pose.position.z);
    vector.push_back(roll);
    vector.push_back(pitch);
    vector.push_back(yaw);

    return vector;
  }

  std::vector<double> extractPoseVector(KDL::FrameVel frame_vel)
  {
    std::vector<double> vector;
    vector.push_back(frame_vel.p.v.x());
    vector.push_back(frame_vel.p.v.y());
    vector.push_back(frame_vel.p.v.z());
    vector.push_back(frame_vel.M.w.x());
    vector.push_back(frame_vel.M.w.y());
    vector.push_back(frame_vel.M.w.z());
    return vector;
  }

  KDL::Twist poseVectorToTwist(std::vector<double> pose_vector)
  {
    assert(pose_vector.size() == 6);
    return KDL::Twist(KDL::Vector(pose_vector[0], pose_vector[1], pose_vector[2]), KDL::Vector(pose_vector[3], pose_vector[4], pose_vector[5]));
  }

  KDL::JntArray stdToKDL(std::vector<double> array, std::vector<unsigned char> indices = std::vector<unsigned char>())
  {
    if(indices.size() > 0) //use subset of array
    {
      KDL::JntArray kdl_array(indices.size());
      for(unsigned int i = 0; i < indices.size(); i++)
      {
        kdl_array(i) = array[indices[i]];
      }
      return kdl_array;
    }
    else //use whole array
    {
      KDL::JntArray kdl_array(array.size());
      for(unsigned int i = 0; i < array.size(); i++)
      {
        kdl_array(i) = array[i];
      }
      return kdl_array;
    }
  }

  std::vector<double> KDLToStd(KDL::JntArray kdl_array, std::vector<unsigned char> indices = std::vector<unsigned char>())
  {
    std::vector<double> array;
    if(indices.size() > 0) //use subset of array
    {
      for(unsigned int i = 0; i < indices.size(); i++)
      {
        assert(!isnan(kdl_array(indices[i])));
        array.push_back(kdl_array(indices[i]));
      }
    }
    else //use whole array
    {
      for(unsigned int i = 0; i < kdl_array.rows() * kdl_array.columns(); i++)
      {
        assert(!isnan(kdl_array(i)));
        array.push_back(kdl_array(i));
      }
    }
    return array;
  }

  double smashSmall(double val)
  {
    if(val <= 1)
    {
      return 0;
    }
    return val - 1.0;
  }

  double scoreFrame(KDL::Frame iter, KDL::Frame target, Tolerance tol)
  {
    KDL::Frame frame_shift = target.Inverse() * iter;
    double delta_roll, delta_pitch, delta_yaw;
    frame_shift.M.GetRPY(delta_roll, delta_pitch, delta_yaw);

//    ROS_INFO("(%g,%g,%g):(%g,%g,%g)", tol.x, tol.y, tol.z, tol.roll, tol.pitch, tol.yaw);

    double x_score = smashSmall(std::abs(frame_shift.p.x()) / tol.x);
    double y_score = smashSmall(std::abs(frame_shift.p.y()) / tol.y);
    double z_score = smashSmall(std::abs(frame_shift.p.z()) / tol.z);
    double roll_score = smashSmall(std::abs(delta_roll) / tol.roll);
    double pitch_score = smashSmall(std::abs(delta_pitch) / tol.pitch);
    double yaw_score = smashSmall(std::abs(delta_yaw) / tol.yaw);

    return (x_score + y_score + z_score + roll_score + pitch_score + yaw_score) / 6;
  }

  double scoreMovement(KDL::JntArray start, KDL::JntArray iter)
  {
    double sum = 0;

    assert(start.rows()*start.columns() == iter.rows()*iter.columns());

    for(unsigned int i = 0; i < start.rows() * start.columns(); i++)
    {
      double err = iter(i) - start(i);
      sum += err * err;
    }

    return sum;
  }

  double scoreOscillation(KDL::JntArray start, KDL::JntArray iter)
  {
    double sum = 0;
    assert(start.rows()*start.columns() == iter.rows()*iter.columns());

    for(unsigned int i = 0; i < start.rows() * start.columns(); i++)
    {
      double err = iter(i) - start(i);
      sum += err * err;
    }

    return sum;
  }

  bool TolerantCartesianPoseControl::getKDLSolution(matec_msgs::FullJointStates& current, geometry_msgs::PoseStamped pose_carrot, geometry_msgs::PoseStamped current_pose, KDL::JntArray& solution)
  {
    KDL::JntArray q, qd;
    q = stdToKDL(current.position, m_joint_map);
    qd = stdToKDL(current.velocity, m_joint_map);
    KDL::JntArrayVel q_qd(q, qd);
    KDL::FrameVel frame_vel;
    m_fwd_vel_solver->JntToCart(q_qd, frame_vel);

    std::vector<double> current_pose_velocity_vector = extractPoseVector(frame_vel);
    std::vector<double> current_pose_vector = extractPoseVector(current_pose);
    std::vector<double> carrot_pose_vector = extractPoseVector(pose_carrot);

    std::vector<double> control_pose_vector(carrot_pose_vector.size());
    std::vector<double> error_pose_vector(carrot_pose_vector.size());

    for(unsigned int i = 0; i < m_field_names.size(); i++)
    {
      double pos_error = carrot_pose_vector[i] - current_pose_vector[i];
      double vel_error = 0 - current_pose_velocity_vector[i]; //TODO: feed forward velocity and acceleration?

      if(fabs(pos_error) <= m_i_livebands[i])
      {
        m_i_accumulators[i] = clamp(m_i_accumulators[i] + pos_error, -m_i_clamps[i], m_i_clamps[i]);
      }
      else
      {
        m_i_accumulators[i] = 0;
      }

      control_pose_vector[i] = m_kps[i] * pos_error + m_kvs[i] * vel_error + m_kis[i] * m_i_accumulators[i];
      error_pose_vector[i] = pos_error;
    }

    KDL::JntArray qd_soln;
    if(m_inv_vel_solver->CartToJnt(q, poseVectorToTwist(control_pose_vector), qd_soln) != 0)
    {
      return false;
    }

    double max_output_vel = 0.001; //nonzero to prevent division by zero
    for(unsigned int i = 0; i < qd_soln.rows(); i++)
    {
      if(fabs(qd_soln(i)) > max_output_vel)
      {
        max_output_vel = fabs(qd_soln(i));
      }
    }

    double scale_factor = std::min(1.0, m_max_velocity / max_output_vel);

    solution = q;
    for(unsigned int i = 0; i < solution.rows(); i++)
    {
      solution(i) += scale_factor * qd_soln(i) * m_dt; //todo: use trapezoidal integration
    }

    return true;
  }

  void TolerantCartesianPoseControl::getSearchBounds(KDL::JntArray& current, double dt, double max_velocity, std::vector<double>& mins, std::vector<double>& maxes)
  {
    assert(m_controlled_joint_mins.size() == m_joint_map.size());
    assert(m_controlled_joint_maxes.size() == m_joint_map.size());
    mins = m_controlled_joint_mins;
    maxes = m_controlled_joint_maxes;
    for(unsigned int i = 0; i < m_chain.getNrOfJoints(); i++)
    {
      current(i) = clamp(current(i), mins[i], maxes[i]); //fix things that shouldn't have to be fixed...

      mins[i] = std::max(mins[i], current(i) - m_controlled_joint_weights[i] * max_velocity * dt);
      maxes[i] = std::min(maxes[i], current(i) + m_controlled_joint_weights[i] * max_velocity * dt);

      ROS_DEBUG("Bounding %s between %g and %g", m_controlled_joints[i].c_str(), mins[i], maxes[i]);
    }
  }

  std::string TolerantCartesianPoseControl::getChainBase(KDL::Chain& chain)
  {
    return m_urdf_model.getJoint(m_chain.getSegment(0).getJoint().getName())->parent_link_name;
  }

  bool TolerantCartesianPoseControl::moveCarrotToChainBaseFrame(geometry_msgs::PoseStamped& pose_carrot)
  {
    if(!m_tf_listener.canTransform(m_chain_base_frame, pose_carrot.header.frame_id, pose_carrot.header.stamp))
    {
      pose_carrot.header.stamp = ros::Time(0);
    }
    if(!m_tf_listener.canTransform(m_chain_base_frame, pose_carrot.header.frame_id, pose_carrot.header.stamp))
    {
      ROS_ERROR("Can't transform from %s to %s at any time!", m_chain_base_frame.c_str(), pose_carrot.header.frame_id.c_str());
      return false;
    }
    m_tf_listener.transformPose(m_chain_base_frame, pose_carrot, pose_carrot);
    return true;
  }

  bool TolerantCartesianPoseControl::replaceIfBetter(KDL::JntArray& iter_joints, KDL::JntArray& current_joints, KDL::Frame& target_frame, KDL::JntArray& best_joints, KDL::Frame& best_frame, double& best_accuracy_score, double& best_movement_score)
  {
    KDL::Frame iter_frame;
    m_fwd_pos_solver->JntToCart(iter_joints, iter_frame);
    double accuracy_score = scoreFrame(iter_frame, target_frame, m_tolerance);
    if(accuracy_score <= best_accuracy_score)
    {
      double movement_score = scoreMovement(current_joints, iter_joints);
      if((accuracy_score < best_accuracy_score) || (movement_score < best_movement_score))
      {
        best_accuracy_score = accuracy_score;
        best_movement_score = movement_score;
        best_joints = iter_joints;
        best_frame = iter_frame;
        return true;
      }
    }
    return false;
  }

  void TolerantCartesianPoseControl::send(KDL::JntArray positions, KDL::JntArray velocities)
  {
    matec_msgs::DynamicsCommand behavior_command;
    behavior_command.feedforward.joint_indices = m_joint_map;
    behavior_command.feedforward.position = KDLToStd(positions);
    behavior_command.feedforward.velocity = KDLToStd(velocities);
    assert(behavior_command.feedforward.joint_indices.size() == behavior_command.feedforward.position.size());
    for(int i = m_controlled_joint_weights.size() - 1; i >= 0; --i)
    {
      if(m_controlled_joint_weights[i] == 0)
      {
        behavior_command.feedforward.joint_indices.erase(behavior_command.feedforward.joint_indices.begin() + i);
        behavior_command.feedforward.position.erase(behavior_command.feedforward.position.begin() + i);
        behavior_command.feedforward.velocity.erase(behavior_command.feedforward.velocity.begin() + i);
      }
    }
    m_command_pub.publish(behavior_command);
  }

  void TolerantCartesianPoseControl::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    if(m_muted)
    {
      return;
    }

    boost::mutex::scoped_lock lock(m_mutex);

    geometry_msgs::PoseStamped pose_carrot;
    if(!m_pose_carrot_sub.getCurrentMessage(pose_carrot))
    {
      ROS_WARN_THROTTLE(0.5, "%s: No carrot received!", m_nh.getNamespace().c_str());
      return;
    }

    //TODO: do this better
    KDL::Chain augmented_chain = m_chain;
    if(!addControlledFrameOffsetToChain(augmented_chain))
    {
      return;
    }
    delete m_fwd_vel_solver;
    delete m_fwd_pos_solver;
    delete m_inv_vel_solver;
    m_fwd_vel_solver = new KDL::ChainFkSolverVel_recursive(augmented_chain);
    m_fwd_pos_solver = new KDL::ChainFkSolverPos_recursive(augmented_chain);
    m_inv_vel_solver = new KDL::ChainIkSolverVel_wdls(augmented_chain);
    configureWeights();

    KDL::Frame current_frame, target_frame, commanded_frame;
    if(!moveCarrotToChainBaseFrame(pose_carrot))
    {
      return;
    }
    tf::poseMsgToKDL(pose_carrot.pose, target_frame);
    m_target_pub.publish(pose_carrot);

    KDL::JntArray current_joints = stdToKDL(msg.position, m_joint_map);
    if(m_first_callback)
    {
      m_last_positions = current_joints;
      m_last_velocities.data = Eigen::VectorXd::Zero(m_last_positions.rows() * m_last_positions.columns());
      m_dt = 1.0 / 1000.0;
    }
    else
    {
      m_dt = msg.header.stamp.toSec() - m_last_timestamp; //TODO: calculate average dt
      if(m_dt == 0.0)
      {
        ROS_DEBUG("%s GOT DUPLICATE_TIMESTAMP!!", m_nh.getNamespace().c_str());
        return;
      }
    }

    geometry_msgs::PoseStamped current_pose, commanded_pose;
    m_fwd_pos_solver->JntToCart(current_joints, current_frame);
    tf::poseKDLToMsg(current_frame, current_pose.pose);
    current_pose.header.frame_id = m_chain_base_frame;
    current_pose.header.stamp = ros::Time::now();
    m_current_pub.publish(current_pose);

    m_fwd_pos_solver->JntToCart(m_last_positions, commanded_frame);
    tf::poseKDLToMsg(commanded_frame, commanded_pose.pose);
    commanded_pose.header.frame_id = m_chain_base_frame;
    commanded_pose.header.stamp = ros::Time::now();
    m_current_commanded_pub.publish(commanded_pose);

    std::vector<double> min_bounds, max_bounds;
    getSearchBounds(m_last_positions, m_dt, m_max_velocity, min_bounds, max_bounds);
    //    getSearchBounds(current_joints, m_dt, m_max_velocity, min_bounds, max_bounds);//TODO: set a flag to make sure we don't do this all the time

    //make sure everything is bounded
    for(unsigned int i = 0; i < augmented_chain.getNrOfJoints(); i++)
    {
      current_joints(i) = clamp(current_joints(i), min_bounds[i], max_bounds[i]);
      m_last_positions(i) = clamp(m_last_positions(i), min_bounds[i], max_bounds[i]);
    }

    //begin the search
    KDL::JntArray best_joints = current_joints;
    KDL::JntArray iter_joints = current_joints;
    KDL::Frame best_frame;
    double best_accuracy_score = std::numeric_limits<double>::max();
    double best_movement_score = std::numeric_limits<double>::max();

    //see if we're already good enough
    replaceIfBetter(m_last_positions, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

    if(best_accuracy_score == 0) //everything is already within tolerance! No need to move!
    {
      m_last_velocities.data = Eigen::VectorXd::Zero(m_last_positions.rows() * m_last_positions.columns());
      send(m_last_positions, m_last_velocities);
      return;
    }

    bool stagnating = true;

    //see what happens if we just move the same amount we did last time from the current positions
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      iter_joints(i) = clamp(best_joints(i) + m_last_deltas(i), min_bounds[i], max_bounds[i]);
    }
    stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;

    //see what happens if we use kdl's solution
    if(getKDLSolution(msg, pose_carrot, current_pose, iter_joints))
    {
      //make sure we're within bounds
      for(unsigned int j = 0; j < augmented_chain.getNrOfJoints(); j++)
      {
        iter_joints(j) = clamp(iter_joints(j), min_bounds[j], max_bounds[j]);
      }
      stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;
    }

    //see what happens if we wiggle each joint up and down independently
    iter_joints = best_joints;
    for(int i = m_controlled_joints.size() - 1; i >= 0; --i)
    {
      double scaled_max_velocity = m_controlled_joint_weights[i] * m_max_velocity;
      //      iter_joints(i) = clamp(best_joints(i) - EPSILON_ANGLE, min_bounds[i], max_bounds[i]);
      //      replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_acc
      //      iter_joints(i) = clamp(best_joints(i) + EPSILON_ANGLE, min_bounds[i], max_bounds[i]);
      //      replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);
      //
      //      iter_joints(i) = clamp(best_joints(i) - EPSILON_ANGLE, min_bounds[i], max_bounds[i]);
      //      replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = clamp(best_joints(i) + scaled_max_velocity * m_dt / 2.0, min_bounds[i], max_bounds[i]);
      stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;

      iter_joints(i) = clamp(best_joints(i) - scaled_max_velocity * m_dt / 2.0, min_bounds[i], max_bounds[i]);
      stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;

      iter_joints(i) = clamp(best_joints(i) + scaled_max_velocity * m_dt, min_bounds[i], max_bounds[i]);
      stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;

      iter_joints(i) = clamp(best_joints(i) - scaled_max_velocity * m_dt, min_bounds[i], max_bounds[i]);
      stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;

      iter_joints(i) = best_joints(i);
    }

    if(stagnating)
    {
      //randomly guess over the whole range
      for(int i = 0; i < m_num_whole_range_attempts; i++)
      {
        for(unsigned int j = 0; j < augmented_chain.getNrOfJoints(); j++)
        {
          double min = min_bounds[j];
          double max = max_bounds[j];
          iter_joints(j) = (max - min) * ((double) rand()) / ((double) RAND_MAX) + min;
        }
        stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;
      }

      //randomly guess, centered on the best joint configuration
      for(int i = 0; i < m_num_best_attempts; i++)
      {
        iter_joints = best_joints;
        for(unsigned int j = 0; j < augmented_chain.getNrOfJoints(); j++)
        {
          double min = min_bounds[j];
          double max = max_bounds[j];
          iter_joints(j) += (max - min) * (2.0 * ((double) rand() / (double) RAND_MAX) - 1.0);
          iter_joints(j) = clamp(iter_joints(j), min, max);
        }
        stagnating = !replaceIfBetter(iter_joints, m_last_positions, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score) && stagnating;
      }
    }

    //remember how much we moved this time and calculate velocities
    KDL::JntArray best_velocities(augmented_chain.getNrOfJoints());
    for(unsigned int i = 0; i < augmented_chain.getNrOfJoints(); i++)
    {
//      best_joints(i) = clamp(m_position_filters[i].process(best_joints(i)), min_bounds[i], max_bounds[i]);
      m_last_deltas(i) = (best_joints(i) - m_last_positions(i));
      best_velocities(i) = m_velocity_filters[i].process((best_joints(i) - current_joints(i)) / m_dt); //clamp(m_velocity_filters[i].process(m_velocity_median_filters[i].process(m_last_deltas(i) / m_dt)), -m_max_velocity, m_max_velocity);
//      m_last_velocities(i) = m_velocity_filters[i].process(best_velocities(i));
//      m_last_positions(i) = clamp(m_last_positions(i) + m_last_velocities(i) * m_dt, min_bounds[i], max_bounds[i]);
      m_last_positions(i) = best_joints(i);
      m_last_velocities(i) = best_velocities(i);
    }

    for(unsigned int i = 0; i < augmented_chain.getNrOfJoints(); i++)
    {
      assert(best_joints(i) >= min_bounds[i]);
      assert(best_joints(i) <= max_bounds[i]);
    }
    send(m_last_positions, m_last_velocities);

    m_last_timestamp = msg.header.stamp.toSec();
    m_first_callback = false;
  }

  void TolerantCartesianPoseControl::initModel()
  {
    m_urdf_model.initParam("/robot_description");
    if(!kdl_parser::treeFromUrdfModel((const urdf::Model) m_urdf_model, m_tree))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }

    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(m_controlled_joints[i]);
      m_controlled_joint_mins.push_back(joint->limits->lower);
      m_controlled_joint_maxes.push_back(joint->limits->upper);
    }
  }

  void TolerantCartesianPoseControl::spin()
  {
    ROS_INFO("TolerantCartesianPoseControl started.");
    ros::AsyncSpinner spinner(2);
    spinner.start();

    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "tolerant_cartesian_space_control");
  ros::NodeHandle nh("~");

  cartesian_space_control::TolerantCartesianPoseControl node(nh);
  node.spin();

  return 0;
}
