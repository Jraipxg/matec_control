#ifndef CONTACT_H
#define CONTACT_H

#include "matec_utils/model_helper.h"
#include <matec_msgs/ContactSpecification.h>

namespace com_control
{
  class SlipState
  {
  public:
    SlipState()
    {
      f_x_available = std::numeric_limits<double>::max();
      f_x_over_threshold = 0.0;
      f_y_available = std::numeric_limits<double>::max();
      f_y_over_threshold = 0.0;
      f_z_available = std::numeric_limits<double>::max();
      f_z_over_threshold = 0.0;
      m_x_available = std::numeric_limits<double>::max();
      m_x_over_threshold = 0.0;
      m_y_available = std::numeric_limits<double>::max();
      m_y_over_threshold = 0.0;
      m_z_available = std::numeric_limits<double>::max();
      m_z_over_threshold = 0.0;
    }

    double f_x_available;
    double f_x_over_threshold;
    double f_y_available;
    double f_y_over_threshold;
    double f_z_available;
    double f_z_over_threshold;
    double m_x_available;
    double m_x_over_threshold;
    double m_y_available;
    double m_y_over_threshold;
    double m_z_available;
    double m_z_over_threshold;
  };

  class Contact
  {
  public:
    Contact()
    {

    }

    Contact(matec_msgs::ContactSpecification spec)
    {
      m_spec = spec;
      frame_name = m_spec.link_name;
      contact_frame_name = frame_name + "_contact";
    }

    bool willSlip(matec_utils::KDLWrenchStamped wrench, SlipState& state)
    {
      //TODO: add edge and point contact types
      switch(m_spec.contact_type)
      {
      case matec_msgs::ContactSpecification::FLAT_CONTACT:
        return flatContactWillSlip(wrench, state);
      case matec_msgs::ContactSpecification::PINCH_CONTACT:
        assert(0);
        return pinchContactWillSlip(wrench, state);
        assert(0);
      case matec_msgs::ContactSpecification::ROTATABLE_CONTACT:
        return rotatableContactWillSlip(wrench, state);
        assert(0);
      case matec_msgs::ContactSpecification::FULL_CONTACT:
        return false; //full contacts can't slip by definition
      default:
        assert(0);
        return true;
      }
    }

    matec_msgs::ContactSpecification spec()
    {
      return m_spec;
    }

    std::string frame_name;
    std::string contact_frame_name;

  private:
    //e.g. feet on the ground
    //defined with z pointing up, x and y on the plane of the surface, centered geometrically within the contacting area
    bool flatContactWillSlip(matec_utils::KDLWrenchStamped wrench, SlipState& state)
    {
      double fx = wrench.wrench.force.x();
      double fy = wrench.wrench.force.y();
      double fz = wrench.wrench.force.z();
      //      double mx = wrench.wrench.torque.x();
      //      double my = wrench.wrench.torque.y();
      double mz = wrench.wrench.torque.z();

      if(fz < 0.0) //we think the ground is pulling us down!
      {
//        ROS_INFO("Contact break because bad z");
        state.f_z_available = 0;
        state.f_z_over_threshold = 0.0 - fz;
        return true;
      }
      else
      {
        state.f_z_available = fz;
      }

      double tangent_magnitude_squared = fx * fx + fy * fy;
      double tangent_force_limit_squared = fz * fz * m_spec.linear_friction * m_spec.linear_friction;
      if(tangent_magnitude_squared > tangent_force_limit_squared) //pushing too hard tangentially
      {
//        ROS_INFO("Contact break because bad twist");
        state.f_x_available = 0;
        state.f_x_over_threshold = sqrt(tangent_force_limit_squared) - sqrt(tangent_magnitude_squared);
        state.f_y_available = 0;
        state.f_y_over_threshold = state.f_x_over_threshold;
        return true;
      }

      if(mz > (m_spec.rotational_friction * fz)) //twisting too hard tangentially
      {
//        ROS_INFO("Contact break because not enough friction");
        return true;
      }

      if(!reactionCenterValid(wrench, state))
      {
//        ROS_INFO("Contact break because bad reaction center");
        return true;
      }

      return false;
    }

    //e.g. gripping a table for increased stability
    bool pinchContactWillSlip(matec_utils::KDLWrenchStamped wrench, SlipState& state) //same as flat, but you can apply force in either Z direction
    {
      double fx = wrench.wrench.force.x();
      double fy = wrench.wrench.force.y();
      double fz = wrench.wrench.force.z();
//      double mx = wrench.wrench.torque.x();
//      double my = wrench.wrench.torque.y();
      double mz = wrench.wrench.torque.z();

      double effective_normal_force = std::max(m_spec.pinch_force, fabs(fz)); //TODO: should this be pinch_force + normal force?
      double tangent_magnitude_squared = fx * fx + fy * fy;
      double tangent_force_limit_squared = effective_normal_force * effective_normal_force * m_spec.linear_friction * m_spec.linear_friction;
      if(tangent_magnitude_squared > tangent_force_limit_squared) //pushing too hard tangentially
      {
        return true;
      }

      if(mz > (m_spec.rotational_friction * effective_normal_force)) //twisting too hard tangentially  //TODO: should this be pinch_force + normal force?
      {
        return true;
      }

      return false;
    }

    //z axis is the axis of the a cylinder centered on the axis of rotation
    //e.g. grasping a valve tightly or opening a door (cylinder is defined along axis of rotation of the valve / door)
    bool rotatableContactWillSlip(matec_utils::KDLWrenchStamped wrench, SlipState& state)
    {
      double fx = wrench.wrench.force.x();
      double fy = wrench.wrench.force.y();
//      double fz = wrench.wrench.force.z();
//      double mx = wrench.wrench.torque.x();
//      double my = wrench.wrench.torque.y();
      double mz = wrench.wrench.torque.z();

      double normal_force_magnitude_squared = fx * fx + fy * fy;
      if((mz * mz) > (m_spec.rotational_friction * m_spec.rotational_friction * normal_force_magnitude_squared)) //twisting too hard
      {
        //TODO: set state
        return true;
      }

      return false;
    }

    bool reactionCenterValid(matec_utils::KDLWrenchStamped wrench, SlipState& state)
    {
      double x = -wrench.wrench.torque.y() / wrench.wrench.force.z(); //- m_s_p_y / f_r_z
      double y = wrench.wrench.torque.x() / wrench.wrench.force.z(); //m_s_p_x / f_r_z

      double contact_x_max = m_spec.contact_surface_height / 2.0;
      double contact_y_max = m_spec.contact_surface_width / 2.0;
      return (fabs(x) > contact_x_max) || (fabs(y) > contact_y_max);
    }

    matec_msgs::ContactSpecification m_spec;
  };
}

#endif //CONTACT_H
