#include <ros/ros.h>
#include <interactive_markers/interactive_marker_server.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <geometry_msgs/Pose.h>

using namespace visualization_msgs;

shared_memory_interface::Publisher<geometry_msgs::Pose> m_com_pub;

void processFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{
  geometry_msgs::Pose com_setpoint;
  com_setpoint.position = feedback->pose.position;
  com_setpoint.orientation.w = 1.0;
  ROS_INFO("Changing com setpoint to (%g,%g,%g)", com_setpoint.position.x, com_setpoint.position.y, com_setpoint.position.z);
  m_com_pub.publish(com_setpoint);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "rviz_com_setpoint");

  geometry_msgs::Pose com_setpoint;
  com_setpoint.position.z = 0.92;
  com_setpoint.orientation.w = 1.0;
  m_com_pub.advertise("/com_pose_command");
  m_com_pub.publish(com_setpoint);

  // create an interactive marker server on the topic namespace simple_marker
  interactive_markers::InteractiveMarkerServer server("rviz_com_setpoint");

  // create an interactive marker for our server
  visualization_msgs::InteractiveMarker int_marker;
  int_marker.header.frame_id = "global";
  int_marker.name = "com_setpoint";
  int_marker.description = "COM Setpoint Control";
  int_marker.pose = com_setpoint;

  // create a grey box marker
  visualization_msgs::Marker box_marker;
  box_marker.type = visualization_msgs::Marker::CUBE;
  box_marker.scale.x = 0.25;
  box_marker.scale.y = 0.25;
  box_marker.scale.z = 0.25;
  box_marker.color.r = 0.25;
  box_marker.color.g = 0.75;
  box_marker.color.b = 0.25;
  box_marker.color.a = 1.0;

  // create a non-interactive control which contains the box
  visualization_msgs::InteractiveMarkerControl box_control;
  box_control.always_visible = true;
  box_control.markers.push_back(box_marker);

  // add the control to the interactive marker
  int_marker.controls.push_back(box_control);

//  visualization_msgs::InteractiveMarkerControl move_control;
//  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE_3D;
//  move_control.always_visible = true;
//  move_control.name = "move_control";
//  move_control.orientation_mode = visualization_msgs::InteractiveMarkerControl::FIXED;
//  int_marker.controls.push_back(move_control);

  InteractiveMarkerControl control;
  int_marker.name += "_fixed";
  int_marker.description += "\n(fixed orientation)";
  control.orientation_mode = InteractiveMarkerControl::FIXED;
  if(true)
  {
    control.orientation.w = 1;
    control.orientation.x = 1;
    control.orientation.y = 0;
    control.orientation.z = 0;
    control.name = "move_x";
    control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
    int_marker.controls.push_back(control);

    control.orientation.w = 1;
    control.orientation.x = 0;
    control.orientation.y = 1;
    control.orientation.z = 0;
    control.name = "move_z";
    control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
    int_marker.controls.push_back(control);

    control.orientation.w = 1;
    control.orientation.x = 0;
    control.orientation.y = 0;
    control.orientation.z = 1;
    control.name = "move_y";
    control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
    int_marker.controls.push_back(control);
  }

  int_marker.scale = 0.25;

  // add the interactive marker to our collection &
  // tell the server to call processFeedback() when feedback arrives for it
  server.insert(int_marker, &processFeedback);

  // 'commit' changes and send to all clients
  server.applyChanges();

  // start the ROS main loop
  ros::spin();
}
