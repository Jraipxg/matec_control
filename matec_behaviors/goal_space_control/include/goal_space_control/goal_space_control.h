#ifndef GOAL_SPACE_CONTROL_H
#define GOAL_SPACE_CONTROL_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/GoalSpaceCarrot.h>
#include <matec_msgs/StringArray.h>
#include <matec_msgs/DynamicsCommand.h>
#include <boost/thread.hpp>

#include <tf_conversions/tf_kdl.h>
#include <tf/transform_listener.h>
#include <urdf/model.h>
#include <kdl/tree.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>

#include "matec_utils/common_functions.h"
#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/ManageBehavior.h"
#include "matec_msgs/ManageGoalSpaceInterpolator.h"
#include "matec_msgs/RegisterBehavior.h"
#include "matec_msgs/AwakenBehavior.h"
#include "matec_msgs/RequestJointOwnershipChange.h"

namespace goal_space_control
{
  class GoalSpaceControl
  {
  public:
    GoalSpaceControl(const ros::NodeHandle& nh);
    ~GoalSpaceControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_interpolator;
    bool m_base_behavior;
    bool m_sleeping;
    double m_max_acceleration;
    double m_max_velocity;
    double m_nominal_dt;

    std::string m_controlled_frame;
    std::string m_last_controlled_link;
    std::string m_chain_base_frame;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_owned_joints;
    std::vector<std::string> m_controlled_joints;
    std::vector<double> m_controlled_joint_mins;
    std::vector<double> m_controlled_joint_maxes;
    std::vector<unsigned char> m_joint_map;

    urdf::Model m_urdf_model;
    KDL::Tree m_tree;
    KDL::Chain m_chain;
    KDL::ChainFkSolverPos_recursive* m_fk_solver;
    KDL::ChainJntToJacSolver* m_jac_solver;

    tf::TransformListener m_tf_listener;

    boost::mutex m_mutex;
    matec_msgs::DynamicsCommand m_command;

    ros::ServiceServer m_manage_service_server;
    ros::ServiceServer m_awaken_service_server;

    shared_memory_interface::Publisher<matec_msgs::DynamicsCommand> m_behavior_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<matec_msgs::GoalSpaceCarrot> m_carrot_sub;

    void initializeCommand();
    void initializeModel();

    void findNearestActuatedLink(std::string frame);
    void findLongestControlChain();
    bool addControlledFrameOffsetToChain(KDL::Chain& chain);
    std::string getChainBase(KDL::Chain& chain);

    std::pair<std::vector<double>, std::vector<double> > calculateVelocityLimits();
    matec_utils::Vector rescaleVelocity(matec_utils::Vector vel, std::pair<std::vector<double>, std::vector<double> > limits);

    double error(double value, matec_msgs::ValueRange range);

    bool managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res);
    bool awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res);
    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //GOAL_SPACE_CONTROL_H
