#include "goal_space_control/goal_space_control.h"

namespace goal_space_control
{
  GoalSpaceControl::GoalSpaceControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("interpolator", m_interpolator, std::string(m_nh.getNamespace()));
    m_nh.param("base_behavior", m_base_behavior, false);
    m_nh.param("max_acceleration", m_max_acceleration, 5.0);
    m_nh.param("max_velocity", m_max_velocity, 0.5);
    m_nh.param("nominal_dt", m_nominal_dt, 1.0 / 1000.0);

    m_nh.param("controlled_frame", m_controlled_frame, std::string(""));

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints, m_joint_names))
    {
      ROS_ERROR("%s: Must specify at least one controlled joint!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("%s: Couldn't generate joint mapping! Shutting down!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }
    initializeCommand();

    initializeModel();
    findNearestActuatedLink(m_controlled_frame);
    findLongestControlChain();
    m_chain_base_frame = getChainBase(m_chain);

    m_sleeping = true;
    matec_utils::blockOnBehaviorRegistration(m_nh.getNamespace(), m_base_behavior);

    m_behavior_pub.advertise("behavior_command");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&GoalSpaceControl::sensorCallback, this, _1));
    m_carrot_sub.subscribe("/" + m_interpolator + "/carrot");

    m_manage_service_server = m_nh.advertiseService("manage", &GoalSpaceControl::managementCallback, this);
    m_awaken_service_server = m_nh.advertiseService("awaken", &GoalSpaceControl::awakenCallback, this);

    if(m_base_behavior) //auto-wake base behaviors on startup
    {
      matec_msgs::AwakenBehavior awaken;
      awakenCallback(awaken.request, awaken.response);
    }
  }

  GoalSpaceControl::~GoalSpaceControl()
  {

  }

  void GoalSpaceControl::initializeModel()
  {
    m_urdf_model.initParam("/robot_description");
    if(!kdl_parser::treeFromUrdfModel((const urdf::Model) m_urdf_model, m_tree))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }

    m_tree.getChain("body", "left_foot", m_chain); //TODO: param frames
    std::cerr << "Found chain of length " << m_chain.getNrOfJoints() << ":" << std::endl;

    for(unsigned int i = 0; i < m_chain.getNrOfJoints(); i++)
    {
      std::cerr << m_chain.segments[i].getJoint().getName() << std::endl;
      m_controlled_joints.push_back(m_chain.segments[i].getJoint().getName());
    }

    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(m_controlled_joints[i]);
      m_controlled_joint_mins.push_back(joint->limits->lower);
      m_controlled_joint_maxes.push_back(joint->limits->upper);
    }

    m_fk_solver = new KDL::ChainFkSolverPos_recursive(m_chain);
    m_jac_solver = new KDL::ChainJntToJacSolver(m_chain);
  }

  void GoalSpaceControl::initializeCommand()
  {
    m_command.feedforward.joint_indices = m_joint_map;
    m_command.feedforward.position.resize(m_controlled_joints.size());
    m_command.feedforward.velocity.resize(m_controlled_joints.size());
    m_command.feedforward.acceleration.resize(m_controlled_joints.size());
    m_command.feedforward.torque.resize(m_controlled_joints.size());
    m_command.compliance.resize(m_controlled_joints.size());
  }

  void GoalSpaceControl::findNearestActuatedLink(std::string frame)
  {
    boost::shared_ptr<const urdf::Link> link = m_urdf_model.getLink(frame);
    if(link)
    {
      m_last_controlled_link = frame;
    }
    else
    {
      std::string parent;
      while(ros::ok() && !m_tf_listener.getParent(frame, ros::Time(0), parent))
      {
        ROS_WARN_THROTTLE(1.0, "%s is waiting for tf to find the parent of %s", m_nh.getNamespace().c_str(), frame.c_str());
      }

      findNearestActuatedLink(parent);
    }
  }

  void GoalSpaceControl::findLongestControlChain()
  {
    //NOTE: currently assuming that all controlled joints are in the same chain!
    unsigned int longest_chain_length = 0;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      if(std::find(m_joint_names.begin(), m_joint_names.end(), m_controlled_joints[i]) == m_joint_names.end())
      {
        ROS_FATAL("Tried to control a joint that the SMI didn't know about! Shutting down!");
        ros::shutdown();
      }

      std::string parent_frame = m_urdf_model.getJoint(m_controlled_joints[i])->parent_link_name;

      KDL::Chain chain;
      m_tree.getChain(parent_frame, m_last_controlled_link, chain);

      if(chain.getNrOfJoints() > longest_chain_length)
      {
        longest_chain_length = chain.getNrOfJoints();
        m_chain = chain;
      }
    }

    ROS_INFO("Using chain of %d joints from %s to %s", m_chain.getNrOfJoints(), m_chain.getSegment(0).getJoint().getName().c_str(), m_chain.getSegment(m_chain.getNrOfSegments() - 1).getJoint().getName().c_str());
  }

  bool GoalSpaceControl::addControlledFrameOffsetToChain(KDL::Chain& chain)
  {
    tf::StampedTransform controlled_frame_offset;
    if(m_tf_listener.canTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0)) && m_tf_listener.canTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0)))
    {
      m_tf_listener.lookupTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0), controlled_frame_offset);
    }
    else
    {
      ROS_ERROR("Couldn't find transform from the last controllable link (%s) and the controlled frame (%s)", m_last_controlled_link.c_str(), m_controlled_frame.c_str());
      return false;
    }

    KDL::Frame fixed_offset;
    tf::transformTFToKDL(controlled_frame_offset.inverse(), fixed_offset);
    KDL::Segment fixedSegment = KDL::Segment(m_controlled_frame, KDL::Joint(m_controlled_frame + "_fixed", KDL::Joint::None), fixed_offset);
    chain.addSegment(fixedSegment);

    return true;
  }

  std::string GoalSpaceControl::getChainBase(KDL::Chain& chain)
  {
    return m_urdf_model.getJoint(chain.getSegment(0).getJoint().getName())->parent_link_name;
  }

  bool GoalSpaceControl::awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res)
  {
    std::vector<unsigned char> required_actuators;
    if(req.actuators.size() != 0)
    {
      required_actuators = req.actuators;
    }
    else
    {
      required_actuators = m_joint_map;
    }

    res.awake = true;
    for(unsigned int i = 0; i < required_actuators.size(); i++)
    {
      if(std::find(m_owned_joints.begin(), m_owned_joints.end(), m_joint_names[required_actuators[i]]) == m_owned_joints.end())
      {
        ROS_INFO("%s: Need joint %s! Requesting ownership!", m_nh.getNamespace().c_str(), m_joint_names[required_actuators[i]].c_str());
        res.awake = false;
      }
    }

    if(res.awake)
    {
      return true;
    }
    else
    {
      return matec_utils::requestOwnership(required_actuators);
    }
  }

  //TODO: implement sleep, reject if base behavior

  bool GoalSpaceControl::managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    matec_utils::indicesToNames(m_joint_names, req.owned_joints, m_owned_joints);

    if(m_owned_joints.size() == m_controlled_joints.size())
    {
      for(unsigned int i = 0; i < req.newly_owned_joints.size(); i++)
      {
        unsigned int control_idx = std::find(m_joint_map.begin(), m_joint_map.end(), req.newly_owned_joints[i]) - m_joint_map.begin();
        if(control_idx < m_controlled_joints.size())
        {
          m_command.feedforward.position[control_idx] = req.last_command.position[req.newly_owned_joints[i]];
          m_command.feedforward.velocity[control_idx] = req.last_command.velocity[req.newly_owned_joints[i]];
          m_command.feedforward.acceleration[control_idx] = 0.0;
          m_command.compliance[i] = 1.0;
        }
        else
        {
          ROS_WARN("%s: Control granted for uncontrolled joint %s!", m_nh.getNamespace().c_str(), m_joint_names[req.newly_owned_joints[i]].c_str());
        }
      }

      m_behavior_pub.publish(m_command);
      res.release_remaining_joints = false;
    }
    else
    {
      ROS_WARN("%s: Lost control of too many joints! Giving up control of the rest!", m_nh.getNamespace().c_str());
      res.release_remaining_joints = true;
      return true;
    }

    //forward to interpolator
    bool success = true;
    std::string interpolator_management = "/" + m_interpolator + "/manage";
    if(ros::service::exists(interpolator_management, false))
    {
      matec_msgs::ManageGoalSpaceInterpolator manage_cartesian_interpolator;
      KDL::Frame commanded_frame;
      m_fk_solver->JntToCart(matec_utils::vectorToJntArray(m_command.feedforward.position), commanded_frame);

      geometry_msgs::PoseStamped current_chain_base;
      tf::poseKDLToMsg(commanded_frame, current_chain_base.pose);
      current_chain_base.header.frame_id = m_chain_base_frame;
      current_chain_base.header.stamp = ros::Time(0);
      manage_cartesian_interpolator.request.new_setpoint = current_chain_base;

      success = ros::service::call(interpolator_management, manage_cartesian_interpolator.request, manage_cartesian_interpolator.response);
    }
    else
    {
      ROS_WARN_THROTTLE(1.0, "%s: Interpolator (%s) management service not found!", m_nh.getNamespace().c_str(), m_interpolator.c_str());
      success = false;
    }

    if(m_owned_joints.size() != m_controlled_joints.size())
    {
      //TODO: just change weights if we still have some joints left?
      m_sleeping = true;
    }
    else
    {
      m_sleeping = false;
    }

    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return success;
  }

  double GoalSpaceControl::error(double value, matec_msgs::ValueRange range)
  {
    if(value < range.min)
    {
      return range.min - value; //always positive
    }
    else if(value > range.max)
    {
      return range.max - value; //always negative
    }
    else
    {
      return 0.0;
    }
  }

//  std::vector<geometry_msgs::TwistStamped> generateTwistBox(matec_msgs::GoalSpaceCarrot& carrot)
//  {
//
//  }

  std::pair<std::vector<double>, std::vector<double> > GoalSpaceControl::calculateVelocityLimits()
  {
    std::pair<std::vector<double>, std::vector<double> > limits(std::vector<double>(m_controlled_joints.size()), std::vector<double>(m_controlled_joints.size()));

    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {

    }

    return limits;
  }

  matec_utils::Vector GoalSpaceControl::rescaleVelocity(matec_utils::Vector vel, std::pair<std::vector<double>, std::vector<double> > limits)
  {
    //always run at least one joint at max velocity when not inside the goal region?
    //TODO: decide if this is a good thing or not
    double scale = 1.0;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      double joint_min = limits.first[i];
      double joint_max = limits.second[i];

      if(vel(i) > 0.0)
      {
        scale = fabs(joint_max / (double) vel(i));
      }
      else if(vel(i) < 0.0)
      {
        scale = fabs(joint_min / (double) vel(i));
      }
      else
      {
        scale = 1.0;
      }
    }

    matec_utils::Vector rescaled = vel;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      rescaled(i) *= scale;
    }

    return rescaled;
  }

  void GoalSpaceControl::sensorCallback(matec_msgs::FullJointStates& current)
  {
    if(m_sleeping)
    {
      return; //TODO: fully unsubscribe so we don't waste processing
    }

    //get the carrot
    boost::mutex::scoped_lock lock(m_mutex);
    matec_msgs::GoalSpaceCarrot carrot;
    if(!m_carrot_sub.getCurrentMessage(carrot))
    {
      ROS_WARN_THROTTLE(1.0, "%s: No carrot received (%s)!", m_nh.getNamespace().c_str(), m_nh.resolveName("/" + m_interpolator + "/carrot").c_str());
      return;
    }

    //get the current pose
    KDL::Frame frame;
    KDL::JntArray q = matec_utils::vectorToJntArray(current.position);
    m_fk_solver->JntToCart(q, frame);
    geometry_msgs::PoseStamped current_pose, current_pose_goal_frame;
    tf::poseKDLToMsg(frame, current_pose.pose);
    current_pose.header.stamp = ros::Time(0);
    current_pose.header.frame_id = m_chain_base_frame;

    KDL::Jacobian kdl_jacobian(m_chain.getNrOfJoints());
    m_jac_solver->JntToJac(q, kdl_jacobian);
    Eigen::MatrixXd jac = matec_utils::kdlJacobianToEigen(kdl_jacobian);
    Eigen::MatrixXd jac_t = jac.transpose();

    m_tf_listener.transformPose(carrot.header.frame_id, current_pose, current_pose_goal_frame);
    double x = current_pose_goal_frame.pose.position.x;
    double y = current_pose_goal_frame.pose.position.y;
    double z = current_pose_goal_frame.pose.position.z;
    double roll, pitch, yaw;
    matec_utils::quaternionToRPY(current_pose_goal_frame.pose.orientation, roll, pitch, yaw);

    matec_utils::Vector6 target_cartesian_vel_goal_frame, target_cartesian_vel;
    target_cartesian_vel_goal_frame(0) = carrot.x_importance * error(x, carrot.x);
    target_cartesian_vel_goal_frame(1) = carrot.y_importance * error(y, carrot.y);
    target_cartesian_vel_goal_frame(2) = carrot.z_importance * error(z, carrot.z);
    target_cartesian_vel_goal_frame(3) = carrot.R_importance * error(roll, carrot.R);
    target_cartesian_vel_goal_frame(4) = carrot.P_importance * error(pitch, carrot.P);
    target_cartesian_vel_goal_frame(5) = carrot.Y_importance * error(yaw, carrot.Y);

    bool x_goal_met = target_cartesian_vel_goal_frame(0) == 0.0;
    bool y_goal_met = target_cartesian_vel_goal_frame(1) == 0.0;
    bool z_goal_met = target_cartesian_vel_goal_frame(2) == 0.0;
    bool R_goal_met = target_cartesian_vel_goal_frame(3) == 0.0;
    bool P_goal_met = target_cartesian_vel_goal_frame(4) == 0.0;
    bool Y_goal_met = target_cartesian_vel_goal_frame(5) == 0.0;
    bool goal_met = x_goal_met && y_goal_met && z_goal_met && R_goal_met && P_goal_met && Y_goal_met;

    matec_utils::transformTwist(m_chain_base_frame, carrot.header.frame_id, ros::Time(0), m_tf_listener, target_cartesian_vel_goal_frame, target_cartesian_vel);

    std::pair<std::vector<double>, std::vector<double> > limits = calculateVelocityLimits();
    matec_utils::Vector joint_vel = rescaleVelocity(jac_t * target_cartesian_vel, limits);

//    Eigen::ColPivHouseholderQR<matec_utils::Matrix> decomp = jac.colPivHouseholderQr();
//    matec_utils::Vector joint_vel = decomp.solve(target_vel);
//    std::cerr << "rank: " << decomp.rank() << "\n" << joint_vel << std::endl;

    //if soln bad, use shotgun
    //generateTwistBox

    //calculate control
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
//      unsigned char current_idx = m_joint_map[i];

//      m_command.feedforward.acceleration[i] = 0;
      m_command.feedback.velocity[i] = joint_vel(i);
//      m_command.feedforward.position[i] = 0;
      m_command.compliance[i] = goal_met? 1.0 : 0.0;
    }

    //send off the command
    m_behavior_pub.publish(m_command);
  }

  void GoalSpaceControl::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "goal_space_control");
  ros::NodeHandle nh("~");

  goal_space_control::GoalSpaceControl node(nh);
  node.spin();

  return 0;
}
