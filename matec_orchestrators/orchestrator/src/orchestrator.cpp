#include "orchestrator/orchestrator.h"

namespace orchestrator
{
  Orchestrator::Orchestrator(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 2000.0);
    m_nh.param("num_threads", m_num_threads, 3);
    m_nh.param("heartbeat_timeout", m_heartbeat_timeout, 0.3);
    m_nh.param("enable_heartbeat_monitoring", m_enable_heartbeat_monitoring, false);

    matec_utils::blockOnSMJointNames(m_joint_names);
    m_joint_states_sub.subscribe("/joint_states");
    m_combined_dynamics_command_pub.advertise("/combined_dynamics_command");
    m_combined_joint_command_pub.advertise("/combined_joint_command");
    m_state_pub.advertise("/matec_state");

    initializeCombinedCommand(matec_msgs::FullJointStates());

    m_base_behaviors.resize(m_joint_names.size(), NULL);
    m_joint_owners.resize(m_joint_names.size(), NULL);

    m_last_pulse_check_time = ros::Time(0);

    m_base_behavior_actuator_count = 0;
    m_base_behaviors_configured = false;

    m_registration_service_server = m_nh.advertiseService("/register_behavior", &Orchestrator::registerBehaviorCallback, this);
    m_request_joint_ownership_service_server = m_nh.advertiseService("/request_joint_ownership", &Orchestrator::requestJointOwnershipCallback, this);
    m_release_joint_ownership_service_server = m_nh.advertiseService("/release_joint_ownership", &Orchestrator::releaseJointOwnershipCallback, this);
    m_tare_service_server = m_nh.advertiseService("/tare", &Orchestrator::tareCallback, this);
  }

  Orchestrator::~Orchestrator()
  {

  }

  void Orchestrator::initializeCombinedCommand(matec_msgs::FullJointStates tare_command)
  {
    //ROS_INFO_STREAM(__func__);
    matec_msgs::FullJointStates measurement;
    if(tare_command.joint_indices.size() == m_joint_names.size())
    {
      measurement = tare_command;
    }
    else
    {
      while(!m_joint_states_sub.getCurrentMessage(measurement))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Waiting for joint states...", m_nh.getNamespace().c_str());
      }
    }

    m_combined_dynamics_command = matec_msgs::DynamicsCommand();
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      m_combined_dynamics_command.feedforward.joint_indices.push_back(i);
      m_combined_dynamics_command.feedforward.position.push_back(measurement.position[i]);
      m_combined_dynamics_command.feedforward.velocity.push_back(0);
      m_combined_dynamics_command.feedforward.acceleration.push_back(0);
      m_combined_dynamics_command.feedforward.torque.push_back(0);
      m_combined_dynamics_command.feedforward.compliance.push_back(0.5);

      m_combined_dynamics_command.feedback.joint_indices.push_back(i);
      m_combined_dynamics_command.feedback.position.push_back(0);
      m_combined_dynamics_command.feedback.velocity.push_back(0);
      m_combined_dynamics_command.feedback.acceleration.push_back(0);
      m_combined_dynamics_command.feedback.torque.push_back(0);
      m_combined_dynamics_command.feedback.compliance.push_back(0.0);
    }
    m_combined_joint_command.header.stamp = ros::Time::now();
    m_combined_joint_command = m_combined_dynamics_command.feedforward;
    m_combined_joint_command_pub.publish(m_combined_joint_command);
    m_combined_dynamics_command_pub.publish(m_combined_dynamics_command);
  }

  bool Orchestrator::tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res)
  {
    //ROS_INFO_STREAM(__func__);
    boost::mutex::scoped_lock lock(m_mutex);

    initializeCombinedCommand(req.last_command);

    for(std::set<std::string>::iterator it = m_active_behaviors.begin(); it != m_active_behaviors.end(); ++it)
    {
      m_behavior_list[*it].new_actuators = m_behavior_list[*it].owned_actuators;
    }
    m_transitioning_behaviors = m_active_behaviors;
    updateMATECState();

    return true;
  }

  bool Orchestrator::unregisterBehavior(std::string behavior_name)
  {
    //ROS_INFO_STREAM(__func__);
    m_mutex.lock();

    if(m_behavior_list.find(behavior_name) == m_behavior_list.end())
    {
      ROS_WARN("%s: Tried to unregister unknown behavior %s!", m_nh.getNamespace().c_str(), behavior_name.c_str());
      m_mutex.unlock();
      return false;
    }

    if(m_behavior_list[behavior_name].base_behavior)
    {
      ROS_WARN("%s: Unregistering base behavior %s!", m_nh.getNamespace().c_str(), behavior_name.c_str());

      for(unsigned int i = 0; i < m_behavior_list[behavior_name].owned_actuators.size(); i++)
      {
        unsigned int actuator_idx = m_behavior_list[behavior_name].owned_actuators[i];
        if(m_joint_owners[actuator_idx] == m_base_behaviors[actuator_idx])
        {
          m_joint_owners[actuator_idx] = NULL;
        }
        m_base_behaviors[actuator_idx] = NULL;
        m_base_behavior_actuator_count--;
        m_base_behaviors_configured = false;
      }
    }
    else
    {
      ROS_INFO("%s: Unregistering behavior %s", m_nh.getNamespace().c_str(), behavior_name.c_str());
      matec_msgs::RequestJointOwnershipChange release;
      release.request.behavior_name = behavior_name;

      m_mutex.unlock();
      releaseJointOwnershipCallback(release.request, release.response);
      m_mutex.lock();
    }

    m_behavior_list.erase(behavior_name);
    updateMATECState();

    m_mutex.unlock();
    return true;
  }

  bool Orchestrator::registerBehaviorCallback(matec_msgs::RegisterBehavior::Request& req, matec_msgs::RegisterBehavior::Response& res)
  {
    //ROS_INFO_STREAM(__func__);
    boost::mutex::scoped_lock lock(m_mutex);
    std::string behavior_name = matec_utils::stripLeadingSlash(req.name);

    ROS_INFO("%s: Registering behavior %s", m_nh.getNamespace().c_str(), behavior_name.c_str());

    if(m_behavior_list.find(behavior_name) != m_behavior_list.end())
    {
      ROS_WARN("%s: Behavior %s was registered multiple times! Overwriting old entry!", m_nh.getNamespace().c_str(), behavior_name.c_str());
    }

    m_behavior_list[behavior_name] = BehaviorParameters();
    m_behavior_list[behavior_name].behavior_name = behavior_name;
    m_behavior_list[behavior_name].base_behavior = req.base_behavior;
    m_behavior_list[behavior_name].behavior_sub.subscribe("/" + behavior_name + "/behavior_command");
    m_behavior_list[behavior_name].heartbeat_sub.subscribe("/" + behavior_name + "/heartbeat");
    updateMATECState();

    return true;
  }

  bool Orchestrator::releaseJointOwnershipCallback(matec_msgs::RequestJointOwnershipChange::Request& req, matec_msgs::RequestJointOwnershipChange::Response& res)
  {
    //ROS_INFO_STREAM(__func__);
    boost::mutex::scoped_lock lock(m_mutex);
    std::string behavior_name = matec_utils::stripLeadingSlash(req.behavior_name);

    if(!m_base_behaviors_configured)
    {
      ROS_ERROR("%s: Tried to release ownership when base behaviors weren't configured!", m_nh.getNamespace().c_str());
      return false;
    }

    if(behavior_name.length() > 0 && m_behavior_list.find(behavior_name) == m_behavior_list.end())
    {
      ROS_ERROR("%s: Tried to release joints from an unregistered behavior!", m_nh.getNamespace().c_str());
      return false;
    }

    if(m_behavior_list[behavior_name].base_behavior)
    {
      ROS_ERROR("%s: Tried to release joints from a base behavior!", m_nh.getNamespace().c_str());
      return false;
    }

    if(req.actuators.size() == 0) //empty actuator array => return control of all joints to the base behavior(s)
    {
      for(unsigned int i = 0; i < m_joint_owners.size(); i++)
      {
        req.actuators.push_back(i);
      }
    }

    for(unsigned int i = 0; i < req.actuators.size(); i++)
    {
      unsigned int actuator_idx = req.actuators[i];
      if(actuator_idx >= m_joint_owners.size())
      {
        ROS_ERROR("%s: Tried to release ownership of non-existant actuator #%d!", m_nh.getNamespace().c_str(), (int ) actuator_idx);
        continue;
      }
      if(behavior_name.length() == 0 || m_joint_owners[actuator_idx]->behavior_name == behavior_name) //the behavior currently owns this joint and wants to get rid of it
      {
        std::vector<unsigned char>::iterator iter = std::find(m_joint_owners[actuator_idx]->owned_actuators.begin(), m_joint_owners[actuator_idx]->owned_actuators.end(), actuator_idx);
        if(iter != m_joint_owners[actuator_idx]->owned_actuators.end())
        {
          m_joint_owners[actuator_idx]->owned_actuators.erase(iter);
        }
        else
        {
          ROS_WARN("%s: Caught joint owner mismatch", m_nh.getNamespace().c_str());
        }

        if(m_joint_owners[actuator_idx]->owned_actuators.size() == 0)
        {
          std::set<std::string>::iterator active_iter = m_active_behaviors.find(m_joint_owners[actuator_idx]->behavior_name);
          if(active_iter != m_active_behaviors.end())
          {
            m_active_behaviors.erase(active_iter);
          }
          else
          {
            ROS_WARN("%s: Old owner was not an active behavior!", m_nh.getNamespace().c_str());
          }
        }

        std::string old_owner_name = m_joint_owners[actuator_idx]->behavior_name;
        m_joint_owners[actuator_idx] = m_base_behaviors[actuator_idx];
        m_joint_owners[actuator_idx]->owned_actuators.push_back(actuator_idx);
        m_joint_owners[actuator_idx]->new_actuators.push_back(actuator_idx);
        m_transitioning_behaviors.insert(old_owner_name);
        m_transitioning_behaviors.insert(m_joint_owners[actuator_idx]->behavior_name);
        m_active_behaviors.insert(m_joint_owners[actuator_idx]->behavior_name);

        ROS_INFO("%s: Actuator %s (%d) ownership changed from %s -> %s", m_nh.getNamespace().c_str(), m_joint_names[actuator_idx].c_str(), actuator_idx, old_owner_name.c_str(), m_joint_owners[actuator_idx]->behavior_name.c_str());
      }
    }
    updateMATECState();

    //TODO: lock outputs, manage behaviors

    return true;
  }

  bool Orchestrator::requestJointOwnershipCallback(matec_msgs::RequestJointOwnershipChange::Request& req, matec_msgs::RequestJointOwnershipChange::Response& res)
  {
    //ROS_INFO_STREAM(__func__);
    //todo don't re-tare if the behavior already owned the joints

    boost::mutex::scoped_lock lock(m_mutex);
    std::string behavior_name = matec_utils::stripLeadingSlash(req.behavior_name);

    if(m_behavior_list.find(behavior_name) == m_behavior_list.end())
    {
      ROS_ERROR("%s: Actuator ownership requested for unregistered behavior %s!", m_nh.getNamespace().c_str(), behavior_name.c_str());
      return false;
    }

    if(req.actuators.size() == 0)
    {
      ROS_ERROR("%s: No actuators requested!", m_nh.getNamespace().c_str());
      return false;
    }

    for(unsigned int i = 0; i < req.actuators.size(); i++)
    {
      unsigned int actuator_idx = req.actuators[i];
      if(actuator_idx >= m_joint_owners.size())
      {
        ROS_ERROR("%s: Behavior %s wanted to control non-existant actuator #%d!", m_nh.getNamespace().c_str(), behavior_name.c_str(), actuator_idx);
        continue;
      }

      if(m_behavior_list[behavior_name].base_behavior && m_base_behaviors[actuator_idx] == NULL)
      {
        m_base_behaviors[actuator_idx] = &m_behavior_list[behavior_name];
        m_base_behavior_actuator_count++;
        if(m_base_behavior_actuator_count == m_base_behaviors.size())
        {
          m_base_behaviors_configured = true;
        }
      }

      std::string old_owner_name = "NONE";
      if(m_joint_owners[actuator_idx] != NULL)
      {
        old_owner_name = m_joint_owners[actuator_idx]->behavior_name;
        m_transitioning_behaviors.insert(old_owner_name);
        std::vector<unsigned char>::iterator iter = std::find(m_joint_owners[actuator_idx]->owned_actuators.begin(), m_joint_owners[actuator_idx]->owned_actuators.end(), actuator_idx);
        if(iter != m_joint_owners[actuator_idx]->owned_actuators.end())
        {
          m_joint_owners[actuator_idx]->owned_actuators.erase(iter);
        }
        else
        {
          ROS_WARN("%s: Caught joint owner mismatch", m_nh.getNamespace().c_str());
        }

        if(m_joint_owners[actuator_idx]->owned_actuators.size() == 0)
        {
          std::set<std::string>::iterator active_iter = m_active_behaviors.find(old_owner_name);
          if(active_iter != m_active_behaviors.end())
          {
            m_active_behaviors.erase(old_owner_name);
          }
          else
          {
            ROS_WARN("%s: Old owner was not an active behavior!", m_nh.getNamespace().c_str());
          }
        }
      }

      m_joint_owners[actuator_idx] = &m_behavior_list[behavior_name];
      m_joint_owners[actuator_idx]->owned_actuators.push_back(actuator_idx);
      m_joint_owners[actuator_idx]->new_actuators.push_back(actuator_idx);

      ROS_INFO("%s: Actuator %s (%d) ownership changed from %s -> %s", m_nh.getNamespace().c_str(), m_joint_names[actuator_idx].c_str(), actuator_idx, old_owner_name.c_str(), m_joint_owners[actuator_idx]->behavior_name.c_str());
    }

    m_transitioning_behaviors.insert(behavior_name);
    m_active_behaviors.insert(behavior_name);
    updateMATECState();

    return true;
  }

  void Orchestrator::updateMATECState()
  {
    matec_msgs::MATECState state;
    state.joint_names = m_joint_names;

    std::copy(m_transitioning_behaviors.begin(), m_transitioning_behaviors.end(), std::back_inserter(state.transitioning_behaviors));
    std::copy(m_active_behaviors.begin(), m_active_behaviors.end(), std::back_inserter(state.active_behaviors));

    for(unsigned int i = 0; i < m_joint_owners.size(); i++)
    {
      if(m_joint_owners[i])
      {
        state.joint_owners.push_back(m_joint_owners[i]->behavior_name);
      }
      else
      {
        state.joint_owners.push_back("NONE");
      }
    }

    for(unsigned int i = 0; i < m_base_behaviors.size(); i++)
    {
      if(m_base_behaviors[i])
      {
        state.base_behaviors.push_back(m_base_behaviors[i]->behavior_name);
      }
      else
      {
        state.base_behaviors.push_back("NONE");
      }
    }

    for(std::map<std::string, BehaviorParameters>::iterator iter = m_behavior_list.begin(); iter != m_behavior_list.end(); iter++)
    {
      state.registered_behaviors.push_back(iter->first);
    }

    m_state_pub.publish(state);
  }

  bool Orchestrator::handleTransitioningBehaviors()
  {
    //ROS_INFO_STREAM(__func__);
    m_mutex.lock();

    if(m_transitioning_behaviors.size() == 0)
    {
      m_mutex.unlock();
      return true;
    }

    matec_msgs::ManageBehavior behavior_management;
    behavior_management.request.last_command = m_combined_dynamics_command.feedforward;

    std::vector<std::string> completed_behavior_transitions;
    for(std::set<std::string>::iterator iter = m_transitioning_behaviors.begin(); iter != m_transitioning_behaviors.end(); ++iter)
    {
      std::string behavior_name = *iter;
      std::string management_server_name = "/" + behavior_name + "/manage";

      behavior_management.request.newly_owned_joints = m_behavior_list[behavior_name].new_actuators;
      behavior_management.request.owned_joints = m_behavior_list[behavior_name].owned_actuators;

      if(!ros::service::exists(management_server_name, false))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Attempted to manage %s behavior but couldn't find the service at %s!", m_nh.getNamespace().c_str(), behavior_name.c_str(), management_server_name.c_str());
        continue;
      }

      ROS_INFO("%s: Managing %s", m_nh.getNamespace().c_str(), behavior_name.c_str());
      if(!ros::service::call(management_server_name, behavior_management))
      {
        ROS_WARN_THROTTLE(1.0, "%s: Attempted to manage %s behavior but the service call failed!", m_nh.getNamespace().c_str(), behavior_name.c_str());
        continue;
      }

      if(behavior_management.response.release_remaining_joints)
      {
        matec_msgs::RequestJointOwnershipChange rjoc;
        rjoc.request.behavior_name = behavior_name;

        m_mutex.unlock();
        releaseJointOwnershipCallback(rjoc.request, rjoc.response);
        m_mutex.lock();
      }
      else
      {
        m_behavior_list[behavior_name].new_actuators.clear();
        completed_behavior_transitions.push_back(behavior_name);
      }
    }

    for(unsigned int i = 0; i < completed_behavior_transitions.size(); i++)
    {
      m_transitioning_behaviors.erase(completed_behavior_transitions[i]);
    }
    updateMATECState();

    m_mutex.unlock();
    return false;
  }

  void Orchestrator::updateCommand()
  {
    //ROS_INFO_STREAM(__func__);
    boost::mutex::scoped_lock lock(m_mutex);

    //gather the behaviors' commands
    for(std::set<std::string>::iterator it = m_active_behaviors.begin(); it != m_active_behaviors.end(); ++it)
    {
      m_behavior_list[*it].behavior_sub.getCurrentMessage(m_behavior_list[*it].command);
    }

    m_avg_rate = m_loop_rate;
    m_avg_dt = 1.0 / m_avg_rate; //TODO: actually measure this!

    //map them to the combined command
    for(unsigned int i = 0; i < m_joint_owners.size(); i++)
    {
      matec_msgs::DynamicsCommand* behavior_command = &m_joint_owners[i]->command;
      unsigned int behavior_idx = std::find(behavior_command->feedforward.joint_indices.begin(), behavior_command->feedforward.joint_indices.end(), i) - behavior_command->feedforward.joint_indices.begin();

      if(behavior_idx == behavior_command->feedforward.joint_indices.size())
      {
        ROS_ERROR_THROTTLE(1.0, "%s: Behavior %s did not send a command for joint %s (%d) under its control!", m_nh.getNamespace().c_str(), m_joint_owners[i]->behavior_name.c_str(), m_joint_names[i].c_str(), (int ) i);
        continue;
      }

      //accel
      if(behavior_idx < behavior_command->feedforward.acceleration.size())
      {
        m_combined_dynamics_command.feedforward.acceleration[i] = behavior_command->feedforward.acceleration[behavior_idx];
      }
      else
      {
        if(behavior_idx < behavior_command->feedforward.velocity.size()) //velocity explicitly specified, take derivative to get accel
        {
          m_combined_dynamics_command.feedforward.acceleration[i] = (behavior_command->feedforward.velocity[behavior_idx] - m_combined_dynamics_command.feedforward.velocity[i]) * m_avg_rate;
        }
        else
        {
          m_combined_dynamics_command.feedforward.acceleration[i] = 0;
        }
      }

      if(behavior_idx < behavior_command->feedback.acceleration.size())
      {
        m_combined_dynamics_command.feedback.acceleration[i] = behavior_command->feedback.acceleration[behavior_idx];
      }
      else
      {
        if(behavior_idx < behavior_command->feedback.velocity.size()) //velocity explicitly specified, take derivative to get accel
        {
          m_combined_dynamics_command.feedback.acceleration[i] = (behavior_command->feedback.velocity[behavior_idx] - m_combined_dynamics_command.feedback.velocity[i]) * m_avg_rate;
        }
        else
        {
          m_combined_dynamics_command.feedback.acceleration[i] = 0;
        }
      }

      //vel
      if(behavior_idx < behavior_command->feedforward.velocity.size())
      {
        m_combined_dynamics_command.feedforward.velocity[i] = behavior_command->feedforward.velocity[behavior_idx];
      }
      else
      {
        m_combined_dynamics_command.feedforward.velocity[i] += m_combined_dynamics_command.feedforward.acceleration[i] * m_avg_dt;
      }

      if(behavior_idx < behavior_command->feedback.velocity.size())
      {
        m_combined_dynamics_command.feedback.velocity[i] = behavior_command->feedback.velocity[behavior_idx];
      }
      else
      {
        m_combined_dynamics_command.feedback.velocity[i] += m_combined_dynamics_command.feedback.acceleration[i] * m_avg_dt;
      }

      //pos
      if(behavior_idx < behavior_command->feedforward.position.size())
      {
        m_combined_dynamics_command.feedforward.position[i] = behavior_command->feedforward.position[behavior_idx];
      }
      else
      {
        m_combined_dynamics_command.feedforward.position[i] += m_combined_dynamics_command.feedforward.velocity[i] * m_avg_dt;
      }

      if(behavior_idx < behavior_command->feedback.position.size())
      {
        m_combined_dynamics_command.feedback.position[i] = behavior_command->feedback.position[behavior_idx];
      }
      else
      {
        m_combined_dynamics_command.feedback.position[i] += m_combined_dynamics_command.feedback.velocity[i] * m_avg_dt;
      }

      //torque
      if(behavior_idx < behavior_command->feedforward.torque.size())
      {
        m_combined_dynamics_command.feedforward.torque[i] = behavior_command->feedforward.torque[behavior_idx];
      }
      else
      {
        m_combined_dynamics_command.feedforward.torque[i] = 0;
      }

      if(behavior_idx < behavior_command->feedback.torque.size())
      {
        m_combined_dynamics_command.feedback.torque[i] = behavior_command->feedback.torque[behavior_idx];
      }
      else
      {
        m_combined_dynamics_command.feedback.torque[i] = 0;
      }

      //compliance
      if(behavior_idx < behavior_command->feedforward.compliance.size())
      {
        m_combined_dynamics_command.feedforward.compliance[i] = behavior_command->feedforward.compliance[behavior_idx];
      }
      if(behavior_idx < behavior_command->feedback.compliance.size())
      {
        m_combined_dynamics_command.feedback.compliance[i] = behavior_command->feedback.compliance[behavior_idx];
      }

      //joint command
      m_combined_joint_command.header.stamp = ros::Time::now();
      m_combined_joint_command.position[i] = m_combined_dynamics_command.feedforward.position[i];
      m_combined_joint_command.velocity[i] = m_combined_dynamics_command.feedforward.velocity[i];
      m_combined_joint_command.acceleration[i] = m_combined_dynamics_command.feedforward.acceleration[i];
      m_combined_joint_command.torque[i] = m_combined_dynamics_command.feedforward.torque[i];
      m_combined_joint_command.compliance[i] = m_combined_dynamics_command.feedforward.compliance[i];
    }

    //send the command
    m_combined_dynamics_command_pub.publish(m_combined_dynamics_command);
    m_combined_joint_command_pub.publish(m_combined_joint_command);
  }

  void Orchestrator::checkBaseBehaviors()
  {
    //ROS_INFO_STREAM(__func__);
    boost::mutex::scoped_lock lock(m_mutex);
    m_base_behavior_actuator_count = 0;
    std::vector<std::string> uncontrolled_joints;
    for(unsigned int i = 0; i < m_base_behaviors.size(); i++)
    {
      if(m_base_behaviors[i] != NULL)
      {
        m_base_behavior_actuator_count++;
      }
      else
      {
        uncontrolled_joints.push_back(m_joint_names[i]);
      }
    }

    if(m_base_behavior_actuator_count == m_base_behaviors.size())
    {
      m_base_behaviors_configured = true;
    }
    else
    {
      std::stringstream ss;
      ss << "[";
      for(unsigned int i = 0; i < uncontrolled_joints.size(); i++)
      {
        if(i != 0)
        {
          ss << ", ";
        }
        ss << uncontrolled_joints[i];
      }
      ss << "]" << std::endl;
      ROS_WARN_STREAM_THROTTLE(1.0, m_nh.getNamespace() << ": Waiting for base behaviors. Uncontrolled joints: " << ss.str());
    }
  }

  void Orchestrator::monitorHeartbeats()
  {
    if(!m_enable_heartbeat_monitoring)
    {
      return;
    }

    ros::Time now_time = ros::Time::now();
    if(ros::Duration(now_time - m_last_pulse_check_time).toSec() < m_heartbeat_timeout) //not time to check the pulse yet
    {
      return;
    }

    std::vector<std::string> dead_behaviors;
    for(std::map<std::string, BehaviorParameters>::iterator it = m_behavior_list.begin(); it != m_behavior_list.end(); ++it)
    {
      std_msgs::Int8 heartbeat;
      if(it->second.heartbeat_sub.getCurrentMessage(heartbeat))
      {
        if(heartbeat.data != it->second.last_heartbeat)
        {
          it->second.last_heartbeat = heartbeat.data;
        }
        else
        {
          ROS_ERROR("%s: Behavior %s's heart has stopped beating!", m_nh.getNamespace().c_str(), it->second.behavior_name.c_str());
          dead_behaviors.push_back(it->second.behavior_name);
        }
      }
      else
      {
        ROS_ERROR_THROTTLE(1.0, "%s: Behavior %s did not expose a heartbeat!", m_nh.getNamespace().c_str(), it->second.behavior_name.c_str());
      }
    }

    for(unsigned int i = 0; i < dead_behaviors.size(); i++)
    {
      unregisterBehavior(dead_behaviors[i]);
    }

    m_last_pulse_check_time = now_time;
  }

  void Orchestrator::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::AsyncSpinner spinner(m_num_threads);
    spinner.start();

    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      if(!m_base_behaviors_configured)
      {
        checkBaseBehaviors();
      }
      else
      {
        monitorHeartbeats();

        //TODO: only stop sending commands from joints that are transitioning
        if(m_base_behaviors_configured && handleTransitioningBehaviors()) //only send commands when the behavior setup is stable
        {
          updateCommand();
        }
      }

      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "orchestrator");
  ros::NodeHandle nh("~");

  orchestrator::Orchestrator node(nh);
  node.spin();

  return 0;
}
