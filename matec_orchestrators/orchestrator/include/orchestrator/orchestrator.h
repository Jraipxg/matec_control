#ifndef ORCHESTRATOR_H
#define ORCHESTRATOR_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/StringArray.h>
#include <matec_msgs/DynamicsCommand.h>
#include <boost/thread.hpp>

#include <sensor_msgs/JointState.h>

#include <matec_utils/common_initialization_components.h>
#include <matec_msgs/RegisterBehavior.h>
#include <matec_msgs/RequestJointOwnershipChange.h>
#include <matec_msgs/ManageBehavior.h>
#include <matec_msgs/MATECState.h>
#include <std_srvs/Empty.h>
#include <matec_msgs/Tare.h>

namespace orchestrator
{
  class BehaviorParameters
  {
  public:
    std::string behavior_name;
    bool base_behavior;
    shared_memory_interface::Subscriber<matec_msgs::DynamicsCommand> behavior_sub;
    matec_msgs::DynamicsCommand command;
    shared_memory_interface::Subscriber<std_msgs::Int8> heartbeat_sub;
    char last_heartbeat;

    std::vector<unsigned char> owned_actuators; // joints currently controlled by this behavior
    std::vector<unsigned char> new_actuators; // new joints that may need to be tared
  };

  class Orchestrator
  {
  public:
    Orchestrator(const ros::NodeHandle& nh);
    ~Orchestrator();

    void spin();

  private:
    ros::NodeHandle m_nh;
    int m_num_threads;
    double m_loop_rate;
    double m_heartbeat_timeout;
    bool m_enable_heartbeat_monitoring;

    bool m_base_behaviors_configured;
    unsigned int m_base_behavior_actuator_count;

    double m_avg_rate;
    double m_avg_dt;

    ros::ServiceServer m_registration_service_server;
    ros::ServiceServer m_unregistration_service_server;
    ros::ServiceServer m_request_joint_ownership_service_server;
    ros::ServiceServer m_release_joint_ownership_service_server;
    ros::ServiceServer m_tare_service_server;

    std::vector<std::string> m_joint_names;
    std::map<std::string, BehaviorParameters> m_behavior_list;
    std::vector<BehaviorParameters*> m_base_behaviors;
    std::vector<BehaviorParameters*> m_joint_owners;
    std::set<std::string> m_transitioning_behaviors;
    std::set<std::string> m_active_behaviors;

    ros::Time m_last_pulse_check_time;

    boost::mutex m_mutex;

    matec_msgs::FullJointStates m_combined_joint_command;
    matec_msgs::DynamicsCommand m_combined_dynamics_command;
    shared_memory_interface::Publisher<matec_msgs::DynamicsCommand> m_combined_dynamics_command_pub;
    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_combined_joint_command_pub;
    shared_memory_interface::Publisher<matec_msgs::MATECState> m_state_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;

    void initializeCombinedCommand(matec_msgs::FullJointStates tare_command);
    bool handleTransitioningBehaviors();
    void updateCommand();
    void checkBaseBehaviors();
    void updateMATECState();

    void monitorHeartbeats();
    bool unregisterBehavior(std::string behavior_name);

    bool registerBehaviorCallback(matec_msgs::RegisterBehavior::Request& req, matec_msgs::RegisterBehavior::Response& res);
    bool requestJointOwnershipCallback(matec_msgs::RequestJointOwnershipChange::Request& req, matec_msgs::RequestJointOwnershipChange::Response& res);
    bool releaseJointOwnershipCallback(matec_msgs::RequestJointOwnershipChange::Request& req, matec_msgs::RequestJointOwnershipChange::Response& res);
    bool tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res);
  };
}
#endif //ORCHESTRATOR_H
