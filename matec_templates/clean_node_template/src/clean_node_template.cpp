#include "clean_node_template/clean_node_template.h"
 
namespace clean_node_template
{
  CleanNodeTemplate::CleanNodeTemplate(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
      m_nh.param("loop_rate", m_loop_rate, 10.0);
  }
 
  CleanNodeTemplate::~CleanNodeTemplate()
  {
  }
 
  void CleanNodeTemplate::spin()
  {
    ROS_INFO("CleanNodeTemplate started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      //do stuff!
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}
 
int main(int argc, char **argv)
{
  ros::init(argc, argv, "clean_node_template");
  ros::NodeHandle nh("~");
 
  clean_node_template::CleanNodeTemplate node(nh);
  node.spin();
 
  return 0;
}
