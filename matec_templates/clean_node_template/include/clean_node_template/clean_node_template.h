#ifndef CLEAN_NODE_TEMPLATE_H
#define CLEAN_NODE_TEMPLATE_H
#include <ros/ros.h>
 
namespace clean_node_template
{
  class CleanNodeTemplate
  {
  public:
    CleanNodeTemplate(const ros::NodeHandle& nh);
    ~CleanNodeTemplate();
    void spin();
 
  private:
    ros::NodeHandle m_nh;
 
    double m_loop_rate;
  };
}
#endif //CLEAN_NODE_TEMPLATE_H
