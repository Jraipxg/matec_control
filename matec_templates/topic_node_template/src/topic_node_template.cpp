#include "topic_node_template/topic_node_template.h"

namespace topic_node_template
{
  TopicNodeTemplate::TopicNodeTemplate(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);

    m_example_pub = m_nh.advertise<topic_node_template::ExampleMessage>("/topic_out", 1, true); //The slash before topic_out puts the topic in global space, remove it if you want it to be [node_name]/topic_out instead
    m_example_sub = m_nh.subscribe<topic_node_template::ExampleMessage>("/topic_in", 1, &TopicNodeTemplate::exampleCallback, this);
  }

  TopicNodeTemplate::~TopicNodeTemplate()
  {
  }

  void TopicNodeTemplate::exampleCallback(const topic_node_template::ExampleMessageConstPtr& msg)
  {
    m_message = *msg;

    //process message as necessary
  }

  void TopicNodeTemplate::sendMessage()
  {
    ExampleMessage message;
    message.header.stamp = ros::Time::now();
    message.header.frame_id = "example_frame";
    message.example_int = ExampleMessage::EXAMPLE_CONSTANT;
    message.example_float_array.push_back(0.4);
    message.example_float_array.push_back(1.3);
    message.example_float_array.push_back(2.2);
    message.example_float_array.push_back(3.1);
    message.example_float_array.push_back(4.0);

    m_example_pub.publish(message);
  }

  void TopicNodeTemplate::spin()
  {
    ROS_INFO("TopicNodeTemplate started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      sendMessage();
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "topic_node_template");
  ros::NodeHandle nh("~");

  topic_node_template::TopicNodeTemplate node(nh);
  node.spin();

  return 0;
}
