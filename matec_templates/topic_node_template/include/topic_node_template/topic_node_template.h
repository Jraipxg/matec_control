#ifndef TOPIC_NODE_TEMPLATE_H
#define TOPIC_NODE_TEMPLATE_H
#include <ros/ros.h>
#include <topic_node_template/ExampleMessage.h>
 
namespace topic_node_template
{
  class TopicNodeTemplate
  {
  public:
    TopicNodeTemplate(const ros::NodeHandle& nh);
    ~TopicNodeTemplate();
    void spin();
 
  private:
    ros::NodeHandle m_nh;

    ros::Subscriber m_example_sub;
    ros::Publisher m_example_pub;

    ExampleMessage m_message;
 
    double m_loop_rate;

    void sendMessage();
    void exampleCallback(const topic_node_template::ExampleMessageConstPtr& msg);
  };
}
#endif //TOPIC_NODE_TEMPLATE_H
