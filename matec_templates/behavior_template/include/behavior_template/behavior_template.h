#ifndef BEHAVIOR_TEMPLATE_H
#define BEHAVIOR_TEMPLATE_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <behavior_template/ExampleCarrot.h>
#include <matec_msgs/StringArray.h>
#include <matec_msgs/DynamicsCommand.h>
#include <boost/thread.hpp>

#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/ManageBehavior.h"
#include "matec_msgs/ManageJointInterpolator.h"
#include "matec_msgs/RegisterBehavior.h"
#include "matec_msgs/AwakenBehavior.h"
#include "matec_msgs/RequestJointOwnershipChange.h"

namespace behavior_template
{
  class BehaviorTemplate
  {
  public:
    BehaviorTemplate(const ros::NodeHandle& nh);
    ~BehaviorTemplate();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_interpolator;
    bool m_base_behavior;
    bool m_sleeping;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_owned_joints;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;
    std::vector<unsigned char> m_carrot_map;

    boost::mutex m_mutex;
    matec_msgs::DynamicsCommand m_command;

    ros::ServiceServer m_manage_service_server;
    ros::ServiceServer m_awaken_service_server;

    shared_memory_interface::Publisher<matec_msgs::DynamicsCommand> m_behavior_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<behavior_template::ExampleCarrot> m_carrot_sub;

    void initializeCommand();
    bool analyzeCarrot(matec_msgs::FullJointStates& carrot);

    bool managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res);
    bool awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res);
    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //BEHAVIOR_TEMPLATE_H
