#include "behavior_template/behavior_template.h"

namespace behavior_template
{
  BehaviorTemplate::BehaviorTemplate(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("interpolator", m_interpolator, std::string(m_nh.getNamespace()));
    m_nh.param("base_behavior", m_base_behavior, false);

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints, m_joint_names))
    {
      ROS_ERROR("%s: Must specify at least one controlled joint!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("%s: Couldn't generate joint mapping! Shutting down!", m_nh.getNamespace().c_str());
      ros::shutdown();
      return;
    }
    initializeCommand();

    m_sleeping = true;
    matec_utils::blockOnBehaviorRegistration(m_nh.getNamespace(), m_base_behavior);

    m_behavior_pub.advertise("behavior_command");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&BehaviorTemplate::sensorCallback, this, _1));
    m_carrot_sub.subscribe("/" + m_interpolator + "/carrot");

    m_manage_service_server = m_nh.advertiseService("manage", &BehaviorTemplate::managementCallback, this);
    m_awaken_service_server = m_nh.advertiseService("awaken", &BehaviorTemplate::awakenCallback, this);

    if(m_base_behavior) //auto-wake base behaviors on startup
    {
      matec_msgs::AwakenBehavior awaken;
      awakenCallback(awaken.request, awaken.response);
    }

    //do other initialization here
  }

  BehaviorTemplate::~BehaviorTemplate()
  {

  }

  void BehaviorTemplate::initializeCommand()
  {
    m_command.feedback.joint_indices = m_joint_map;
    m_command.feedback.position.resize(m_controlled_joints.size());
    m_command.feedback.velocity.resize(m_controlled_joints.size());
    m_command.feedback.acceleration.resize(m_controlled_joints.size());
    m_command.feedback.torque.resize(m_controlled_joints.size());
  }

  bool BehaviorTemplate::awakenCallback(matec_msgs::AwakenBehavior::Request& req, matec_msgs::AwakenBehavior::Response& res)
  {
    std::vector<unsigned char> required_actuators;
    if(req.actuators.size() != 0)
    {
      required_actuators = req.actuators;
    }
    else
    {
      required_actuators = m_joint_map;
    }

    res.awake = true;
    for(unsigned int i = 0; i < required_actuators.size(); i++)
    {
      if(std::find(m_owned_joints.begin(), m_owned_joints.end(), m_joint_names[required_actuators[i]]) == m_owned_joints.end())
      {
        ROS_INFO("%s: Need joint %s! Requesting ownership!", m_nh.getNamespace().c_str(), m_joint_names[required_actuators[i]].c_str());
        res.awake = false;
      }
    }

    if(res.awake)
    {
      return true;
    }
    else
    {
      return matec_utils::requestOwnership(required_actuators);
    }
  }

  //TODO: implement sleep, reject if base behavior

  bool BehaviorTemplate::managementCallback(matec_msgs::ManageBehavior::Request& req, matec_msgs::ManageBehavior::Response& res)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    matec_utils::indicesToNames(m_joint_names, req.owned_joints, m_owned_joints);

    for(unsigned int i = 0; i < req.newly_owned_joints.size(); i++)
    {
      unsigned int control_idx = std::find(m_joint_map.begin(), m_joint_map.end(), req.newly_owned_joints[i]) - m_joint_map.begin();
      if(control_idx < m_controlled_joints.size())
      {
        m_command.feedback.position[control_idx] = req.last_command.position[req.newly_owned_joints[i]];
        m_command.feedback.velocity[control_idx] = 0;
        m_command.feedback.acceleration[control_idx] = 0;
      }
      else
      {
        ROS_WARN("%s: Tare requested for uncontrolled joint %s", m_nh.getNamespace().c_str(), m_joint_names[req.newly_owned_joints[i]].c_str());
      }
    }
    m_behavior_pub.publish(m_command);

    //forward to interpolator
    bool success = true;
    std::string interpolator_management = "/" + matec_utils::stripLeadingSlash(m_interpolator) + "/manage";
    if(ros::service::exists(interpolator_management, false))
    {
      matec_msgs::ManageJointInterpolator manage_joint_interpolator;
      manage_joint_interpolator.request.new_setpoint = req.last_command;
      success = ros::service::call(interpolator_management, manage_joint_interpolator.request, manage_joint_interpolator.response);
    }
    else
    {
      ROS_WARN_THROTTLE(1.0, "%s: Interpolator (%s) management service not found at %s!", m_nh.getNamespace().c_str(), m_interpolator.c_str(), interpolator_management.c_str());
      success = false;
    }

    if(req.owned_joints.size() == 0)
    {
      m_sleeping = true;
    }
    else
    {
      m_sleeping = false;
    }

    res.release_remaining_joints = false;
    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return success;
  }

  bool BehaviorTemplate::analyzeCarrot(matec_msgs::FullJointStates& carrot)
  {
    if(carrot.joint_indices.size() == 0)
    {
      m_carrot_map = m_joint_map;
    }
    else
    {
      for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
      {
        unsigned int carrot_idx = std::find(carrot.joint_indices.begin(), carrot.joint_indices.end(), m_joint_map[i]) - carrot.joint_indices.begin();
        if(carrot_idx == carrot.joint_indices.size())
        {
          ROS_ERROR("%s: Carrot did not provide a setpoint for joint %s (%d)!", m_nh.getNamespace().c_str(), m_joint_names[m_joint_map[i]].c_str(), m_joint_map[i]);
          m_carrot_map.clear();
          return false;
        }
        m_carrot_map.push_back(carrot_idx);
      }
    }
    return true;
  }

  void BehaviorTemplate::sensorCallback(matec_msgs::FullJointStates& current)
  {
    if(m_sleeping)
    {
      return; //TODO: fully unsubscribe so we don't waste processing
    }

    //get the carrot
    boost::mutex::scoped_lock lock(m_mutex);
    behavior_template::ExampleCarrot carrot; //Note: carrot does not have to be a matec_msgs::FullJointStates, but does have to match its interpolator / carrot provider
    if(!m_carrot_sub.getCurrentMessage(carrot))
    {
      ROS_WARN_THROTTLE(1.0, "%s: No carrot received (%s)!", m_nh.getNamespace().c_str(), m_nh.resolveName("/" + m_interpolator + "/carrot").c_str());
      return;
    }

    //first time we get a carrot, configure the joint index map
    if((m_carrot_map.size() == 0) && !analyzeCarrot(carrot.joint_carrot)) //Note: specific to matec_msgs::FullJointStates
    {
      return;
    }

    //fill in m_command based on msg (current state) and the carrot. Ex:
    // for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    // {
    //   unsigned char current_idx = m_joint_map[i];
    //   unsigned char carrot_idx = m_carrot_map[i];
    //
    //   double pos_error = carrot.position[carrot_idx] - current.position[current_idx];
    //   double vel_error = carrot.velocity[carrot_idx] - current.velocity[current_idx];
    //
    //   double feedback = pos_error + vel_error;
    //   m_command.joint_commands.acceleration[i] = carrot.acceleration[carrot_idx] + feedback;
    //   m_command.joint_commands.velocity[i] = carrot.velocity[carrot_idx];
    //   m_command.joint_commands.position[i] = carrot.position[carrot_idx];
    // }

    //send off the command
    m_behavior_pub.publish(m_command); //m_command DOES have to be a matec_msgs::FullJointStates
  }

  void BehaviorTemplate::spin()
  {
    ROS_INFO("%s started.", m_nh.getNamespace().c_str());
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "behavior_template");
  ros::NodeHandle nh("~");

  behavior_template::BehaviorTemplate node(nh);
  node.spin();

  return 0;
}
