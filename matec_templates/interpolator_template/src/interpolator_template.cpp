#include "interpolator_template/interpolator_template.h"

namespace interpolator_template
{
  InterpolatorTemplate::InterpolatorTemplate(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "server", boost::bind(&InterpolatorTemplate::goalCallback, this, _1), boost::bind(&InterpolatorTemplate::cancelCallback, this, _1), false)
  {
    m_nh.param("publish_rate", m_publish_rate, 250.0);
    m_nh.param("behavior", m_behavior_name, std::string(m_nh.getNamespace()));

    double max_joint_velocity;
    m_nh.param("max_joint_velocity", max_joint_velocity, 1.0);

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints, m_joint_names)) //Note: not required if the goal is unrelated to joints
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map)) //Note: not required if the goal is unrelated to joints
    {
      ROS_ERROR("InterpolatorTemplate couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }
    initializeCommand();

    m_joint_states_sub.subscribe("/joint_states");
    m_carrot_pub.advertise("carrot");

    m_behavior_awake = false;
    m_awaiting_management = false;
    m_manage_service_server = m_nh.advertiseService("manage", &InterpolatorTemplate::managementCallback, this);

    m_server.start();
  }

  InterpolatorTemplate::~InterpolatorTemplate()
  {

  }

  void InterpolatorTemplate::initializeCommand()
  {
    //Note: this may be completely different if the carrot is not in joint space
    m_last_command.joint_indices = m_joint_map;
    m_last_command.position.resize(m_controlled_joints.size(), 0.0);
    m_last_command.velocity.resize(m_controlled_joints.size(), 0.0);
    m_last_command.acceleration.resize(m_controlled_joints.size(), 0.0);
    m_last_command.torque.resize(m_controlled_joints.size(), 0.0);
  }

  bool InterpolatorTemplate::managementCallback(matec_msgs::ManageJointInterpolator::Request& req, matec_msgs::ManageJointInterpolator::Response& res)
  {
    for(unsigned int i = 0; i < req.new_setpoint.joint_indices.size(); i++)
    {
      unsigned int control_idx = std::find(m_joint_map.begin(), m_joint_map.end(), req.new_setpoint.joint_indices[i]) - m_joint_map.begin();
      if(control_idx < m_controlled_joints.size())
      {
        m_last_command.position[control_idx] = req.new_setpoint.position[i];
        m_last_command.velocity[control_idx] = 0.0;
        m_last_command.acceleration[control_idx] = 0.0;
      }
      else
      {
        ROS_WARN("%s: Tare requested for uncontrolled joint %s", m_nh.getNamespace().c_str(), m_joint_names[req.new_setpoint.joint_indices[i]].c_str());
      }
    }

    //publish tared version of the command here
    behavior_template::ExampleCarrot carrot;
    carrot.joint_carrot = m_last_command;
    m_carrot_pub.publish(carrot);

    m_awaiting_management = false;

    ROS_INFO("%s: Managed.", m_nh.getNamespace().c_str());
    return true;
  }

  void InterpolatorTemplate::goalCallback(Server::GoalHandle goal_handle)
  {
//    if(/*goal is bad*/)
//    {
//      ROS_ERROR(/*Reason why the goal was bad*/);
//      goal_handle.setRejected();
//      return;
//    }

    cancelCurrentGoal();
    m_goal_handle = goal_handle;
    m_goal_handle.setAccepted();

    //set m_goal_joint_subset to be only the joints needed for the goal if it doesn't need them all
    matec_utils::awakenBehavior(m_behavior_name, m_behavior_awake, m_goal_joint_subset);

    if(m_behavior_awake)
    {
      setupGoal();
      ROS_INFO("Starting Trajectory!");
    }
    else
    {
      m_awaiting_management = true;
    }
  }

  void InterpolatorTemplate::cancelCallback(Server::GoalHandle goal_handle)
  {
    cancelCurrentGoal();
  }

  void InterpolatorTemplate::cancelCurrentGoal()
  {
    if(m_goal_handle.getGoal().get()) //goal doesn't actually exist, so no need to cancel
    {
      actionlib_msgs::GoalStatus status = m_goal_handle.getGoalStatus(); //cancel if current goal is pending, recalling, active, or preempting
      if(status.status == actionlib_msgs::GoalStatus::PENDING || status.status == actionlib_msgs::GoalStatus::RECALLING || status.status == actionlib_msgs::GoalStatus::ACTIVE || status.status == actionlib_msgs::GoalStatus::PREEMPTING)
      {
        m_goal_handle.setCanceled();
        m_goal_handle = Server::GoalHandle();
      }
    }

    // handle anything else that needs to be reset to prepare for the arrival of a new goal
  }

  bool InterpolatorTemplate::goalActive()
  {
    return (bool) m_goal_handle.getGoal();
  }

  void InterpolatorTemplate::finishGoal(bool success)
  {
    if(success)
    {
      ROS_INFO("%s: Finished interpolation!", m_nh.getNamespace().c_str());
      interpolator_template::ExampleActionFeedback feedback;
      feedback.progress = 1.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setSucceeded();
      m_goal_handle = Server::GoalHandle();
    }
    else
    {
      ROS_ERROR("%s: Interpolation failed!", m_nh.getNamespace().c_str());
      interpolator_template::ExampleActionFeedback feedback;
      feedback.progress = 0.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setAborted();
      m_goal_handle = Server::GoalHandle();
    }
  }

  void InterpolatorTemplate::setupGoal()
  {
    boost::mutex::scoped_lock lock(m_mutex);
    //set up the goal for execution, assuming that the last command sent by any controller on the controlled joints is in m_last_command

    m_goal_start_time = ros::Time::now();
  }

  void InterpolatorTemplate::calculateNextCommand()
  {
    boost::mutex::scoped_lock lock(m_mutex);
//    double dt = ros::Duration(ros::Time::now() - m_goal_start_time).toSec();

    //figure out what we should send next based on the goal,
    // the last command sent to the joints (m_last_command)
    // the interpolation state,
    // and the time since the start of the goal (dt)
    behavior_template::ExampleCarrot carrot;

    //send it
    m_carrot_pub.publish(carrot);

    //optionally update the last command with the thing we just sent
    // note, however, that it might not be compatible
    m_last_command = carrot.joint_carrot;
  }

  void InterpolatorTemplate::spin()
  {
    ROS_INFO("InterpolatorTemplate started.");
    ros::Rate loop_rate(m_publish_rate);
    while(ros::ok())
    {
      if(goalActive())
      {
        if(m_behavior_awake) //ready to go!
        {
          calculateNextCommand();
        }
        else if(!m_awaiting_management) //not ready to go, but either got the management signal back or haven't sent it yet
        {
          if(matec_utils::awakenBehavior(m_behavior_name, m_behavior_awake, m_goal_joint_subset)) //make sure the behavior is ready
          {
            if(m_behavior_awake) //behavior is now ready to go, configure the trajectories
            {
              setupGoal();
              ROS_INFO("Starting Trajectory!");
            }
            else
            {
              m_awaiting_management = true;
            }
          }
        }
      }

      loop_rate.sleep();
      ros::spinOnce();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "interpolator_template");
  ros::NodeHandle nh("~");

  interpolator_template::InterpolatorTemplate node(nh);
  node.spin();

  return 0;
}
