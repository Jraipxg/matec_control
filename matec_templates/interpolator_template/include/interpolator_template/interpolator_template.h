#ifndef INTERPOLATOR_TEMPLATE_H
#define INTERPOLATOR_TEMPLATE_H

#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <interpolator_template/ExampleActionAction.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread/mutex.hpp>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <behavior_template/ExampleCarrot.h>
#include <matec_msgs/StringArray.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/ManageJointInterpolator.h"
#include "matec_msgs/AwakenBehavior.h"

namespace interpolator_template
{
  typedef actionlib::ActionServer<interpolator_template::ExampleActionAction> Server;

  class InterpolatorTemplate
  {
  public:
    InterpolatorTemplate(const ros::NodeHandle& nh);
    ~InterpolatorTemplate();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_publish_rate;
    std::string m_behavior_name;

    bool m_awaiting_management;
    bool m_behavior_awake;

    Server m_server;
    Server::GoalHandle m_goal_handle;
    std::vector<unsigned char> m_goal_joint_subset;

    ros::ServiceServer m_manage_service_server;

    ros::Time m_goal_start_time;

    shared_memory_interface::Publisher<behavior_template::ExampleCarrot> m_carrot_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    matec_msgs::FullJointStates m_last_command;

    boost::mutex m_mutex;

    void goalCallback(Server::GoalHandle goal_handle);
    void cancelCallback(Server::GoalHandle goal_handle);
    void cancelCurrentGoal();
    void setupGoal();
    bool goalActive();
    void finishGoal(bool success);

    void initializeCommand();
    void calculateNextCommand();

    bool managementCallback(matec_msgs::ManageJointInterpolator::Request& req, matec_msgs::ManageJointInterpolator::Response& res);
  };
}
#endif //INTERPOLATOR_TEMPLATE_H
