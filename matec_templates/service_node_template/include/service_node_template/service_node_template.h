#ifndef SERVICE_NODE_TEMPLATE_H
#define SERVICE_NODE_TEMPLATE_H
#include <ros/ros.h>
#include <ros/service_server.h>
#include <service_node_template/ExampleService.h>
 
namespace service_node_template
{
  class ServiceNodeTemplate
  {
  public:
    ServiceNodeTemplate(const ros::NodeHandle& nh);
    ~ServiceNodeTemplate();
    void spin();
 
  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    ros::ServiceServer m_example_service_server;

    bool exampleServiceCallback(ExampleService::Request& req, ExampleService::Response& res);
  };
}
#endif //SERVICE_NODE_TEMPLATE_H
