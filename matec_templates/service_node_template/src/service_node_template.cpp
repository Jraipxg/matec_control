#include "service_node_template/service_node_template.h"
 
namespace service_node_template
{
  ServiceNodeTemplate::ServiceNodeTemplate(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
      m_nh.param("loop_rate", m_loop_rate, 10.0);

      m_example_service_server = m_nh.advertiseService("/example_service", &ServiceNodeTemplate::exampleServiceCallback, this);
  }
 
  ServiceNodeTemplate::~ServiceNodeTemplate()
  {
  }

  bool ServiceNodeTemplate::exampleServiceCallback(ExampleService::Request& req, ExampleService::Response& res)
  {
    res.example_reponse = req.example_request;
    return true;
  }

 
  void ServiceNodeTemplate::spin()
  {
    ROS_INFO("ServiceNodeTemplate started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}
 
int main(int argc, char **argv)
{
  ros::init(argc, argv, "service_node_template");
  ros::NodeHandle nh("~");
 
  service_node_template::ServiceNodeTemplate node(nh);
  node.spin();
 
  return 0;
}
